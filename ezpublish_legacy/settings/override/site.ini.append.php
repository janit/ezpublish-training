<?php /* #?ini charset="utf-8"?

[ExtensionSettings]
ActiveExtensions[]
ActiveExtensions[]=ezjscore
ActiveExtensions[]=ezoe
ActiveExtensions[]=ezformtoken
ActiveExtensions[]=ezstarrating
ActiveExtensions[]=ezgmaplocation
ActiveExtensions[]=ezdemo
ActiveExtensions[]=ezwt
ActiveExtensions[]=ezflow
ActiveExtensions[]=ezie
ActiveExtensions[]=ezodf
ActiveExtensions[]=ezprestapiprovider
ActiveExtensions[]=ezmultiupload
ActiveExtensions[]=ezautosave

[Session]
SessionNameHandler=custom

[SiteSettings]
DefaultAccess=eng
SiteList[]
SiteList[]=ezdemo_site_clean
SiteList[]=eng
SiteList[]=ezdemo_site_clean_admin
RootNodeDepth=1

[UserSettings]
LogoutRedirect=/

[SiteAccessSettings]
CheckValidity=false
AvailableSiteAccessList[]
AvailableSiteAccessList[]=ezdemo_site_clean
AvailableSiteAccessList[]=eng
AvailableSiteAccessList[]=ezdemo_site_clean_admin
MatchOrder=uri
HostMatchMapItems[]

[DesignSettings]
DesignLocationCache=enabled

[RegionalSettings]
TranslationSA[]
TranslationSA[eng]=Eng

[FileSettings]
VarDir=var/ezdemo_site

[MailSettings]
Transport=sendmail
AdminEmail=foo@example.com
EmailSender=

[EmbedViewModeSettings]
AvailableViewModes[]
AvailableViewModes[]=embed
AvailableViewModes[]=embed-inline
InlineViewModes[]
InlineViewModes[]=embed-inline
*/ ?>