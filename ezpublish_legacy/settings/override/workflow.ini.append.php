[OperationSettings]

# Other extra available operations
# Note that the code which is used to 
# implement them may be changed if needed
# we will not apply BC rules for these ones
AvailableOperationList[]=content_sort
AvailableOperationList[]=content_move
AvailableOperationList[]=content_swap
AvailableOperationList[]=content_updatemainassignment
AvailableOperationList[]=content_addlocation
AvailableOperationList[]=content_removelocation
AvailableOperationList[]=content_updatepriority
AvailableOperationList[]=content_hide
AvailableOperationList[]=content_delete
AvailableOperationList[]=content_updatesection
AvailableOperationList[]=content_read
AvailableOperationList[]=content_updateinitiallanguage
AvailableOperationList[]=content_updatealwaysavailable
AvailableOperationList[]=content_removetranslation
AvailableOperationList[]=content_updateobjectstate
AvailableOperationList[]=content_createnodefeed
AvailableOperationList[]=content_removenodefeed
AvailableOperationList[]=user_activation
AvailableOperationList[]=user_password
AvailableOperationList[]=user_forgotpassword
AvailableOperationList[]=user_preferences
AvailableOperationList[]=user_setsettings
