<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=root
Password=
Database=yamaha
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=eZ Publish Demo Site (without demo content)
SiteURL=yamaha.local/eng
LoginPage=embedded
AdditionalLoginFormActionURL=http://yamaha.local/ezdemo_site_clean_admin/user/login

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ezdemo_site_clean
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=ezdemo_site_clean_admin
ShowHiddenNodes=false

[DesignSettings]
SiteDesign=ezdemo
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=base
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=eng-US
TextTranslation=enabled

[FileSettings]
VarDir=var/ezdemo_site_clean

[ContentSettings]
TranslationList=

[MailSettings]
AdminEmail=foo@example.com
EmailSender=
*/ ?>