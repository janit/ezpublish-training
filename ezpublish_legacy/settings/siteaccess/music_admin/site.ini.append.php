<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezmysqli
Server=localhost
Port=
User=root
Password=
Database=yamaha
Charset=
Socket=disabled

[InformationCollectionSettings]
EmailReceiver=

[SiteSettings]
SiteName=eZ Publish Demo Site (without demo content)
SiteURL=yamaha.local
DefaultPage=content/dashboard
LoginPage=custom

[UserSettings]
RegistrationEmail=

[SiteAccessSettings]
RequireUserLogin=true
RelatedSiteAccessList[]=yamaha_admin
RelatedSiteAccessList[]=eng
RelatedSiteAccessList[]=jap
ShowHiddenNodes=true

[DesignSettings]
SiteDesign=ezdemo_site_clean_admin
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin
AdditionalSiteDesignList[]=ezdemo

[RegionalSettings]
Locale=eng-US
ContentObjectLocale=eng-US
ShowUntranslatedObjects=enabled
SiteLanguageList[]=eng-US
TextTranslation=enabled

[FileSettings]
VarDir=var/ezdemo_site_clean

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=foo@example.com
EmailSender=
*/ ?>
