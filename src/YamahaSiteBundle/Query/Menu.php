<?php

namespace YamahaSiteBundle\Query;

use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;


class Menu {

    private $searchService;
    private $locationService;
    private $queryHelper;

    public function __construct(SearchService $searchService, LocationService $locationService, Helper $queryHelper)
    {

        $this->searchService = $searchService;
        $this->locationService = $locationService;
        $this->queryHelper = $queryHelper;

    }

    public function getMainMenuItems($siteRootLocationId, $currentLocationId)
    {

        $location = $this->locationService->loadLocation($siteRootLocationId);

        // Filter criteria reference: https://doc.ez.no/display/EZP/Criteria+reference

        $filter = new Criterion\LogicalAnd([
            new Criterion\ContentTypeIdentifier(['product_category','content_page']),
            new Criterion\Subtree($location->pathString),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\Location\Depth(Criterion\Operator::EQ, 3)
        ]);

        // Sort criteria reference: https://doc.ez.no/display/EZP/Sort+Clauses+reference

        $sortClauses = [
            new SortClause\ContentName( Query::SORT_ASC )
        ];

        $query = new LocationQuery([
            'filter' => $filter,
            'sortClauses' => $sortClauses
        ]);

        $searchResults = $this->searchService->findLocations($query);

        return $this->queryHelper->getContentAndLocations($searchResults);

    }

    public function getParentLocations($pathString){

        $locations = [];

        // explode and do some cleaning up of the path string (/1/2/37373/../37/)
        $locationIds = array_filter( explode('/',$pathString) ,'strlen');
        $parentsIds = array_slice($locationIds,3);

        foreach($parentsIds as $locationId){
            $locations[] = $this->locationService->loadLocation($locationId);
        }

        return $locations;

    }

}