<?php

namespace YamahaSiteBundle\Query;

use eZ\Publish\API\Repository\ContentService;

class Helper {

    public $contentService;

    public function __construct(ContentService $contentService)
    {

        $this->contentService = $contentService;

    }

    public function getContentAndLocations($searchResults){

        $mergedResults = [];

        foreach($searchResults->searchHits as $searchHit){

            $result = [];
            $result['location'] = $searchHit->valueObject;
            $result['content'] = $this->contentService->loadContent($searchHit->valueObject->contentInfo->id);
            $mergedResults[] = $result;
        }

        return $mergedResults;

    }

}