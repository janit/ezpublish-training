<?php

namespace YamahaSiteBundle\Query;

use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use Symfony\Component\Config\Definition\Exception\Exception;


class Product {

    private $searchService;
    private $locationService;
    private $contentService;
    private $queryHelper;

    public function __construct(SearchService $searchService, LocationService $locationService, ContentService $contentService, Helper $queryHelper)
    {

        $this->searchService = $searchService;
        $this->locationService = $locationService;
        $this->contentService = $contentService;
        $this->queryHelper = $queryHelper;

    }

    public function getProductCategories($locationId)
    {

        $location = $this->locationService->loadLocation($locationId);

        // Filter criteria reference: https://doc.ez.no/display/EZP/Criteria+reference

        $filter = new Criterion\LogicalAnd([
            new Criterion\ContentTypeIdentifier(['product_category','product']),
            new Criterion\ParentLocationId($locationId),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\Location\Depth(Criterion\Operator::GTE, $location->depth)
        ]);

        // Sort criteria reference: https://doc.ez.no/display/EZP/Sort+Clauses+reference

        $sortClauses = [
            new SortClause\ContentName( Query::SORT_ASC )
        ];

        $query = new LocationQuery([
            'filter' => $filter,
            'sortClauses' => $sortClauses
        ]);

        $query->limit = 10;
        $query->offset = 0;

        $searchResults = $this->searchService->findLocations($query);

        return $this->queryHelper->getContentAndLocations($searchResults);

    }



    public function getProducts($locationId)
    {

        $location = $this->locationService->loadLocation($locationId);

        // Filter criteria reference: https://doc.ez.no/display/EZP/Criteria+reference

        $filter = new Criterion\LogicalAnd([
            new Criterion\ContentTypeIdentifier(['product']),
            new Criterion\Subtree($location->pathString),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\Location\IsMainLocation(Criterion\Location\IsMainLocation::MAIN)
        ]);

        // Sort criteria reference: https://doc.ez.no/display/EZP/Sort+Clauses+reference

        $sortClauses = [
            new SortClause\ContentName( Query::SORT_ASC )
        ];

        $query = new LocationQuery([
            'filter' => $filter,
            'sortClauses' => $sortClauses
        ]);

        $query->limit = 10;
        $query->offset = 0;

        $searchResults = $this->searchService->findLocations($query);

        return $this->queryHelper->getContentAndLocations($searchResults);

    }

    public function getProductComparisonData($productIds){


        $comparisonData = [];

        foreach ($productIds as $productId){

            $numericProductId = (int) $productId;

            if($numericProductId){

                $product = $this->contentService->loadContent($numericProductId);

                try {

                    $productData = $product->getFieldValue('product_data_xml');
                    $xml = file_get_contents(__DIR__ . '/../../../web' . $productData->uri);
                    $productData = simplexml_load_string($xml);
                    $comparisonData[] = (array) $productData;

                } catch (Exception $e){

                    echo $e->getMessage();

                }

            }

        }

        return $comparisonData;

    }

}