<?php

namespace YamahaSiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContentController extends Controller
{
    public function frontpageAction( $locationId, $viewType, $layout = false, array $params = array() )
    {

        // All our extra parameters will be placed here
        $params = [];

        $productService = $this->get('yamaha_site.query.product');

        $productCategories = $productService->getProductCategories($locationId);

        $params['product_categories'] = $productCategories;

        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );

        return $response;

    }

    public function topLevelProductCategoryAction( $locationId, $viewType, $layout = false, array $params = array() )
    {

        // All our extra parameters will be placed here
        $params = [];

        $productService = $this->get('yamaha_site.query.product');

        $productCategories = $productService->getProductCategories($locationId);

        $allProducts = $productService->getProducts($locationId);

        $params['product_categories'] = $productCategories;
        $params['products'] = $allProducts;

        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );

        return $response;

    }

    public function productCategoryAction( $locationId, $viewType, $layout = false, array $params = array() )
    {

        // All our extra parameters will be placed here
        $params = [];

        $productService = $this->get('yamaha_site.query.product');

        $products = $productService->getProductCategories($locationId);

        $params['products'] = $products;

        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );

        return $response;

    }

    public function productAction( $locationId, $viewType, $layout = false, array $params = array() )
    {

        // All our extra parameters will be placed here
        $params = [];

        $imageObjects = [];

        $location = $this->get('ezpublish.api.service.location')->loadLocation($locationId);

        $content = $this->get('ezpublish.api.service.content')->loadContentByContentInfo($location->contentInfo);

        foreach($content->getFieldValue('related_images')->destinationContentIds as $imageObjectId){

            $imageObjects[] = $this->get('ezpublish.api.service.content')->loadContent($imageObjectId);

        }

        $params['related_images'] = $imageObjects;

        $response = $this->get( 'ez_content' )->viewLocation( $locationId, $viewType, $layout, $params );

        return $response;

    }

}
