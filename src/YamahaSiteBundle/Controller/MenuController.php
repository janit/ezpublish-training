<?php

namespace YamahaSiteBundle\Controller;

use eZ\Publish\API\Repository\Values\Content\Location;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuController extends Controller
{
    public function mainAction($currentLocationId)
    {

        $configResolver = $this->get('ezpublish.config.resolver');

        $siteRootLocationId = $configResolver->getParameter('content.tree_root.location_id');

        $menuService = $this->get('yamaha_site.query.menu');

        $menuItems = $menuService->getMainMenuItems($siteRootLocationId, $currentLocationId);

        return $this->render('menu/main.html.twig', ['menuItems' => $menuItems]);
    }

    public function breadcrumbsAction(Location $currentLocation){

        $menuService = $this->get('yamaha_site.query.menu');

        $locations = $menuService->getParentLocations($currentLocation->pathString);

        return $this->render('menu/breadcrumbs.html.twig', ['locations' => $locations]);


    }
}
