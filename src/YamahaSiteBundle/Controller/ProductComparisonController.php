<?php

namespace YamahaSiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductComparisonController extends Controller
{
    public function indexAction($productIds)
    {

        $productIds = explode(',',$productIds);

        $productComparisonData = $this->get('yamaha_site.query.product')->getProductComparisonData($productIds);

        return new JsonResponse($productComparisonData);

    }
}
