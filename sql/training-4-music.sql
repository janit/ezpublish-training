-- MySQL dump 10.13  Distrib 5.7.17, for osx10.11 (x86_64)
--
-- Host: localhost    Database: yamaha_music
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ezapprove_items`
--

DROP TABLE IF EXISTS `ezapprove_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezapprove_items` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_process_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezapprove_items`
--

LOCK TABLES `ezapprove_items` WRITE;
/*!40000 ALTER TABLE `ezapprove_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezapprove_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezbasket`
--

DROP TABLE IF EXISTS `ezbasket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezbasket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `session_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezbasket_session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezbasket`
--

LOCK TABLES `ezbasket` WRITE;
/*!40000 ALTER TABLE `ezbasket` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezbasket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezbinaryfile`
--

DROP TABLE IF EXISTS `ezbinaryfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezbinaryfile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `download_count` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `mime_type` varchar(255) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezbinaryfile`
--

LOCK TABLES `ezbinaryfile` WRITE;
/*!40000 ALTER TABLE `ezbinaryfile` DISABLE KEYS */;
INSERT INTO `ezbinaryfile` VALUES (412,0,'83a566036c2253de17e15a0bec96b2f2.xml','text/xml','product_specs_2.xml',8),(422,0,'290f3332c3e15ae74abe364e32bf7590.xml','text/xml','product_specs.xml',5);
/*!40000 ALTER TABLE `ezbinaryfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state`
--

DROP TABLE IF EXISTS `ezcobj_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state` (
  `default_language_id` bigint(20) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(45) NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_identifier` (`group_id`,`identifier`),
  KEY `ezcobj_state_lmask` (`language_mask`),
  KEY `ezcobj_state_priority` (`priority`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state`
--

LOCK TABLES `ezcobj_state` WRITE;
/*!40000 ALTER TABLE `ezcobj_state` DISABLE KEYS */;
INSERT INTO `ezcobj_state` VALUES (2,2,1,'not_locked',3,0),(2,2,2,'locked',3,1),(2,3,3,'offline',3,0),(2,3,4,'online',3,1);
/*!40000 ALTER TABLE `ezcobj_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group`
--

DROP TABLE IF EXISTS `ezcobj_state_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group` (
  `default_language_id` bigint(20) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(45) NOT NULL DEFAULT '',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcobj_state_group_identifier` (`identifier`),
  KEY `ezcobj_state_group_lmask` (`language_mask`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group`
--

LOCK TABLES `ezcobj_state_group` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group` VALUES (2,2,'ez_lock',3),(2,3,'publishing_workflow',3);
/*!40000 ALTER TABLE `ezcobj_state_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_group_language`
--

DROP TABLE IF EXISTS `ezcobj_state_group_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_group_language` (
  `contentobject_state_group_id` int(11) NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  `real_language_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_state_group_id`,`real_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_group_language`
--

LOCK TABLES `ezcobj_state_group_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_group_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_group_language` VALUES (2,'',3,'Lock',2),(3,'',3,'Publishing Workflow',2);
/*!40000 ALTER TABLE `ezcobj_state_group_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_language`
--

DROP TABLE IF EXISTS `ezcobj_state_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_language` (
  `contentobject_state_id` int(11) NOT NULL DEFAULT '0',
  `description` longtext NOT NULL,
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_state_id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_language`
--

LOCK TABLES `ezcobj_state_language` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_language` DISABLE KEYS */;
INSERT INTO `ezcobj_state_language` VALUES (1,'',3,'Not locked'),(2,'',3,'Locked'),(3,'',3,'Offline'),(4,'',3,'Online');
/*!40000 ALTER TABLE `ezcobj_state_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcobj_state_link`
--

DROP TABLE IF EXISTS `ezcobj_state_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcobj_state_link` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_state_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contentobject_id`,`contentobject_state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcobj_state_link`
--

LOCK TABLES `ezcobj_state_link` WRITE;
/*!40000 ALTER TABLE `ezcobj_state_link` DISABLE KEYS */;
INSERT INTO `ezcobj_state_link` VALUES (4,1),(4,3),(10,1),(10,3),(12,1),(12,3),(14,1),(14,3),(41,1),(41,3),(42,1),(42,3),(45,1),(45,3),(49,1),(49,3),(50,1),(50,3),(51,1),(51,3),(52,1),(52,3),(54,1),(54,3),(56,1),(56,3),(57,1),(57,4),(62,1),(62,4),(63,1),(63,3),(64,1),(64,3),(65,1),(65,4),(66,1),(66,4),(67,1),(67,4),(68,1),(68,4),(70,1),(70,3),(71,1),(71,4),(72,1),(72,3),(73,1),(73,3),(74,1),(74,3),(75,1),(75,3),(76,1),(76,4),(77,1),(77,4),(78,1),(78,4),(79,1),(79,4),(81,1),(81,3),(82,1),(82,3),(83,1),(83,3),(84,1),(84,3),(85,1),(85,3),(86,1),(86,3),(87,1),(87,3),(88,1),(88,3),(89,1),(89,3),(90,1),(90,4),(121,1),(121,3),(122,1),(122,3),(123,1),(123,3),(124,1),(124,3),(127,1),(127,3),(128,1),(128,3),(129,1),(129,3),(130,1),(130,3),(131,1),(131,3),(132,1),(132,3),(133,1),(133,3),(135,1),(135,3);
/*!40000 ALTER TABLE `ezcobj_state_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_group`
--

DROP TABLE IF EXISTS `ezcollab_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_group` (
  `created` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_open` int(11) NOT NULL DEFAULT '1',
  `modified` int(11) NOT NULL DEFAULT '0',
  `parent_group_id` int(11) NOT NULL DEFAULT '0',
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcollab_group_depth` (`depth`),
  KEY `ezcollab_group_path` (`path_string`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_group`
--

LOCK TABLES `ezcollab_group` WRITE;
/*!40000 ALTER TABLE `ezcollab_group` DISABLE KEYS */;
INSERT INTO `ezcollab_group` VALUES (1486997385,0,1,1,1486997385,0,'1',0,'Inbox',135),(1486997385,0,2,1,1486997385,0,'2',0,'Inbox',14);
/*!40000 ALTER TABLE `ezcollab_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item`
--

DROP TABLE IF EXISTS `ezcollab_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` float NOT NULL DEFAULT '0',
  `data_float2` float NOT NULL DEFAULT '0',
  `data_float3` float NOT NULL DEFAULT '0',
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `type_identifier` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item`
--

LOCK TABLES `ezcollab_item` WRITE;
/*!40000 ALTER TABLE `ezcollab_item` DISABLE KEYS */;
INSERT INTO `ezcollab_item` VALUES (1486997385,135,0,0,0,136,1,2,'','','',1,1486998241,2,'ezapprove');
/*!40000 ALTER TABLE `ezcollab_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_group_link`
--

DROP TABLE IF EXISTS `ezcollab_item_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_group_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`collaboration_id`,`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_group_link`
--

LOCK TABLES `ezcollab_item_group_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_group_link` DISABLE KEYS */;
INSERT INTO `ezcollab_item_group_link` VALUES (1,1486997385,1,1,0,0,1486997385,135),(1,1486997385,2,1,0,0,1486997385,14);
/*!40000 ALTER TABLE `ezcollab_item_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_message_link`
--

DROP TABLE IF EXISTS `ezcollab_item_message_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_message_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `message_type` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `participant_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_message_link`
--

LOCK TABLES `ezcollab_item_message_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_message_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_item_message_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_participant_link`
--

DROP TABLE IF EXISTS `ezcollab_item_participant_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_participant_link` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `participant_id` int(11) NOT NULL DEFAULT '0',
  `participant_role` int(11) NOT NULL DEFAULT '1',
  `participant_type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`collaboration_id`,`participant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_participant_link`
--

LOCK TABLES `ezcollab_item_participant_link` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_participant_link` DISABLE KEYS */;
INSERT INTO `ezcollab_item_participant_link` VALUES (1,1486997385,1,0,1486998238,1486997385,14,4,1),(1,1486997385,1,0,1486998244,1486997385,135,5,1);
/*!40000 ALTER TABLE `ezcollab_item_participant_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_item_status`
--

DROP TABLE IF EXISTS `ezcollab_item_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_item_status` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_read` int(11) NOT NULL DEFAULT '0',
  `last_read` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`collaboration_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_item_status`
--

LOCK TABLES `ezcollab_item_status` WRITE;
/*!40000 ALTER TABLE `ezcollab_item_status` DISABLE KEYS */;
INSERT INTO `ezcollab_item_status` VALUES (1,0,1,1486998238,14),(1,1,1,1486998244,135);
/*!40000 ALTER TABLE `ezcollab_item_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_notification_rule`
--

DROP TABLE IF EXISTS `ezcollab_notification_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_notification_rule` (
  `collab_identifier` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_notification_rule`
--

LOCK TABLES `ezcollab_notification_rule` WRITE;
/*!40000 ALTER TABLE `ezcollab_notification_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_notification_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_profile`
--

DROP TABLE IF EXISTS `ezcollab_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_profile` (
  `created` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_group` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_profile`
--

LOCK TABLES `ezcollab_profile` WRITE;
/*!40000 ALTER TABLE `ezcollab_profile` DISABLE KEYS */;
INSERT INTO `ezcollab_profile` VALUES (1486997385,'',1,1,1486997385,135),(1486997385,'',2,2,1486997385,14);
/*!40000 ALTER TABLE `ezcollab_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcollab_simple_message`
--

DROP TABLE IF EXISTS `ezcollab_simple_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcollab_simple_message` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` float NOT NULL DEFAULT '0',
  `data_float2` float NOT NULL DEFAULT '0',
  `data_float3` float NOT NULL DEFAULT '0',
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_type` varchar(40) NOT NULL DEFAULT '',
  `modified` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcollab_simple_message`
--

LOCK TABLES `ezcollab_simple_message` WRITE;
/*!40000 ALTER TABLE `ezcollab_simple_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcollab_simple_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontent_language`
--

DROP TABLE IF EXISTS `ezcontent_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontent_language` (
  `disabled` int(11) NOT NULL DEFAULT '0',
  `id` bigint(20) NOT NULL DEFAULT '0',
  `locale` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcontent_language_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontent_language`
--

LOCK TABLES `ezcontent_language` WRITE;
/*!40000 ALTER TABLE `ezcontent_language` DISABLE KEYS */;
INSERT INTO `ezcontent_language` VALUES (0,2,'eng-US','English (American)'),(0,4,'jpn-JP','Japanese'),(0,8,'eng-NZ','English (New Zealand)');
/*!40000 ALTER TABLE `ezcontent_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentbrowsebookmark`
--

DROP TABLE IF EXISTS `ezcontentbrowsebookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentbrowsebookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowsebookmark_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentbrowsebookmark`
--

LOCK TABLES `ezcontentbrowsebookmark` WRITE;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcontentbrowsebookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentbrowserecent`
--

DROP TABLE IF EXISTS `ezcontentbrowserecent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentbrowserecent` (
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcontentbrowserecent_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentbrowserecent`
--

LOCK TABLES `ezcontentbrowserecent` WRITE;
/*!40000 ALTER TABLE `ezcontentbrowserecent` DISABLE KEYS */;
INSERT INTO `ezcontentbrowserecent` VALUES (1486121151,1,'eZ Publish',2,14),(1485763836,3,'Product Category 1',63,14),(1486123295,5,'Images',51,14),(1485715363,6,'Products',71,14),(1486121234,7,'サイトのトップページ',80,14),(1486121295,8,'すべての製品',82,14),(1486121440,9,'製品カテゴリ2',84,14),(1486123377,10,'Product photos',91,14),(1486366668,11,'All products',76,14),(1486996262,12,'Country editors',133,14),(1486997214,13,'Administrator users',13,14);
/*!40000 ALTER TABLE `ezcontentbrowserecent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass`
--

DROP TABLE IF EXISTS `ezcontentclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass` (
  `always_available` int(11) NOT NULL DEFAULT '0',
  `contentobject_name` varchar(255) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL DEFAULT '',
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `is_container` int(11) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `serialized_description_list` longtext,
  `serialized_name_list` longtext,
  `sort_field` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `url_alias_name` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_version` (`version`),
  KEY `ezcontentclass_identifier` (`identifier`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass`
--

LOCK TABLES `ezcontentclass` WRITE;
/*!40000 ALTER TABLE `ezcontentclass` DISABLE KEYS */;
INSERT INTO `ezcontentclass` VALUES (1,'<short_name|name>',1024392098,14,1,'folder',2,1,3,1082454875,14,'a3d405b81be900468eb153d774f4f0d2','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:6:\"Folder\";}',1,1,'',0),(1,'<name>',1024392098,14,3,'user_group',2,1,3,1048494743,14,'25b4268cdcd01921b808a0d854b877ef','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:10:\"User group\";}',1,1,'',0),(1,'<first_name> <last_name>',1024392098,14,4,'user',2,0,3,1082018364,14,'40faa822edc579b02c25f6bb7beec3ad','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"User\";}',1,1,'',0),(1,'<name>',1081858024,14,14,'common_ini_settings',2,0,3,1081858024,14,'ffedf2e73b1ea0c3e630e42e2db9c900','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:19:\"Common ini settings\";}',1,1,'',0),(1,'<title>',1081858045,14,15,'template_look',2,0,3,1081858045,14,'59b43cd9feaaf0e45ac974fb4bbd3f92','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:13:\"Template look\";}',1,1,'',0),(0,'<name>',1484920000,14,23,'landing_page',2,1,3,1485106307,14,'e36c458e3e4a81298a0945f53a2c81f4','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:12:\"Landing Page\";}',1,1,'',0),(0,'<name>',1484920000,14,26,'file',2,0,3,1485106350,14,'637d58bfddf164627bdfd265733280a0','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"File\";}',1,1,'',0),(1,'<name>',1484920000,14,27,'image',2,0,3,1485521891,14,'f6df12aa74e36230eb675f364fccd25a','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Image\";}',1,1,'',0),(0,'<name>',1484920000,14,37,'video',2,0,3,1485106422,14,'b38417e8194fb8f893ca918d297b4fa8','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Video\";}',1,1,'',0),(0,'<title>',1485152256,14,40,'product_category',2,1,3,1485153360,14,'de1ab47a1db76c7ea38ce80239a2fd23','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:16:\"Product category\";s:16:\"always-available\";s:6:\"eng-US\";}',1,1,'',0),(0,'<product_number>: <title>',1485152645,14,41,'product',2,0,3,1486367274,14,'2fb23dad839affc336291345dd0c985c','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:7:\"Product\";s:16:\"always-available\";s:6:\"eng-US\";}',1,1,'',0),(0,'<title>',1485522020,14,43,'content_page',2,0,3,1485522048,14,'fb8b6c76211b002bf7b04062d339fa7c','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:12:\"Content Page\";s:16:\"always-available\";s:6:\"eng-US\";}',1,1,'',0);
/*!40000 ALTER TABLE `ezcontentclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_attribute`
--

DROP TABLE IF EXISTS `ezcontentclass_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_attribute` (
  `can_translate` int(11) DEFAULT '1',
  `category` varchar(25) NOT NULL DEFAULT '',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `data_float1` double DEFAULT NULL,
  `data_float2` double DEFAULT NULL,
  `data_float3` double DEFAULT NULL,
  `data_float4` double DEFAULT NULL,
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(50) DEFAULT NULL,
  `data_text2` varchar(50) DEFAULT NULL,
  `data_text3` varchar(50) DEFAULT NULL,
  `data_text4` varchar(255) DEFAULT NULL,
  `data_text5` longtext,
  `data_type_string` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL DEFAULT '',
  `is_information_collector` int(11) NOT NULL DEFAULT '0',
  `is_required` int(11) NOT NULL DEFAULT '0',
  `is_searchable` int(11) NOT NULL DEFAULT '0',
  `placement` int(11) NOT NULL DEFAULT '0',
  `serialized_data_text` longtext,
  `serialized_description_list` longtext,
  `serialized_name_list` longtext NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentclass_attr_ccid` (`contentclass_id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_attribute`
--

LOCK TABLES `ezcontentclass_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentclass_attribute` VALUES (1,'',1,0,0,0,0,255,0,0,0,'Folder','','','','','ezstring',4,'name',0,1,1,1,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',3,0,0,0,0,255,0,0,0,'','','','','','ezstring',6,'name',0,1,1,1,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',3,0,0,0,0,255,0,0,0,'','','','','','ezstring',7,'description',0,0,1,2,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Description\";}',0),(1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring',8,'first_name',0,1,1,1,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:10:\"First name\";}',0),(1,'',4,0,0,0,0,255,0,0,0,'','','','','','ezstring',9,'last_name',0,1,1,2,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:9:\"Last name\";}',0),(0,'',4,0,0,0,0,0,0,0,0,'','','','','','ezuser',12,'user_account',0,1,1,3,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:12:\"User account\";}',0),(1,'',1,0,0,0,0,5,0,0,0,'','','','','','ezxmltext',119,'short_description',0,0,1,3,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:7:\"Summary\";}',0),(1,'',1,0,0,0,0,100,0,0,0,'','','','','','ezstring',155,'short_name',0,0,1,2,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:10:\"Short name\";}',0),(1,'',1,0,0,0,0,20,0,0,0,'','','','','','ezxmltext',156,'description',0,0,1,4,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Description\";}',0),(0,'',1,0,0,0,0,0,0,1,0,'','','','','','ezboolean',158,'show_children',0,0,0,5,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:17:\"Display sub items\";}',0),(1,'',14,0,0,0,0,0,0,0,0,'','','','','','ezstring',159,'name',0,0,1,1,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(0,'',14,0,0,0,0,1,0,0,0,'site.ini','SiteSettings','IndexPage','','override;user;admin;demo','ezinisetting',160,'indexpage',0,0,0,2,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:10:\"Index Page\";}',0),(0,'',14,0,0,0,0,1,0,0,0,'site.ini','SiteSettings','DefaultPage','','override;user;admin;demo','ezinisetting',161,'defaultpage',0,0,0,3,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:12:\"Default Page\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','DebugSettings','DebugOutput','','override;user;admin;demo','ezinisetting',162,'debugoutput',0,0,0,4,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:12:\"Debug Output\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','DebugSettings','DebugByIP','','override;user;admin;demo','ezinisetting',163,'debugbyip',0,0,0,5,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Debug By IP\";}',0),(0,'',14,0,0,0,0,6,0,0,0,'site.ini','DebugSettings','DebugIPList','','override;user;admin;demo','ezinisetting',164,'debugiplist',0,0,0,6,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:13:\"Debug IP List\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','DebugSettings','DebugRedirection','','override;user;admin;demo','ezinisetting',165,'debugredirection',0,0,0,7,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:17:\"Debug Redirection\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','ContentSettings','ViewCaching','','override;user;admin;demo','ezinisetting',166,'viewcaching',0,0,0,8,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:12:\"View Caching\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','TemplateSettings','TemplateCache','','override;user;admin;demo','ezinisetting',167,'templatecache',0,0,0,9,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:14:\"Template Cache\";}',0),(0,'',14,0,0,0,0,2,0,0,0,'site.ini','TemplateSettings','TemplateCompile','','override;user;admin;demo','ezinisetting',168,'templatecompile',0,0,0,10,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:16:\"Template Compile\";}',0),(0,'',14,0,0,0,0,6,0,0,0,'image.ini','small','Filters','','override;user;admin;demo','ezinisetting',169,'imagesmall',0,0,0,11,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:16:\"Image Small Size\";}',0),(0,'',14,0,0,0,0,6,0,0,0,'image.ini','medium','Filters','','override;user;admin;demo','ezinisetting',170,'imagemedium',0,0,0,12,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:17:\"Image Medium Size\";}',0),(0,'',14,0,0,0,0,6,0,0,0,'image.ini','large','Filters','','override;user;admin;demo','ezinisetting',171,'imagelarge',0,0,0,13,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:16:\"Image Large Size\";}',0),(0,'',15,0,0,0,0,1,0,0,0,'site.ini','SiteSettings','SiteName','','override;user;admin;demo','ezinisetting',172,'title',0,0,0,1,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Title\";}',0),(0,'',15,0,0,0,0,6,0,0,0,'site.ini','SiteSettings','MetaDataArray','','override;user;admin;demo','ezinisetting',173,'meta_data',0,0,0,2,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:9:\"Meta data\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezimage',174,'image',0,0,0,3,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Image\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'sitestyle','','','','','ezpackage',175,'sitestyle',0,0,0,4,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:9:\"Sitestyle\";}',0),(0,'',15,0,0,0,0,1,0,0,0,'site.ini','MailSettings','AdminEmail','','override;user;admin;demo','ezinisetting',177,'email',0,0,0,6,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Email\";}',0),(0,'',15,0,0,0,0,1,0,0,0,'site.ini','SiteSettings','SiteURL','','override;user;admin;demo','ezinisetting',178,'siteurl',0,0,0,7,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:8:\"Site URL\";}',0),(1,'',4,0,0,0,0,10,0,0,0,'','','','','','eztext',179,'signature',0,0,1,4,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:9:\"Signature\";}',0),(1,'',4,0,0,0,0,1,0,0,0,'','','','','','ezimage',180,'image',0,0,0,5,'a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{i:0;s:0:\"\";s:16:\"always-available\";b:0;}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Image\";}',0),(1,'',23,0,0,0,0,0,0,0,0,'','','','','','ezstring',230,'name',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',23,0,0,0,0,0,0,0,0,'','','','','','ezpage',231,'page',0,0,0,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:6:\"Layout\";}',0),(1,'',26,0,0,0,0,0,0,0,0,'New file','','','','','ezstring',240,'name',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',26,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',241,'description',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Description\";}',0),(1,'',26,0,0,0,0,0,0,0,0,'','','','','','ezbinaryfile',242,'file',0,1,0,3,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"File\";}',0),(1,'',26,0,0,0,0,0,0,0,0,'','','','','','ezsrrating',243,'star_rating',0,0,1,4,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Star Rating\";}',0),(1,'',26,0,0,0,0,0,0,0,0,'','','','','','ezkeyword',244,'tags',0,0,1,5,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Tags\";}',0),(1,'',27,0,0,0,0,150,0,0,0,'','','','','','ezstring',245,'name',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',27,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',246,'caption',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:7:\"Caption\";}',0),(1,'',27,0,0,0,0,2,0,0,0,'','','','','','ezimage',247,'image',0,0,0,3,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:5:\"Image\";}',0),(1,'',27,0,0,0,0,0,0,0,0,'','','','','','ezsrrating',248,'star_rating',0,0,1,4,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Star Rating\";}',0),(1,'',27,0,0,0,0,0,0,0,0,'','','','','','ezkeyword',249,'tags',0,0,1,5,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Tags\";}',0),(1,'',37,0,0,0,0,0,0,0,0,'','','','','','ezstring',283,'name',0,0,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"Name\";}',0),(1,'',37,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',284,'caption',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:7:\"Caption\";}',0),(1,'',37,0,0,0,0,0,0,0,0,'','','','','','ezbinaryfile',285,'file',0,0,1,3,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:4:\"File\";}',0),(1,'',37,0,0,0,0,0,0,0,0,'','','','','','ezsrrating',286,'star_rating',0,0,1,4,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:0:\"\";}','a:2:{s:16:\"always-available\";s:6:\"eng-US\";s:6:\"eng-US\";s:11:\"Star Rating\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezurl',296,'site_map_url',0,0,0,8,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:12:\"Site map URL\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezurl',297,'tag_cloud_url',0,0,0,9,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:13:\"Tag Cloud URL\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',298,'login_label',0,0,0,10,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:13:\"Login (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',299,'logout_label',0,0,0,11,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:14:\"Logout (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',300,'my_profile_label',0,0,0,12,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:18:\"My profile (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',301,'register_user_label',0,0,0,13,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:25:\"Register new user (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',302,'rss_feed',0,0,0,14,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:8:\"RSS feed\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',303,'shopping_basket_label',0,0,0,15,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:23:\"Shopping basket (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezstring',304,'site_settings_label',0,0,0,16,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:21:\"Site settings (label)\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,10,0,0,0,'','','','','','eztext',305,'footer_text',0,0,0,17,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:11:\"Footer text\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,0,0,0,0,'','','','','','ezboolean',306,'hide_powered_by',0,0,0,18,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:17:\"Hide \"Powered by\"\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',15,0,0,0,0,10,0,0,0,'','','','','','eztext',307,'footer_script',0,0,0,19,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:17:\"Footer Javascript\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',1,0,0,0,0,0,0,0,0,'','','','','','ezpage',308,'call_for_action',0,0,0,6,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:15:\"Call For Action\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',1,0,0,0,0,0,0,0,0,'','','','','','ezkeyword',309,'tags',0,0,0,7,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:4:\"Tags\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',40,0,0,0,0,0,0,0,0,'','','','','','ezstring',310,'title',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',40,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',311,'description',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',41,0,0,0,0,0,0,0,0,'','','','','','ezstring',312,'title',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',41,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',313,'description',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',41,0,0,0,0,0,0,0,0,'','','','','','ezimage',314,'main_image',0,0,0,3,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:10:\"Main image\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(0,'',41,0,0,0,0,0,0,0,0,'','','','','','ezstring',315,'product_number',0,1,1,4,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:14:\"Product Number\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(0,'',41,0,0,0,0,0,0,0,0,'','','','','<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><constraints><allowed-class contentclass-identifier=\"image\"/></constraints><type value=\"2\"/><selection_type value=\"0\"/><object_class value=\"\"/><contentobject-placement node-id=\"51\"/></related-objects>\n','ezobjectrelationlist',316,'related_images',0,0,1,5,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:14:\"Related Images\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',43,0,0,0,0,0,0,0,0,'','','','','','ezstring',319,'title',0,1,1,1,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:5:\"Title\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',43,0,0,0,0,10,0,0,0,'','','','','','ezxmltext',320,'description',0,0,1,2,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:11:\"Description\";s:16:\"always-available\";s:6:\"eng-US\";}',0),(1,'',41,0,0,0,0,0,0,0,0,'','','','','','ezbinaryfile',324,'product_data_xml',0,0,1,6,'a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:0:\"\";s:16:\"always-available\";s:6:\"eng-US\";}','a:2:{s:6:\"eng-US\";s:16:\"Product data XML\";s:16:\"always-available\";s:6:\"eng-US\";}',0);
/*!40000 ALTER TABLE `ezcontentclass_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_classgroup`
--

DROP TABLE IF EXISTS `ezcontentclass_classgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_classgroup` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_version` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_classgroup`
--

LOCK TABLES `ezcontentclass_classgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_classgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclass_classgroup` VALUES (1,0,1,'Content'),(3,0,2,'Users'),(4,0,2,'Users'),(14,0,4,'Setup'),(15,0,4,'Setup'),(23,0,1,'Content'),(26,0,3,'Media'),(27,0,3,'Media'),(37,0,3,'Media'),(40,0,1,'Content'),(41,0,1,'Content'),(43,0,1,'Content');
/*!40000 ALTER TABLE `ezcontentclass_classgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclass_name`
--

DROP TABLE IF EXISTS `ezcontentclass_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclass_name` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_version` int(11) NOT NULL DEFAULT '0',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_locale` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentclass_id`,`contentclass_version`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclass_name`
--

LOCK TABLES `ezcontentclass_name` WRITE;
/*!40000 ALTER TABLE `ezcontentclass_name` DISABLE KEYS */;
INSERT INTO `ezcontentclass_name` VALUES (1,0,3,'eng-US','Folder'),(3,0,3,'eng-US','User group'),(4,0,3,'eng-US','User'),(14,0,3,'eng-US','Common ini settings'),(15,0,3,'eng-US','Template look'),(23,0,3,'eng-US','Landing Page'),(26,0,3,'eng-US','File'),(27,0,3,'eng-US','Image'),(37,0,3,'eng-US','Video'),(40,0,3,'eng-US','Product category'),(41,0,3,'eng-US','Product'),(43,0,3,'eng-US','Content Page');
/*!40000 ALTER TABLE `ezcontentclass_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentclassgroup`
--

DROP TABLE IF EXISTS `ezcontentclassgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentclassgroup` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentclassgroup`
--

LOCK TABLES `ezcontentclassgroup` WRITE;
/*!40000 ALTER TABLE `ezcontentclassgroup` DISABLE KEYS */;
INSERT INTO `ezcontentclassgroup` VALUES (1031216928,14,1,1033922106,14,'Content'),(1031216941,14,2,1033922113,14,'Users'),(1032009743,14,3,1033922120,14,'Media'),(1081858024,14,4,1081858024,14,'Setup');
/*!40000 ALTER TABLE `ezcontentclassgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject`
--

DROP TABLE IF EXISTS `ezcontentobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject` (
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `current_version` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) DEFAULT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezcontentobject_remote_id` (`remote_id`),
  KEY `ezcontentobject_classid` (`contentclass_id`),
  KEY `ezcontentobject_currentversion` (`current_version`),
  KEY `ezcontentobject_lmask` (`language_mask`),
  KEY `ezcontentobject_owner` (`owner_id`),
  KEY `ezcontentobject_pub` (`published`),
  KEY `ezcontentobject_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject`
--

LOCK TABLES `ezcontentobject` WRITE;
/*!40000 ALTER TABLE `ezcontentobject` DISABLE KEYS */;
INSERT INTO `ezcontentobject` VALUES (3,1,4,2,3,1033917596,'Users',14,1033917596,'f5c88a2209584891056f987fd965b0ba',2,1),(4,2,10,2,3,1072180405,'Anonymous User',14,1033920665,'faaeb9be3bd98ed09f606fc16d144eca',2,1),(3,1,12,2,3,1033920775,'Administrator users',14,1033920775,'9b47a45624b023b1a76c73b74d704acf',2,1),(4,4,14,2,3,1484920002,'Administrator User',14,1033920830,'1bb4fe25487f05527efa8bfd394cecc7',2,1),(1,1,41,2,3,1060695457,'Media',14,1060695457,'a6e35cbcb7cd6ae4b691f3eee30cd262',3,1),(3,1,42,2,3,1072180330,'Anonymous Users',14,1072180330,'15b256dbea2ae72418ff5facc999e8f9',2,1),(1,1,45,2,3,1079684190,'Setup',14,1079684190,'241d538ce310074e602f29f49e44e938',4,1),(1,1,49,2,3,1080220197,'Images',14,1080220197,'e7ff633c6b8e0fd3531e74c6e712bead',3,1),(1,1,50,2,3,1080220220,'Files',14,1080220220,'732a5acd01b51a6fe6eab448ad4138a9',3,1),(1,1,51,2,3,1080220233,'Multimedia',14,1080220233,'09082deb98662a104f325aaa8c4933d3',3,1),(14,1,52,2,2,1082016591,'Common INI settings',14,1082016591,'27437f3547db19cf81a33c92578b2c89',4,1),(15,2,54,2,2,1301062376,'eZ Publish Demo Site (without demo content)',14,1082016652,'8b8b22fe3c6061ed500fbd2b377b885f',5,1),(1,1,56,2,3,1103023132,'Design',14,1103023132,'08799e609893f7aba22f10cb466d9cc8',5,1),(23,2,57,2,3,1485160438,'Home',14,1195480486,'8a9c9c761004866fb458d89910f52bee',1,1),(40,2,62,2,6,1485156947,'Product Category 1',14,1485153046,'bc29ce78f90bb5cf9b2d50c730cf9c8f',1,1),(41,8,63,2,6,1486367446,'PR-192: Product One',14,1485153471,'9470ae34897eacc1ff903ae1ff92950c',1,1),(41,2,64,2,2,1486121622,'XY-827: Product Two',14,1485153514,'f642e266becf9d21114d15d9ed3d13e8',1,1),(40,2,65,2,2,1485153639,'Product Category 2',14,1485153593,'ba1327f873820a24de887b4f052eda66',1,1),(41,4,66,2,6,1486121839,'CJ-831: Product Three',14,1485153594,'87d729738a74ddbe419af390526a9b70',1,1),(41,5,67,2,6,1486367395,'PR-228: Product Four',14,1485153594,'4964cc2c2178872de3eddb5b3aa05726',1,1),(41,4,68,2,6,1486121786,'XX-837: Product Hello 9',14,1485153762,'d28a451a7c006e1cc224f8de1b7fd0f9',1,1),(1,1,70,2,3,1485521863,'Products',14,1485521863,'2372c70c31ac3be278103f5a6001ee99',3,1),(43,2,71,2,6,1486121874,'About us',14,1485522423,'4360db968a847944a1ae984d299cf5a6',1,1),(27,1,72,2,3,1485525584,'Product photo 1',14,1485525584,'84c94309379063be68d8b78f3f862244',3,1),(27,1,73,2,3,0,'New Image',14,0,'f9259370ea53cdc91a648939a1f93806',3,0),(27,3,74,2,3,1486122870,'Product photo 2',14,1485715347,'0f2a6e542f1c2adc10cb1beacd14724b',3,1),(27,2,75,2,3,1485715497,'Product photo 3',14,1485715362,'fffc91f9b79f9457a1d3da5c8b67cd75',3,1),(40,1,76,2,2,1485762975,'All products',14,1485762975,'21384bfa88e981825b3c53656980f87b',1,1),(40,1,77,2,2,1485763065,'Subcategory 1',14,1485763065,'1231b212c9e50b63d96920a765194ebb',1,1),(40,2,78,2,2,1485764737,'Subcategory 2',14,1485763836,'86b79c3f114bf9213e82941e7d66a0a8',1,1),(23,2,79,2,10,1486366463,'English',14,1486121030,'414766d7e67f83aa0b30636db15ff669',1,1),(23,1,81,4,4,1486121151,'サイトのトップページ',14,1486121151,'063bedce7c536bca2b584c5cc1e7baa6',1,1),(40,1,82,4,4,1486121234,'すべての製品',14,1486121234,'d75d0c713c3e4856423cc89faf9926e8',1,1),(40,1,83,4,4,1486121276,'製品カテゴリ1',14,1486121276,'af492d8de6374401ba04bf903f8ec937',1,1),(40,2,84,4,4,1486121309,'製品カテゴリ2',14,1486121295,'28bf734e5bb684a0471c9037ca63075a',1,1),(41,2,85,4,4,1486121482,'JP-271: 日本の唯一の製品',14,1486121440,'347296dae22a05c3d134870e03973d22',1,1),(1,1,86,2,3,1486123295,'Product photos',14,1486123295,'879172d3ae58b0f72f6bf30654c21cb0',3,1),(27,1,87,2,3,1486123347,'Product Photo 1',14,1486123347,'60c20bc282458d46568b64c0add4a4a2',3,1),(27,3,88,2,7,1486123633,'Product Photo 2',14,1486123361,'68bd8d129bf3611e2966ae1bb6d82a86',3,1),(27,3,89,2,3,1486123599,'Product Photo 3',14,1486123377,'af60cbb26f6b9e89312029993bb9af64',3,1),(40,1,90,2,2,1486366668,'Product category 3',14,1486366668,'1b7ac139bdf2edd6579e7d673fd34027',1,1),(3,1,121,2,3,1486995136,'Country editors',14,1486995136,'40b9f7d9f5f135d8768ab1410ca3ef19',2,2),(3,1,122,2,3,1486995136,'Japanese',14,1486995136,'9ac0b9db527f4487690b372ac9479e05',2,2),(3,1,123,2,3,1486995136,'Chinese',14,1486995136,'ab82b7dee790d45e0e2a7a30c172f1cc',2,2),(3,1,124,2,3,1486995136,'English',14,1486995136,'202cef3e5e08843cfdb998b1133117f0',2,2),(3,1,127,2,3,1486995248,'Country editors',14,1486995248,'e3eba508da4f664df3f943fb707af840',2,1),(3,1,128,2,3,1486995248,'Japanese',14,1486995248,'96f0223d11fd144023444700bf509eb5',2,1),(3,1,129,2,3,1486995248,'Chinese',14,1486995248,'bbc66918f2eb5881f298ca18f27c0d30',2,1),(3,1,130,2,3,1486995248,'English',14,1486995248,'8a12af8d1199aa5e782a1137ad4b7e77',2,1),(4,1,131,2,2,1486995248,'James Japanese',14,1486995248,'e5caa924ba317dd8fe02baef58646d3d',2,1),(4,1,132,2,2,1486995248,'Eaton English',14,1486995248,'10ee95f50c1749c11cb38a7223924b6d',2,1),(3,1,133,2,3,1486996262,'Approving users',14,1486996262,'f7b5b045e1292f3543c5561ca27adc35',2,1),(4,1,135,2,3,1486997214,'Second Admin',14,1486997214,'897f1ce829ba06f7a8f1256cc2937678',2,1);
/*!40000 ALTER TABLE `ezcontentobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_attribute`
--

DROP TABLE IF EXISTS `ezcontentobject_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_attribute` (
  `attribute_original_id` int(11) DEFAULT '0',
  `contentclassattribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `data_float` double DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext,
  `data_type_string` varchar(50) DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(20) NOT NULL DEFAULT '',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `sort_key_int` int(11) NOT NULL DEFAULT '0',
  `sort_key_string` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ezcontentobject_attribute_co_id_ver_lang_code` (`contentobject_id`,`version`,`language_code`),
  KEY `ezcontentobject_attribute_language_code` (`language_code`),
  KEY `ezcontentobject_classattr_id` (`contentclassattribute_id`),
  KEY `sort_key_int` (`sort_key_int`),
  KEY `sort_key_string` (`sort_key_string`)
) ENGINE=InnoDB AUTO_INCREMENT=555 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_attribute`
--

LOCK TABLES `ezcontentobject_attribute` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_attribute` DISABLE KEYS */;
INSERT INTO `ezcontentobject_attribute` VALUES (0,7,4,NULL,NULL,'Main group','ezstring',7,'eng-US',3,0,'',1),(0,6,4,NULL,NULL,'Users','ezstring',8,'eng-US',3,0,'',1),(0,8,10,0,0,'Anonymous','ezstring',19,'eng-US',3,0,'anonymous',2),(0,9,10,0,0,'User','ezstring',20,'eng-US',3,0,'user',2),(0,12,10,0,0,'','ezuser',21,'eng-US',3,0,'',2),(0,6,12,0,0,'Administrator users','ezstring',24,'eng-US',3,0,'',1),(0,7,12,0,0,'','ezstring',25,'eng-US',3,0,'',1),(0,8,14,0,0,'Administrator','ezstring',28,'eng-US',3,0,'administrator',3),(0,8,14,0,0,'Administrator','ezstring',28,'eng-US',3,0,'administrator',4),(0,9,14,0,0,'User','ezstring',29,'eng-US',3,0,'user',3),(0,9,14,0,0,'User','ezstring',29,'eng-US',3,0,'user',4),(30,12,14,0,0,'','ezuser',30,'eng-US',3,0,'',3),(30,12,14,0,0,'{\"login\":\"admin\",\"password_hash\":\"c78e3b0f3d9244ed8c6d1c29464bdff9\",\"email\":\"foo@example.com\",\"password_hash_type\":2}','ezuser',30,'eng-US',3,0,'',4),(0,4,41,0,0,'Media','ezstring',98,'eng-US',3,0,'',1),(0,119,41,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',99,'eng-US',3,0,'',1),(0,6,42,0,0,'Anonymous Users','ezstring',100,'eng-US',3,0,'anonymous users',1),(0,7,42,0,0,'User group for the anonymous user','ezstring',101,'eng-US',3,0,'user group for the anonymous user',1),(0,155,41,0,0,'','ezstring',103,'eng-US',3,0,'',1),(0,156,41,0,1045487555,'','ezxmltext',105,'eng-US',3,0,'',1),(0,158,41,0,0,'','ezboolean',109,'eng-US',3,0,'',1),(0,4,45,0,0,'Setup','ezstring',123,'eng-US',3,0,'setup',1),(0,155,45,0,0,'','ezstring',124,'eng-US',3,0,'',1),(0,119,45,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',125,'eng-US',3,0,'',1),(0,156,45,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',126,'eng-US',3,0,'',1),(0,158,45,0,0,'','ezboolean',128,'eng-US',3,0,'',1),(0,4,49,0,0,'Images','ezstring',142,'eng-US',3,0,'images',1),(0,155,49,0,0,'','ezstring',143,'eng-US',3,0,'',1),(0,119,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',144,'eng-US',3,0,'',1),(0,156,49,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',145,'eng-US',3,0,'',1),(0,158,49,0,1,'','ezboolean',146,'eng-US',3,1,'',1),(0,4,50,0,0,'Files','ezstring',147,'eng-US',3,0,'files',1),(0,155,50,0,0,'','ezstring',148,'eng-US',3,0,'',1),(0,119,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',149,'eng-US',3,0,'',1),(0,156,50,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',150,'eng-US',3,0,'',1),(0,158,50,0,1,'','ezboolean',151,'eng-US',3,1,'',1),(0,4,51,0,0,'Multimedia','ezstring',152,'eng-US',3,0,'multimedia',1),(0,155,51,0,0,'','ezstring',153,'eng-US',3,0,'',1),(0,119,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',154,'eng-US',3,0,'',1),(0,156,51,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',155,'eng-US',3,0,'',1),(0,158,51,0,1,'','ezboolean',156,'eng-US',3,1,'',1),(0,159,52,0,0,'Common INI settings','ezstring',157,'eng-US',2,0,'common ini settings',1),(0,160,52,0,0,'/content/view/full/2/','ezinisetting',158,'eng-US',2,0,'',1),(0,161,52,0,0,'/content/view/full/2','ezinisetting',159,'eng-US',2,0,'',1),(0,162,52,0,0,'disabled','ezinisetting',160,'eng-US',2,0,'',1),(0,163,52,0,0,'disabled','ezinisetting',161,'eng-US',2,0,'',1),(0,164,52,0,0,'','ezinisetting',162,'eng-US',2,0,'',1),(0,165,52,0,0,'enabled','ezinisetting',163,'eng-US',2,0,'',1),(0,166,52,0,0,'disabled','ezinisetting',164,'eng-US',2,0,'',1),(0,167,52,0,0,'enabled','ezinisetting',165,'eng-US',2,0,'',1),(0,168,52,0,0,'enabled','ezinisetting',166,'eng-US',2,0,'',1),(0,169,52,0,0,'=geometry/scale=100;100','ezinisetting',167,'eng-US',2,0,'',1),(0,170,52,0,0,'=geometry/scale=200;200','ezinisetting',168,'eng-US',2,0,'',1),(0,171,52,0,0,'=geometry/scale=300;300','ezinisetting',169,'eng-US',2,0,'',1),(0,172,54,0,0,'eZ Publish Demo Site (without demo content)','ezinisetting',170,'eng-US',2,0,'',2),(0,173,54,0,0,'author=eZ Systems\ncopyright=eZ Systems\ndescription=Content Management System\nkeywords=cms, publish, e-commerce, content management, development framework','ezinisetting',171,'eng-US',2,0,'',2),(0,174,54,0,0,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"eZ-Publish-Demo-Site-without-demo-content1.png\" suffix=\"png\" basename=\"eZ-Publish-Demo-Site-without-demo-content1\" dirpath=\"var/ezdemo_site/storage/images/design/plain-site/172-2-eng-US\" url=\"var/ezdemo_site/storage/images/design/plain-site/172-2-eng-US/eZ-Publish-Demo-Site-without-demo-content1.png\" original_filename=\"logo.png\" mime_type=\"image/png\" width=\"138\" height=\"46\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1484920003\"><original attribute_id=\"172\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',172,'eng-US',2,0,'',2),(0,175,54,0,0,'0','ezpackage',173,'eng-US',2,0,'0',2),(0,177,54,0,0,'foo@example.com','ezinisetting',175,'eng-US',2,0,'',2),(0,178,54,0,0,'yamaha.local','ezinisetting',176,'eng-US',2,0,'',2),(0,179,10,0,0,'','eztext',177,'eng-US',3,0,'',2),(0,179,14,0,0,'','eztext',178,'eng-US',3,0,'',3),(0,179,14,0,0,'','eztext',178,'eng-US',3,0,'',4),(0,180,10,0,0,'','ezimage',179,'eng-US',3,0,'',2),(0,180,14,0,0,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1301057722\"><original attribute_id=\"180\" attribute_version=\"3\" attribute_language=\"eng-GB\"/></ezimage>\n','ezimage',180,'eng-US',3,0,'',3),(0,180,14,0,0,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1301057722\"><original attribute_id=\"180\" attribute_version=\"3\" attribute_language=\"eng-GB\"/></ezimage>\n','ezimage',180,'eng-US',3,0,'',4),(0,4,56,0,NULL,'Design','ezstring',181,'eng-US',3,0,'design',1),(0,155,56,0,NULL,'','ezstring',182,'eng-US',3,0,'',1),(0,119,56,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',183,'eng-US',3,0,'',1),(0,156,56,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\"\n         xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\"\n         xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\" />','ezxmltext',184,'eng-US',3,0,'',1),(0,158,56,0,1,'','ezboolean',185,'eng-US',3,1,'',1),(0,230,57,0,NULL,'Home','ezstring',186,'eng-US',3,0,'home',1),(0,230,57,0,NULL,'Home','ezstring',186,'eng-US',3,0,'home',2),(0,231,57,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_865346aabbcc48a9839274cc554868be\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_f742abffba08fc849b6e80dec769a74c\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',187,'eng-US',3,0,'',1),(0,231,57,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_865346aabbcc48a9839274cc554868be\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_f742abffba08fc849b6e80dec769a74c\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',187,'eng-US',3,0,'',2),(0,296,54,0,29,'Site map','ezurl',200,'eng-US',2,0,'',2),(0,297,54,0,30,'Tag cloud','ezurl',201,'eng-US',2,0,'',2),(0,298,54,0,NULL,'Login','ezstring',202,'eng-US',2,0,'login',2),(0,299,54,0,NULL,'Logout','ezstring',203,'eng-US',2,0,'logout',2),(0,300,54,0,NULL,'My profile','ezstring',204,'eng-US',2,0,'my profile',2),(0,301,54,0,NULL,'Register','ezstring',205,'eng-US',2,0,'register',2),(0,302,54,0,NULL,'/rss/feed/my_feed','ezstring',206,'eng-US',2,0,'/rss/feed/my_feed',2),(0,303,54,0,NULL,'Shopping basket','ezstring',207,'eng-US',2,0,'shopping basket',2),(0,304,54,0,NULL,'Site settings','ezstring',208,'eng-US',2,0,'site settings',2),(0,305,54,0,NULL,'Copyright &#169; 2017 <a href=\"http://ez.no\" title=\"eZ Systems\">eZ Systems AS</a> (except where otherwise noted). All rights reserved.','eztext',209,'eng-US',2,0,'',2),(0,306,54,0,0,'','ezboolean',210,'eng-US',2,0,'',2),(0,307,54,0,NULL,'','eztext',211,'eng-US',2,0,'',2),(0,308,41,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',214,'eng-US',2,0,'',1),(0,308,45,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',215,'eng-US',2,0,'',1),(0,308,49,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',216,'eng-US',2,0,'',1),(0,308,50,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',217,'eng-US',2,0,'',1),(0,308,51,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',218,'eng-US',2,0,'',1),(0,308,56,0,NULL,'<?xml version=\"1.0\"?>\n<page/>\n','ezpage',219,'eng-US',2,0,'',1),(0,309,41,0,NULL,'','ezkeyword',220,'eng-US',2,0,'',1),(0,309,45,0,NULL,'','ezkeyword',221,'eng-US',2,0,'',1),(0,309,49,0,NULL,'','ezkeyword',222,'eng-US',2,0,'',1),(0,309,50,0,NULL,'','ezkeyword',223,'eng-US',2,0,'',1),(0,309,51,0,NULL,'','ezkeyword',224,'eng-US',2,0,'',1),(0,309,56,0,NULL,'','ezkeyword',225,'eng-US',2,0,'',1),(0,310,62,0,NULL,'Product Category 1','ezstring',250,'eng-US',2,0,'product category 1',1),(0,310,62,0,NULL,'Product Category 1','ezstring',250,'eng-US',2,0,'product category 1',2),(0,311,62,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>asdh kjhaskdjhakjsdhajksdhjkaksdhakj haskjdhsad</paragraph></section>\n','ezxmltext',251,'eng-US',2,0,'',1),(0,311,62,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>asdh kjhaskdjhakjsdhajksdhjkaksdhakj haskjdhsad</paragraph></section>\n','ezxmltext',251,'eng-US',2,0,'',2),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',1),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',2),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',3),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',4),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',5),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',6),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',7),(0,312,63,0,NULL,'Product One','ezstring',252,'eng-US',2,0,'product one',8),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',1),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',2),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',3),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',4),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',5),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',6),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',7),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',253,'eng-US',2,0,'',8),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-One.jpg\" suffix=\"jpg\" basename=\"Product-One\" dirpath=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"360\" Width=\"600\" IsColor=\"1\"/><alias name=\"reference\" filename=\"Product-One_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alias_key=\"2605465115\" timestamp=\"1485157465\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-One_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"60\" alias_key=\"2343348577\" timestamp=\"1485157465\" is_valid=\"1\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',1),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',2),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',3),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',4),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',5),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',6),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',7),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',254,'eng-US',2,0,'',8),(0,315,63,0,NULL,'123123','ezstring',255,'eng-US',2,0,'123123',1),(0,315,63,0,NULL,'123123','ezstring',255,'eng-US',2,0,'123123',2),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',3),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',4),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',5),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',6),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',7),(0,315,63,0,NULL,'PR-192','ezstring',255,'eng-US',2,0,'pr-192',8),(0,312,64,0,NULL,'Product Two','ezstring',256,'eng-US',2,0,'product two',1),(0,312,64,0,NULL,'Product Two','ezstring',256,'eng-US',2,0,'product two',2),(0,313,64,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Two</paragraph></section>\n','ezxmltext',257,'eng-US',2,0,'',1),(0,313,64,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Two</paragraph></section>\n','ezxmltext',257,'eng-US',2,0,'',2),(0,314,64,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',258,'eng-US',2,0,'',1),(0,314,64,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',258,'eng-US',2,0,'',2),(0,315,64,0,NULL,'2271723','ezstring',259,'eng-US',2,0,'2271723',1),(0,315,64,0,NULL,'XY-827','ezstring',259,'eng-US',2,0,'xy-827',2),(0,310,65,0,NULL,'Product Category 1','ezstring',260,'eng-US',2,0,'product category 1',1),(0,310,65,0,NULL,'Product Category 2','ezstring',260,'eng-US',2,0,'product category 2',2),(0,311,65,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>asdh kjhaskdjhakjsdhajksdhjkaksdhakj haskjdhsad</paragraph></section>\n','ezxmltext',261,'eng-US',2,0,'',1),(0,311,65,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>askdljalskd jalksdjlkasdasd </paragraph></section>\n','ezxmltext',261,'eng-US',2,0,'',2),(0,312,66,0,NULL,'Product One','ezstring',262,'eng-US',2,0,'product one',1),(0,312,66,0,NULL,'Product Three','ezstring',262,'eng-US',2,0,'product three',2),(0,312,66,0,NULL,'Product Three','ezstring',262,'eng-US',2,0,'product three',3),(0,312,66,0,NULL,'Product Three','ezstring',262,'eng-US',2,0,'product three',4),(0,313,66,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',263,'eng-US',2,0,'',1),(0,313,66,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',263,'eng-US',2,0,'',2),(0,313,66,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',263,'eng-US',2,0,'',3),(0,313,66,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>qwej hqwkjehkjqwhe kjqwehk qhwekjhqjwkejhq wkjhekjqwhekj</paragraph></section>\n','ezxmltext',263,'eng-US',2,0,'',4),(0,314,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-One.jpg\" suffix=\"jpg\" basename=\"Product-One\" dirpath=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"360\" Width=\"600\" IsColor=\"1\"/><alias name=\"reference\" filename=\"Product-One_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alias_key=\"2605465115\" timestamp=\"1485153475\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-One_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"60\" alias_key=\"2343348577\" timestamp=\"1485153475\" is_valid=\"1\"/></ezimage>\n','ezimage',264,'eng-US',2,0,'',1),(0,314,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Three.jpg\" suffix=\"jpg\" basename=\"Product-Three\" dirpath=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"360\" Width=\"600\" IsColor=\"1\"/><alias name=\"reference\" filename=\"Product-Three_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alias_key=\"2605465115\" timestamp=\"1485153475\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Three_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"60\" alias_key=\"2343348577\" timestamp=\"1485153475\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-Three_medium.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US\" url=\"var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"120\" alias_key=\"405413724\" timestamp=\"1485153652\" is_valid=\"1\"/></ezimage>\n','ezimage',264,'eng-US',2,0,'',2),(0,314,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"CJ-831-Product-Three.jpg\" suffix=\"jpg\" basename=\"CJ-831-Product-Three\" dirpath=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US\" url=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US/CJ-831-Product-Three.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',264,'eng-US',2,0,'',3),(0,314,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"CJ-831-Product-Three.jpg\" suffix=\"jpg\" basename=\"CJ-831-Product-Three\" dirpath=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US\" url=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US/CJ-831-Product-Three.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',264,'eng-US',2,0,'',4),(0,315,66,0,NULL,'123123','ezstring',265,'eng-US',2,0,'123123',1),(0,315,66,0,NULL,'123123','ezstring',265,'eng-US',2,0,'123123',2),(0,315,66,0,NULL,'CJ-831','ezstring',265,'eng-US',2,0,'cj-831',3),(0,315,66,0,NULL,'CJ-831','ezstring',265,'eng-US',2,0,'cj-831',4),(0,312,67,0,NULL,'Product Two','ezstring',266,'eng-US',2,0,'product two',1),(0,312,67,0,NULL,'Product Four','ezstring',266,'eng-US',2,0,'product four',2),(0,312,67,0,NULL,'Product Four','ezstring',266,'eng-US',2,0,'product four',3),(0,312,67,0,NULL,'Product Four','ezstring',266,'eng-US',2,0,'product four',4),(0,312,67,0,NULL,'Product Four','ezstring',266,'eng-US',2,0,'product four',5),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Two</paragraph></section>\n','ezxmltext',267,'eng-US',2,0,'',1),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Four</paragraph></section>\n','ezxmltext',267,'eng-US',2,0,'',2),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Four</paragraph></section>\n','ezxmltext',267,'eng-US',2,0,'',3),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Four</paragraph></section>\n','ezxmltext',267,'eng-US',2,0,'',4),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product Four</paragraph></section>\n','ezxmltext',267,'eng-US',2,0,'',5),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',268,'eng-US',2,0,'',1),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',268,'eng-US',2,0,'',2),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',268,'eng-US',2,0,'',3),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',268,'eng-US',2,0,'',4),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',268,'eng-US',2,0,'',5),(0,315,67,0,NULL,'2271723','ezstring',269,'eng-US',2,0,'2271723',1),(0,315,67,0,NULL,'2271723','ezstring',269,'eng-US',2,0,'2271723',2),(0,315,67,0,NULL,'PR-228','ezstring',269,'eng-US',2,0,'pr-228',3),(0,315,67,0,NULL,'PR-228','ezstring',269,'eng-US',2,0,'pr-228',4),(0,315,67,0,NULL,'PR-228','ezstring',269,'eng-US',2,0,'pr-228',5),(0,312,68,0,NULL,'Product Five','ezstring',270,'eng-US',2,0,'product five',1),(0,312,68,0,NULL,'Product Hello 123','ezstring',270,'eng-US',2,0,'product hello 123',2),(0,312,68,0,NULL,'Product Hello 9','ezstring',270,'eng-US',2,0,'product hello 9',3),(0,312,68,0,NULL,'Product Hello 9','ezstring',270,'eng-US',2,0,'product hello 9',4),(0,313,68,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>lkasjdlkjasdlkj </paragraph></section>\n','ezxmltext',271,'eng-US',2,0,'',1),(0,313,68,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>lkasjdlkjasdlkj </paragraph></section>\n','ezxmltext',271,'eng-US',2,0,'',2),(0,313,68,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>lkasjdlkjasdlkj </paragraph></section>\n','ezxmltext',271,'eng-US',2,0,'',3),(0,313,68,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>lkasjdlkjasdlkj </paragraph></section>\n','ezxmltext',271,'eng-US',2,0,'',4),(0,314,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153747\"><original attribute_id=\"272\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',272,'eng-US',2,0,'',1),(0,314,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153747\"><original attribute_id=\"272\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',272,'eng-US',2,0,'',2),(0,314,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153747\"><original attribute_id=\"272\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',272,'eng-US',2,0,'',3),(0,314,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153747\"><original attribute_id=\"272\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',272,'eng-US',2,0,'',4),(0,315,68,0,NULL,'228821','ezstring',273,'eng-US',2,0,'228821',1),(0,315,68,0,NULL,'228821','ezstring',273,'eng-US',2,0,'228821',2),(0,315,68,0,NULL,'XX-837','ezstring',273,'eng-US',2,0,'xx-837',3),(0,315,68,0,NULL,'XX-837','ezstring',273,'eng-US',2,0,'xx-837',4),(0,310,62,0,NULL,'製品カテゴリ1','ezstring',274,'jpn-JP',4,0,'製品カテゴリ1',2),(0,311,62,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>こんにちは。</paragraph></section>\n','ezxmltext',275,'jpn-JP',4,0,'',2),(0,316,63,NULL,NULL,NULL,'ezobjectrelationlist',278,'eng-US',2,0,'',1),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"72\" contentobject-version=\"1\" node-id=\"73\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"84c94309379063be68d8b78f3f862244\"/><relation-item priority=\"2\" contentobject-id=\"74\" contentobject-version=\"2\" node-id=\"74\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"0f2a6e542f1c2adc10cb1beacd14724b\"/><relation-item priority=\"3\" contentobject-id=\"75\" contentobject-version=\"2\" node-id=\"75\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"fffc91f9b79f9457a1d3da5c8b67cd75\"/></relation-list></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',2),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"72\" contentobject-version=\"1\" node-id=\"73\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"84c94309379063be68d8b78f3f862244\"/><relation-item priority=\"2\" contentobject-id=\"74\" contentobject-version=\"2\" node-id=\"74\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"0f2a6e542f1c2adc10cb1beacd14724b\"/><relation-item priority=\"3\" contentobject-id=\"75\" contentobject-version=\"2\" node-id=\"75\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"fffc91f9b79f9457a1d3da5c8b67cd75\"/></relation-list></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',3),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"72\" contentobject-version=\"1\" node-id=\"73\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"84c94309379063be68d8b78f3f862244\"/><relation-item priority=\"2\" contentobject-id=\"74\" contentobject-version=\"2\" node-id=\"74\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"0f2a6e542f1c2adc10cb1beacd14724b\"/><relation-item priority=\"3\" contentobject-id=\"75\" contentobject-version=\"2\" node-id=\"75\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"fffc91f9b79f9457a1d3da5c8b67cd75\"/></relation-list></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',4),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',5),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',6),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"87\" contentobject-version=\"1\" node-id=\"92\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"60c20bc282458d46568b64c0add4a4a2\"/><relation-item priority=\"2\" contentobject-id=\"88\" contentobject-version=\"2\" node-id=\"93\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"68bd8d129bf3611e2966ae1bb6d82a86\"/><relation-item priority=\"3\" contentobject-id=\"89\" contentobject-version=\"2\" node-id=\"94\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"af60cbb26f6b9e89312029993bb9af64\"/></relation-list></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',7),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"87\" contentobject-version=\"1\" node-id=\"92\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"60c20bc282458d46568b64c0add4a4a2\"/><relation-item priority=\"2\" contentobject-id=\"88\" contentobject-version=\"2\" node-id=\"93\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"68bd8d129bf3611e2966ae1bb6d82a86\"/><relation-item priority=\"3\" contentobject-id=\"89\" contentobject-version=\"2\" node-id=\"94\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"af60cbb26f6b9e89312029993bb9af64\"/></relation-list></related-objects>\n','ezobjectrelationlist',278,'eng-US',2,0,'',8),(0,316,64,NULL,NULL,NULL,'ezobjectrelationlist',279,'eng-US',2,0,'',1),(0,316,64,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',279,'eng-US',2,0,'',2),(0,316,66,NULL,NULL,NULL,'ezobjectrelationlist',281,'eng-US',2,0,'',1),(0,316,66,NULL,NULL,NULL,'ezobjectrelationlist',281,'eng-US',2,0,'',2),(0,316,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',281,'eng-US',2,0,'',3),(0,316,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',281,'eng-US',2,0,'',4),(0,316,67,NULL,NULL,NULL,'ezobjectrelationlist',282,'eng-US',2,0,'',1),(0,316,67,NULL,NULL,NULL,'ezobjectrelationlist',282,'eng-US',2,0,'',2),(0,316,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',282,'eng-US',2,0,'',3),(0,316,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',282,'eng-US',2,0,'',4),(0,316,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',282,'eng-US',2,0,'',5),(0,316,68,NULL,NULL,NULL,'ezobjectrelationlist',284,'eng-US',2,0,'',1),(0,316,68,NULL,NULL,NULL,'ezobjectrelationlist',284,'eng-US',2,0,'',2),(0,316,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',284,'eng-US',2,0,'',3),(0,316,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',284,'eng-US',2,0,'',4),(0,4,70,0,NULL,'Products','ezstring',298,'eng-US',3,0,'products',1),(0,155,70,0,NULL,'','ezstring',299,'eng-US',3,0,'',1),(0,119,70,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',300,'eng-US',3,0,'',1),(0,156,70,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',301,'eng-US',3,0,'',1),(302,158,70,0,1,'','ezboolean',302,'eng-US',3,1,'',1),(0,308,70,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>CallForActionLayout</zone_layout>\n  <zone id=\"id_b7ae6d66624e1e45be09f0039e0eca59\">\n    <zone_identifier>main</zone_identifier>\n  </zone>\n</page>\n','ezpage',303,'eng-US',3,0,'',1),(0,309,70,0,NULL,'','ezkeyword',304,'eng-US',3,0,'',1),(0,319,71,0,NULL,'About us','ezstring',305,'eng-US',2,0,'about us',1),(0,319,71,0,NULL,'About us','ezstring',305,'eng-US',2,0,'about us',2),(0,320,71,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>JavaScript code with focus on improved data model. Chai is an assertion library to a task runner aiming at explaining the increasing speed up consecutive function with the host environment for most popular browsers share support for creating Web browsers It is a JavaScript transformation toolkit to create and mobile applications, and interact.</paragraph><paragraph>Despite the process of any state changes, usually by analogy to transform CSS. Gulp is a library that they are embedded in the popularity of V8 is a JavaScript source code can respond to create host environment in which could also increased the subject, maintains MongoDB. XHR is the concept. Vanilla is not available in which could also known immediately.</paragraph><paragraph>Pages and response objects, which it and Node. IIFE is a list of software modules, defined by JavaScript framework based on a simple, pluggable static site generator. Coffeescript is a way of any compatible web applications in C.</paragraph></section>\n','ezxmltext',306,'eng-US',2,0,'',1),(0,320,71,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>JavaScript code with focus on improved data model. Chai is an assertion library to a task runner aiming at explaining the increasing speed up consecutive function with the host environment for most popular browsers share support for creating Web browsers It is a JavaScript transformation toolkit to create and mobile applications, and interact.</paragraph><paragraph>Despite the process of any state changes, usually by analogy to transform CSS. Gulp is a library that they are embedded in the popularity of V8 is a JavaScript source code can respond to create host environment in which could also increased the subject, maintains MongoDB. XHR is the concept. Vanilla is not available in which could also known immediately.</paragraph><paragraph>Pages and response objects, which it and Node. IIFE is a list of software modules, defined by JavaScript framework based on a simple, pluggable static site generator. Coffeescript is a way of any compatible web applications in C.</paragraph></section>\n','ezxmltext',306,'eng-US',2,0,'',2),(0,245,72,0,NULL,'Product photo 1','ezstring',307,'eng-US',3,0,'product photo 1',1),(0,246,72,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',308,'eng-US',3,0,'',1),(0,247,72,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-1.jpg\" suffix=\"jpg\" basename=\"Product-photo-1\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"1950\" height=\"1309\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485525584\"><original attribute_id=\"309\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"1309\" Width=\"1950\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"435\" alias_key=\"2605465115\" timestamp=\"1486122941\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"73\" alias_key=\"2343348577\" timestamp=\"1486123255\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-photo-1_large.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"218\" alias_key=\"1592566908\" timestamp=\"1486122941\" is_valid=\"1\"/></ezimage>\n','ezimage',309,'eng-US',3,0,'',1),(0,247,72,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-1.jpg\" suffix=\"jpg\" basename=\"Product-photo-1\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"1950\" height=\"1309\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485525584\"><original attribute_id=\"309\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"1309\" Width=\"1950\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"435\" alias_key=\"2605465115\" timestamp=\"1486122925\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"67\" alias_key=\"2343348577\" timestamp=\"1485761402\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-photo-1_medium.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"145\" alias_key=\"405413724\" timestamp=\"1486122925\" is_valid=\"1\"/></ezimage>\n','ezimage',309,'eng-US',2,0,'',2),(0,248,72,0,NULL,'','ezsrrating',310,'eng-US',3,0,'',1),(0,248,72,0,NULL,'','ezsrrating',310,'eng-US',2,0,'',2),(0,249,72,0,NULL,'','ezkeyword',311,'eng-US',3,0,'',1),(0,249,72,0,NULL,'','ezkeyword',311,'eng-US',2,0,'',2),(0,245,73,0,NULL,'Product photo 2','ezstring',312,'eng-US',2,0,'product photo 2',1),(0,246,73,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',313,'eng-US',2,0,'',1),(0,247,73,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"2\" is_valid=\"1\" filename=\"Product-photo-22.jpg\" suffix=\"jpg\" basename=\"Product-photo-22\" dirpath=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US\" url=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485525652\"><original attribute_id=\"314\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-22_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US\" url=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1485525652\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-photo-22_large.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US\" url=\"var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"144\" alias_key=\"1592566908\" timestamp=\"1485525652\" is_valid=\"1\"/></ezimage>\n','ezimage',314,'eng-US',2,0,'',1),(0,248,73,0,NULL,'','ezsrrating',315,'eng-US',2,0,'',1),(0,249,73,0,NULL,'','ezkeyword',316,'eng-US',2,0,'',1),(0,245,74,0,NULL,'Product photo 1','ezstring',317,'eng-US',3,0,'product photo 1',1),(0,245,74,0,NULL,'Product photo 2','ezstring',317,'eng-US',3,0,'product photo 2',2),(0,245,74,0,NULL,'Product photo 2','ezstring',317,'eng-US',3,0,'product photo 2',3),(0,246,74,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',318,'eng-US',3,0,'',1),(0,246,74,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',318,'eng-US',3,0,'',2),(0,246,74,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',318,'eng-US',3,0,'',3),(0,247,74,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-1.jpg\" suffix=\"jpg\" basename=\"Product-photo-1\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"1950\" height=\"1309\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485525584\"><original attribute_id=\"309\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"1309\" Width=\"1950\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"403\" alias_key=\"2605465115\" timestamp=\"1485525588\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"67\" alias_key=\"2343348577\" timestamp=\"1485525588\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-photo-1_large.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"202\" alias_key=\"1592566908\" timestamp=\"1485715451\" is_valid=\"1\"/></ezimage>\n','ezimage',319,'eng-US',3,0,'',1),(0,247,74,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-2.jpg\" suffix=\"jpg\" basename=\"Product-photo-2\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715475\"><original attribute_id=\"319\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-2_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1485761402\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-2_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"48\" alias_key=\"2343348577\" timestamp=\"1485761402\" is_valid=\"1\"/></ezimage>\n','ezimage',319,'eng-US',3,0,'',2),(0,247,74,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-2.jpg\" suffix=\"jpg\" basename=\"Product-photo-2\" dirpath=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715475\"><original attribute_id=\"319\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-2_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1486122864\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-photo-2_medium.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"96\" alias_key=\"405413724\" timestamp=\"1486122864\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-2_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"48\" alias_key=\"2343348577\" timestamp=\"1486122874\" is_valid=\"1\"/></ezimage>\n','ezimage',319,'eng-US',3,0,'',3),(0,248,74,0,NULL,'','ezsrrating',320,'eng-US',3,0,'',1),(0,248,74,0,NULL,'','ezsrrating',320,'eng-US',3,0,'',2),(0,248,74,0,NULL,'','ezsrrating',320,'eng-US',3,0,'',3),(0,249,74,0,NULL,'','ezkeyword',321,'eng-US',3,0,'',1),(0,249,74,0,NULL,'','ezkeyword',321,'eng-US',3,0,'',2),(0,249,74,0,NULL,'','ezkeyword',321,'eng-US',3,0,'',3),(0,245,75,0,NULL,'Product photo 1','ezstring',322,'eng-US',3,0,'product photo 1',1),(0,245,75,0,NULL,'Product photo 3','ezstring',322,'eng-US',3,0,'product photo 3',2),(0,245,75,0,NULL,'Product photo 3','ezstring',322,'eng-US',2,0,'product photo 3',3),(0,246,75,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',323,'eng-US',3,0,'',1),(0,246,75,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',323,'eng-US',3,0,'',2),(0,246,75,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',323,'eng-US',2,0,'',3),(0,247,75,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-1.jpg\" suffix=\"jpg\" basename=\"Product-photo-1\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"1950\" height=\"1309\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485525584\"><original attribute_id=\"309\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"1309\" Width=\"1950\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"403\" alias_key=\"2605465115\" timestamp=\"1485525588\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"67\" alias_key=\"2343348577\" timestamp=\"1485525588\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-photo-1_large.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"202\" alias_key=\"1592566908\" timestamp=\"1485715450\" is_valid=\"1\"/></ezimage>\n','ezimage',324,'eng-US',3,0,'',1),(0,247,75,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-3.jpg\" suffix=\"jpg\" basename=\"Product-photo-3\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3.jpg\" original_filename=\"yamaha-photo-3.jpg\" mime_type=\"image/jpeg\" width=\"1264\" height=\"1000\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715497\"><original attribute_id=\"324\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"1000\" Width=\"1264\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-3_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"475\" alias_key=\"2605465115\" timestamp=\"1486123101\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-3_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"79\" alias_key=\"2343348577\" timestamp=\"1486123255\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-photo-3_large.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"238\" alias_key=\"1592566908\" timestamp=\"1486123101\" is_valid=\"1\"/></ezimage>\n','ezimage',324,'eng-US',3,0,'',2),(0,247,75,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-photo-3.jpg\" suffix=\"jpg\" basename=\"Product-photo-3\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3.jpg\" original_filename=\"yamaha-photo-3.jpg\" mime_type=\"image/jpeg\" width=\"1264\" height=\"1000\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715497\"><original attribute_id=\"324\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"1000\" Width=\"1264\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-photo-3_reference.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"475\" alias_key=\"2605465115\" timestamp=\"1486123092\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-photo-3_small.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"79\" alias_key=\"2343348577\" timestamp=\"1485761402\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-photo-3_medium.jpg\" suffix=\"jpg\" dirpath=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US\" url=\"var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"158\" alias_key=\"405413724\" timestamp=\"1486123092\" is_valid=\"1\"/></ezimage>\n','ezimage',324,'eng-US',2,0,'',3),(0,248,75,0,NULL,'','ezsrrating',325,'eng-US',3,0,'',1),(0,248,75,0,NULL,'','ezsrrating',325,'eng-US',3,0,'',2),(0,248,75,0,NULL,'','ezsrrating',325,'eng-US',2,0,'',3),(0,249,75,0,NULL,'','ezkeyword',326,'eng-US',3,0,'',1),(0,249,75,0,NULL,'','ezkeyword',326,'eng-US',3,0,'',2),(0,249,75,0,NULL,'','ezkeyword',326,'eng-US',2,0,'',3),(0,310,76,0,NULL,'All products','ezstring',327,'eng-US',2,0,'all products',1),(0,311,76,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>asd</paragraph></section>\n','ezxmltext',328,'eng-US',2,0,'',1),(0,310,77,0,NULL,'Subcategory 1','ezstring',329,'eng-US',2,0,'subcategory 1',1),(0,311,77,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',330,'eng-US',2,0,'',1),(0,310,78,0,NULL,'Subcategory 1','ezstring',331,'eng-US',2,0,'subcategory 1',1),(0,310,78,0,NULL,'Subcategory 2','ezstring',331,'eng-US',2,0,'subcategory 2',2),(0,311,78,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',332,'eng-US',2,0,'',1),(0,311,78,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',332,'eng-US',2,0,'',2),(0,230,79,0,NULL,'English','ezstring',333,'eng-US',2,0,'english',1),(0,230,79,0,NULL,'English','ezstring',333,'eng-US',2,0,'english',2),(0,231,79,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_f9eed42791d4ac6ef597c789f601090c\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_976119f514162bf60db655398ccc01eb\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',334,'eng-US',2,0,'',1),(0,231,79,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_f9eed42791d4ac6ef597c789f601090c\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_976119f514162bf60db655398ccc01eb\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',334,'eng-US',2,0,'',2),(0,230,81,0,NULL,'サイトのトップページ','ezstring',337,'jpn-JP',4,0,'サイトのトップページ',1),(0,231,81,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_a72a31200b6df137c12aa862890866b4\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_00f4fbd39a1bdf038b1e3b98109c3fc3\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',338,'jpn-JP',4,0,'',1),(0,310,82,0,NULL,'すべての製品','ezstring',339,'jpn-JP',4,0,'すべての製品',1),(0,311,82,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',340,'jpn-JP',4,0,'',1),(0,310,83,0,NULL,'製品カテゴリ1','ezstring',341,'jpn-JP',4,0,'製品カテゴリ1',1),(0,311,83,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',342,'jpn-JP',4,0,'',1),(0,310,84,0,NULL,'製品カテゴリ1','ezstring',343,'jpn-JP',4,0,'製品カテゴリ1',1),(0,310,84,0,NULL,'製品カテゴリ2','ezstring',343,'jpn-JP',4,0,'製品カテゴリ2',2),(0,311,84,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',344,'jpn-JP',4,0,'',1),(0,311,84,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',344,'jpn-JP',4,0,'',2),(0,312,85,0,NULL,'日本の唯一の製品','ezstring',345,'jpn-JP',4,0,'日本の唯一の製品',1),(0,312,85,0,NULL,'日本の唯一の製品','ezstring',345,'jpn-JP',4,0,'日本の唯一の製品',2),(0,313,85,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>球ヱ試4教図ケ済袋ヌ当利づさぞ朗野ニセヨ草述わぎつく利固スレカロ帯5幌むぴ浪技ぐずぶさ少改よッ航報ろび軽棋ミチ傑著ム前告やに黒対くづち海参せみ目音ゅ。鼓イミ日13新そフ際撤トへもぼ一無ウシレラ発置や条北ニチヨ公印ゆきぶ述透ょゆづた闘能ヌエ数政キチテサ付援和府ウナト治九聞びぞもせ表定れイー策僕がゆ。</paragraph><paragraph>葉観サ属主通ロ鳥覧ンあぜて市報ょをス点一ぱぶみ居稿わ間務ン銭大ぱド条長ネ所誕暫械ラづきぴ。覧キカ牛紀ぴつほげ室護ワナ辞神ラ停労輸ウロヌリ爆3鮮着ぐ感3奈オレ困史り町16意ヘク事慶いくょほ。防でんざひ託税済すむざほ経切あずこ内説発ヤヨチ齢地ドなげえ型情ヱスヌメ方要よねルク的対2新カ衆82朴さぶドみ真株ハネア発弟ヨフ百代クトへ木世ず読記うぴ巻民皇あぱ。</paragraph><paragraph>判レせッ内教ヨ航将のゃん網透オ末元べ歳企ヌ際少題ゃろま守紛カミヘ堅死ナヨラ鎖多北関め第仲倫剖卵ぽレド。2済ロタサホ豊3向ドざぴ紀促利けたレ献厩授ツ見公イコセヘ洋稿トエユツ交特リがめる容時めなん理番ヤスカ空俳寺豊打ン。気なのゃ言断けルま数済ウネ経政そぞーフ季主ほル上院れッ票82教くわ報西こん祝歳比ーっドぎ本62座ハイリ触目ばむし験釧ホ紹券集リメイ教動乱皇さやぜこ。</paragraph></section>\n','ezxmltext',346,'jpn-JP',4,0,'',1),(0,313,85,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>球ヱ試4教図ケ済袋ヌ当利づさぞ朗野ニセヨ草述わぎつく利固スレカロ帯5幌むぴ浪技ぐずぶさ少改よッ航報ろび軽棋ミチ傑著ム前告やに黒対くづち海参せみ目音ゅ。鼓イミ日13新そフ際撤トへもぼ一無ウシレラ発置や条北ニチヨ公印ゆきぶ述透ょゆづた闘能ヌエ数政キチテサ付援和府ウナト治九聞びぞもせ表定れイー策僕がゆ。</paragraph><paragraph>葉観サ属主通ロ鳥覧ンあぜて市報ょをス点一ぱぶみ居稿わ間務ン銭大ぱド条長ネ所誕暫械ラづきぴ。覧キカ牛紀ぴつほげ室護ワナ辞神ラ停労輸ウロヌリ爆3鮮着ぐ感3奈オレ困史り町16意ヘク事慶いくょほ。防でんざひ託税済すむざほ経切あずこ内説発ヤヨチ齢地ドなげえ型情ヱスヌメ方要よねルク的対2新カ衆82朴さぶドみ真株ハネア発弟ヨフ百代クトへ木世ず読記うぴ巻民皇あぱ。</paragraph><paragraph>判レせッ内教ヨ航将のゃん網透オ末元べ歳企ヌ際少題ゃろま守紛カミヘ堅死ナヨラ鎖多北関め第仲倫剖卵ぽレド。2済ロタサホ豊3向ドざぴ紀促利けたレ献厩授ツ見公イコセヘ洋稿トエユツ交特リがめる容時めなん理番ヤスカ空俳寺豊打ン。気なのゃ言断けルま数済ウネ経政そぞーフ季主ほル上院れッ票82教くわ報西こん祝歳比ーっドぎ本62座ハイリ触目ばむし験釧ホ紹券集リメイ教動乱皇さやぜこ。</paragraph></section>\n','ezxmltext',346,'jpn-JP',4,0,'',2),(0,314,85,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486121389\"><original attribute_id=\"347\" attribute_version=\"1\" attribute_language=\"jpn-JP\"/></ezimage>\n','ezimage',347,'jpn-JP',4,0,'',1),(0,314,85,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486121389\"><original attribute_id=\"347\" attribute_version=\"1\" attribute_language=\"jpn-JP\"/></ezimage>\n','ezimage',347,'jpn-JP',4,0,'',2),(0,315,85,0,NULL,'JP-271','ezstring',348,'jpn-JP',4,0,'jp-271',1),(0,315,85,0,NULL,'JP-271','ezstring',348,'jpn-JP',4,0,'jp-271',2),(0,316,85,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',349,'jpn-JP',4,0,'',1),(0,316,85,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',349,'jpn-JP',4,0,'',2),(0,312,63,0,NULL,'商品1','ezstring',350,'jpn-JP',4,0,'商品1',4),(0,312,63,0,NULL,'商品1','ezstring',350,'jpn-JP',4,0,'商品1',5),(0,312,63,0,NULL,'商品1','ezstring',350,'jpn-JP',4,0,'商品1',6),(0,312,63,0,NULL,'商品1','ezstring',350,'jpn-JP',4,0,'商品1',7),(0,312,63,0,NULL,'商品1','ezstring',350,'jpn-JP',4,0,'商品1',8),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>9都ホ政行レ名棄ルちがと無申月ぎぶ舞承載まれむ品読ロヲツエ浜57起みラ国国読うび持岡び。採問ヤタテ日略フたおわ頭治オヒ文然な真知ロ保担ぽへ他善がとず際消ヘホキ修意京形マヒ大舵るレむぜ原書と疑刊ラめ付史がひ無明業理ぶまクる。済びぞ案意カ速属ぴ輪更オセムワ証輪相ニレフ作情むれ戦8集ラリ用高碁なへい君広ちレて新86静ヱキサミ市庁課さふだよ。</paragraph><paragraph>6青治クヘサフ市京本にょ局童ナコ代治ヨイノ胴封意ゆつゅだ投岡断ムメラミ千込責ラマ機状や作他略スユマ学松ス光律泊の。化ラなえつ始秋ゅト横院ぐ学発シヤ校人スヲ好2見俸ワ基13手よべぼけ庭方あずだ迫画着むび変冷勧封慮たてぱ。保ムサリヌ円81米配るね図索ぽイば帝社きフやレ検聞かへす事81主むぴけざ樹信タ情百ゆ滝洗ヌウヲト南歩けぐさ間備まみもき。</paragraph><paragraph>食っち乗稿メ校稿形けぴ防関ルたッ再孝キハサメ帯円9逆アソ志電だス堀維オヲソレ優定かまげな供92向光8慎ど。産操すごぞし改市ホ困供ねずどぞ治魔ノワ担対一供ゃレ峡論ロコツメ給宝こゅえ以治時ニヨス交18従つほぞ的埋めあげ。泊ヘハリム激覧ウレ料済ハナヤ市賞べ属87奮開ワシモナ貢真掲ぽ無相ウロヱハ事松をめ家権ゆクおだ然文ルヱメ板策ぱとぴ山連ヒハチケ和態奏訳うほの。</paragraph></section>\n','ezxmltext',351,'jpn-JP',4,0,'',4),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>9都ホ政行レ名棄ルちがと無申月ぎぶ舞承載まれむ品読ロヲツエ浜57起みラ国国読うび持岡び。採問ヤタテ日略フたおわ頭治オヒ文然な真知ロ保担ぽへ他善がとず際消ヘホキ修意京形マヒ大舵るレむぜ原書と疑刊ラめ付史がひ無明業理ぶまクる。済びぞ案意カ速属ぴ輪更オセムワ証輪相ニレフ作情むれ戦8集ラリ用高碁なへい君広ちレて新86静ヱキサミ市庁課さふだよ。</paragraph><paragraph>6青治クヘサフ市京本にょ局童ナコ代治ヨイノ胴封意ゆつゅだ投岡断ムメラミ千込責ラマ機状や作他略スユマ学松ス光律泊の。化ラなえつ始秋ゅト横院ぐ学発シヤ校人スヲ好2見俸ワ基13手よべぼけ庭方あずだ迫画着むび変冷勧封慮たてぱ。保ムサリヌ円81米配るね図索ぽイば帝社きフやレ検聞かへす事81主むぴけざ樹信タ情百ゆ滝洗ヌウヲト南歩けぐさ間備まみもき。</paragraph><paragraph>食っち乗稿メ校稿形けぴ防関ルたッ再孝キハサメ帯円9逆アソ志電だス堀維オヲソレ優定かまげな供92向光8慎ど。産操すごぞし改市ホ困供ねずどぞ治魔ノワ担対一供ゃレ峡論ロコツメ給宝こゅえ以治時ニヨス交18従つほぞ的埋めあげ。泊ヘハリム激覧ウレ料済ハナヤ市賞べ属87奮開ワシモナ貢真掲ぽ無相ウロヱハ事松をめ家権ゆクおだ然文ルヱメ板策ぱとぴ山連ヒハチケ和態奏訳うほの。</paragraph></section>\n','ezxmltext',351,'jpn-JP',4,0,'',5),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>9都ホ政行レ名棄ルちがと無申月ぎぶ舞承載まれむ品読ロヲツエ浜57起みラ国国読うび持岡び。採問ヤタテ日略フたおわ頭治オヒ文然な真知ロ保担ぽへ他善がとず際消ヘホキ修意京形マヒ大舵るレむぜ原書と疑刊ラめ付史がひ無明業理ぶまクる。済びぞ案意カ速属ぴ輪更オセムワ証輪相ニレフ作情むれ戦8集ラリ用高碁なへい君広ちレて新86静ヱキサミ市庁課さふだよ。</paragraph><paragraph>6青治クヘサフ市京本にょ局童ナコ代治ヨイノ胴封意ゆつゅだ投岡断ムメラミ千込責ラマ機状や作他略スユマ学松ス光律泊の。化ラなえつ始秋ゅト横院ぐ学発シヤ校人スヲ好2見俸ワ基13手よべぼけ庭方あずだ迫画着むび変冷勧封慮たてぱ。保ムサリヌ円81米配るね図索ぽイば帝社きフやレ検聞かへす事81主むぴけざ樹信タ情百ゆ滝洗ヌウヲト南歩けぐさ間備まみもき。</paragraph><paragraph>食っち乗稿メ校稿形けぴ防関ルたッ再孝キハサメ帯円9逆アソ志電だス堀維オヲソレ優定かまげな供92向光8慎ど。産操すごぞし改市ホ困供ねずどぞ治魔ノワ担対一供ゃレ峡論ロコツメ給宝こゅえ以治時ニヨス交18従つほぞ的埋めあげ。泊ヘハリム激覧ウレ料済ハナヤ市賞べ属87奮開ワシモナ貢真掲ぽ無相ウロヱハ事松をめ家権ゆクおだ然文ルヱメ板策ぱとぴ山連ヒハチケ和態奏訳うほの。</paragraph></section>\n','ezxmltext',351,'jpn-JP',4,0,'',6),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>9都ホ政行レ名棄ルちがと無申月ぎぶ舞承載まれむ品読ロヲツエ浜57起みラ国国読うび持岡び。採問ヤタテ日略フたおわ頭治オヒ文然な真知ロ保担ぽへ他善がとず際消ヘホキ修意京形マヒ大舵るレむぜ原書と疑刊ラめ付史がひ無明業理ぶまクる。済びぞ案意カ速属ぴ輪更オセムワ証輪相ニレフ作情むれ戦8集ラリ用高碁なへい君広ちレて新86静ヱキサミ市庁課さふだよ。</paragraph><paragraph>6青治クヘサフ市京本にょ局童ナコ代治ヨイノ胴封意ゆつゅだ投岡断ムメラミ千込責ラマ機状や作他略スユマ学松ス光律泊の。化ラなえつ始秋ゅト横院ぐ学発シヤ校人スヲ好2見俸ワ基13手よべぼけ庭方あずだ迫画着むび変冷勧封慮たてぱ。保ムサリヌ円81米配るね図索ぽイば帝社きフやレ検聞かへす事81主むぴけざ樹信タ情百ゆ滝洗ヌウヲト南歩けぐさ間備まみもき。</paragraph><paragraph>食っち乗稿メ校稿形けぴ防関ルたッ再孝キハサメ帯円9逆アソ志電だス堀維オヲソレ優定かまげな供92向光8慎ど。産操すごぞし改市ホ困供ねずどぞ治魔ノワ担対一供ゃレ峡論ロコツメ給宝こゅえ以治時ニヨス交18従つほぞ的埋めあげ。泊ヘハリム激覧ウレ料済ハナヤ市賞べ属87奮開ワシモナ貢真掲ぽ無相ウロヱハ事松をめ家権ゆクおだ然文ルヱメ板策ぱとぴ山連ヒハチケ和態奏訳うほの。</paragraph></section>\n','ezxmltext',351,'jpn-JP',4,0,'',7),(0,313,63,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>9都ホ政行レ名棄ルちがと無申月ぎぶ舞承載まれむ品読ロヲツエ浜57起みラ国国読うび持岡び。採問ヤタテ日略フたおわ頭治オヒ文然な真知ロ保担ぽへ他善がとず際消ヘホキ修意京形マヒ大舵るレむぜ原書と疑刊ラめ付史がひ無明業理ぶまクる。済びぞ案意カ速属ぴ輪更オセムワ証輪相ニレフ作情むれ戦8集ラリ用高碁なへい君広ちレて新86静ヱキサミ市庁課さふだよ。</paragraph><paragraph>6青治クヘサフ市京本にょ局童ナコ代治ヨイノ胴封意ゆつゅだ投岡断ムメラミ千込責ラマ機状や作他略スユマ学松ス光律泊の。化ラなえつ始秋ゅト横院ぐ学発シヤ校人スヲ好2見俸ワ基13手よべぼけ庭方あずだ迫画着むび変冷勧封慮たてぱ。保ムサリヌ円81米配るね図索ぽイば帝社きフやレ検聞かへす事81主むぴけざ樹信タ情百ゆ滝洗ヌウヲト南歩けぐさ間備まみもき。</paragraph><paragraph>食っち乗稿メ校稿形けぴ防関ルたッ再孝キハサメ帯円9逆アソ志電だス堀維オヲソレ優定かまげな供92向光8慎ど。産操すごぞし改市ホ困供ねずどぞ治魔ノワ担対一供ゃレ峡論ロコツメ給宝こゅえ以治時ニヨス交18従つほぞ的埋めあげ。泊ヘハリム激覧ウレ料済ハナヤ市賞べ属87奮開ワシモナ貢真掲ぽ無相ウロヱハ事松をめ家権ゆクおだ然文ルヱメ板策ぱとぴ山連ヒハチケ和態奏訳うほの。</paragraph></section>\n','ezxmltext',351,'jpn-JP',4,0,'',8),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',352,'jpn-JP',4,0,'',4),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',352,'jpn-JP',4,0,'',5),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',352,'jpn-JP',4,0,'',6),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',352,'jpn-JP',4,0,'',7),(0,314,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485715544\"><original attribute_id=\"254\" attribute_version=\"2\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',352,'jpn-JP',4,0,'',8),(353,315,63,0,NULL,'PR-192','ezstring',353,'jpn-JP',4,0,'pr-192',4),(255,315,63,0,NULL,'PR-192','ezstring',353,'jpn-JP',4,0,'pr-192',5),(353,315,63,0,NULL,'PR-192','ezstring',353,'jpn-JP',4,0,'pr-192',6),(255,315,63,0,NULL,'PR-192','ezstring',353,'jpn-JP',4,0,'pr-192',7),(255,315,63,0,NULL,'PR-192','ezstring',353,'jpn-JP',4,0,'pr-192',8),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"72\" contentobject-version=\"1\" node-id=\"73\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"84c94309379063be68d8b78f3f862244\"/><relation-item priority=\"2\" contentobject-id=\"74\" contentobject-version=\"2\" node-id=\"74\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"0f2a6e542f1c2adc10cb1beacd14724b\"/><relation-item priority=\"3\" contentobject-id=\"75\" contentobject-version=\"2\" node-id=\"75\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"fffc91f9b79f9457a1d3da5c8b67cd75\"/></relation-list></related-objects>\n','ezobjectrelationlist',354,'jpn-JP',4,0,'',4),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"72\" contentobject-version=\"1\" node-id=\"73\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"84c94309379063be68d8b78f3f862244\"/><relation-item priority=\"2\" contentobject-id=\"74\" contentobject-version=\"2\" node-id=\"74\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"0f2a6e542f1c2adc10cb1beacd14724b\"/><relation-item priority=\"3\" contentobject-id=\"75\" contentobject-version=\"2\" node-id=\"75\" parent-node-id=\"71\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"fffc91f9b79f9457a1d3da5c8b67cd75\"/></relation-list></related-objects>\n','ezobjectrelationlist',354,'jpn-JP',4,0,'',5),(0,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',354,'jpn-JP',4,0,'',6),(278,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"87\" contentobject-version=\"1\" node-id=\"92\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"60c20bc282458d46568b64c0add4a4a2\"/><relation-item priority=\"2\" contentobject-id=\"88\" contentobject-version=\"2\" node-id=\"93\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"68bd8d129bf3611e2966ae1bb6d82a86\"/><relation-item priority=\"3\" contentobject-id=\"89\" contentobject-version=\"2\" node-id=\"94\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"af60cbb26f6b9e89312029993bb9af64\"/></relation-list></related-objects>\n','ezobjectrelationlist',354,'jpn-JP',4,0,'',7),(278,316,63,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list><relation-item priority=\"1\" contentobject-id=\"87\" contentobject-version=\"1\" node-id=\"92\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"60c20bc282458d46568b64c0add4a4a2\"/><relation-item priority=\"2\" contentobject-id=\"88\" contentobject-version=\"2\" node-id=\"93\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"68bd8d129bf3611e2966ae1bb6d82a86\"/><relation-item priority=\"3\" contentobject-id=\"89\" contentobject-version=\"2\" node-id=\"94\" parent-node-id=\"91\" contentclass-id=\"27\" contentclass-identifier=\"image\" contentobject-remote-id=\"af60cbb26f6b9e89312029993bb9af64\"/></relation-list></related-objects>\n','ezobjectrelationlist',354,'jpn-JP',4,0,'',8),(0,312,67,0,NULL,'製品4','ezstring',355,'jpn-JP',4,0,'製品4',4),(0,312,67,0,NULL,'製品4','ezstring',355,'jpn-JP',4,0,'製品4',5),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>館にをめッ都雪当えんこリ勝請て載構シヨアリ作育野必朝テ氏状れぴざ児43岩亨ほたラフ。雑げ社聞際ステ誌早りあろ断毎るでご浜業ソテ運検くト内通シ主大ニオセユ年東大コヱホニ画機たよて通受ノヨヌシ方禁セカ紙購別碁候法ぼつ。暁ヲユキ真円ッべら氏著メオテ謀始むけ分映ぴやせな公道ぎーざ部応入ざっら国図イへみ鳥表すざぐよ女断連テキ演女をやべ今場ラクロ開記べをリ禁4進スモソワ社九ミセ敬9好術クぱず。</paragraph><paragraph>弾ッでき黒9取ぞれん投頂ぱお谷舞ノ残傘ヌタ傾40備カフ看玲メケヱ能権ざ無事取項ーょ転科幅採洋びき。京も購祀チ率東俳テワシ受銀ヌ通多著情るねとへ幌図ハ申台ひラフ株68輸さわ冬51両どをて八作でたトり客無へば見着みほト経倉セチユヒ競暮亜才滅れ。合ミチメ的熊スあ命検阜正任セシヨヒ同速芸ね必適とー日供ン力94関サミワ題前ラれ国来ホアヌ聴料コケウヌ際竹マシ八除ろでをえ。</paragraph><paragraph>針ろせぼ調係テ周止イ党最エルナ米報アルヲ知測れば甲祭ぐごむ手市た来題ぱ最細セク毎約じーら題第設エ睦木ソウ断勝う稿告介カア安各ワ受要げひドは岡率憲切子るら。5熊や応6日ゆイはン康読行のッど転従ソキユマ講12滞延アセメ縁訟題ナヒ汚化クぎぜ録9景クキモツ見鹿おる池垣ぜなてが決間ヘ為歯没牧ンじべけ。</paragraph></section>\n','ezxmltext',356,'jpn-JP',4,0,'',4),(0,313,67,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>館にをめッ都雪当えんこリ勝請て載構シヨアリ作育野必朝テ氏状れぴざ児43岩亨ほたラフ。雑げ社聞際ステ誌早りあろ断毎るでご浜業ソテ運検くト内通シ主大ニオセユ年東大コヱホニ画機たよて通受ノヨヌシ方禁セカ紙購別碁候法ぼつ。暁ヲユキ真円ッべら氏著メオテ謀始むけ分映ぴやせな公道ぎーざ部応入ざっら国図イへみ鳥表すざぐよ女断連テキ演女をやべ今場ラクロ開記べをリ禁4進スモソワ社九ミセ敬9好術クぱず。</paragraph><paragraph>弾ッでき黒9取ぞれん投頂ぱお谷舞ノ残傘ヌタ傾40備カフ看玲メケヱ能権ざ無事取項ーょ転科幅採洋びき。京も購祀チ率東俳テワシ受銀ヌ通多著情るねとへ幌図ハ申台ひラフ株68輸さわ冬51両どをて八作でたトり客無へば見着みほト経倉セチユヒ競暮亜才滅れ。合ミチメ的熊スあ命検阜正任セシヨヒ同速芸ね必適とー日供ン力94関サミワ題前ラれ国来ホアヌ聴料コケウヌ際竹マシ八除ろでをえ。</paragraph><paragraph>針ろせぼ調係テ周止イ党最エルナ米報アルヲ知測れば甲祭ぐごむ手市た来題ぱ最細セク毎約じーら題第設エ睦木ソウ断勝う稿告介カア安各ワ受要げひドは岡率憲切子るら。5熊や応6日ゆイはン康読行のッど転従ソキユマ講12滞延アセメ縁訟題ナヒ汚化クぎぜ録9景クキモツ見鹿おる池垣ぜなてが決間ヘ為歯没牧ンじべけ。</paragraph></section>\n','ezxmltext',356,'jpn-JP',4,0,'',5),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',357,'jpn-JP',4,0,'',4),(0,314,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153501\"><original attribute_id=\"258\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',357,'jpn-JP',4,0,'',5),(358,315,67,0,NULL,'PR-228','ezstring',358,'jpn-JP',4,0,'pr-228',4),(269,315,67,0,NULL,'PR-228','ezstring',358,'jpn-JP',4,0,'pr-228',5),(0,316,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',359,'jpn-JP',4,0,'',4),(282,316,67,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',359,'jpn-JP',4,0,'',5),(0,312,68,0,NULL,'プロダクトハロー9','ezstring',360,'jpn-JP',4,0,'プロダクトハロー9',4),(0,313,68,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>富イーえ物透ホ割小折ん小中エメ読39約かぞ見準読レぱさぎ付旦エマ件福偽サカ活買指亡好ゅ。画す議目ソサヲヱ新全ヘケ泥埼んよンろ棋支お務5時ー所硬ナツユイ止当ドごクて表関前5稿んや査県レけむ線界よつ球見くへ資色ひ。記ウレ済襲両テ表老びぼ子9離ゆイぎ古文ぼでがリ性裁ルナヘト投湯ヘキエミ眠参ヌク能院備ツノ親告ゆ手子85書カ初報ムルヲ鉢尚い。</paragraph><paragraph>吉ムメルネ賞滞どいも制高エソ弘秘ホスヨノ否数とあも兵申イノヱ商日うぱ知臓テヤヨソ覚富ケト山投ら疎点広めおい約十ホテチク知25更べず東7談リ員族イドえゆ筋保わ絶不挙尾滋ご。家レ団恵メ応指4信ぜ念6少カミ場相や対相人ろのルお界罪みまばど権描フ治羅誘逮ぶ。録メク用女社ア告聞ルマオ氏使版まごンし泉選さリ獣愛伊ぼそっあ際棋員んぽ録転リ取良ミ搭法誠苦省ゅ。</paragraph><paragraph>53量術ひち挙作ロ前作ろ担合クすめ班他イモ棒略へぐ牡総スサヲ済平界ふ高昨ひ画2月な住村ぎ信乾凍刷往フみあめ。速チナケ愛残事済ニウヌメ小転ヨヌ剤神ら輔南づんゅと氏克つと抗計企セヲムロ思公重大的俳寺尊きぞゃ。紙ヨ軍営クな観用まぴめゆ申住みぴ暮給ヨヲレテ祝画ナヲチ再学ロウシツ造定4一すづきさ終母ド近比ト活旅はじ市壮フそけ果保ソヱウイ談新キ保2戸リ皆13務英傍やげつイ。</paragraph></section>\n','ezxmltext',361,'jpn-JP',4,0,'',4),(0,314,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153747\"><original attribute_id=\"272\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',362,'jpn-JP',4,0,'',4),(363,315,68,0,NULL,'XX-837','ezstring',363,'jpn-JP',4,0,'xx-837',4),(0,316,68,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',364,'jpn-JP',4,0,'',4),(0,312,66,0,NULL,'商品3','ezstring',365,'jpn-JP',4,0,'商品3',4),(0,313,66,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>期誕的ニウ界特慎ミスコツ暮1広そひ丁応裏ケネ左回イ限間す約主にへ能67遺ばじひ稿原が愛27楽漏頂て。境テレ健無づ葉談はゃづ先八セク調売でゅレ四載書ヘスエル姫株ぱろめづ岸装クふ中途ス縦督略っべーう吾属よそリど初更とせあび受佐又でみわ。極え遺展児ンせけ地6好ムケルシ秋都ぐっ研検ツス韓圧カヌ断前リ府因6司ハコチ運査セナツメ域優サヱラソ月斐ケ商下トサヤ終育娘汁班ょご。</paragraph><paragraph>20妹廷訓誘1皇ホフ住會覧雄ぎ費真んれ分演イ軽経ト無碁レぱぽ表警ほ強進ヨホヤ勝後午鮮果あ。95賞キマサウ地源ぼもれ字前ヤリソ作一町えはばっ有障レ水打ぽ松場だるゆ物毎ルヱ賞半くね覧1決ミヌ年今とざづ木分ゃ浩択どへかひ応住本族被なきぴ。出ケ来断りもばく質1出コツレア高黒レぽ展総メユソセ合鮮努ハチ岡載そんレつ最掲ッンク歩人オヌ読使ヤメ当酒文来機習テ長泉ナ星果寿崩なぜりと。</paragraph><paragraph>程ナトシキ時路だほずろ億告物ねむフや争故ぼ究西リワ紙新添トをど閣害ご輔告ヤ実展チ身春みどめ暮具辺の。右ぎせ庭雲ロアオ近成つず行64夜テナ供和ちよつ際提ノヱカエ決入ヱ慮4困つよ電響たンリ動惑室乳ゃづぐろ応7開びずーレ輪工ウヘ名川舎賛こけド。催むこ変済高設なゆらの訪縮をめが老脳掲スじ部最文アハメ歩2対ニセム止車クヒワ健多でっトフ環73正幸績仕んぶフ。</paragraph></section>\n','ezxmltext',366,'jpn-JP',4,0,'',4),(0,314,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"CJ-831-Product-Three.jpg\" suffix=\"jpg\" basename=\"CJ-831-Product-Three\" dirpath=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US\" url=\"var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US/CJ-831-Product-Three.jpg\" original_filename=\"vammas-major-kaivuri.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"360\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1485153471\"><original attribute_id=\"254\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',367,'jpn-JP',4,0,'',4),(368,315,66,0,NULL,'CJ-831','ezstring',368,'jpn-JP',4,0,'cj-831',4),(0,316,66,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<related-objects><relation-list/></related-objects>\n','ezobjectrelationlist',369,'jpn-JP',4,0,'',4),(0,319,71,0,NULL,'私たちに関しては','ezstring',370,'jpn-JP',4,0,'私たちに関しては',2),(0,320,71,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>奉32供フカ校作めぱらみ取若競ス殺右れ長36今テロサク本著ンりあ読能オホモ良嫌業理音設ず。存転ゃスぽう主勝件ドゅび付甲ぼ売士東ラせ式算ヒエリキ首天ヱヌ達約際マノオメ震千セケ能密ねだラ東46愛ねら掲成恋ち。全フ見作チヘ型軽じまづ一優べょも介方マラチル候行みら九断ノツ家阪ウヤハツ挙岡は辺最掲じ気盛ヌ外宅ワオ注真ト一芝づふじ無24税あ向原を行祖直護めす。</paragraph><paragraph>大ゅづた病客転ヌフ可道ぜびれ味者景応ノカヱモ相悦エレス度面フ停司にみょし断勢きルにイ左密ぱざそぜ秒間西よ津回ッドぽ部票みクいそ突覚布ゆ。視ゆひぱ勝貴マ運提テナ科93香ケスマセ質府ゃむク替外タフウ兵改ヤ投旅合シ表不ノル支聞ヒテ亡51重演6次購詳列岸レけ。離ぱ我盗ムトテ査感テソキ週高ハネヘ進北づ作試ぜべた済利ばこ広単けざしえ主著ざぞゃ臨2政だク主白ヲケ費96克即橋ちリ。</paragraph><paragraph>新死ヌル業編で都追へべう指村かぎすげ足発ちスげ補未セ第故達もびき選統ミ権沿名ス熱左ニ面供ト門力経アタ訴北あめ載53鉄トム人総イヲト三輸回らべ。妓てうド島蓄ヱケラ合抑のあひ店韓ケミヤラ最年ニエシ担左みじ教誠さ的2地元なきげ浴1覧っちリさ講止コタ評正ヱ医1組そづぽ連共セ載天始謙ず。</paragraph></section>\n','ezxmltext',371,'jpn-JP',4,0,'',2),(0,4,86,0,NULL,'Product photos','ezstring',372,'eng-US',3,0,'product photos',1),(0,155,86,0,NULL,'','ezstring',373,'eng-US',3,0,'',1),(0,119,86,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',374,'eng-US',3,0,'',1),(0,156,86,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',375,'eng-US',3,0,'',1),(376,158,86,0,1,'','ezboolean',376,'eng-US',3,1,'',1),(0,308,86,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>CallForActionLayout</zone_layout>\n  <zone id=\"id_a550de3f809ad44d47ed23d3e7ac33de\">\n    <zone_identifier>main</zone_identifier>\n  </zone>\n</page>\n','ezpage',377,'eng-US',3,0,'',1),(0,309,86,0,NULL,'','ezkeyword',378,'eng-US',3,0,'',1),(0,245,87,0,NULL,'Product Photo 1','ezstring',379,'eng-US',3,0,'product photo 1',1),(0,246,87,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',380,'eng-US',3,0,'',1),(0,247,87,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-1.jpg\" suffix=\"jpg\" basename=\"Product-Photo-1\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"864\" height=\"627\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123347\"><original attribute_id=\"381\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"627\" Width=\"864\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"435\" alias_key=\"2605465115\" timestamp=\"1486123351\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"73\" alias_key=\"2343348577\" timestamp=\"1486123351\" is_valid=\"1\"/></ezimage>\n','ezimage',381,'eng-US',3,0,'',1),(0,248,87,0,NULL,'','ezsrrating',382,'eng-US',3,0,'',1),(0,249,87,0,NULL,'','ezkeyword',383,'eng-US',3,0,'',1),(0,245,88,0,NULL,'Product Photo 1','ezstring',384,'eng-US',3,0,'product photo 1',1),(0,245,88,0,NULL,'Product Photo 2','ezstring',384,'eng-US',3,0,'product photo 2',2),(0,245,88,0,NULL,'Product Photo 2','ezstring',384,'eng-US',3,0,'product photo 2',3),(0,246,88,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',385,'eng-US',3,0,'',1),(0,246,88,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',385,'eng-US',3,0,'',2),(0,246,88,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',385,'eng-US',3,0,'',3),(0,247,88,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-1.jpg\" suffix=\"jpg\" basename=\"Product-Photo-1\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"864\" height=\"627\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123347\"><original attribute_id=\"381\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"627\" Width=\"864\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"435\" alias_key=\"2605465115\" timestamp=\"1486123351\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"73\" alias_key=\"2343348577\" timestamp=\"1486123351\" is_valid=\"1\"/></ezimage>\n','ezimage',386,'eng-US',3,0,'',1),(0,247,88,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-2.jpg\" suffix=\"jpg\" basename=\"Product-Photo-2\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123422\"><original attribute_id=\"386\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-2_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-2_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"48\" alias_key=\"2343348577\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-Photo-2_large.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"144\" alias_key=\"1592566908\" timestamp=\"1486123613\" is_valid=\"1\"/></ezimage>\n','ezimage',386,'eng-US',3,0,'',2),(0,247,88,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-2.jpg\" suffix=\"jpg\" basename=\"Product-Photo-2\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123422\"><original attribute_id=\"386\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-2_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-2_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"48\" alias_key=\"2343348577\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"large\" filename=\"Product-Photo-2_large.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_large.jpg\" mime_type=\"image/jpeg\" width=\"300\" height=\"144\" alias_key=\"1592566908\" timestamp=\"1486123613\" is_valid=\"1\"/></ezimage>\n','ezimage',386,'eng-US',3,0,'',3),(0,248,88,0,NULL,'','ezsrrating',387,'eng-US',3,0,'',1),(0,248,88,0,NULL,'','ezsrrating',387,'eng-US',3,0,'',2),(0,248,88,0,NULL,'','ezsrrating',387,'eng-US',3,0,'',3),(0,249,88,0,NULL,'','ezkeyword',388,'eng-US',3,0,'',1),(0,249,88,0,NULL,'','ezkeyword',388,'eng-US',3,0,'',2),(0,249,88,0,NULL,'','ezkeyword',388,'eng-US',3,0,'',3),(0,245,89,0,NULL,'Product Photo 1','ezstring',389,'eng-US',3,0,'product photo 1',1),(0,245,89,0,NULL,'Product Photo 1','ezstring',389,'eng-US',3,0,'product photo 1',2),(0,245,89,0,NULL,'Product Photo 3','ezstring',389,'eng-US',3,0,'product photo 3',3),(0,246,89,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',390,'eng-US',3,0,'',1),(0,246,89,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',390,'eng-US',3,0,'',2),(0,246,89,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',390,'eng-US',3,0,'',3),(0,247,89,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-1.jpg\" suffix=\"jpg\" basename=\"Product-Photo-1\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1.jpg\" original_filename=\"yamaha-photo-1.jpg\" mime_type=\"image/jpeg\" width=\"864\" height=\"627\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123347\"><original attribute_id=\"381\" attribute_version=\"1\" attribute_language=\"eng-US\"/><information Height=\"627\" Width=\"864\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"435\" alias_key=\"2605465115\" timestamp=\"1486123351\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"73\" alias_key=\"2343348577\" timestamp=\"1486123351\" is_valid=\"1\"/></ezimage>\n','ezimage',391,'eng-US',3,0,'',1),(0,247,89,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-1.jpg\" suffix=\"jpg\" basename=\"Product-Photo-1\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1.jpg\" original_filename=\"yamaha-photo-3.jpg\" mime_type=\"image/jpeg\" width=\"1264\" height=\"1000\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123433\"><original attribute_id=\"391\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"1000\" Width=\"1264\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-1_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"475\" alias_key=\"2605465115\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-1_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"79\" alias_key=\"2343348577\" timestamp=\"1486123437\" is_valid=\"1\"/></ezimage>\n','ezimage',391,'eng-US',3,0,'',2),(0,247,89,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-3.jpg\" suffix=\"jpg\" basename=\"Product-Photo-3\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3.jpg\" original_filename=\"yamaha-photo-3.jpg\" mime_type=\"image/jpeg\" width=\"1264\" height=\"1000\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123433\"><original attribute_id=\"391\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"1000\" Width=\"1264\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-3_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"475\" alias_key=\"2605465115\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-3_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"79\" alias_key=\"2343348577\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-Photo-3_medium.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"158\" alias_key=\"405413724\" timestamp=\"1486123594\" is_valid=\"1\"/></ezimage>\n','ezimage',391,'eng-US',3,0,'',3),(0,248,89,0,NULL,'','ezsrrating',392,'eng-US',3,0,'',1),(0,248,89,0,NULL,'','ezsrrating',392,'eng-US',3,0,'',2),(0,248,89,0,NULL,'','ezsrrating',392,'eng-US',3,0,'',3),(0,249,89,0,NULL,'','ezkeyword',393,'eng-US',3,0,'',1),(0,249,89,0,NULL,'','ezkeyword',393,'eng-US',3,0,'',2),(0,249,89,0,NULL,'','ezkeyword',393,'eng-US',3,0,'',3),(0,245,88,0,NULL,'商品写真2','ezstring',394,'jpn-JP',4,0,'商品写真2',3),(0,246,88,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"/>\n','ezxmltext',395,'jpn-JP',4,0,'',3),(0,247,88,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"1\" filename=\"Product-Photo-2.jpg\" suffix=\"jpg\" basename=\"Product-Photo-2\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2.jpg\" original_filename=\"yamaha-photo-2.jpg\" mime_type=\"image/jpeg\" width=\"1239\" height=\"593\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486123422\"><original attribute_id=\"386\" attribute_version=\"2\" attribute_language=\"eng-US\"/><information Height=\"593\" Width=\"1239\" IsColor=\"1\" ByteOrderMotorola=\"0\"/><alias name=\"reference\" filename=\"Product-Photo-2_reference.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_reference.jpg\" mime_type=\"image/jpeg\" width=\"600\" height=\"287\" alias_key=\"2605465115\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"small\" filename=\"Product-Photo-2_small.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_small.jpg\" mime_type=\"image/jpeg\" width=\"100\" height=\"48\" alias_key=\"2343348577\" timestamp=\"1486123437\" is_valid=\"1\"/><alias name=\"medium\" filename=\"Product-Photo-2_medium.jpg\" suffix=\"jpg\" dirpath=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US\" url=\"var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_medium.jpg\" mime_type=\"image/jpeg\" width=\"200\" height=\"96\" alias_key=\"405413724\" timestamp=\"1486123613\" is_valid=\"1\"/></ezimage>\n','ezimage',396,'jpn-JP',4,0,'',3),(0,248,88,0,NULL,'','ezsrrating',397,'jpn-JP',4,0,'',3),(0,249,88,0,NULL,'','ezkeyword',398,'jpn-JP',4,0,'',3),(0,230,79,0,NULL,'Hello New Zealand','ezstring',399,'eng-NZ',8,0,'hello new zealand',2),(0,231,79,0,NULL,'<?xml version=\"1.0\"?>\n<page>\n  <zone_layout>2ZonesLayout1</zone_layout>\n  <zone id=\"id_b72353c16f203a4c74ca8711321567f6\">\n    <zone_identifier>left</zone_identifier>\n  </zone>\n  <zone id=\"id_d0fd8a1bb989d514e03dad1dff741fce\">\n    <zone_identifier>right</zone_identifier>\n  </zone>\n</page>\n','ezpage',400,'eng-NZ',8,0,'',2),(0,310,90,0,NULL,'Product category 3','ezstring',401,'eng-US',2,0,'product category 3',1),(0,311,90,0,1045487555,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<section xmlns:image=\"http://ez.no/namespaces/ezpublish3/image/\" xmlns:xhtml=\"http://ez.no/namespaces/ezpublish3/xhtml/\" xmlns:custom=\"http://ez.no/namespaces/ezpublish3/custom/\"><paragraph>Product category 3. Product category 3.</paragraph></section>\n','ezxmltext',402,'eng-US',2,0,'',1),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',1),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',2),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',3),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',4),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',5),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',6),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',412,'eng-US',2,0,'',7),(0,324,63,0,NULL,'','ezbinaryfile',412,'eng-US',2,0,'',8),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',413,'jpn-JP',4,0,'',4),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',413,'jpn-JP',4,0,'',5),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',413,'jpn-JP',4,0,'',6),(0,324,63,NULL,NULL,NULL,'ezbinaryfile',413,'jpn-JP',4,0,'',7),(0,324,63,0,NULL,'','ezbinaryfile',413,'jpn-JP',4,0,'',8),(0,324,64,NULL,NULL,NULL,'ezbinaryfile',414,'eng-US',2,0,'',1),(0,324,64,NULL,NULL,NULL,'ezbinaryfile',414,'eng-US',2,0,'',2),(0,324,66,NULL,NULL,NULL,'ezbinaryfile',419,'eng-US',2,0,'',1),(0,324,66,NULL,NULL,NULL,'ezbinaryfile',419,'eng-US',2,0,'',2),(0,324,66,NULL,NULL,NULL,'ezbinaryfile',419,'eng-US',2,0,'',3),(0,324,66,NULL,NULL,NULL,'ezbinaryfile',419,'eng-US',2,0,'',4),(0,324,66,NULL,NULL,NULL,'ezbinaryfile',420,'jpn-JP',4,0,'',4),(0,324,67,NULL,NULL,NULL,'ezbinaryfile',422,'eng-US',2,0,'',1),(0,324,67,NULL,NULL,NULL,'ezbinaryfile',422,'eng-US',2,0,'',2),(0,324,67,NULL,NULL,NULL,'ezbinaryfile',422,'eng-US',2,0,'',3),(0,324,67,NULL,NULL,NULL,'ezbinaryfile',422,'eng-US',2,0,'',4),(0,324,67,0,NULL,'','ezbinaryfile',422,'eng-US',2,0,'',5),(0,324,67,NULL,NULL,NULL,'ezbinaryfile',425,'jpn-JP',4,0,'',4),(0,324,67,0,NULL,'','ezbinaryfile',425,'jpn-JP',4,0,'',5),(0,324,68,NULL,NULL,NULL,'ezbinaryfile',429,'eng-US',2,0,'',1),(0,324,68,NULL,NULL,NULL,'ezbinaryfile',429,'eng-US',2,0,'',2),(0,324,68,NULL,NULL,NULL,'ezbinaryfile',429,'eng-US',2,0,'',3),(0,324,68,NULL,NULL,NULL,'ezbinaryfile',429,'eng-US',2,0,'',4),(0,324,68,NULL,NULL,NULL,'ezbinaryfile',430,'jpn-JP',4,0,'',4),(0,324,85,NULL,NULL,NULL,'ezbinaryfile',432,'jpn-JP',4,0,'',1),(0,324,85,NULL,NULL,NULL,'ezbinaryfile',432,'jpn-JP',4,0,'',2),(0,6,121,NULL,NULL,'Country editors','ezstring',500,'eng-US',3,0,'country editors',1),(0,7,121,NULL,NULL,NULL,'ezstring',501,'eng-US',3,0,'',1),(0,6,122,NULL,NULL,'Japanese','ezstring',502,'eng-US',3,0,'japanese',1),(0,7,122,NULL,NULL,NULL,'ezstring',503,'eng-US',3,0,'',1),(0,6,123,NULL,NULL,'Chinese','ezstring',504,'eng-US',3,0,'chinese',1),(0,7,123,NULL,NULL,NULL,'ezstring',505,'eng-US',3,0,'',1),(0,6,124,NULL,NULL,'English','ezstring',506,'eng-US',3,0,'english',1),(0,7,124,NULL,NULL,NULL,'ezstring',507,'eng-US',3,0,'',1),(0,6,127,NULL,NULL,'Country editors','ezstring',518,'eng-US',3,0,'country editors',1),(0,7,127,NULL,NULL,NULL,'ezstring',519,'eng-US',3,0,'',1),(0,6,128,NULL,NULL,'Japanese','ezstring',520,'eng-US',3,0,'japanese',1),(0,7,128,NULL,NULL,NULL,'ezstring',521,'eng-US',3,0,'',1),(0,6,129,NULL,NULL,'Chinese','ezstring',522,'eng-US',3,0,'chinese',1),(0,7,129,NULL,NULL,NULL,'ezstring',523,'eng-US',3,0,'',1),(0,6,130,NULL,NULL,'English','ezstring',524,'eng-US',3,0,'english',1),(0,7,130,NULL,NULL,NULL,'ezstring',525,'eng-US',3,0,'',1),(0,8,131,NULL,NULL,'James','ezstring',526,'eng-US',2,0,'james',1),(0,9,131,NULL,NULL,'Japanese','ezstring',527,'eng-US',2,0,'japanese',1),(0,12,131,NULL,NULL,NULL,'ezuser',528,'eng-US',2,0,'',1),(0,179,131,NULL,NULL,NULL,'eztext',529,'eng-US',2,0,'',1),(0,180,131,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486995800\"/>\n','ezimage',530,'eng-US',2,0,'',1),(0,8,132,NULL,NULL,'Eaton','ezstring',531,'eng-US',2,0,'eaton',1),(0,9,132,NULL,NULL,'English','ezstring',532,'eng-US',2,0,'english',1),(0,12,132,NULL,NULL,NULL,'ezuser',533,'eng-US',2,0,'',1),(0,179,132,NULL,NULL,NULL,'eztext',534,'eng-US',2,0,'',1),(0,180,132,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486995261\"/>\n','ezimage',535,'eng-US',2,0,'',1),(0,6,133,0,NULL,'Approving users','ezstring',536,'eng-US',3,0,'approving users',1),(0,7,133,0,NULL,'','ezstring',537,'eng-US',3,0,'',1),(0,8,135,0,NULL,'Second','ezstring',543,'eng-US',3,0,'second',1),(0,9,135,0,NULL,'Admin','ezstring',544,'eng-US',3,0,'admin',1),(545,12,135,0,NULL,'{\"login\":\"second\",\"password_hash\":\"a9527890b54993c3d94be20dbe99b660\",\"email\":\"second@example.com\",\"password_hash_type\":2}','ezuser',545,'eng-US',3,0,'',1),(0,179,135,0,NULL,'','eztext',546,'eng-US',3,0,'',1),(0,180,135,0,NULL,'<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<ezimage serial_number=\"1\" is_valid=\"\" filename=\"\" suffix=\"\" basename=\"\" dirpath=\"\" url=\"\" original_filename=\"\" mime_type=\"\" width=\"\" height=\"\" alternative_text=\"\" alias_key=\"1293033771\" timestamp=\"1486997198\"><original attribute_id=\"547\" attribute_version=\"1\" attribute_language=\"eng-US\"/></ezimage>\n','ezimage',547,'eng-US',3,0,'',1);
/*!40000 ALTER TABLE `ezcontentobject_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_link`
--

DROP TABLE IF EXISTS `ezcontentobject_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_link` (
  `contentclassattribute_id` int(11) NOT NULL DEFAULT '0',
  `from_contentobject_id` int(11) NOT NULL DEFAULT '0',
  `from_contentobject_version` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relation_type` int(11) NOT NULL DEFAULT '1',
  `to_contentobject_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezco_link_from` (`from_contentobject_id`,`from_contentobject_version`,`contentclassattribute_id`),
  KEY `ezco_link_to_co_id` (`to_contentobject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_link`
--

LOCK TABLES `ezcontentobject_link` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_link` DISABLE KEYS */;
INSERT INTO `ezcontentobject_link` VALUES (316,63,2,10,8,72),(316,63,2,11,8,74),(316,63,2,12,8,75),(316,63,3,25,8,72),(316,63,3,26,8,74),(316,63,3,27,8,75),(316,63,4,43,8,72),(316,63,4,44,8,74),(316,63,4,45,8,75),(316,63,5,52,8,72),(316,63,5,53,8,74),(316,63,5,54,8,75),(316,63,7,70,8,87),(316,63,7,71,8,88),(316,63,7,72,8,89),(316,63,8,88,8,87),(316,63,8,89,8,88),(316,63,8,90,8,89);
/*!40000 ALTER TABLE `ezcontentobject_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_name`
--

DROP TABLE IF EXISTS `ezcontentobject_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_name` (
  `content_translation` varchar(20) NOT NULL DEFAULT '',
  `content_version` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `language_id` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `real_translation` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`contentobject_id`,`content_version`,`content_translation`),
  KEY `ezcontentobject_name_cov_id` (`content_version`),
  KEY `ezcontentobject_name_lang_id` (`language_id`),
  KEY `ezcontentobject_name_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_name`
--

LOCK TABLES `ezcontentobject_name` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_name` DISABLE KEYS */;
INSERT INTO `ezcontentobject_name` VALUES ('eng-US',1,4,3,'Users','eng-US'),('eng-US',2,10,3,'Anonymous User','eng-US'),('eng-US',1,12,3,'Administrator users','eng-US'),('eng-US',3,14,3,'Administrator User','eng-US'),('eng-US',4,14,3,'Administrator User','eng-US'),('eng-US',1,41,3,'Media','eng-US'),('eng-US',1,42,3,'Anonymous Users','eng-US'),('eng-US',1,45,3,'Setup','eng-US'),('eng-US',1,49,3,'Images','eng-US'),('eng-US',1,50,3,'Files','eng-US'),('eng-US',1,51,3,'Multimedia','eng-US'),('eng-US',1,52,2,'Common INI settings','eng-US'),('eng-US',2,54,2,'eZ Publish Demo Site (without demo content)','eng-US'),('eng-US',1,56,3,'Design','eng-US'),('eng-US',1,57,3,'Home','eng-US'),('eng-US',2,57,3,'Home','eng-US'),('eng-US',1,62,2,'Product Category 1','eng-US'),('eng-US',2,62,2,'Product Category 1','eng-US'),('jpn-JP',2,62,4,'製品カテゴリ1','jpn-JP'),('eng-US',1,63,2,'Product One','eng-US'),('eng-US',2,63,2,'Product One','eng-US'),('eng-US',3,63,2,'PR-192: Product One','eng-US'),('eng-US',4,63,2,'PR-192: Product One','eng-US'),('jpn-JP',4,63,4,'PR-192: 商品1','jpn-JP'),('eng-US',5,63,2,'PR-192: Product One','eng-US'),('jpn-JP',5,63,4,'PR-192: 商品1','jpn-JP'),('eng-US',6,63,2,'PR-192: Product One','eng-US'),('jpn-JP',6,63,4,'PR-192: 商品1','jpn-JP'),('eng-US',7,63,2,'PR-192: Product One','eng-US'),('jpn-JP',7,63,4,'PR-192: 商品1','jpn-JP'),('eng-US',8,63,2,'PR-192: Product One','eng-US'),('jpn-JP',8,63,4,'PR-192: 商品1','jpn-JP'),('eng-US',1,64,2,'Product Two','eng-US'),('eng-US',2,64,2,'XY-827: Product Two','eng-US'),('eng-US',1,65,2,'Product Category 1','eng-US'),('eng-US',2,65,2,'Product Category 2','eng-US'),('eng-US',1,66,2,'Product One','eng-US'),('eng-US',2,66,2,'Product Three','eng-US'),('eng-US',3,66,2,'CJ-831: Product Three','eng-US'),('eng-US',4,66,2,'CJ-831: Product Three','eng-US'),('jpn-JP',4,66,4,'CJ-831: 商品3','jpn-JP'),('eng-US',1,67,2,'Product Two','eng-US'),('eng-US',2,67,2,'Product Four','eng-US'),('eng-US',3,67,2,'PR-228: Product Four','eng-US'),('eng-US',4,67,2,'PR-228: Product Four','eng-US'),('jpn-JP',4,67,4,'PR-228: 製品4','jpn-JP'),('eng-US',5,67,2,'PR-228: Product Four','eng-US'),('jpn-JP',5,67,4,'PR-228: 製品4','jpn-JP'),('eng-US',1,68,2,'Product Five','eng-US'),('eng-US',2,68,2,'Product Hello 123','eng-US'),('eng-US',3,68,2,'XX-837: Product Hello 9','eng-US'),('eng-US',4,68,2,'XX-837: Product Hello 9','eng-US'),('jpn-JP',4,68,4,'XX-837: プロダクトハロー9','jpn-JP'),('eng-US',1,70,3,'Products','eng-US'),('eng-US',1,71,2,'About us','eng-US'),('eng-US',2,71,2,'About us','eng-US'),('jpn-JP',2,71,4,'私たちに関しては','jpn-JP'),('eng-US',1,72,3,'Product photo 1','eng-US'),('eng-US',1,73,2,'Product photo 2','eng-US'),('eng-US',1,74,3,'Product photo 1','eng-US'),('jpn-JP',1,74,4,'Product photo 1','jpn-JP'),('eng-US',2,74,3,'Product photo 2','eng-US'),('eng-US',3,74,3,'Product photo 2','eng-US'),('eng-US',1,75,3,'Product photo 1','eng-US'),('jpn-JP',1,75,4,'Product photo 1','jpn-JP'),('eng-US',2,75,3,'Product photo 3','eng-US'),('eng-US',1,76,2,'All products','eng-US'),('eng-US',1,77,2,'Subcategory 1','eng-US'),('eng-US',1,78,2,'Subcategory 1','eng-US'),('jpn-JP',1,78,4,'Subcategory 1','jpn-JP'),('eng-US',2,78,2,'Subcategory 2','eng-US'),('eng-US',1,79,2,'English','eng-US'),('eng-NZ',2,79,8,'Hello New Zealand','eng-NZ'),('eng-US',2,79,2,'English','eng-US'),('jpn-JP',1,81,4,'サイトのトップページ','jpn-JP'),('jpn-JP',1,82,4,'すべての製品','jpn-JP'),('jpn-JP',1,83,4,'製品カテゴリ1','jpn-JP'),('eng-US',1,84,2,'製品カテゴリ1','eng-US'),('jpn-JP',1,84,4,'製品カテゴリ1','jpn-JP'),('jpn-JP',2,84,4,'製品カテゴリ2','jpn-JP'),('jpn-JP',1,85,4,'JP-271-日本の唯一の製品','jpn-JP'),('jpn-JP',2,85,4,'JP-271: 日本の唯一の製品','jpn-JP'),('eng-US',1,86,3,'Product photos','eng-US'),('eng-US',1,87,3,'Product Photo 1','eng-US'),('eng-US',1,88,3,'Product Photo 1','eng-US'),('jpn-JP',1,88,4,'Product Photo 1','jpn-JP'),('eng-US',2,88,3,'Product Photo 2','eng-US'),('eng-US',3,88,3,'Product Photo 2','eng-US'),('jpn-JP',3,88,4,'商品写真2','jpn-JP'),('eng-US',1,89,3,'Product Photo 1','eng-US'),('jpn-JP',1,89,4,'Product Photo 1','jpn-JP'),('eng-US',2,89,3,'Product Photo 1','eng-US'),('eng-US',3,89,3,'Product Photo 3','eng-US'),('eng-US',1,90,2,'Product category 3','eng-US'),('eng-US',1,121,2,'Country editors','eng-US'),('eng-US',1,122,2,'Japanese','eng-US'),('eng-US',1,123,2,'Chinese','eng-US'),('eng-US',1,124,2,'English','eng-US'),('eng-US',1,127,2,'Country editors','eng-US'),('eng-US',1,128,2,'Japanese','eng-US'),('eng-US',1,129,2,'Chinese','eng-US'),('eng-US',1,130,2,'English','eng-US'),('eng-US',1,131,2,'James Japanese','eng-US'),('eng-US',1,132,2,'Eaton English','eng-US'),('eng-US',1,133,3,'Approving users','eng-US'),('eng-US',1,135,3,'Second Admin','eng-US');
/*!40000 ALTER TABLE `ezcontentobject_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_trash`
--

DROP TABLE IF EXISTS `ezcontentobject_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_trash` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  `is_invisible` int(11) NOT NULL DEFAULT '0',
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `parent_node_id` int(11) NOT NULL DEFAULT '0',
  `path_identification_string` longtext,
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  PRIMARY KEY (`node_id`),
  KEY `ezcobj_trash_co_id` (`contentobject_id`),
  KEY `ezcobj_trash_depth` (`depth`),
  KEY `ezcobj_trash_modified_subnode` (`modified_subnode`),
  KEY `ezcobj_trash_p_node_id` (`parent_node_id`),
  KEY `ezcobj_trash_path` (`path_string`),
  KEY `ezcobj_trash_path_ident` (`path_identification_string`(50))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_trash`
--

LOCK TABLES `ezcontentobject_trash` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_trash` DISABLE KEYS */;
INSERT INTO `ezcontentobject_trash` VALUES (121,1,2,0,0,127,1486995136,127,5,'users/country_editors','/1/5/127/',0,'887318deea8e66c1c23b30c4ddc16c96',1,1),(122,1,3,0,0,128,1486995136,128,127,'users/country_editors/japanese','/1/5/127/128/',0,'c3fbbf7764421fcd5ed7009372c7b16b',1,1),(123,1,3,0,0,129,1486995136,129,127,'users/country_editors/chinese','/1/5/127/129/',0,'97b1637eff9bd77d66aab697e13c3084',1,1),(124,1,3,0,0,130,1486995136,130,127,'users/country_editors/english','/1/5/127/130/',0,'ac10b0929e6e6404effacbf1f4520b86',1,1);
/*!40000 ALTER TABLE `ezcontentobject_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_tree`
--

DROP TABLE IF EXISTS `ezcontentobject_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_tree` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_is_published` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  `is_invisible` int(11) NOT NULL DEFAULT '0',
  `main_node_id` int(11) DEFAULT NULL,
  `modified_subnode` int(11) DEFAULT '0',
  `node_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_node_id` int(11) NOT NULL DEFAULT '0',
  `path_identification_string` longtext,
  `path_string` varchar(255) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '0',
  `remote_id` varchar(100) NOT NULL DEFAULT '',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  PRIMARY KEY (`node_id`),
  KEY `ezcontentobject_tree_remote_id` (`remote_id`),
  KEY `ezcontentobject_tree_co_id` (`contentobject_id`),
  KEY `ezcontentobject_tree_depth` (`depth`),
  KEY `ezcontentobject_tree_p_node_id` (`parent_node_id`),
  KEY `ezcontentobject_tree_path` (`path_string`),
  KEY `ezcontentobject_tree_path_ident` (`path_identification_string`(50)),
  KEY `modified_subnode` (`modified_subnode`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_tree`
--

LOCK TABLES `ezcontentobject_tree` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_tree` DISABLE KEYS */;
INSERT INTO `ezcontentobject_tree` VALUES (0,1,1,0,0,0,1,1486997214,1,1,'','/1/',0,'629709ba256fe317c3ddcee35453a96a',1,1),(57,1,2,1,0,0,2,1486367446,2,1,'','/1/2/',0,'f3e90596361e31d496d4026eb624c983',8,1),(4,1,1,1,0,0,5,1486997214,5,1,'users','/1/5/',0,'3f6d92f8044aed134f32153517850f5a',1,1),(12,1,1,2,0,0,13,1486997214,13,5,'users/administrator_users','/1/5/13/',0,'769380b7aa94541679167eab817ca893',1,1),(14,1,4,3,0,0,15,1484920002,15,13,'users/administrator_users/administrator_user','/1/5/13/15/',0,'e5161a99f733200b9ed4e80f9c16187b',1,1),(41,1,1,1,0,0,43,1486123633,43,1,'media','/1/43/',0,'75c715a51699d2d309a924eca6a95145',9,1),(42,1,1,2,0,0,44,1081860719,44,5,'users/anonymous_users','/1/5/44/',0,'4fdf0072da953bb276c0c7e0141c5c9b',9,1),(10,1,2,3,0,0,45,1081860719,45,44,'users/anonymous_users/anonymous_user','/1/5/44/45/',0,'2cf8343bee7b482bab82b269d8fecd76',9,1),(45,1,1,1,0,0,48,1184592117,48,1,'setup2','/1/48/',0,'182ce1b5af0c09fa378557c462ba2617',9,1),(49,1,1,2,0,0,51,1486123633,51,43,'media/images','/1/43/51/',0,'1b26c0454b09bb49dfb1b9190ffd67cb',9,1),(50,1,1,2,0,0,52,1081860720,52,43,'media/files','/1/43/52/',0,'0b113a208f7890f9ad3c24444ff5988c',9,1),(51,1,1,2,0,0,53,1486123388,53,43,'media/multimedia','/1/43/53/',0,'4f18b82c75f10aad476cae5adf98c11f',9,1),(52,1,1,2,0,0,54,1184592117,54,48,'setup2/common_ini_settings','/1/48/54/',0,'fa9f3cff9cf90ecfae335718dcbddfe2',1,1),(54,1,2,2,0,0,56,1484920003,56,58,'design/plain_site','/1/58/56/',0,'772da20ecf88b3035d73cbdfcea0f119',1,1),(56,1,1,1,0,0,58,1484920003,58,1,'design','/1/58/',0,'79f2d67372ab56f59b5d65bb9e0ca3b9',2,0),(62,1,2,4,0,0,63,1486367446,63,76,'english/all_products/product_category_1','/1/2/79/76/63/',0,'24a4ce2b7ebe12bf24242c24c389ff82',1,1),(64,1,2,6,0,0,65,1486121622,65,78,'english/all_products/product_category_1/subcategory_2/xy_827_product_two','/1/2/79/76/63/78/65/',0,'f491d30a7d9bfd44a4bf913411b962bc',1,1),(65,1,2,4,0,0,66,1486121839,66,76,'english/all_products/product_category_2','/1/2/79/76/66/',0,'e1d308937856e101ea6b0b5d9c6f5011',1,1),(66,1,4,5,0,0,67,1486121839,67,66,'english/all_products/product_category_2/cj_831_product_three','/1/2/79/76/66/67/',0,'a45e10ceb3b9725ce0ff38a78d105d65',1,1),(67,1,5,6,0,0,68,1486367395,68,77,'english/all_products/product_category_1/subcategory_1/pr_228_product_four','/1/2/79/76/63/77/68/',0,'aeb6839208af8431f3e73ba433d9f376',1,1),(68,1,4,5,0,0,69,1486121786,69,66,'english/all_products/product_category_2/xx_837_product_hello_9','/1/2/79/76/66/69/',0,'7905841b0d9e40032598999fa6f00300',1,1),(68,1,4,6,0,0,69,1486121786,70,78,'english/all_products/product_category_1/subcategory_2/xx_837_product_hello_9','/1/2/79/76/63/78/70/',0,'8642f80cc62404765c918821bc9af6aa',1,1),(70,1,1,3,0,0,71,1486123388,71,53,'media/multimedia/products','/1/43/53/71/',0,'c5b9407476c50a01d5604559f1602aea',1,1),(71,1,2,3,0,0,72,1486121874,72,79,'english/about_us','/1/2/79/72/',0,'1bff2c7d5159364e0e0e0295ccf3ded2',1,1),(72,1,1,4,0,0,73,1485525584,73,71,'media/multimedia/products/product_photo_1','/1/43/53/71/73/',0,'4d7d0f131d8a359f205014d402f58525',1,1),(74,1,3,4,0,0,74,1486122870,74,71,'media/multimedia/products/product_photo_2','/1/43/53/71/74/',0,'9df7104665b75628e59ad9b6c1f2d442',2,0),(75,1,2,4,0,0,75,1485715497,75,71,'media/multimedia/products/product_photo_3','/1/43/53/71/75/',0,'1e871aaf39073925d0998942f193e57e',2,0),(76,1,1,3,0,0,76,1486367446,76,79,'english/all_products','/1/2/79/76/',0,'61dbf5b2f26e1a2960dec527ec26b630',1,1),(77,1,1,5,0,0,77,1486367446,77,63,'english/all_products/product_category_1/subcategory_1','/1/2/79/76/63/77/',0,'95c14b08c66bcdd68e863a4ab2535fed',1,1),(78,1,2,5,0,0,78,1486363335,78,63,'english/all_products/product_category_1/subcategory_2','/1/2/79/76/63/78/',0,'30e96fead6a744c6e11a647b116e55ee',2,0),(79,1,2,2,0,0,79,1486367446,79,2,'english','/1/2/79/',200,'bf8c49aca044ddb7b68e7e18d7f351fb',1,1),(81,1,1,2,0,0,80,1486367446,80,2,'node_80','/1/2/80/',100,'fbaed7f46812f7f661bdd3e30757f828',1,1),(71,1,2,3,0,0,72,1486121874,81,80,'node_80/about_us','/1/2/80/81/',0,'b39ef0d672d03c3c85cbe2bf2118684a',1,1),(82,1,1,3,0,0,82,1486367446,82,80,'node_80/node_82','/1/2/80/82/',0,'98c50fd329aac8af14ac7318c5426823',1,1),(83,1,1,4,0,0,83,1486367446,83,82,'node_80/node_82/1','/1/2/80/82/83/',0,'c6f171854bdcfd0f5e99f49115f558b8',1,1),(84,1,2,4,0,0,84,1486367395,84,82,'node_80/node_82/2','/1/2/80/82/84/',0,'940ac0b86984a4cddcf169eddf9de294',2,0),(66,1,4,5,0,0,67,1486121839,85,83,'node_80/node_82/1/cj_831_product_three','/1/2/80/82/83/85/',0,'a8f28bfd738701e65b84dcafd2d25f4a',1,1),(68,1,4,5,0,0,69,1486121786,86,83,'node_80/node_82/1/xx_837_product_hello_9','/1/2/80/82/83/86/',0,'4dd9bc7a7b66c1878bb8c530b1814ecb',1,1),(67,1,5,5,0,0,68,1486367395,87,84,'node_80/node_82/2/pr_228_product_four','/1/2/80/82/84/87/',0,'8aaea6357c02aaca04b808121511dd5a',1,1),(85,1,2,5,0,0,88,1486121482,88,84,'node_80/node_82/2/jp_271','/1/2/80/82/84/88/',0,'7a4634e7107deea703598a7a18e397c5',1,1),(63,1,8,5,0,0,89,1486367446,89,83,'node_80/node_82/1/pr_192_product_one','/1/2/80/82/83/89/',0,'e17e95b9309b9c171b0f58147f5abe5b',1,1),(63,1,8,6,0,0,89,1486367446,90,77,'english/all_products/product_category_1/subcategory_1/pr_192_product_one','/1/2/79/76/63/77/90/',0,'86fb1bf652da2764ea57f51c2da17cda',1,1),(86,1,1,3,0,0,91,1486123633,91,51,'media/images/product_photos','/1/43/51/91/',0,'8f3db3cbcd280ad94a7e2ef6c03b8684',1,1),(87,1,1,4,0,0,92,1486123347,92,91,'media/images/product_photos/product_photo_1','/1/43/51/91/92/',0,'c097820388d5d88c53cd629047be62d7',1,1),(88,1,3,4,0,0,93,1486123633,93,91,'media/images/product_photos/product_photo_2','/1/43/51/91/93/',0,'a41a37ad1117cc93267fa5b2814ce8fb',2,0),(89,1,3,4,0,0,94,1486123599,94,91,'media/images/product_photos/product_photo_3','/1/43/51/91/94/',0,'2e5d84344593d9ef51f569c6cb454f37',2,0),(90,1,1,4,1,1,96,1486366677,96,76,'english/all_products/product_category_3','/1/2/79/76/96/',0,'fe7f05e0d5ecd337612fcd9e8db6d9da',1,1),(127,1,1,2,0,0,133,1486996605,133,5,'users/country_editors','/1/5/133/',0,'d9e1aa7d187477642100e1e2689d308d',1,1),(128,1,1,3,0,0,134,1486995248,134,133,'users/country_editors/japanese','/1/5/133/134/',0,'8b236a336b8f38bc46418e4fff544ca0',1,1),(129,1,1,3,0,0,135,1486995248,135,133,'users/country_editors/chinese','/1/5/133/135/',0,'437d8c779e5b16c40fc01577bc7be625',1,1),(130,1,1,3,0,0,136,1486995248,136,133,'users/country_editors/english','/1/5/133/136/',0,'6859c49cfbbe4611e4a314f4053e81b6',1,1),(131,1,1,4,0,0,137,1486995248,137,134,'users/country_editors/japanese/james_japanese','/1/5/133/134/137/',0,'1af29271b82a27c55094247851fca4cb',9,1),(132,1,1,4,0,0,138,1486995248,138,136,'users/country_editors/english/eaton_english','/1/5/133/136/138/',0,'b04909fcdc9e89eb95c9efddaf8f6256',9,1),(133,1,1,2,0,0,139,1486996605,139,5,'users/approving_users','/1/5/139/',0,'0d6f636ebc5584e37e1140e8004f202e',1,1),(132,1,1,3,0,0,138,1486996575,140,139,'users/approving_users/eaton_english','/1/5/139/140/',0,'447bc111acdaade14d28b99129bd28e8',1,1),(135,1,1,3,0,0,141,1486997214,141,13,'users/administrator_users/second_admin','/1/5/13/141/',0,'96504b49ea22faa0ed996370fda30b59',1,1);
/*!40000 ALTER TABLE `ezcontentobject_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcontentobject_version`
--

DROP TABLE IF EXISTS `ezcontentobject_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcontentobject_version` (
  `contentobject_id` int(11) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initial_language_id` bigint(20) NOT NULL DEFAULT '0',
  `language_mask` bigint(20) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_event_pos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezcobj_version_creator_id` (`creator_id`),
  KEY `ezcobj_version_status` (`status`),
  KEY `idx_object_version_objver` (`contentobject_id`,`version`),
  KEY `ezcontobj_version_obj_status` (`contentobject_id`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcontentobject_version`
--

LOCK TABLES `ezcontentobject_version` WRITE;
/*!40000 ALTER TABLE `ezcontentobject_version` DISABLE KEYS */;
INSERT INTO `ezcontentobject_version` VALUES (4,0,14,4,2,3,0,1,0,1,1),(12,1033920760,14,440,2,3,1033920775,1,0,1,0),(41,1060695450,14,472,2,3,1060695457,1,0,1,0),(42,1072180278,14,473,2,3,1072180330,1,0,1,0),(10,1072180337,14,474,2,3,1072180405,1,0,2,0),(45,1079684084,14,477,2,3,1079684190,1,0,1,0),(49,1080220181,14,488,2,3,1080220197,1,0,1,0),(50,1080220211,14,489,2,3,1080220220,1,0,1,0),(51,1080220225,14,490,2,3,1080220233,1,0,1,0),(52,1082016497,14,491,2,2,1082016591,1,0,1,0),(56,1103023120,14,495,2,3,1103023120,1,0,1,0),(14,1301061783,14,499,2,3,1301062024,3,0,3,0),(54,1301062300,14,500,2,2,1301062375,1,0,2,0),(57,1196268655,14,504,2,2,1196268696,3,0,1,0),(14,1484920002,14,506,2,3,1484920002,1,0,4,0),(62,1485153010,14,512,2,3,1485153046,3,0,1,0),(63,1485153426,14,513,2,3,1485153471,3,0,1,0),(64,1485153500,14,514,2,3,1485153514,3,0,1,0),(65,1485153593,14,515,2,3,1485153593,3,0,1,0),(66,1485153594,14,516,2,3,1485153594,3,0,1,0),(67,1485153594,14,517,2,3,1485153594,3,0,1,0),(65,1485153630,14,518,2,3,1485153639,1,0,2,0),(66,1485153651,14,519,2,3,1485153659,3,0,2,0),(67,1485153669,14,520,2,3,1485153680,3,0,2,0),(68,1485153746,14,521,2,3,1485153762,3,0,1,0),(68,1485153905,14,522,2,3,1485153927,3,0,2,0),(62,1485156891,14,523,4,7,1485156947,1,0,2,0),(57,1485160432,14,525,2,2,1485160438,1,0,2,0),(70,1485521854,14,527,2,3,1485521863,1,0,1,0),(71,1485522106,14,528,2,3,1485522422,3,0,1,0),(72,1485525565,14,529,2,3,1485525584,1,0,1,0),(73,1485525593,14,530,2,2,1485525652,0,0,1,0),(74,1485715347,14,531,2,3,1485715347,3,0,1,0),(75,1485715362,14,532,2,3,1485715362,3,0,1,0),(74,1485715456,14,533,2,3,1485715474,3,0,2,0),(75,1485715485,14,534,2,3,1485715497,1,0,2,0),(63,1485715536,14,535,2,3,1485715569,3,0,2,0),(76,1485762965,14,537,2,3,1485762975,1,0,1,0),(77,1485763050,14,538,2,3,1485763065,1,0,1,0),(78,1485763836,14,539,2,3,1485763836,3,0,1,0),(78,1485764732,14,540,2,3,1485764737,1,0,2,0),(79,1486121018,14,541,2,3,1486121030,3,0,1,0),(81,1486121140,14,543,4,5,1486121151,1,0,1,0),(82,1486121215,14,544,4,5,1486121234,1,0,1,0),(83,1486121272,14,545,4,5,1486121276,1,0,1,0),(84,1486121295,14,546,4,5,1486121295,3,0,1,0),(84,1486121304,14,547,4,5,1486121309,1,0,2,0),(85,1486121389,14,548,4,5,1486121440,3,0,1,0),(85,1486121477,14,549,4,5,1486121482,1,0,2,0),(67,1486121498,14,550,2,3,1486121513,3,0,3,0),(66,1486121523,14,551,2,3,1486121542,3,0,3,0),(68,1486121553,14,552,2,3,1486121574,3,0,3,0),(63,1486121592,14,553,2,3,1486121602,3,0,3,0),(64,1486121613,14,554,2,3,1486121622,1,0,2,0),(63,1486121653,14,555,4,7,1486121689,3,0,4,0),(67,1486121716,14,556,4,7,1486121737,3,0,4,0),(68,1486121759,14,557,4,7,1486121786,1,0,4,0),(66,1486121804,14,558,4,7,1486121839,1,0,4,0),(71,1486121853,14,559,4,7,1486121874,1,0,2,0),(72,1486122645,14,560,2,3,1486122662,0,0,2,0),(74,1486122797,14,561,2,3,1486122870,1,0,3,0),(75,1486122877,14,562,2,3,1486123102,0,0,3,0),(63,1486123129,14,563,2,7,1486123140,3,0,5,0),(63,1486123233,14,564,4,7,1486123243,3,0,6,0),(86,1486123288,14,565,2,3,1486123295,1,0,1,0),(87,1486123336,14,566,2,3,1486123347,1,0,1,0),(88,1486123361,14,567,2,3,1486123361,3,0,1,0),(89,1486123377,14,568,2,3,1486123377,3,0,1,0),(88,1486123404,14,569,2,3,1486123422,3,0,2,0),(89,1486123407,14,570,2,3,1486123433,3,0,2,0),(63,1486123469,14,571,2,7,1486123486,3,0,7,0),(89,1486123593,14,572,2,3,1486123599,1,0,3,0),(88,1486123612,14,573,4,7,1486123633,1,0,3,0),(79,1486366452,14,574,8,11,1486366463,1,0,2,0),(90,1486366643,14,575,2,3,1486366668,1,0,1,0),(67,1486367306,14,576,2,7,1486367395,1,0,5,0),(63,1486367436,14,578,2,7,1486367445,1,0,8,0),(121,1486995136,14,610,2,3,1486995136,1,0,1,0),(122,1486995136,14,611,2,3,1486995136,1,0,1,0),(123,1486995136,14,612,2,3,1486995136,1,0,1,0),(124,1486995136,14,613,2,3,1486995137,1,0,1,0),(127,1486995248,14,616,2,3,1486995248,1,0,1,0),(128,1486995248,14,617,2,3,1486995248,1,0,1,0),(129,1486995248,14,618,2,3,1486995248,1,0,1,0),(130,1486995248,14,619,2,3,1486995248,1,0,1,0),(131,1486995248,14,620,2,2,1486995248,1,0,1,0),(132,1486995248,14,621,2,2,1486995248,1,0,1,0),(133,1486996250,14,622,2,3,1486996262,1,0,1,0),(135,1486997197,14,624,2,3,1486997214,1,0,1,0);
/*!40000 ALTER TABLE `ezcontentobject_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezcurrencydata`
--

DROP TABLE IF EXISTS `ezcurrencydata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezcurrencydata` (
  `auto_rate_value` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `code` varchar(4) NOT NULL DEFAULT '',
  `custom_rate_value` decimal(10,5) NOT NULL DEFAULT '0.00000',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) NOT NULL DEFAULT '',
  `rate_factor` decimal(10,5) NOT NULL DEFAULT '1.00000',
  `status` int(11) NOT NULL DEFAULT '1',
  `symbol` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezcurrencydata_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezcurrencydata`
--

LOCK TABLES `ezcurrencydata` WRITE;
/*!40000 ALTER TABLE `ezcurrencydata` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezcurrencydata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountrule`
--

DROP TABLE IF EXISTS `ezdiscountrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountrule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountrule`
--

LOCK TABLES `ezdiscountrule` WRITE;
/*!40000 ALTER TABLE `ezdiscountrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountsubrule`
--

DROP TABLE IF EXISTS `ezdiscountsubrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountsubrule` (
  `discount_percent` float DEFAULT NULL,
  `discountrule_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation` char(1) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountsubrule`
--

LOCK TABLES `ezdiscountsubrule` WRITE;
/*!40000 ALTER TABLE `ezdiscountsubrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountsubrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezdiscountsubrule_value`
--

DROP TABLE IF EXISTS `ezdiscountsubrule_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezdiscountsubrule_value` (
  `discountsubrule_id` int(11) NOT NULL DEFAULT '0',
  `issection` int(11) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`discountsubrule_id`,`value`,`issection`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezdiscountsubrule_value`
--

LOCK TABLES `ezdiscountsubrule_value` WRITE;
/*!40000 ALTER TABLE `ezdiscountsubrule_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezdiscountsubrule_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezenumobjectvalue`
--

DROP TABLE IF EXISTS `ezenumobjectvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezenumobjectvalue` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT '0',
  `enumelement` varchar(255) NOT NULL DEFAULT '',
  `enumid` int(11) NOT NULL DEFAULT '0',
  `enumvalue` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_attribute_version`,`enumid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezenumobjectvalue`
--

LOCK TABLES `ezenumobjectvalue` WRITE;
/*!40000 ALTER TABLE `ezenumobjectvalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezenumobjectvalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezenumvalue`
--

DROP TABLE IF EXISTS `ezenumvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezenumvalue` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_attribute_version` int(11) NOT NULL DEFAULT '0',
  `enumelement` varchar(255) NOT NULL DEFAULT '',
  `enumvalue` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`contentclass_attribute_id`,`contentclass_attribute_version`),
  KEY `ezenumvalue_co_cl_attr_id_co_class_att_ver` (`contentclass_attribute_id`,`contentclass_attribute_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezenumvalue`
--

LOCK TABLES `ezenumvalue` WRITE;
/*!40000 ALTER TABLE `ezenumvalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezenumvalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezforgot_password`
--

DROP TABLE IF EXISTS `ezforgot_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezforgot_password` (
  `hash_key` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezforgot_password_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezforgot_password`
--

LOCK TABLES `ezforgot_password` WRITE;
/*!40000 ALTER TABLE `ezforgot_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezforgot_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezgeneral_digest_user_settings`
--

DROP TABLE IF EXISTS `ezgeneral_digest_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezgeneral_digest_user_settings` (
  `day` varchar(255) NOT NULL DEFAULT '',
  `digest_type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receive_digest` int(11) NOT NULL DEFAULT '0',
  `time` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezgeneral_digest_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezgeneral_digest_user_settings`
--

LOCK TABLES `ezgeneral_digest_user_settings` WRITE;
/*!40000 ALTER TABLE `ezgeneral_digest_user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezgeneral_digest_user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezgmaplocation`
--

DROP TABLE IF EXISTS `ezgmaplocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezgmaplocation` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_version` int(11) NOT NULL DEFAULT '0',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `address` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`contentobject_version`),
  KEY `latitude_longitude_key` (`latitude`,`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezgmaplocation`
--

LOCK TABLES `ezgmaplocation` WRITE;
/*!40000 ALTER TABLE `ezgmaplocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezgmaplocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezimagefile`
--

DROP TABLE IF EXISTS `ezimagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezimagefile` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `filepath` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `ezimagefile_coid` (`contentobject_attribute_id`),
  KEY `ezimagefile_file` (`filepath`(200))
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezimagefile`
--

LOCK TABLES `ezimagefile` WRITE;
/*!40000 ALTER TABLE `ezimagefile` DISABLE KEYS */;
INSERT INTO `ezimagefile` VALUES (172,'var/ezdemo_site/storage/images/design/plain-site/172-2-eng-US/eZ-Publish-Demo-Site-without-demo-content1.png',1),(254,'var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One.jpg',4),(254,'var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One_reference.jpg',5),(254,'var/network_yamaha_site/storage/images/product-category-1/product-one/254-1-eng-US/Product-One_small.jpg',6),(264,'var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One.jpg',10),(264,'var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One_reference.jpg',11),(264,'var/network_yamaha_site/storage/images/product-category-12/product-one/254-1-eng-US/Product-One_small.jpg',12),(264,'var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three.jpg',14),(264,'var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_reference.jpg',15),(264,'var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_small.jpg',16),(264,'var/network_yamaha_site/storage/images/product-category-2/product-three/254-1-eng-US/Product-Three_medium.jpg',17),(309,'var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1.jpg',20),(309,'var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_reference.jpg',21),(309,'var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_small.jpg',22),(314,'var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22.jpg',23),(314,'var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22_reference.jpg',24),(314,'var/network_yamaha_site/storage/images-versioned/314/1-eng-US/Product-photo-22_large.jpg',25),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1.jpg',29),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_reference.jpg',30),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_small.jpg',31),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1.jpg',35),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_reference.jpg',36),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_small.jpg',37),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-13/309-1-eng-US/Product-photo-1_large.jpg',38),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-12/309-1-eng-US/Product-photo-1_large.jpg',39),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2.jpg',43),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_reference.jpg',44),(319,'var/network_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_small.jpg',45),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3.jpg',49),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_reference.jpg',50),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_small.jpg',51),(264,'var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US/CJ-831-Product-Three.jpg',52),(367,'var/music_yamaha_site/storage/images/english/all-products/product-category-2/cj-831-product-three/254-1-eng-US/CJ-831-Product-Three.jpg',53),(319,'var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2.jpg',56),(319,'var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_reference.jpg',57),(319,'var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_medium.jpg',58),(319,'var/music_yamaha_site/storage/images/media/images/products/product-photo-2/319-2-eng-US/Product-photo-2_small.jpg',59),(309,'var/network_yamaha_site/storage/images/media/images/products/product-photo-1/309-1-eng-US/Product-photo-1_large.jpg',61),(324,'var/network_yamaha_site/storage/images/media/images/products/product-photo-3/324-2-eng-US/Product-photo-3_large.jpg',63),(381,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1.jpg',66),(381,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1_reference.jpg',67),(381,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-1/381-1-eng-US/Product-Photo-1_small.jpg',68),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1.jpg',72),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1_reference.jpg',73),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/381-1-eng-US/Product-Photo-1_small.jpg',74),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1.jpg',78),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1_reference.jpg',79),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-13/381-1-eng-US/Product-Photo-1_small.jpg',80),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2.jpg',85),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1.jpg',87),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_reference.jpg',88),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_small.jpg',89),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1_reference.jpg',90),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-12/391-2-eng-US/Product-Photo-1_small.jpg',91),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3.jpg',93),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_reference.jpg',94),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_small.jpg',95),(391,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-3/391-2-eng-US/Product-Photo-3_medium.jpg',96),(396,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2.jpg',97),(396,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_reference.jpg',98),(396,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_small.jpg',99),(386,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_large.jpg',100),(396,'var/music_yamaha_site/storage/images/media/images/product-photos/product-photo-2/386-2-eng-US/Product-Photo-2_medium.jpg',101);
/*!40000 ALTER TABLE `ezimagefile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezinfocollection`
--

DROP TABLE IF EXISTS `ezinfocollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezinfocollection` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) DEFAULT '0',
  `user_identifier` varchar(34) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezinfocollection_co_id_created` (`contentobject_id`,`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezinfocollection`
--

LOCK TABLES `ezinfocollection` WRITE;
/*!40000 ALTER TABLE `ezinfocollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezinfocollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezinfocollection_attribute`
--

DROP TABLE IF EXISTS `ezinfocollection_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezinfocollection_attribute` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_id` int(11) DEFAULT NULL,
  `contentobject_id` int(11) DEFAULT NULL,
  `data_float` float DEFAULT NULL,
  `data_int` int(11) DEFAULT NULL,
  `data_text` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `informationcollection_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezinfocollection_attr_cca_id` (`contentclass_attribute_id`),
  KEY `ezinfocollection_attr_co_id` (`contentobject_id`),
  KEY `ezinfocollection_attr_coa_id` (`contentobject_attribute_id`),
  KEY `ezinfocollection_attr_ic_id` (`informationcollection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezinfocollection_attribute`
--

LOCK TABLES `ezinfocollection_attribute` WRITE;
/*!40000 ALTER TABLE `ezinfocollection_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezinfocollection_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_group`
--

DROP TABLE IF EXISTS `ezisbn_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_group` (
  `description` varchar(255) NOT NULL DEFAULT '',
  `group_number` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_group`
--

LOCK TABLES `ezisbn_group` WRITE;
/*!40000 ALTER TABLE `ezisbn_group` DISABLE KEYS */;
INSERT INTO `ezisbn_group` VALUES ('English language',0,1),('English language',1,2),('French language',2,3),('German language',3,4),('Japan',4,5),('Russian Federation and former USSR',5,6),('Iran',600,7),('Kazakhstan',601,8),('Indonesia',602,9),('Saudi Arabia',603,10),('Vietnam',604,11),('Turkey',605,12),('Romania',606,13),('Mexico',607,14),('Macedonia',608,15),('Lithuania',609,16),('Thailand',611,17),('Peru',612,18),('Mauritius',613,19),('Lebanon',614,20),('Hungary',615,21),('Thailand',616,22),('Ukraine',617,23),('China, People\'s Republic',7,24),('Czech Republic and Slovakia',80,25),('India',81,26),('Norway',82,27),('Poland',83,28),('Spain',84,29),('Brazil',85,30),('Serbia and Montenegro',86,31),('Denmark',87,32),('Italy',88,33),('Korea, Republic',89,34),('Netherlands',90,35),('Sweden',91,36),('International NGO Publishers and EC Organizations',92,37),('India',93,38),('Netherlands',94,39),('Argentina',950,40),('Finland',951,41),('Finland',952,42),('Croatia',953,43),('Bulgaria',954,44),('Sri Lanka',955,45),('Chile',956,46),('Taiwan',957,47),('Colombia',958,48),('Cuba',959,49),('Greece',960,50),('Slovenia',961,51),('Hong Kong, China',962,52),('Hungary',963,53),('Iran',964,54),('Israel',965,55),('Ukraine',966,56),('Malaysia',967,57),('Mexico',968,58),('Pakistan',969,59),('Mexico',970,60),('Philippines',971,61),('Portugal',972,62),('Romania',973,63),('Thailand',974,64),('Turkey',975,65),('Caribbean Community',976,66),('Egypt',977,67),('Nigeria',978,68),('Indonesia',979,69),('Venezuela',980,70),('Singapore',981,71),('South Pacific',982,72),('Malaysia',983,73),('Bangladesh',984,74),('Belarus',985,75),('Taiwan',986,76),('Argentina',987,77),('Hong Kong, China',988,78),('Portugal',989,79),('Qatar',9927,80),('Albania',9928,81),('Guatemala',9929,82),('Costa Rica',9930,83),('Algeria',9931,84),('Lao People\'s Democratic Republic',9932,85),('Syria',9933,86),('Latvia',9934,87),('Iceland',9935,88),('Afghanistan',9936,89),('Nepal',9937,90),('Tunisia',9938,91),('Armenia',9939,92),('Montenegro',9940,93),('Georgia',9941,94),('Ecuador',9942,95),('Uzbekistan',9943,96),('Turkey',9944,97),('Dominican Republic',9945,98),('Korea, P.D.R.',9946,99),('Algeria',9947,100),('United Arab Emirates',9948,101),('Estonia',9949,102),('Palestine',9950,103),('Kosova',9951,104),('Azerbaijan',9952,105),('Lebanon',9953,106),('Morocco',9954,107),('Lithuania',9955,108),('Cameroon',9956,109),('Jordan',9957,110),('Bosnia and Herzegovina',9958,111),('Libya',9959,112),('Saudi Arabia',9960,113),('Algeria',9961,114),('Panama',9962,115),('Cyprus',9963,116),('Ghana',9964,117),('Kazakhstan',9965,118),('Kenya',9966,119),('Kyrgyz Republic',9967,120),('Costa Rica',9968,121),('Uganda',9970,122),('Singapore',9971,123),('Peru',9972,124),('Tunisia',9973,125),('Uruguay',9974,126),('Moldova',9975,127),('Tanzania',9976,128),('Costa Rica',9977,129),('Ecuador',9978,130),('Iceland',9979,131),('Papua New Guinea',9980,132),('Morocco',9981,133),('Zambia',9982,134),('Gambia',9983,135),('Latvia',9984,136),('Estonia',9985,137),('Lithuania',9986,138),('Tanzania',9987,139),('Ghana',9988,140),('Macedonia',9989,141),('Bahrain',99901,142),('Gabon',99902,143),('Mauritius',99903,144),('Netherlands Antilles and Aruba',99904,145),('Bolivia',99905,146),('Kuwait',99906,147),('Malawi',99908,148),('Malta',99909,149),('Sierra Leone',99910,150),('Lesotho',99911,151),('Botswana',99912,152),('Andorra',99913,153),('Suriname',99914,154),('Maldives',99915,155),('Namibia',99916,156),('Brunei Darussalam',99917,157),('Faroe Islands',99918,158),('Benin',99919,159),('Andorra',99920,160),('Qatar',99921,161),('Guatemala',99922,162),('El Salvador',99923,163),('Nicaragua',99924,164),('Paraguay',99925,165),('Honduras',99926,166),('Albania',99927,167),('Georgia',99928,168),('Mongolia',99929,169),('Armenia',99930,170),('Seychelles',99931,171),('Malta',99932,172),('Nepal',99933,173),('Dominican Republic',99934,174),('Haiti',99935,175),('Bhutan',99936,176),('Macau',99937,177),('Srpska, Republic of',99938,178),('Guatemala',99939,179),('Georgia',99940,180),('Armenia',99941,181),('Sudan',99942,182),('Albania',99943,183),('Ethiopia',99944,184),('Namibia',99945,185),('Nepal',99946,186),('Tajikistan',99947,187),('Eritrea',99948,188),('Mauritius',99949,189),('Cambodia',99950,190),('Congo',99951,191),('Mali',99952,192),('Paraguay',99953,193),('Bolivia',99954,194),('Srpska, Republic of',99955,195),('Albania',99956,196),('Malta',99957,197),('Bahrain',99958,198),('Luxembourg',99959,199),('Malawi',99960,200),('El Salvador',99961,201),('Mongolia',99962,202),('Cambodia',99963,203),('Nicaragua',99964,204),('Macau',99965,205),('Kuwait',99966,206),('Paraguay',99967,207),('Botswana',99968,208),('France',10,209);
/*!40000 ALTER TABLE `ezisbn_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_group_range`
--

DROP TABLE IF EXISTS `ezisbn_group_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_group_range` (
  `from_number` int(11) NOT NULL DEFAULT '0',
  `group_from` varchar(32) NOT NULL DEFAULT '',
  `group_length` int(11) NOT NULL DEFAULT '0',
  `group_to` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_group_range`
--

LOCK TABLES `ezisbn_group_range` WRITE;
/*!40000 ALTER TABLE `ezisbn_group_range` DISABLE KEYS */;
INSERT INTO `ezisbn_group_range` VALUES (0,'0',1,'5',1,59999),(60000,'600',3,'649',2,64999),(70000,'7',1,'7',3,79999),(80000,'80',2,'94',4,94999),(95000,'950',3,'989',5,98999),(99000,'9900',4,'9989',6,99899),(99900,'99900',5,'99999',7,99999),(10000,'10',2,'10',8,10999);
/*!40000 ALTER TABLE `ezisbn_group_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezisbn_registrant_range`
--

DROP TABLE IF EXISTS `ezisbn_registrant_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezisbn_registrant_range` (
  `from_number` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn_group_id` int(11) NOT NULL DEFAULT '0',
  `registrant_from` varchar(32) NOT NULL DEFAULT '',
  `registrant_length` int(11) NOT NULL DEFAULT '0',
  `registrant_to` varchar(32) NOT NULL DEFAULT '',
  `to_number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=927 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezisbn_registrant_range`
--

LOCK TABLES `ezisbn_registrant_range` WRITE;
/*!40000 ALTER TABLE `ezisbn_registrant_range` DISABLE KEYS */;
INSERT INTO `ezisbn_registrant_range` VALUES (0,1,1,'00',2,'19',19999),(20000,2,1,'200',3,'699',69999),(70000,3,1,'7000',4,'8499',84999),(85000,4,1,'85000',5,'89999',89999),(90000,5,1,'900000',6,'949999',94999),(95000,6,1,'9500000',7,'9999999',99999),(0,7,2,'00',2,'09',9999),(10000,8,2,'100',3,'399',39999),(40000,9,2,'4000',4,'5499',54999),(55000,10,2,'55000',5,'86979',86979),(86980,11,2,'869800',6,'998999',99899),(99900,12,2,'9990000',7,'9999999',99999),(0,13,3,'00',2,'19',19999),(20000,14,3,'200',3,'349',34999),(35000,15,3,'35000',5,'39999',39999),(40000,16,3,'400',3,'699',69999),(70000,17,3,'7000',4,'8399',83999),(84000,18,3,'84000',5,'89999',89999),(90000,19,3,'900000',6,'949999',94999),(95000,20,3,'9500000',7,'9999999',99999),(0,21,4,'00',2,'02',2999),(3000,22,4,'030',3,'033',3399),(3400,23,4,'0340',4,'0369',3699),(3700,24,4,'03700',5,'03999',3999),(4000,25,4,'04',2,'19',19999),(20000,26,4,'200',3,'699',69999),(70000,27,4,'7000',4,'8499',84999),(85000,28,4,'85000',5,'89999',89999),(90000,29,4,'900000',6,'949999',94999),(95000,30,4,'9500000',7,'9539999',95399),(95400,31,4,'95400',5,'96999',96999),(97000,32,4,'9700000',7,'9899999',98999),(99000,33,4,'99000',5,'99499',99499),(99500,34,4,'99500',5,'99999',99999),(0,35,5,'00',2,'19',19999),(20000,36,5,'200',3,'699',69999),(70000,37,5,'7000',4,'8499',84999),(85000,38,5,'85000',5,'89999',89999),(90000,39,5,'900000',6,'949999',94999),(95000,40,5,'9500000',7,'9999999',99999),(0,41,6,'00',2,'19',19999),(20000,42,6,'200',3,'420',42099),(42100,43,6,'4210',4,'4299',42999),(43000,44,6,'430',3,'430',43099),(43100,45,6,'4310',4,'4399',43999),(44000,46,6,'440',3,'440',44099),(44100,47,6,'4410',4,'4499',44999),(45000,48,6,'450',3,'699',69999),(70000,49,6,'7000',4,'8499',84999),(85000,50,6,'85000',5,'89999',89999),(90000,51,6,'900000',6,'909999',90999),(91000,52,6,'91000',5,'91999',91999),(92000,53,6,'9200',4,'9299',92999),(93000,54,6,'93000',5,'94999',94999),(95000,55,6,'9500000',7,'9500999',95009),(95010,56,6,'9501',4,'9799',97999),(98000,57,6,'98000',5,'98999',98999),(99000,58,6,'9900000',7,'9909999',99099),(99100,59,6,'9910',4,'9999',99999),(0,60,7,'00',2,'09',9999),(10000,61,7,'100',3,'499',49999),(50000,62,7,'5000',4,'8999',89999),(90000,63,7,'90000',5,'99999',99999),(0,64,8,'00',2,'19',19999),(20000,65,8,'200',3,'699',69999),(70000,66,8,'7000',4,'7999',79999),(80000,67,8,'80000',5,'84999',84999),(85000,68,8,'85',2,'99',99999),(0,69,9,'00',2,'19',19999),(20000,70,9,'200',3,'799',79999),(80000,71,9,'8000',4,'9499',94999),(95000,72,9,'95000',5,'99999',99999),(0,73,10,'00',2,'04',4999),(5000,74,10,'05',2,'49',49999),(50000,75,10,'500',3,'799',79999),(80000,76,10,'8000',4,'8999',89999),(90000,77,10,'90000',5,'99999',99999),(0,78,11,'0',1,'4',49999),(50000,79,11,'50',2,'89',89999),(90000,80,11,'900',3,'979',97999),(98000,81,11,'9800',4,'9999',99999),(1000,82,12,'01',2,'09',9999),(10000,83,12,'100',3,'399',39999),(40000,84,12,'4000',4,'5999',59999),(60000,85,12,'60000',5,'89999',89999),(90000,86,12,'90',2,'99',99999),(0,87,13,'0',1,'0',9999),(10000,88,13,'10',2,'49',49999),(50000,89,13,'500',3,'799',79999),(80000,90,13,'8000',4,'9199',91999),(92000,91,13,'92000',5,'99999',99999),(0,92,14,'00',2,'39',39999),(40000,93,14,'400',3,'749',74999),(75000,94,14,'7500',4,'9499',94999),(95000,95,14,'95000',5,'99999',99999),(0,96,15,'0',1,'0',9999),(10000,97,15,'10',2,'19',19999),(20000,98,15,'200',3,'449',44999),(45000,99,15,'4500',4,'6499',64999),(65000,100,15,'65000',5,'69999',69999),(70000,101,15,'7',1,'9',99999),(0,102,16,'00',2,'39',39999),(40000,103,16,'400',3,'799',79999),(80000,104,16,'8000',4,'9499',94999),(95000,105,16,'95000',5,'99999',99999),(0,106,18,'00',2,'29',29999),(30000,107,18,'300',3,'399',39999),(40000,108,18,'4000',4,'4499',44999),(45000,109,18,'45000',5,'49999',49999),(50000,110,18,'50',2,'99',99999),(0,111,19,'0',1,'9',99999),(0,112,20,'00',2,'39',39999),(40000,113,20,'400',3,'799',79999),(80000,114,20,'8000',4,'9499',94999),(95000,115,20,'95000',5,'99999',99999),(0,116,21,'00',2,'09',9999),(10000,117,21,'100',3,'499',49999),(50000,118,21,'5000',4,'7999',79999),(80000,119,21,'80000',5,'89999',89999),(0,120,22,'00',2,'19',19999),(20000,121,22,'200',3,'699',69999),(70000,122,22,'7000',4,'8999',89999),(90000,123,22,'90000',5,'99999',99999),(0,124,23,'00',2,'49',49999),(50000,125,23,'500',3,'699',69999),(70000,126,23,'7000',4,'8999',89999),(90000,127,23,'90000',5,'99999',99999),(0,128,24,'00',2,'09',9999),(10000,129,24,'100',3,'499',49999),(50000,130,24,'5000',4,'7999',79999),(80000,131,24,'80000',5,'89999',89999),(90000,132,24,'900000',6,'999999',99999),(0,133,25,'00',2,'19',19999),(20000,134,25,'200',3,'699',69999),(70000,135,25,'7000',4,'8499',84999),(85000,136,25,'85000',5,'89999',89999),(90000,137,25,'900000',6,'999999',99999),(0,138,26,'00',2,'19',19999),(20000,139,26,'200',3,'699',69999),(70000,140,26,'7000',4,'8499',84999),(85000,141,26,'85000',5,'89999',89999),(90000,142,26,'900000',6,'999999',99999),(0,143,27,'00',2,'19',19999),(20000,144,27,'200',3,'699',69999),(70000,145,27,'7000',4,'8999',89999),(90000,146,27,'90000',5,'98999',98999),(99000,147,27,'990000',6,'999999',99999),(0,148,28,'00',2,'19',19999),(20000,149,28,'200',3,'599',59999),(60000,150,28,'60000',5,'69999',69999),(70000,151,28,'7000',4,'8499',84999),(85000,152,28,'85000',5,'89999',89999),(90000,153,28,'900000',6,'999999',99999),(0,154,29,'00',2,'14',14999),(15000,155,29,'15000',5,'19999',19999),(20000,156,29,'200',3,'699',69999),(70000,157,29,'7000',4,'8499',84999),(85000,158,29,'85000',5,'89999',89999),(90000,159,29,'9000',4,'9199',91999),(92000,160,29,'920000',6,'923999',92399),(92400,161,29,'92400',5,'92999',92999),(93000,162,29,'930000',6,'949999',94999),(95000,163,29,'95000',5,'96999',96999),(97000,164,29,'9700',4,'9999',99999),(0,165,30,'00',2,'19',19999),(20000,166,30,'200',3,'599',59999),(60000,167,30,'60000',5,'69999',69999),(70000,168,30,'7000',4,'8499',84999),(85000,169,30,'85000',5,'89999',89999),(90000,170,30,'900000',6,'979999',97999),(98000,171,30,'98000',5,'99999',99999),(0,172,31,'00',2,'29',29999),(30000,173,31,'300',3,'599',59999),(60000,174,31,'6000',4,'7999',79999),(80000,175,31,'80000',5,'89999',89999),(90000,176,31,'900000',6,'999999',99999),(0,177,32,'00',2,'29',29999),(40000,178,32,'400',3,'649',64999),(70000,179,32,'7000',4,'7999',79999),(85000,180,32,'85000',5,'94999',94999),(97000,181,32,'970000',6,'999999',99999),(0,182,33,'00',2,'19',19999),(20000,183,33,'200',3,'599',59999),(60000,184,33,'6000',4,'8499',84999),(85000,185,33,'85000',5,'89999',89999),(90000,186,33,'900000',6,'949999',94999),(95000,187,33,'95000',5,'99999',99999),(0,188,34,'00',2,'24',24999),(25000,189,34,'250',3,'549',54999),(55000,190,34,'5500',4,'8499',84999),(85000,191,34,'85000',5,'94999',94999),(95000,192,34,'950000',6,'969999',96999),(97000,193,34,'97000',5,'98999',98999),(99000,194,34,'990',3,'999',99999),(0,195,35,'00',2,'19',19999),(20000,196,35,'200',3,'499',49999),(50000,197,35,'5000',4,'6999',69999),(70000,198,35,'70000',5,'79999',79999),(80000,199,35,'800000',6,'849999',84999),(85000,200,35,'8500',4,'8999',89999),(90000,201,35,'90',2,'90',90999),(91000,202,35,'910000',6,'939999',93999),(94000,203,35,'94',2,'94',94999),(95000,204,35,'950000',6,'999999',99999),(0,205,36,'0',1,'1',19999),(20000,206,36,'20',2,'49',49999),(50000,207,36,'500',3,'649',64999),(70000,208,36,'7000',4,'7999',79999),(85000,209,36,'85000',5,'94999',94999),(97000,210,36,'970000',6,'999999',99999),(0,211,37,'0',1,'5',59999),(60000,212,37,'60',2,'79',79999),(80000,213,37,'800',3,'899',89999),(90000,214,37,'9000',4,'9499',94999),(95000,215,37,'95000',5,'98999',98999),(99000,216,37,'990000',6,'999999',99999),(0,217,38,'00',2,'09',9999),(10000,218,38,'100',3,'499',49999),(50000,219,38,'5000',4,'7999',79999),(80000,220,38,'80000',5,'94999',94999),(95000,221,38,'950000',6,'999999',99999),(0,222,39,'000',3,'599',59999),(60000,223,39,'6000',4,'8999',89999),(90000,224,39,'90000',5,'99999',99999),(0,225,40,'00',2,'49',49999),(50000,226,40,'500',3,'899',89999),(90000,227,40,'9000',4,'9899',98999),(99000,228,40,'99000',5,'99999',99999),(0,229,41,'0',1,'1',19999),(20000,230,41,'20',2,'54',54999),(55000,231,41,'550',3,'889',88999),(89000,232,41,'8900',4,'9499',94999),(95000,233,41,'95000',5,'99999',99999),(0,234,42,'00',2,'19',19999),(20000,235,42,'200',3,'499',49999),(50000,236,42,'5000',4,'5999',59999),(60000,237,42,'60',2,'65',65999),(66000,238,42,'6600',4,'6699',66999),(67000,239,42,'67000',5,'69999',69999),(70000,240,42,'7000',4,'7999',79999),(80000,241,42,'80',2,'94',94999),(95000,242,42,'9500',4,'9899',98999),(99000,243,42,'99000',5,'99999',99999),(0,244,43,'0',1,'0',9999),(10000,245,43,'10',2,'14',14999),(15000,246,43,'150',3,'549',54999),(55000,247,43,'55000',5,'59999',59999),(60000,248,43,'6000',4,'9499',94999),(95000,249,43,'95000',5,'99999',99999),(0,250,44,'00',2,'28',28999),(29000,251,44,'2900',4,'2999',29999),(30000,252,44,'300',3,'799',79999),(80000,253,44,'8000',4,'8999',89999),(90000,254,44,'90000',5,'92999',92999),(93000,255,44,'9300',4,'9999',99999),(0,256,45,'0000',4,'1999',19999),(20000,257,45,'20',2,'49',49999),(50000,258,45,'50000',5,'54999',54999),(55000,259,45,'550',3,'799',79999),(80000,260,45,'8000',4,'9499',94999),(95000,261,45,'95000',5,'99999',99999),(0,262,46,'00',2,'19',19999),(20000,263,46,'200',3,'699',69999),(70000,264,46,'7000',4,'9999',99999),(0,265,47,'00',2,'02',2999),(3000,266,47,'0300',4,'0499',4999),(5000,267,47,'05',2,'19',19999),(20000,268,47,'2000',4,'2099',20999),(21000,269,47,'21',2,'27',27999),(28000,270,47,'28000',5,'30999',30999),(31000,271,47,'31',2,'43',43999),(44000,272,47,'440',3,'819',81999),(82000,273,47,'8200',4,'9699',96999),(97000,274,47,'97000',5,'99999',99999),(0,275,48,'00',2,'56',56999),(57000,276,48,'57000',5,'59999',59999),(60000,277,48,'600',3,'799',79999),(80000,278,48,'8000',4,'9499',94999),(95000,279,48,'95000',5,'99999',99999),(0,280,49,'00',2,'19',19999),(20000,281,49,'200',3,'699',69999),(70000,282,49,'7000',4,'8499',84999),(85000,283,49,'85000',5,'99999',99999),(0,284,50,'00',2,'19',19999),(20000,285,50,'200',3,'659',65999),(66000,286,50,'6600',4,'6899',68999),(69000,287,50,'690',3,'699',69999),(70000,288,50,'7000',4,'8499',84999),(85000,289,50,'85000',5,'92999',92999),(93000,290,50,'93',2,'93',93999),(94000,291,50,'9400',4,'9799',97999),(98000,292,50,'98000',5,'99999',99999),(0,293,51,'00',2,'19',19999),(20000,294,51,'200',3,'599',59999),(60000,295,51,'6000',4,'8999',89999),(90000,296,51,'90000',5,'94999',94999),(0,297,52,'00',2,'19',19999),(20000,298,52,'200',3,'699',69999),(70000,299,52,'7000',4,'8499',84999),(85000,300,52,'85000',5,'86999',86999),(87000,301,52,'8700',4,'8999',89999),(90000,302,52,'900',3,'999',99999),(0,303,53,'00',2,'19',19999),(20000,304,53,'200',3,'699',69999),(70000,305,53,'7000',4,'8499',84999),(85000,306,53,'85000',5,'89999',89999),(90000,307,53,'9000',4,'9999',99999),(0,308,54,'00',2,'14',14999),(15000,309,54,'150',3,'249',24999),(25000,310,54,'2500',4,'2999',29999),(30000,311,54,'300',3,'549',54999),(55000,312,54,'5500',4,'8999',89999),(90000,313,54,'90000',5,'96999',96999),(97000,314,54,'970',3,'989',98999),(99000,315,54,'9900',4,'9999',99999),(0,316,55,'00',2,'19',19999),(20000,317,55,'200',3,'599',59999),(70000,318,55,'7000',4,'7999',79999),(90000,319,55,'90000',5,'99999',99999),(0,320,56,'00',2,'14',14999),(15000,321,56,'1500',4,'1699',16999),(17000,322,56,'170',3,'199',19999),(20000,323,56,'2000',4,'2999',29999),(30000,324,56,'300',3,'699',69999),(70000,325,56,'7000',4,'8999',89999),(90000,326,56,'90000',5,'99999',99999),(0,327,57,'00',2,'00',999),(1000,328,57,'0100',4,'0999',9999),(10000,329,57,'10000',5,'19999',19999),(30000,330,57,'300',3,'499',49999),(50000,331,57,'5000',4,'5999',59999),(60000,332,57,'60',2,'89',89999),(90000,333,57,'900',3,'989',98999),(99000,334,57,'9900',4,'9989',99899),(99900,335,57,'99900',5,'99999',99999),(1000,336,58,'01',2,'39',39999),(40000,337,58,'400',3,'499',49999),(50000,338,58,'5000',4,'7999',79999),(80000,339,58,'800',3,'899',89999),(90000,340,58,'9000',4,'9999',99999),(0,341,59,'0',1,'1',19999),(20000,342,59,'20',2,'39',39999),(40000,343,59,'400',3,'799',79999),(80000,344,59,'8000',4,'9999',99999),(1000,345,60,'01',2,'59',59999),(60000,346,60,'600',3,'899',89999),(90000,347,60,'9000',4,'9099',90999),(91000,348,60,'91000',5,'96999',96999),(97000,349,60,'9700',4,'9999',99999),(0,350,61,'000',3,'015',1599),(1600,351,61,'0160',4,'0199',1999),(2000,352,61,'02',2,'02',2999),(3000,353,61,'0300',4,'0599',5999),(6000,354,61,'06',2,'09',9999),(10000,355,61,'10',2,'49',49999),(50000,356,61,'500',3,'849',84999),(85000,357,61,'8500',4,'9099',90999),(91000,358,61,'91000',5,'98999',98999),(99000,359,61,'9900',4,'9999',99999),(0,360,62,'0',1,'1',19999),(20000,361,62,'20',2,'54',54999),(55000,362,62,'550',3,'799',79999),(80000,363,62,'8000',4,'9499',94999),(95000,364,62,'95000',5,'99999',99999),(0,365,63,'0',1,'0',9999),(10000,366,63,'100',3,'169',16999),(17000,367,63,'1700',4,'1999',19999),(20000,368,63,'20',2,'54',54999),(55000,369,63,'550',3,'759',75999),(76000,370,63,'7600',4,'8499',84999),(85000,371,63,'85000',5,'88999',88999),(89000,372,63,'8900',4,'9499',94999),(95000,373,63,'95000',5,'99999',99999),(0,374,64,'00',2,'19',19999),(20000,375,64,'200',3,'699',69999),(70000,376,64,'7000',4,'8499',84999),(85000,377,64,'85000',5,'89999',89999),(90000,378,64,'90000',5,'94999',94999),(95000,379,64,'9500',4,'9999',99999),(0,380,65,'00000',5,'01999',1999),(2000,381,65,'02',2,'24',24999),(25000,382,65,'250',3,'599',59999),(60000,383,65,'6000',4,'9199',91999),(92000,384,65,'92000',5,'98999',98999),(99000,385,65,'990',3,'999',99999),(0,386,66,'0',1,'3',39999),(40000,387,66,'40',2,'59',59999),(60000,388,66,'600',3,'799',79999),(80000,389,66,'8000',4,'9499',94999),(95000,390,66,'95000',5,'99999',99999),(0,391,67,'00',2,'19',19999),(20000,392,67,'200',3,'499',49999),(50000,393,67,'5000',4,'6999',69999),(70000,394,67,'700',3,'999',99999),(0,395,68,'000',3,'199',19999),(20000,396,68,'2000',4,'2999',29999),(30000,397,68,'30000',5,'79999',79999),(80000,398,68,'8000',4,'8999',89999),(90000,399,68,'900',3,'999',99999),(0,400,69,'000',3,'099',9999),(10000,401,69,'1000',4,'1499',14999),(15000,402,69,'15000',5,'19999',19999),(20000,403,69,'20',2,'29',29999),(30000,404,69,'3000',4,'3999',39999),(40000,405,69,'400',3,'799',79999),(80000,406,69,'8000',4,'9499',94999),(95000,407,69,'95000',5,'99999',99999),(0,408,70,'00',2,'19',19999),(20000,409,70,'200',3,'599',59999),(60000,410,70,'6000',4,'9999',99999),(0,411,71,'00',2,'11',11999),(12000,412,71,'1200',4,'1999',19999),(20000,413,71,'200',3,'289',28999),(29000,414,71,'2900',4,'9999',99999),(0,415,72,'00',2,'09',9999),(10000,416,72,'100',3,'699',69999),(70000,417,72,'70',2,'89',89999),(90000,418,72,'9000',4,'9799',97999),(98000,419,72,'98000',5,'99999',99999),(0,420,73,'00',2,'01',1999),(2000,421,73,'020',3,'199',19999),(20000,422,73,'2000',4,'3999',39999),(40000,423,73,'40000',5,'44999',44999),(45000,424,73,'45',2,'49',49999),(50000,425,73,'50',2,'79',79999),(80000,426,73,'800',3,'899',89999),(90000,427,73,'9000',4,'9899',98999),(99000,428,73,'99000',5,'99999',99999),(0,429,74,'00',2,'39',39999),(40000,430,74,'400',3,'799',79999),(80000,431,74,'8000',4,'8999',89999),(90000,432,74,'90000',5,'99999',99999),(0,433,75,'00',2,'39',39999),(40000,434,75,'400',3,'599',59999),(60000,435,75,'6000',4,'8999',89999),(90000,436,75,'90000',5,'99999',99999),(0,437,76,'00',2,'11',11999),(12000,438,76,'120',3,'559',55999),(56000,439,76,'5600',4,'7999',79999),(80000,440,76,'80000',5,'99999',99999),(0,441,77,'00',2,'09',9999),(10000,442,77,'1000',4,'1999',19999),(20000,443,77,'20000',5,'29999',29999),(30000,444,77,'30',2,'49',49999),(50000,445,77,'500',3,'899',89999),(90000,446,77,'9000',4,'9499',94999),(95000,447,77,'95000',5,'99999',99999),(0,448,78,'00',2,'14',14999),(15000,449,78,'15000',5,'16999',16999),(17000,450,78,'17000',5,'19999',19999),(20000,451,78,'200',3,'799',79999),(80000,452,78,'8000',4,'9699',96999),(97000,453,78,'97000',5,'99999',99999),(0,454,79,'0',1,'1',19999),(20000,455,79,'20',2,'54',54999),(55000,456,79,'550',3,'799',79999),(80000,457,79,'8000',4,'9499',94999),(95000,458,79,'95000',5,'99999',99999),(0,459,80,'00',2,'09',9999),(10000,460,80,'100',3,'399',39999),(40000,461,80,'4000',4,'4999',49999),(0,462,81,'00',2,'09',9999),(10000,463,81,'100',3,'399',39999),(40000,464,81,'4000',4,'4999',49999),(0,465,82,'0',1,'3',39999),(40000,466,82,'40',2,'54',54999),(55000,467,82,'550',3,'799',79999),(80000,468,82,'8000',4,'9999',99999),(0,469,83,'00',2,'49',49999),(50000,470,83,'500',3,'939',93999),(94000,471,83,'9400',4,'9999',99999),(0,472,84,'00',2,'29',29999),(30000,473,84,'300',3,'899',89999),(90000,474,84,'9000',4,'9999',99999),(0,475,85,'00',2,'39',39999),(40000,476,85,'400',3,'849',84999),(85000,477,85,'8500',4,'9999',99999),(0,478,86,'0',1,'0',9999),(10000,479,86,'10',2,'39',39999),(40000,480,86,'400',3,'899',89999),(90000,481,86,'9000',4,'9999',99999),(0,482,87,'0',1,'0',9999),(10000,483,87,'10',2,'49',49999),(50000,484,87,'500',3,'799',79999),(80000,485,87,'8000',4,'9999',99999),(0,486,88,'0',1,'0',9999),(10000,487,88,'10',2,'39',39999),(40000,488,88,'400',3,'899',89999),(90000,489,88,'9000',4,'9999',99999),(0,490,89,'0',1,'1',19999),(20000,491,89,'20',2,'39',39999),(40000,492,89,'400',3,'799',79999),(80000,493,89,'8000',4,'9999',99999),(0,494,90,'0',1,'2',29999),(30000,495,90,'30',2,'49',49999),(50000,496,90,'500',3,'799',79999),(80000,497,90,'8000',4,'9999',99999),(0,498,91,'00',2,'79',79999),(80000,499,91,'800',3,'949',94999),(95000,500,91,'9500',4,'9999',99999),(0,501,92,'0',1,'4',49999),(50000,502,92,'50',2,'79',79999),(80000,503,92,'800',3,'899',89999),(90000,504,92,'9000',4,'9999',99999),(0,505,93,'0',1,'1',19999),(20000,506,93,'20',2,'49',49999),(50000,507,93,'500',3,'899',89999),(90000,508,93,'9000',4,'9999',99999),(0,509,94,'0',1,'0',9999),(10000,510,94,'10',2,'39',39999),(40000,511,94,'400',3,'899',89999),(90000,512,94,'9000',4,'9999',99999),(0,513,95,'00',2,'89',89999),(90000,514,95,'900',3,'984',98499),(98500,515,95,'9850',4,'9999',99999),(0,516,96,'00',2,'29',29999),(30000,517,96,'300',3,'399',39999),(40000,518,96,'4000',4,'9999',99999),(0,519,97,'0000',4,'0999',9999),(10000,520,97,'100',3,'499',49999),(50000,521,97,'5000',4,'5999',59999),(60000,522,97,'60',2,'69',69999),(70000,523,97,'700',3,'799',79999),(80000,524,97,'80',2,'89',89999),(90000,525,97,'900',3,'999',99999),(0,526,98,'00',2,'00',999),(1000,527,98,'010',3,'079',7999),(8000,528,98,'08',2,'39',39999),(40000,529,98,'400',3,'569',56999),(57000,530,98,'57',2,'57',57999),(58000,531,98,'580',3,'849',84999),(85000,532,98,'8500',4,'9999',99999),(0,533,99,'0',1,'1',19999),(20000,534,99,'20',2,'39',39999),(40000,535,99,'400',3,'899',89999),(90000,536,99,'9000',4,'9999',99999),(0,537,100,'0',1,'1',19999),(20000,538,100,'20',2,'79',79999),(80000,539,100,'800',3,'999',99999),(0,540,101,'00',2,'39',39999),(40000,541,101,'400',3,'849',84999),(85000,542,101,'8500',4,'9999',99999),(0,543,102,'0',1,'0',9999),(10000,544,102,'10',2,'39',39999),(40000,545,102,'400',3,'899',89999),(90000,546,102,'9000',4,'9999',99999),(0,547,103,'00',2,'29',29999),(30000,548,103,'300',3,'849',84999),(85000,549,103,'8500',4,'9999',99999),(0,550,104,'00',2,'39',39999),(40000,551,104,'400',3,'849',84999),(85000,552,104,'8500',4,'9999',99999),(0,553,105,'0',1,'1',19999),(20000,554,105,'20',2,'39',39999),(40000,555,105,'400',3,'799',79999),(80000,556,105,'8000',4,'9999',99999),(0,557,106,'0',1,'0',9999),(10000,558,106,'10',2,'39',39999),(40000,559,106,'400',3,'599',59999),(60000,560,106,'60',2,'89',89999),(90000,561,106,'9000',4,'9999',99999),(0,562,107,'0',1,'1',19999),(20000,563,107,'20',2,'39',39999),(40000,564,107,'400',3,'799',79999),(80000,565,107,'8000',4,'9999',99999),(0,566,108,'00',2,'39',39999),(40000,567,108,'400',3,'929',92999),(93000,568,108,'9300',4,'9999',99999),(0,569,109,'0',1,'0',9999),(10000,570,109,'10',2,'39',39999),(40000,571,109,'400',3,'899',89999),(90000,572,109,'9000',4,'9999',99999),(0,573,110,'00',2,'39',39999),(40000,574,110,'400',3,'699',69999),(70000,575,110,'70',2,'84',84999),(85000,576,110,'8500',4,'8799',87999),(88000,577,110,'88',2,'99',99999),(0,578,111,'0',1,'0',9999),(10000,579,111,'10',2,'18',18999),(19000,580,111,'1900',4,'1999',19999),(20000,581,111,'20',2,'49',49999),(50000,582,111,'500',3,'899',89999),(90000,583,111,'9000',4,'9999',99999),(0,584,112,'0',1,'1',19999),(20000,585,112,'20',2,'79',79999),(80000,586,112,'800',3,'949',94999),(95000,587,112,'9500',4,'9999',99999),(0,588,113,'00',2,'59',59999),(60000,589,113,'600',3,'899',89999),(90000,590,113,'9000',4,'9999',99999),(0,591,114,'0',1,'2',29999),(30000,592,114,'30',2,'69',69999),(70000,593,114,'700',3,'949',94999),(95000,594,114,'9500',4,'9999',99999),(0,595,115,'00',2,'54',54999),(55000,596,115,'5500',4,'5599',55999),(56000,597,115,'56',2,'59',59999),(60000,598,115,'600',3,'849',84999),(85000,599,115,'8500',4,'9999',99999),(0,600,116,'0',1,'2',29999),(30000,601,116,'30',2,'54',54999),(55000,602,116,'550',3,'734',73499),(73500,603,116,'7350',4,'7499',74999),(75000,604,116,'7500',4,'9999',99999),(0,605,117,'0',1,'6',69999),(70000,606,117,'70',2,'94',94999),(95000,607,117,'950',3,'999',99999),(0,608,118,'00',2,'39',39999),(40000,609,118,'400',3,'899',89999),(90000,610,118,'9000',4,'9999',99999),(0,611,119,'000',3,'149',14999),(15000,612,119,'1500',4,'1999',19999),(20000,613,119,'20',2,'69',69999),(70000,614,119,'7000',4,'7499',74999),(75000,615,119,'750',3,'959',95999),(96000,616,119,'9600',4,'9999',99999),(0,617,120,'00',2,'39',39999),(40000,618,120,'400',3,'899',89999),(90000,619,120,'9000',4,'9999',99999),(0,620,121,'00',2,'49',49999),(50000,621,121,'500',3,'939',93999),(94000,622,121,'9400',4,'9999',99999),(0,623,122,'00',2,'39',39999),(40000,624,122,'400',3,'899',89999),(90000,625,122,'9000',4,'9999',99999),(0,626,123,'0',1,'5',59999),(60000,627,123,'60',2,'89',89999),(90000,628,123,'900',3,'989',98999),(99000,629,123,'9900',4,'9999',99999),(0,630,124,'00',2,'09',9999),(10000,631,124,'1',1,'1',19999),(20000,632,124,'200',3,'249',24999),(25000,633,124,'2500',4,'2999',29999),(30000,634,124,'30',2,'59',59999),(60000,635,124,'600',3,'899',89999),(90000,636,124,'9000',4,'9999',99999),(0,637,125,'00',2,'05',5999),(6000,638,125,'060',3,'089',8999),(9000,639,125,'0900',4,'0999',9999),(10000,640,125,'10',2,'69',69999),(70000,641,125,'700',3,'969',96999),(97000,642,125,'9700',4,'9999',99999),(0,643,126,'0',1,'2',29999),(30000,644,126,'30',2,'54',54999),(55000,645,126,'550',3,'749',74999),(75000,646,126,'7500',4,'9499',94999),(95000,647,126,'95',2,'99',99999),(0,648,127,'0',1,'0',9999),(10000,649,127,'100',3,'399',39999),(40000,650,127,'4000',4,'4499',44999),(45000,651,127,'45',2,'89',89999),(90000,652,127,'900',3,'949',94999),(95000,653,127,'9500',4,'9999',99999),(0,654,128,'0',1,'5',59999),(60000,655,128,'60',2,'89',89999),(90000,656,128,'900',3,'989',98999),(99000,657,128,'9900',4,'9999',99999),(0,658,129,'00',2,'89',89999),(90000,659,129,'900',3,'989',98999),(99000,660,129,'9900',4,'9999',99999),(0,661,130,'00',2,'29',29999),(30000,662,130,'300',3,'399',39999),(40000,663,130,'40',2,'94',94999),(95000,664,130,'950',3,'989',98999),(99000,665,130,'9900',4,'9999',99999),(0,666,131,'0',1,'4',49999),(50000,667,131,'50',2,'64',64999),(65000,668,131,'650',3,'659',65999),(66000,669,131,'66',2,'75',75999),(76000,670,131,'760',3,'899',89999),(90000,671,131,'9000',4,'9999',99999),(0,672,132,'0',1,'3',39999),(40000,673,132,'40',2,'89',89999),(90000,674,132,'900',3,'989',98999),(99000,675,132,'9900',4,'9999',99999),(0,676,133,'00',2,'09',9999),(10000,677,133,'100',3,'159',15999),(16000,678,133,'1600',4,'1999',19999),(20000,679,133,'20',2,'79',79999),(80000,680,133,'800',3,'949',94999),(95000,681,133,'9500',4,'9999',99999),(0,682,134,'00',2,'79',79999),(80000,683,134,'800',3,'989',98999),(99000,684,134,'9900',4,'9999',99999),(80000,685,135,'80',2,'94',94999),(95000,686,135,'950',3,'989',98999),(99000,687,135,'9900',4,'9999',99999),(0,688,136,'00',2,'49',49999),(50000,689,136,'500',3,'899',89999),(90000,690,136,'9000',4,'9999',99999),(0,691,137,'0',1,'4',49999),(50000,692,137,'50',2,'79',79999),(80000,693,137,'800',3,'899',89999),(90000,694,137,'9000',4,'9999',99999),(0,695,138,'00',2,'39',39999),(40000,696,138,'400',3,'899',89999),(90000,697,138,'9000',4,'9399',93999),(94000,698,138,'940',3,'969',96999),(97000,699,138,'97',2,'99',99999),(0,700,139,'00',2,'39',39999),(40000,701,139,'400',3,'879',87999),(88000,702,139,'8800',4,'9999',99999),(0,703,140,'0',1,'2',29999),(30000,704,140,'30',2,'54',54999),(55000,705,140,'550',3,'749',74999),(75000,706,140,'7500',4,'9999',99999),(0,707,141,'0',1,'0',9999),(10000,708,141,'100',3,'199',19999),(20000,709,141,'2000',4,'2999',29999),(30000,710,141,'30',2,'59',59999),(60000,711,141,'600',3,'949',94999),(95000,712,141,'9500',4,'9999',99999),(0,713,142,'00',2,'49',49999),(50000,714,142,'500',3,'799',79999),(80000,715,142,'80',2,'99',99999),(0,716,144,'0',1,'1',19999),(20000,717,144,'20',2,'89',89999),(90000,718,144,'900',3,'999',99999),(0,719,145,'0',1,'5',59999),(60000,720,145,'60',2,'89',89999),(90000,721,145,'900',3,'999',99999),(0,722,146,'0',1,'3',39999),(40000,723,146,'40',2,'79',79999),(80000,724,146,'800',3,'999',99999),(0,725,147,'0',1,'2',29999),(30000,726,147,'30',2,'59',59999),(60000,727,147,'600',3,'699',69999),(70000,728,147,'70',2,'89',89999),(90000,729,147,'90',2,'94',94999),(95000,730,147,'950',3,'999',99999),(0,731,148,'0',1,'0',9999),(10000,732,148,'10',2,'89',89999),(90000,733,148,'900',3,'999',99999),(0,734,149,'0',1,'3',39999),(40000,735,149,'40',2,'94',94999),(95000,736,149,'950',3,'999',99999),(0,737,150,'0',1,'2',29999),(30000,738,150,'30',2,'89',89999),(90000,739,150,'900',3,'999',99999),(0,740,151,'00',2,'59',59999),(60000,741,151,'600',3,'999',99999),(0,742,152,'0',1,'3',39999),(40000,743,152,'400',3,'599',59999),(60000,744,152,'60',2,'89',89999),(90000,745,152,'900',3,'999',99999),(0,746,153,'0',1,'2',29999),(30000,747,153,'30',2,'35',35999),(60000,748,153,'600',3,'604',60499),(0,749,154,'0',1,'4',49999),(50000,750,154,'50',2,'89',89999),(90000,751,154,'900',3,'999',99999),(0,752,155,'0',1,'4',49999),(50000,753,155,'50',2,'79',79999),(80000,754,155,'800',3,'999',99999),(0,755,156,'0',1,'2',29999),(30000,756,156,'30',2,'69',69999),(70000,757,156,'700',3,'999',99999),(0,758,157,'0',1,'2',29999),(30000,759,157,'30',2,'89',89999),(90000,760,157,'900',3,'999',99999),(0,761,158,'0',1,'3',39999),(40000,762,158,'40',2,'79',79999),(80000,763,158,'800',3,'999',99999),(0,764,159,'0',1,'2',29999),(30000,765,159,'300',3,'399',39999),(40000,766,159,'40',2,'69',69999),(90000,767,159,'900',3,'999',99999),(0,768,160,'0',1,'4',49999),(50000,769,160,'50',2,'89',89999),(90000,770,160,'900',3,'999',99999),(0,771,161,'0',1,'1',19999),(20000,772,161,'20',2,'69',69999),(70000,773,161,'700',3,'799',79999),(80000,774,161,'8',1,'8',89999),(90000,775,161,'90',2,'99',99999),(0,776,162,'0',1,'3',39999),(40000,777,162,'40',2,'69',69999),(70000,778,162,'700',3,'999',99999),(0,779,163,'0',1,'1',19999),(20000,780,163,'20',2,'79',79999),(80000,781,163,'800',3,'999',99999),(0,782,164,'0',1,'1',19999),(20000,783,164,'20',2,'79',79999),(80000,784,164,'800',3,'999',99999),(0,785,165,'0',1,'3',39999),(40000,786,165,'40',2,'79',79999),(80000,787,165,'800',3,'999',99999),(0,788,166,'0',1,'0',9999),(10000,789,166,'10',2,'59',59999),(60000,790,166,'600',3,'999',99999),(0,791,167,'0',1,'2',29999),(30000,792,167,'30',2,'59',59999),(60000,793,167,'600',3,'999',99999),(0,794,168,'0',1,'0',9999),(10000,795,168,'10',2,'79',79999),(80000,796,168,'800',3,'999',99999),(0,797,169,'0',1,'4',49999),(50000,798,169,'50',2,'79',79999),(80000,799,169,'800',3,'999',99999),(0,800,170,'0',1,'4',49999),(50000,801,170,'50',2,'79',79999),(80000,802,170,'800',3,'999',99999),(0,803,171,'0',1,'4',49999),(50000,804,171,'50',2,'79',79999),(80000,805,171,'800',3,'999',99999),(0,806,172,'0',1,'0',9999),(10000,807,172,'10',2,'59',59999),(60000,808,172,'600',3,'699',69999),(70000,809,172,'7',1,'7',79999),(80000,810,172,'80',2,'99',99999),(0,811,173,'0',1,'2',29999),(30000,812,173,'30',2,'59',59999),(60000,813,173,'600',3,'999',99999),(0,814,174,'0',1,'1',19999),(20000,815,174,'20',2,'79',79999),(80000,816,174,'800',3,'999',99999),(0,817,175,'0',1,'2',29999),(30000,818,175,'30',2,'59',59999),(60000,819,175,'600',3,'699',69999),(70000,820,175,'7',1,'8',89999),(90000,821,175,'90',2,'99',99999),(0,822,176,'0',1,'0',9999),(10000,823,176,'10',2,'59',59999),(60000,824,176,'600',3,'999',99999),(0,825,177,'0',1,'1',19999),(20000,826,177,'20',2,'59',59999),(60000,827,177,'600',3,'999',99999),(0,828,178,'0',1,'1',19999),(20000,829,178,'20',2,'59',59999),(60000,830,178,'600',3,'899',89999),(90000,831,178,'90',2,'99',99999),(0,832,179,'0',1,'5',59999),(60000,833,179,'60',2,'89',89999),(90000,834,179,'900',3,'999',99999),(0,835,180,'0',1,'0',9999),(10000,836,180,'10',2,'69',69999),(70000,837,180,'700',3,'999',99999),(0,838,181,'0',1,'2',29999),(30000,839,181,'30',2,'79',79999),(80000,840,181,'800',3,'999',99999),(0,841,182,'0',1,'4',49999),(50000,842,182,'50',2,'79',79999),(80000,843,182,'800',3,'999',99999),(0,844,183,'0',1,'2',29999),(30000,845,183,'30',2,'59',59999),(60000,846,183,'600',3,'999',99999),(0,847,184,'0',1,'4',49999),(50000,848,184,'50',2,'79',79999),(80000,849,184,'800',3,'999',99999),(0,850,185,'0',1,'5',59999),(60000,851,185,'60',2,'89',89999),(90000,852,185,'900',3,'999',99999),(0,853,186,'0',1,'2',29999),(30000,854,186,'30',2,'59',59999),(60000,855,186,'600',3,'999',99999),(0,856,187,'0',1,'2',29999),(30000,857,187,'30',2,'69',69999),(70000,858,187,'700',3,'999',99999),(0,859,188,'0',1,'4',49999),(50000,860,188,'50',2,'79',79999),(80000,861,188,'800',3,'999',99999),(0,862,189,'0',1,'1',19999),(20000,863,189,'20',2,'89',89999),(90000,864,189,'900',3,'999',99999),(0,865,190,'0',1,'4',49999),(50000,866,190,'50',2,'79',79999),(80000,867,190,'800',3,'999',99999),(0,868,192,'0',1,'4',49999),(50000,869,192,'50',2,'79',79999),(80000,870,192,'800',3,'999',99999),(0,871,193,'0',1,'2',29999),(30000,872,193,'30',2,'79',79999),(80000,873,193,'800',3,'939',93999),(94000,874,193,'94',2,'99',99999),(0,875,194,'0',1,'2',29999),(30000,876,194,'30',2,'69',69999),(70000,877,194,'700',3,'999',99999),(0,878,195,'0',1,'1',19999),(20000,879,195,'20',2,'59',59999),(60000,880,195,'600',3,'799',79999),(80000,881,195,'80',2,'89',89999),(90000,882,195,'90',2,'99',99999),(0,883,196,'00',2,'59',59999),(60000,884,196,'600',3,'859',85999),(86000,885,196,'86',2,'99',99999),(0,886,197,'0',1,'1',19999),(20000,887,197,'20',2,'79',79999),(80000,888,197,'800',3,'999',99999),(0,889,198,'0',1,'4',49999),(50000,890,198,'50',2,'94',94999),(95000,891,198,'950',3,'999',99999),(0,892,199,'0',1,'2',29999),(30000,893,199,'30',2,'59',59999),(60000,894,199,'600',3,'999',99999),(0,895,200,'0',1,'0',9999),(10000,896,200,'10',2,'94',94999),(95000,897,200,'950',3,'999',99999),(0,898,201,'0',1,'3',39999),(40000,899,201,'40',2,'89',89999),(90000,900,201,'900',3,'999',99999),(0,901,202,'0',1,'4',49999),(50000,902,202,'50',2,'79',79999),(80000,903,202,'800',3,'999',99999),(0,904,203,'00',2,'49',49999),(50000,905,203,'500',3,'999',99999),(0,906,204,'0',1,'1',19999),(20000,907,204,'20',2,'79',79999),(80000,908,204,'800',3,'999',99999),(0,909,205,'0',1,'3',39999),(40000,910,205,'40',2,'79',79999),(80000,911,205,'800',3,'999',99999),(0,912,206,'0',1,'2',29999),(30000,913,206,'30',2,'69',69999),(70000,914,206,'700',3,'799',79999),(0,915,207,'0',1,'1',19999),(20000,916,207,'20',2,'59',59999),(60000,917,207,'600',3,'899',89999),(0,918,208,'0',1,'3',39999),(40000,919,208,'400',3,'599',59999),(60000,920,208,'60',2,'89',89999),(90000,921,208,'900',3,'999',99999),(0,922,209,'00',2,'19',19999),(20000,923,209,'200',3,'699',69999),(70000,924,209,'7000',4,'8999',89999),(90000,925,209,'90000',5,'97599',97599),(97600,926,209,'976000',6,'999999',99999);
/*!40000 ALTER TABLE `ezisbn_registrant_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword`
--

DROP TABLE IF EXISTS `ezkeyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword` (
  `class_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezkeyword_keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword`
--

LOCK TABLES `ezkeyword` WRITE;
/*!40000 ALTER TABLE `ezkeyword` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezkeyword_attribute_link`
--

DROP TABLE IF EXISTS `ezkeyword_attribute_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezkeyword_attribute_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL DEFAULT '0',
  `objectattribute_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezkeyword_attr_link_kid_oaid` (`keyword_id`,`objectattribute_id`),
  KEY `ezkeyword_attr_link_oaid` (`objectattribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezkeyword_attribute_link`
--

LOCK TABLES `ezkeyword_attribute_link` WRITE;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezkeyword_attribute_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezm_block`
--

DROP TABLE IF EXISTS `ezm_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezm_block` (
  `id` char(32) NOT NULL,
  `zone_id` char(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `overflow_id` char(32) DEFAULT NULL,
  `last_update` int(10) unsigned DEFAULT '0',
  `block_type` varchar(255) DEFAULT NULL,
  `fetch_params` longtext,
  `rotation_type` int(10) unsigned DEFAULT NULL,
  `rotation_interval` int(10) unsigned DEFAULT NULL,
  `is_removed` int(2) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezm_block__is_removed` (`is_removed`),
  KEY `ezm_block__node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezm_block`
--

LOCK TABLES `ezm_block` WRITE;
/*!40000 ALTER TABLE `ezm_block` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezm_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezm_pool`
--

DROP TABLE IF EXISTS `ezm_pool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezm_pool` (
  `block_id` char(32) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  `priority` int(10) unsigned DEFAULT '0',
  `ts_publication` int(11) DEFAULT '0',
  `ts_visible` int(10) unsigned DEFAULT '0',
  `ts_hidden` int(10) unsigned DEFAULT '0',
  `rotation_until` int(10) unsigned DEFAULT '0',
  `moved_to` char(32) DEFAULT NULL,
  PRIMARY KEY (`block_id`,`object_id`),
  KEY `ezm_pool__block_id__ts_publication__priority` (`block_id`,`ts_publication`,`priority`),
  KEY `ezm_pool__block_id__ts_visible` (`block_id`,`ts_visible`),
  KEY `ezm_pool__block_id__ts_hidden` (`block_id`,`ts_hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezm_pool`
--

LOCK TABLES `ezm_pool` WRITE;
/*!40000 ALTER TABLE `ezm_pool` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezm_pool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmedia`
--

DROP TABLE IF EXISTS `ezmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmedia` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `controls` varchar(50) DEFAULT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `has_controller` int(11) DEFAULT '0',
  `height` int(11) DEFAULT NULL,
  `is_autoplay` int(11) DEFAULT '0',
  `is_loop` int(11) DEFAULT '0',
  `mime_type` varchar(50) NOT NULL DEFAULT '',
  `original_filename` varchar(255) NOT NULL DEFAULT '',
  `pluginspage` varchar(255) DEFAULT NULL,
  `quality` varchar(50) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`contentobject_attribute_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmedia`
--

LOCK TABLES `ezmedia` WRITE;
/*!40000 ALTER TABLE `ezmedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmessage`
--

DROP TABLE IF EXISTS `ezmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmessage` (
  `body` longtext,
  `destination_address` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_sent` int(11) NOT NULL DEFAULT '0',
  `send_method` varchar(50) NOT NULL DEFAULT '',
  `send_time` varchar(50) NOT NULL DEFAULT '',
  `send_weekday` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmessage`
--

LOCK TABLES `ezmessage` WRITE;
/*!40000 ALTER TABLE `ezmessage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmodule_run`
--

DROP TABLE IF EXISTS `ezmodule_run`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmodule_run` (
  `function_name` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_data` longtext,
  `module_name` varchar(255) DEFAULT NULL,
  `workflow_process_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezmodule_run_workflow_process_id_s` (`workflow_process_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmodule_run`
--

LOCK TABLES `ezmodule_run` WRITE;
/*!40000 ALTER TABLE `ezmodule_run` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmodule_run` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezmultipricedata`
--

DROP TABLE IF EXISTS `ezmultipricedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezmultipricedata` (
  `contentobject_attr_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attr_version` int(11) NOT NULL DEFAULT '0',
  `currency_code` varchar(4) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '0',
  `value` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `ezmultipricedata_coa_id` (`contentobject_attr_id`),
  KEY `ezmultipricedata_coa_version` (`contentobject_attr_version`),
  KEY `ezmultipricedata_currency_code` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezmultipricedata`
--

LOCK TABLES `ezmultipricedata` WRITE;
/*!40000 ALTER TABLE `ezmultipricedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezmultipricedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznode_assignment`
--

DROP TABLE IF EXISTS `eznode_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznode_assignment` (
  `contentobject_id` int(11) DEFAULT NULL,
  `contentobject_version` int(11) DEFAULT NULL,
  `from_node_id` int(11) DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `op_code` int(11) NOT NULL DEFAULT '0',
  `parent_node` int(11) DEFAULT NULL,
  `parent_remote_id` varchar(100) NOT NULL DEFAULT '',
  `remote_id` varchar(100) NOT NULL DEFAULT '0',
  `sort_field` int(11) DEFAULT '1',
  `sort_order` int(11) DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  `is_hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `eznode_assignment_co_version` (`contentobject_version`),
  KEY `eznode_assignment_coid_cov` (`contentobject_id`,`contentobject_version`),
  KEY `eznode_assignment_is_main` (`is_main`),
  KEY `eznode_assignment_parent_node` (`parent_node`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznode_assignment`
--

LOCK TABLES `eznode_assignment` WRITE;
/*!40000 ALTER TABLE `eznode_assignment` DISABLE KEYS */;
INSERT INTO `eznode_assignment` VALUES (8,2,0,4,1,2,5,'','0',1,1,0,0),(42,1,0,5,1,2,5,'','0',9,1,0,0),(10,2,-1,6,1,2,44,'','0',9,1,0,0),(4,1,0,7,1,2,1,'','0',1,1,0,0),(12,1,0,8,1,2,5,'','0',1,1,0,0),(41,1,0,11,1,2,1,'','0',1,1,0,0),(45,1,-1,16,1,2,1,'','0',9,1,0,0),(49,1,0,27,1,2,43,'','0',9,1,0,0),(50,1,0,28,1,2,43,'','0',9,1,0,0),(51,1,0,29,1,2,43,'','0',9,1,0,0),(52,1,0,30,1,2,48,'','0',1,1,0,0),(56,1,0,34,1,2,1,'','0',2,0,0,0),(14,3,-1,38,1,2,13,'','0',1,1,0,0),(54,2,-1,39,1,2,58,'','0',1,1,0,0),(57,1,0,43,1,2,2,'07cdfd23373b17c6b337251c22b7ea57','0',8,1,0,0),(14,4,-1,45,1,2,13,'e5161a99f733200b9ed4e80f9c16187b','0',1,1,0,0),(62,1,0,51,1,2,2,'24a4ce2b7ebe12bf24242c24c389ff82','0',1,1,0,0),(63,1,0,52,1,2,63,'b631a31310b581fd44c08e13914bc849','0',1,1,0,0),(64,1,0,53,1,5,78,'f491d30a7d9bfd44a4bf913411b962bc','0',1,1,0,0),(65,1,-1,54,1,2,2,'e1d308937856e101ea6b0b5d9c6f5011','b2c90202e30519345dc52596b353df17',1,1,0,0),(66,1,-1,55,1,2,66,'a45e10ceb3b9725ce0ff38a78d105d65','4218be7b63353028ec2b1c5a146506e3',1,1,0,0),(67,1,-1,56,1,2,66,'aeb6839208af8431f3e73ba433d9f376','b6c95ab9bc2dbae54d70dbb17ee91755',1,1,0,0),(65,2,-1,57,1,5,76,'e1d308937856e101ea6b0b5d9c6f5011','0',1,1,0,0),(66,2,-1,59,1,2,66,'a45e10ceb3b9725ce0ff38a78d105d65','4218be7b63353028ec2b1c5a146506e3',1,1,0,0),(67,2,-1,60,1,5,77,'aeb6839208af8431f3e73ba433d9f376','0',1,1,0,0),(68,1,0,61,1,2,66,'7905841b0d9e40032598999fa6f00300','0',1,1,0,0),(68,1,0,62,0,2,63,'8642f80cc62404765c918821bc9af6aa','0',2,0,0,0),(68,2,-1,63,0,5,78,'8642f80cc62404765c918821bc9af6aa','0',1,1,0,0),(68,2,-1,64,1,2,66,'7905841b0d9e40032598999fa6f00300','0',1,1,0,0),(62,2,-1,65,1,5,76,'24a4ce2b7ebe12bf24242c24c389ff82','0',1,1,0,0),(57,2,-1,67,1,2,1,'f3e90596361e31d496d4026eb624c983','0',8,1,0,0),(70,1,0,69,1,5,53,'c5b9407476c50a01d5604559f1602aea','0',1,1,0,0),(71,1,0,70,1,5,79,'1bff2c7d5159364e0e0e0295ccf3ded2','0',1,1,0,0),(72,1,0,71,1,2,71,'4d7d0f131d8a359f205014d402f58525','0',1,1,0,0),(73,1,0,72,1,3,71,'44e2d5aa85dceab4e87d95cd7857b54b','0',1,1,0,0),(74,1,0,74,1,2,71,'9df7104665b75628e59ad9b6c1f2d442','0',2,0,0,0),(75,1,0,76,1,2,71,'1e871aaf39073925d0998942f193e57e','0',2,0,0,0),(74,2,-1,77,1,2,71,'9df7104665b75628e59ad9b6c1f2d442','0',2,0,0,0),(75,2,-1,78,1,2,71,'1e871aaf39073925d0998942f193e57e','0',2,0,0,0),(76,1,0,81,1,5,79,'61dbf5b2f26e1a2960dec527ec26b630','0',1,1,0,0),(77,1,0,82,1,2,63,'95c14b08c66bcdd68e863a4ab2535fed','0',1,1,0,0),(78,1,0,84,1,2,63,'30e96fead6a744c6e11a647b116e55ee','0',2,0,0,0),(78,2,-1,85,1,2,63,'30e96fead6a744c6e11a647b116e55ee','0',2,0,0,0),(79,1,0,86,1,2,2,'bf8c49aca044ddb7b68e7e18d7f351fb','0',1,1,0,0),(81,1,0,88,1,2,2,'fbaed7f46812f7f661bdd3e30757f828','0',1,1,0,0),(71,1,0,89,0,2,80,'b39ef0d672d03c3c85cbe2bf2118684a','0',2,0,0,0),(82,1,0,90,1,2,80,'98c50fd329aac8af14ac7318c5426823','0',1,1,0,0),(83,1,0,91,1,2,82,'c6f171854bdcfd0f5e99f49115f558b8','0',1,1,0,0),(84,1,0,93,1,2,82,'940ac0b86984a4cddcf169eddf9de294','0',2,0,0,0),(84,2,-1,94,1,2,82,'940ac0b86984a4cddcf169eddf9de294','0',2,0,0,0),(66,2,0,95,0,2,83,'a8f28bfd738701e65b84dcafd2d25f4a','0',2,0,0,0),(68,2,0,96,0,2,83,'4dd9bc7a7b66c1878bb8c530b1814ecb','0',2,0,0,0),(67,2,0,97,0,2,84,'8aaea6357c02aaca04b808121511dd5a','0',2,0,0,0),(85,1,0,98,1,2,84,'7a4634e7107deea703598a7a18e397c5','0',1,1,0,0),(85,2,-1,99,1,2,84,'7a4634e7107deea703598a7a18e397c5','0',1,1,0,0),(67,3,-1,100,1,2,77,'aeb6839208af8431f3e73ba433d9f376','0',1,1,0,0),(67,3,-1,101,0,2,84,'8aaea6357c02aaca04b808121511dd5a','0',1,1,0,0),(66,3,-1,103,1,2,66,'a45e10ceb3b9725ce0ff38a78d105d65','4218be7b63353028ec2b1c5a146506e3',1,1,0,0),(66,3,-1,104,0,2,83,'a8f28bfd738701e65b84dcafd2d25f4a','0',1,1,0,0),(68,3,-1,105,0,2,78,'8642f80cc62404765c918821bc9af6aa','0',1,1,0,0),(68,3,-1,106,1,2,66,'7905841b0d9e40032598999fa6f00300','0',1,1,0,0),(68,3,-1,107,0,2,83,'4dd9bc7a7b66c1878bb8c530b1814ecb','0',1,1,0,0),(64,2,-1,109,1,2,78,'f491d30a7d9bfd44a4bf913411b962bc','0',1,1,0,0),(67,4,-1,111,1,2,77,'aeb6839208af8431f3e73ba433d9f376','0',1,1,0,0),(67,4,-1,112,0,2,84,'8aaea6357c02aaca04b808121511dd5a','0',1,1,0,0),(68,4,-1,113,0,2,78,'8642f80cc62404765c918821bc9af6aa','0',1,1,0,0),(68,4,-1,114,1,2,66,'7905841b0d9e40032598999fa6f00300','0',1,1,0,0),(68,4,-1,115,0,2,83,'4dd9bc7a7b66c1878bb8c530b1814ecb','0',1,1,0,0),(66,4,-1,117,1,2,66,'a45e10ceb3b9725ce0ff38a78d105d65','4218be7b63353028ec2b1c5a146506e3',1,1,0,0),(66,4,-1,118,0,2,83,'a8f28bfd738701e65b84dcafd2d25f4a','0',1,1,0,0),(71,2,-1,119,1,2,79,'1bff2c7d5159364e0e0e0295ccf3ded2','0',1,1,0,0),(71,2,-1,120,0,2,80,'b39ef0d672d03c3c85cbe2bf2118684a','0',1,1,0,0),(63,4,0,121,0,2,83,'e17e95b9309b9c171b0f58147f5abe5b','0',2,0,0,0),(72,2,-1,122,1,2,71,'4d7d0f131d8a359f205014d402f58525','0',1,1,0,0),(74,3,-1,123,1,2,71,'9df7104665b75628e59ad9b6c1f2d442','0',2,0,0,0),(75,3,-1,124,1,2,71,'1e871aaf39073925d0998942f193e57e','0',2,0,0,0),(63,5,-1,126,1,2,83,'e17e95b9309b9c171b0f58147f5abe5b','0',1,1,0,0),(63,5,0,127,0,2,77,'86fb1bf652da2764ea57f51c2da17cda','0',2,0,0,0),(63,6,-1,128,0,2,77,'86fb1bf652da2764ea57f51c2da17cda','0',1,1,0,0),(63,6,-1,129,1,2,83,'e17e95b9309b9c171b0f58147f5abe5b','0',1,1,0,0),(86,1,0,130,1,2,51,'8f3db3cbcd280ad94a7e2ef6c03b8684','0',1,1,0,0),(87,1,0,131,1,2,91,'c097820388d5d88c53cd629047be62d7','0',1,1,0,0),(88,1,0,133,1,2,91,'a41a37ad1117cc93267fa5b2814ce8fb','0',2,0,0,0),(89,1,0,135,1,2,91,'2e5d84344593d9ef51f569c6cb454f37','0',2,0,0,0),(88,2,-1,136,1,2,91,'a41a37ad1117cc93267fa5b2814ce8fb','0',2,0,0,0),(89,2,-1,137,1,2,91,'2e5d84344593d9ef51f569c6cb454f37','0',2,0,0,0),(63,7,-1,138,0,2,77,'86fb1bf652da2764ea57f51c2da17cda','0',1,1,0,0),(63,7,-1,139,1,2,83,'e17e95b9309b9c171b0f58147f5abe5b','0',1,1,0,0),(89,3,-1,140,1,2,91,'2e5d84344593d9ef51f569c6cb454f37','0',2,0,0,0),(88,3,-1,141,1,2,91,'a41a37ad1117cc93267fa5b2814ce8fb','0',2,0,0,0),(79,2,-1,143,1,2,2,'bf8c49aca044ddb7b68e7e18d7f351fb','0',1,1,0,0),(90,1,0,144,1,2,76,'fe7f05e0d5ecd337612fcd9e8db6d9da','0',1,1,0,0),(67,5,-1,145,1,2,77,'aeb6839208af8431f3e73ba433d9f376','0',1,1,0,0),(67,5,-1,146,0,2,84,'8aaea6357c02aaca04b808121511dd5a','0',1,1,0,0),(63,8,-1,149,0,2,77,'86fb1bf652da2764ea57f51c2da17cda','0',1,1,0,0),(63,8,-1,150,1,2,83,'e17e95b9309b9c171b0f58147f5abe5b','0',1,1,0,0),(127,1,0,189,1,2,5,'d9e1aa7d187477642100e1e2689d308d','0',1,1,0,0),(128,1,0,190,1,2,133,'8b236a336b8f38bc46418e4fff544ca0','0',1,1,0,0),(129,1,0,191,1,2,133,'437d8c779e5b16c40fc01577bc7be625','0',1,1,0,0),(130,1,0,192,1,2,133,'6859c49cfbbe4611e4a314f4053e81b6','0',1,1,0,0),(131,1,0,193,1,2,134,'1af29271b82a27c55094247851fca4cb','0',9,1,0,0),(132,1,0,194,1,2,136,'b04909fcdc9e89eb95c9efddaf8f6256','0',9,1,0,0),(133,1,0,195,1,5,5,'0d6f636ebc5584e37e1140e8004f202e','0',1,1,0,0),(132,1,0,196,0,2,139,'447bc111acdaade14d28b99129bd28e8','0',2,0,0,0),(135,1,0,198,1,2,13,'96504b49ea22faa0ed996370fda30b59','0',1,1,0,0);
/*!40000 ALTER TABLE `eznode_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationcollection`
--

DROP TABLE IF EXISTS `eznotificationcollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationcollection` (
  `data_subject` longtext NOT NULL,
  `data_text` longtext NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `handler` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transport` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationcollection`
--

LOCK TABLES `eznotificationcollection` WRITE;
/*!40000 ALTER TABLE `eznotificationcollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationcollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationcollection_item`
--

DROP TABLE IF EXISTS `eznotificationcollection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationcollection_item` (
  `address` varchar(255) NOT NULL DEFAULT '',
  `collection_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationcollection_item`
--

LOCK TABLES `eznotificationcollection_item` WRITE;
/*!40000 ALTER TABLE `eznotificationcollection_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationcollection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eznotificationevent`
--

DROP TABLE IF EXISTS `eznotificationevent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eznotificationevent` (
  `data_int1` int(11) NOT NULL DEFAULT '0',
  `data_int2` int(11) NOT NULL DEFAULT '0',
  `data_int3` int(11) NOT NULL DEFAULT '0',
  `data_int4` int(11) NOT NULL DEFAULT '0',
  `data_text1` longtext NOT NULL,
  `data_text2` longtext NOT NULL,
  `data_text3` longtext NOT NULL,
  `data_text4` longtext NOT NULL,
  `event_type_string` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eznotificationevent`
--

LOCK TABLES `eznotificationevent` WRITE;
/*!40000 ALTER TABLE `eznotificationevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `eznotificationevent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezoperation_memento`
--

DROP TABLE IF EXISTS `ezoperation_memento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezoperation_memento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main` int(11) NOT NULL DEFAULT '0',
  `main_key` varchar(32) NOT NULL DEFAULT '',
  `memento_data` longtext NOT NULL,
  `memento_key` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`memento_key`),
  KEY `ezoperation_memento_memento_key_main` (`memento_key`,`main`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezoperation_memento`
--

LOCK TABLES `ezoperation_memento` WRITE;
/*!40000 ALTER TABLE `ezoperation_memento` DISABLE KEYS */;
INSERT INTO `ezoperation_memento` VALUES (2,1,'ab883fd8af06a6eb0c73396e03d8ba42','a:3:{s:11:\"module_name\";s:7:\"content\";s:14:\"operation_name\";s:7:\"publish\";s:8:\"loop_run\";a:5:{s:17:\"begin-transaction\";i:0;s:19:\"set-version-pending\";i:0;s:24:\"send-to-publishing-queue\";i:1;s:17:\"update-section-id\";i:0;s:11:\"pre_publish\";i:0;}}','ab883fd8af06a6eb0c73396e03d8ba42');
/*!40000 ALTER TABLE `ezoperation_memento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder`
--

DROP TABLE IF EXISTS `ezorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder` (
  `account_identifier` varchar(100) NOT NULL DEFAULT 'default',
  `created` int(11) NOT NULL DEFAULT '0',
  `data_text_1` longtext,
  `data_text_2` longtext,
  `email` varchar(150) DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ignore_vat` int(11) NOT NULL DEFAULT '0',
  `is_archived` int(11) NOT NULL DEFAULT '0',
  `is_temporary` int(11) NOT NULL DEFAULT '1',
  `order_nr` int(11) NOT NULL DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) DEFAULT '0',
  `status_modified` int(11) DEFAULT '0',
  `status_modifier_id` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_is_archived` (`is_archived`),
  KEY `ezorder_is_tmp` (`is_temporary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder`
--

LOCK TABLES `ezorder` WRITE;
/*!40000 ALTER TABLE `ezorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_item`
--

DROP TABLE IF EXISTS `ezorder_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_item` (
  `description` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_vat_inc` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `price` float DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `vat_value` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_item_order_id` (`order_id`),
  KEY `ezorder_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_item`
--

LOCK TABLES `ezorder_item` WRITE;
/*!40000 ALTER TABLE `ezorder_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_nr_incr`
--

DROP TABLE IF EXISTS `ezorder_nr_incr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_nr_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_nr_incr`
--

LOCK TABLES `ezorder_nr_incr` WRITE;
/*!40000 ALTER TABLE `ezorder_nr_incr` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_nr_incr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_status`
--

DROP TABLE IF EXISTS `ezorder_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_status_active` (`is_active`),
  KEY `ezorder_status_name` (`name`),
  KEY `ezorder_status_sid` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_status`
--

LOCK TABLES `ezorder_status` WRITE;
/*!40000 ALTER TABLE `ezorder_status` DISABLE KEYS */;
INSERT INTO `ezorder_status` VALUES (1,1,'Pending',1),(2,1,'Processing',2),(3,1,'Delivered',3);
/*!40000 ALTER TABLE `ezorder_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezorder_status_history`
--

DROP TABLE IF EXISTS `ezorder_status_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezorder_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `status_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezorder_status_history_mod` (`modified`),
  KEY `ezorder_status_history_oid` (`order_id`),
  KEY `ezorder_status_history_sid` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezorder_status_history`
--

LOCK TABLES `ezorder_status_history` WRITE;
/*!40000 ALTER TABLE `ezorder_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezorder_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpackage`
--

DROP TABLE IF EXISTS `ezpackage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpackage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `install_date` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `version` varchar(30) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpackage`
--

LOCK TABLES `ezpackage` WRITE;
/*!40000 ALTER TABLE `ezpackage` DISABLE KEYS */;
INSERT INTO `ezpackage` VALUES (1,1301057838,'plain_site_data','1.0-1'),(2,1484919995,'ezwt_extension','5.3-0'),(3,1484919995,'ezstarrating_extension','5.3-0'),(4,1484919996,'ezgmaplocation_extension','5.3-0'),(5,1484919998,'ezflow_extension','5.3-0'),(6,1484919999,'ezdemo_extension','5.4-0'),(7,1484920000,'ezdemo_classes','5.4-0'),(8,1484920002,'ezdemo_democontent_clean','5.4-0');
/*!40000 ALTER TABLE `ezpackage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpaymentobject`
--

DROP TABLE IF EXISTS `ezpaymentobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpaymentobject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `payment_string` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `workflowprocess_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpaymentobject`
--

LOCK TABLES `ezpaymentobject` WRITE;
/*!40000 ALTER TABLE `ezpaymentobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpaymentobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpdf_export`
--

DROP TABLE IF EXISTS `ezpdf_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpdf_export` (
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `export_classes` varchar(255) DEFAULT NULL,
  `export_structure` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intro_text` longtext,
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `pdf_filename` varchar(255) DEFAULT NULL,
  `show_frontpage` int(11) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `source_node_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sub_text` longtext,
  `title` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpdf_export`
--

LOCK TABLES `ezpdf_export` WRITE;
/*!40000 ALTER TABLE `ezpdf_export` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpdf_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpending_actions`
--

DROP TABLE IF EXISTS `ezpending_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpending_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(64) NOT NULL DEFAULT '',
  `created` int(11) DEFAULT NULL,
  `param` longtext,
  PRIMARY KEY (`id`),
  KEY `ezpending_actions_action` (`action`),
  KEY `ezpending_actions_created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpending_actions`
--

LOCK TABLES `ezpending_actions` WRITE;
/*!40000 ALTER TABLE `ezpending_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpending_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy`
--

DROP TABLE IF EXISTS `ezpolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy` (
  `function_name` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) DEFAULT NULL,
  `original_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_original_id` (`original_id`),
  KEY `ezpolicy_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=450 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy`
--

LOCK TABLES `ezpolicy` WRITE;
/*!40000 ALTER TABLE `ezpolicy` DISABLE KEYS */;
INSERT INTO `ezpolicy` VALUES ('*',308,'*',0,2),('login',319,'user',0,3),('*',330,'ezoe',0,3),('*',332,'ezoe',0,3),('create',339,'content',0,3),('create',340,'content',0,3),('create',341,'content',0,3),('create',342,'content',0,3),('create',343,'content',0,3),('create',344,'content',0,3),('use',345,'websitetoolbar',0,3),('edit',346,'content',0,3),('read',347,'content',0,3),('use',348,'notification',0,3),('manage_locations',349,'content',0,3),('*',350,'ezodf',0,3),('*',351,'ezflow',0,3),('*',352,'ezajax',0,3),('diff',353,'content',0,3),('versionread',354,'content',0,3),('versionremove',355,'content',0,3),('remove',356,'content',0,3),('translate',357,'content',0,3),('feed',358,'rss',0,3),('bookmark',359,'content',0,3),('pendinglist',360,'content',0,3),('dashboard',361,'content',0,3),('view_embed',362,'content',0,3),('read',363,'content',0,4),('create',364,'content',0,4),('create',365,'content',0,4),('create',366,'content',0,4),('edit',367,'content',0,4),('selfedit',368,'user',0,4),('use',369,'notification',0,4),('create',370,'content',0,5),('create',371,'content',0,5),('create',372,'content',0,5),('edit',373,'content',0,5),('selfedit',374,'user',0,5),('use',375,'notification',0,5),('password',376,'user',0,5),('call',377,'ezjscore',0,5),('edit',400,'content',0,10),('create',401,'content',0,10),('remove',403,'content',0,10),('create',412,'content',0,13),('edit',413,'content',0,13),('remove',415,'content',0,13),('assign',428,'state',0,20),('create',429,'content',0,15),('edit',430,'content',0,15),('remove',431,'content',0,15),('pdf',432,'content',0,1),('read',434,'content',0,1),('view_embed',435,'content',0,1),('view_embed',436,'content',0,1),('feed',437,'rss',0,1),('login',438,'user',0,1),('login',439,'user',0,1),('read',440,'content',0,1);
/*!40000 ALTER TABLE `ezpolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation`
--

DROP TABLE IF EXISTS `ezpolicy_limitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `policy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `policy_id` (`policy_id`)
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation`
--

LOCK TABLES `ezpolicy_limitation` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation` VALUES (261,'Class',339),(262,'ParentClass',339),(263,'Class',340),(264,'ParentClass',340),(265,'Class',341),(266,'ParentClass',341),(267,'Class',342),(268,'ParentClass',342),(269,'Class',343),(270,'ParentClass',343),(271,'Class',344),(272,'ParentClass',344),(273,'Class',345),(274,'Section',347),(275,'Section',363),(276,'Class',364),(277,'Section',364),(278,'ParentClass',364),(279,'Class',365),(280,'Section',365),(281,'ParentClass',365),(282,'Class',366),(283,'Section',366),(284,'ParentClass',366),(285,'Class',367),(286,'Section',367),(287,'Owner',367),(288,'Class',370),(289,'Section',370),(290,'ParentClass',370),(291,'Class',371),(292,'Section',371),(293,'ParentClass',371),(294,'Class',372),(295,'Section',372),(296,'ParentClass',372),(297,'Class',373),(298,'Section',373),(299,'Owner',373),(321,'Class',400),(322,'Section',400),(325,'Class',401),(326,'Section',401),(329,'Class',403),(330,'Section',403),(349,'Class',412),(350,'Language',412),(353,'Class',413),(354,'Language',413),(359,'Class',415),(385,'StateGroup_publishing_workflow',428),(386,'NewState',428),(387,'Class',429),(388,'Language',429),(389,'Class',430),(390,'Language',430),(391,'Class',431),(392,'Section',432),(394,'Class',434),(395,'Section',434),(396,'Section',435),(397,'Class',436),(398,'Section',436),(400,'Section',440),(401,'StateGroup_publishing_workflow',440);
/*!40000 ALTER TABLE `ezpolicy_limitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpolicy_limitation_value`
--

DROP TABLE IF EXISTS `ezpolicy_limitation_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpolicy_limitation_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limitation_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezpolicy_limitation_value_val` (`value`),
  KEY `ezpolicy_limit_value_limit_id` (`limitation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=799 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpolicy_limitation_value`
--

LOCK TABLES `ezpolicy_limitation_value` WRITE;
/*!40000 ALTER TABLE `ezpolicy_limitation_value` DISABLE KEYS */;
INSERT INTO `ezpolicy_limitation_value` VALUES (493,261,'1'),(494,261,'28'),(495,261,'26'),(496,261,'21'),(497,261,'22'),(498,261,'23'),(499,261,'16'),(500,261,'17'),(501,261,'25'),(502,261,'24'),(503,261,'29'),(504,261,'30'),(505,261,'36'),(506,261,'34'),(507,261,'35'),(508,261,'27'),(509,262,'1'),(510,263,'18'),(511,264,'17'),(512,265,'31'),(513,266,'30'),(514,267,'33'),(515,268,'34'),(516,269,'27'),(517,270,'29'),(518,271,'1'),(519,271,'28'),(520,271,'22'),(521,271,'23'),(522,271,'24'),(523,271,'29'),(524,271,'34'),(525,271,'36'),(526,272,'23'),(527,273,'1'),(528,273,'28'),(529,273,'16'),(530,273,'17'),(531,273,'18'),(532,273,'21'),(533,273,'22'),(534,273,'23'),(535,273,'24'),(536,273,'25'),(537,273,'26'),(538,273,'27'),(539,273,'29'),(540,273,'30'),(541,273,'33'),(542,273,'34'),(543,273,'36'),(544,274,'1'),(545,274,'6'),(546,274,'3'),(547,275,'6'),(548,276,'31'),(549,277,'6'),(550,278,'30'),(551,279,'32'),(552,280,'6'),(553,281,'31'),(554,282,'13'),(555,283,'6'),(556,284,'16'),(557,285,'13'),(558,285,'31'),(559,285,'32'),(560,286,'6'),(561,287,'1'),(562,288,'31'),(563,289,'1'),(564,290,'30'),(565,291,'32'),(566,292,'1'),(567,293,'31'),(568,294,'13'),(569,295,'1'),(570,296,'16'),(571,296,'18'),(572,297,'13'),(573,297,'31'),(574,297,'32'),(575,298,'1'),(576,299,'1'),(611,321,'26'),(612,321,'1'),(613,321,'27'),(614,322,'3'),(618,325,'26'),(619,325,'1'),(620,325,'27'),(621,326,'3'),(628,329,'26'),(629,329,'1'),(630,329,'27'),(631,330,'3'),(672,349,'43'),(673,349,'41'),(674,349,'40'),(675,350,'eng-US'),(680,353,'43'),(681,353,'41'),(682,353,'40'),(683,354,'eng-US'),(694,359,'43'),(695,359,'41'),(696,359,'40'),(752,385,'3'),(753,385,'4'),(754,386,'3'),(755,386,'4'),(756,387,'40'),(757,387,'41'),(758,387,'43'),(759,388,'jpn-JP'),(760,389,'40'),(761,389,'41'),(762,389,'43'),(763,390,'jpn-JP'),(764,391,'40'),(765,391,'41'),(766,391,'43'),(767,392,'1'),(769,394,'26'),(770,394,'27'),(771,394,'35'),(772,394,'37'),(773,395,'3'),(774,396,'1'),(775,397,'26'),(776,397,'27'),(777,397,'35'),(778,397,'37'),(779,398,'3'),(781,400,'1'),(782,401,'4');
/*!40000 ALTER TABLE `ezpolicy_limitation_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpreferences`
--

DROP TABLE IF EXISTS `ezpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` longtext,
  PRIMARY KEY (`id`),
  KEY `ezpreferences_name` (`name`),
  KEY `ezpreferences_user_id_idx` (`user_id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpreferences`
--

LOCK TABLES `ezpreferences` WRITE;
/*!40000 ALTER TABLE `ezpreferences` DISABLE KEYS */;
INSERT INTO `ezpreferences` VALUES (1,'admin_navigation_content',14,'1'),(2,'admin_navigation_roles',14,'1'),(3,'admin_navigation_policies',14,'1'),(4,'admin_list_limit',14,'2'),(5,'admin_treemenu',14,'1'),(6,'admin_bookmark_menu',14,'1'),(7,'admin_left_menu_size',14,'24.91666667em'),(8,'admin_navigation_class_translations',14,'1'),(9,'admin_left_menu_size',132,'34.83333333em'),(10,'admin_left_menu_size',135,'23.58333333em');
/*!40000 ALTER TABLE `ezpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_authcode`
--

DROP TABLE IF EXISTS `ezprest_authcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_authcode` (
  `client_id` varchar(200) NOT NULL DEFAULT '',
  `expirytime` bigint(20) NOT NULL DEFAULT '0',
  `id` varchar(200) NOT NULL DEFAULT '',
  `scope` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `authcode_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_authcode`
--

LOCK TABLES `ezprest_authcode` WRITE;
/*!40000 ALTER TABLE `ezprest_authcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_authcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_authorized_clients`
--

DROP TABLE IF EXISTS `ezprest_authorized_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_authorized_clients` (
  `created` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rest_client_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_user` (`rest_client_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_authorized_clients`
--

LOCK TABLES `ezprest_authorized_clients` WRITE;
/*!40000 ALTER TABLE `ezprest_authorized_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_authorized_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_clients`
--

DROP TABLE IF EXISTS `ezprest_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_clients` (
  `client_id` varchar(200) DEFAULT NULL,
  `client_secret` varchar(200) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '0',
  `description` longtext,
  `endpoint_uri` varchar(200) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `updated` int(11) NOT NULL DEFAULT '0',
  `version` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id_unique` (`client_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_clients`
--

LOCK TABLES `ezprest_clients` WRITE;
/*!40000 ALTER TABLE `ezprest_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezprest_token`
--

DROP TABLE IF EXISTS `ezprest_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezprest_token` (
  `client_id` varchar(200) NOT NULL DEFAULT '',
  `expirytime` bigint(20) NOT NULL DEFAULT '0',
  `id` varchar(200) NOT NULL DEFAULT '',
  `refresh_token` varchar(200) NOT NULL DEFAULT '',
  `scope` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `token_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezprest_token`
--

LOCK TABLES `ezprest_token` WRITE;
/*!40000 ALTER TABLE `ezprest_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezprest_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcategory`
--

DROP TABLE IF EXISTS `ezproductcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcategory`
--

LOCK TABLES `ezproductcategory` WRITE;
/*!40000 ALTER TABLE `ezproductcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection`
--

DROP TABLE IF EXISTS `ezproductcollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection` (
  `created` int(11) DEFAULT NULL,
  `currency_code` varchar(4) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection`
--

LOCK TABLES `ezproductcollection` WRITE;
/*!40000 ALTER TABLE `ezproductcollection` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection_item`
--

DROP TABLE IF EXISTS `ezproductcollection_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection_item` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `discount` float DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_vat_inc` int(11) DEFAULT NULL,
  `item_count` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `price` float DEFAULT '0',
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `vat_value` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezproductcollection_item_contentobject_id` (`contentobject_id`),
  KEY `ezproductcollection_item_productcollection_id` (`productcollection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection_item`
--

LOCK TABLES `ezproductcollection_item` WRITE;
/*!40000 ALTER TABLE `ezproductcollection_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezproductcollection_item_opt`
--

DROP TABLE IF EXISTS `ezproductcollection_item_opt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezproductcollection_item_opt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `object_attribute_id` int(11) DEFAULT NULL,
  `option_item_id` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ezproductcollection_item_opt_item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezproductcollection_item_opt`
--

LOCK TABLES `ezproductcollection_item_opt` WRITE;
/*!40000 ALTER TABLE `ezproductcollection_item_opt` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezproductcollection_item_opt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezpublishingqueueprocesses`
--

DROP TABLE IF EXISTS `ezpublishingqueueprocesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezpublishingqueueprocesses` (
  `created` int(11) DEFAULT NULL,
  `ezcontentobject_version_id` int(11) NOT NULL DEFAULT '0',
  `finished` int(11) DEFAULT NULL,
  `pid` int(8) DEFAULT NULL,
  `started` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`ezcontentobject_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezpublishingqueueprocesses`
--

LOCK TABLES `ezpublishingqueueprocesses` WRITE;
/*!40000 ALTER TABLE `ezpublishingqueueprocesses` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezpublishingqueueprocesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrole`
--

DROP TABLE IF EXISTS `ezrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_new` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` char(1) DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrole`
--

LOCK TABLES `ezrole` WRITE;
/*!40000 ALTER TABLE `ezrole` DISABLE KEYS */;
INSERT INTO `ezrole` VALUES (1,0,'Anonymous','',0),(2,0,'Administrator','*',0),(3,0,'Editor','',0),(4,0,'Partner',NULL,0),(5,0,'Member',NULL,0),(10,0,'Country Editor',NULL,0),(13,0,'Country Editor (English)',NULL,0),(15,0,'Country Editor (Japanese)',NULL,0),(20,0,'Approval user',NULL,0);
/*!40000 ALTER TABLE `ezrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_export`
--

DROP TABLE IF EXISTS `ezrss_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_export` (
  `access_url` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `description` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) DEFAULT NULL,
  `main_node_only` int(11) NOT NULL DEFAULT '1',
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `node_id` int(11) DEFAULT NULL,
  `number_of_objects` int(11) NOT NULL DEFAULT '0',
  `rss_version` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_export`
--

LOCK TABLES `ezrss_export` WRITE;
/*!40000 ALTER TABLE `ezrss_export` DISABLE KEYS */;
INSERT INTO `ezrss_export` VALUES ('my_feed',1,1484920003,14,'',1,0,1,1484920003,14,0,10,'2.0','',1,'My RSS Feed','http://example.com');
/*!40000 ALTER TABLE `ezrss_export` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_export_item`
--

DROP TABLE IF EXISTS `ezrss_export_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_export_item` (
  `category` varchar(255) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enclosure` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rssexport_id` int(11) DEFAULT NULL,
  `source_node_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `subnodes` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`status`),
  KEY `ezrss_export_rsseid` (`rssexport_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_export_item`
--

LOCK TABLES `ezrss_export_item` WRITE;
/*!40000 ALTER TABLE `ezrss_export_item` DISABLE KEYS */;
INSERT INTO `ezrss_export_item` VALUES ('',16,'intro','',1,1,139,1,0,'title');
/*!40000 ALTER TABLE `ezrss_export_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezrss_import`
--

DROP TABLE IF EXISTS `ezrss_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezrss_import` (
  `active` int(11) DEFAULT NULL,
  `class_description` varchar(255) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `class_title` varchar(255) DEFAULT NULL,
  `class_url` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `destination_node_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_description` longtext NOT NULL,
  `modified` int(11) DEFAULT NULL,
  `modifier_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `object_owner_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `url` longtext,
  PRIMARY KEY (`id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezrss_import`
--

LOCK TABLES `ezrss_import` WRITE;
/*!40000 ALTER TABLE `ezrss_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezrss_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezscheduled_script`
--

DROP TABLE IF EXISTS `ezscheduled_script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezscheduled_script` (
  `command` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_report_timestamp` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `process_id` int(11) NOT NULL DEFAULT '0',
  `progress` int(3) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezscheduled_script_timestamp` (`last_report_timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezscheduled_script`
--

LOCK TABLES `ezscheduled_script` WRITE;
/*!40000 ALTER TABLE `ezscheduled_script` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezscheduled_script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_object_word_link`
--

DROP TABLE IF EXISTS `ezsearch_object_word_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_object_word_link` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `frequency` float NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL DEFAULT '',
  `integer_value` int(11) NOT NULL DEFAULT '0',
  `next_word_id` int(11) NOT NULL DEFAULT '0',
  `placement` int(11) NOT NULL DEFAULT '0',
  `prev_word_id` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL DEFAULT '0',
  `word_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezsearch_object_word_link_frequency` (`frequency`),
  KEY `ezsearch_object_word_link_identifier` (`identifier`),
  KEY `ezsearch_object_word_link_integer_value` (`integer_value`),
  KEY `ezsearch_object_word_link_object` (`contentobject_id`),
  KEY `ezsearch_object_word_link_word` (`word_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11631 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_object_word_link`
--

LOCK TABLES `ezsearch_object_word_link` WRITE;
/*!40000 ALTER TABLE `ezsearch_object_word_link` DISABLE KEYS */;
INSERT INTO `ezsearch_object_word_link` VALUES (6,3,4,0,4663,'name',0,951,0,0,1033917596,2,930),(7,3,4,0,4664,'description',0,952,1,930,1033917596,2,951),(7,3,4,0,4665,'description',0,0,2,951,1033917596,2,952),(8,4,10,0,4666,'first_name',0,954,0,0,1033920665,2,953),(9,4,10,0,4667,'last_name',0,953,1,953,1033920665,2,954),(12,4,10,0,4668,'user_account',0,955,2,954,1033920665,2,953),(12,4,10,0,4669,'user_account',0,927,3,953,1033920665,2,955),(12,4,10,0,4670,'user_account',0,0,4,955,1033920665,2,927),(6,3,12,0,4673,'name',0,930,0,0,1033920775,2,958),(6,3,12,0,4674,'name',0,0,1,958,1033920775,2,930),(4,1,41,0,4681,'name',0,0,0,0,1060695457,3,961),(6,3,42,0,4682,'name',0,930,0,0,1072180330,2,953),(6,3,42,0,4683,'name',0,954,1,953,1072180330,2,930),(7,3,42,0,4684,'description',0,952,2,930,1072180330,2,954),(7,3,42,0,4685,'description',0,816,3,954,1072180330,2,952),(7,3,42,0,4686,'description',0,814,4,952,1072180330,2,816),(7,3,42,0,4687,'description',0,953,5,816,1072180330,2,814),(7,3,42,0,4688,'description',0,954,6,814,1072180330,2,953),(7,3,42,0,4689,'description',0,0,7,953,1072180330,2,954),(4,1,45,0,4690,'name',0,0,0,0,1079684190,4,812),(4,1,49,0,4691,'name',0,0,0,0,1080220197,3,962),(4,1,50,0,4692,'name',0,0,0,0,1080220220,3,963),(4,1,51,0,4693,'name',0,0,0,0,1080220233,3,964),(159,14,52,0,4694,'name',0,965,0,0,1082016591,4,877),(159,14,52,0,4695,'name',0,966,1,877,1082016591,4,965),(159,14,52,0,4696,'name',0,0,2,965,1082016591,4,966),(176,15,54,0,4697,'id',0,0,0,0,1082016652,5,967),(4,1,56,0,4698,'name',0,0,0,0,1103023132,5,968),(8,4,14,0,4772,'first_name',0,954,0,0,1033920830,2,958),(9,4,14,0,4773,'last_name',0,1017,1,958,1033920830,2,954),(12,4,14,0,4774,'user_account',0,1018,2,954,1033920830,2,1017),(12,4,14,0,4775,'user_account',0,1019,3,1017,1033920830,2,1018),(12,4,14,0,4776,'user_account',0,0,4,1018,1033920830,2,1019),(310,40,65,0,4821,'title',0,1026,0,0,1485153593,1,1025),(310,40,65,0,4822,'title',0,1040,1,1025,1485153593,1,1026),(310,40,65,0,4823,'title',0,1041,2,1026,1485153593,1,1040),(311,40,65,0,4824,'description',0,1042,3,1040,1485153593,1,1041),(311,40,65,0,4825,'description',0,0,4,1041,1485153593,1,1042),(310,40,62,0,4848,'title',0,1026,0,0,1485153046,1,1025),(310,40,62,0,4849,'title',0,1052,1,1025,1485153046,1,1026),(310,40,62,0,4850,'title',0,1053,2,1026,1485153046,1,1052),(310,40,62,0,4851,'title',0,1054,3,1052,1485153046,1,1053),(310,40,62,0,4852,'title',0,1055,4,1053,1485153046,1,1054),(310,40,62,0,4853,'title',0,1056,5,1054,1485153046,1,1055),(310,40,62,0,4854,'title',0,1057,6,1055,1485153046,1,1056),(310,40,62,0,4855,'title',0,1058,7,1056,1485153046,1,1057),(310,40,62,0,4856,'title',0,1052,8,1057,1485153046,1,1058),(310,40,62,0,4857,'title',0,1059,9,1058,1485153046,1,1052),(311,40,62,0,4858,'description',0,1060,10,1052,1485153046,1,1059),(311,40,62,0,4859,'description',0,1061,11,1059,1485153046,1,1060),(311,40,62,0,4860,'description',0,1062,12,1060,1485153046,1,1061),(311,40,62,0,4861,'description',0,1063,13,1061,1485153046,1,1062),(311,40,62,0,4862,'description',0,1064,14,1062,1485153046,1,1063),(311,40,62,0,4863,'description',0,1065,15,1063,1485153046,1,1064),(311,40,62,0,4864,'description',0,1066,16,1064,1485153046,1,1065),(311,40,62,0,4865,'description',0,0,17,1065,1485153046,1,1066),(4,1,70,0,4869,'name',0,0,0,0,1485521863,3,1070),(245,27,72,0,5023,'name',0,1171,0,0,1485525584,3,1025),(245,27,72,0,5024,'name',0,1052,1,1025,1485525584,3,1171),(245,27,72,0,5025,'name',0,0,2,1171,1485525584,3,1052),(245,27,75,0,5035,'name',0,1171,0,0,1485715362,3,1025),(245,27,75,0,5036,'name',0,1172,1,1025,1485715362,3,1171),(245,27,75,0,5037,'name',0,0,2,1171,1485715362,3,1172),(310,40,76,0,5055,'title',0,1070,0,0,1485762975,1,1174),(310,40,76,0,5056,'title',0,1175,1,1174,1485762975,1,1070),(311,40,76,0,5057,'description',0,0,2,1070,1485762975,1,1175),(310,40,77,0,5058,'title',0,1052,0,0,1485763065,1,1176),(310,40,77,0,5059,'title',0,0,1,1176,1485763065,1,1052),(310,40,78,0,5062,'title',0,1040,0,0,1485763836,1,1176),(310,40,78,0,5063,'title',0,0,1,1176,1485763836,1,1040),(230,23,57,0,5064,'name',0,0,0,0,1195480486,1,1177),(230,23,81,0,5066,'name',0,1180,0,0,1486121151,1,1179),(230,23,81,0,5067,'name',0,1181,1,1179,1486121151,1,1180),(230,23,81,0,5068,'name',0,1182,2,1180,1486121151,1,1181),(230,23,81,0,5069,'name',0,1183,3,1181,1486121151,1,1182),(230,23,81,0,5070,'name',0,1184,4,1182,1486121151,1,1183),(230,23,81,0,5071,'name',0,1185,5,1183,1486121151,1,1184),(230,23,81,0,5072,'name',0,1186,6,1184,1486121151,1,1185),(230,23,81,0,5073,'name',0,1187,7,1185,1486121151,1,1186),(230,23,81,0,5074,'name',0,0,8,1186,1486121151,1,1187),(310,40,82,0,5075,'title',0,1189,0,0,1486121234,1,1188),(310,40,82,0,5076,'title',0,1190,1,1188,1486121234,1,1189),(310,40,82,0,5077,'title',0,1191,2,1189,1486121234,1,1190),(310,40,82,0,5078,'title',0,1053,3,1190,1486121234,1,1191),(310,40,82,0,5079,'title',0,0,4,1191,1486121234,1,1053),(310,40,83,0,5080,'title',0,1054,0,0,1486121276,1,1053),(310,40,83,0,5081,'title',0,1055,1,1053,1486121276,1,1054),(310,40,83,0,5082,'title',0,1056,2,1054,1486121276,1,1055),(310,40,83,0,5083,'title',0,1057,3,1055,1486121276,1,1056),(310,40,83,0,5084,'title',0,1058,4,1056,1486121276,1,1057),(310,40,83,0,5085,'title',0,1052,5,1057,1486121276,1,1058),(310,40,83,0,5086,'title',0,0,6,1058,1486121276,1,1052),(310,40,84,0,5094,'title',0,1054,0,0,1486121295,1,1053),(310,40,84,0,5095,'title',0,1055,1,1053,1486121295,1,1054),(310,40,84,0,5096,'title',0,1056,2,1054,1486121295,1,1055),(310,40,84,0,5097,'title',0,1057,3,1055,1486121295,1,1056),(310,40,84,0,5098,'title',0,1058,4,1056,1486121295,1,1057),(310,40,84,0,5099,'title',0,1040,5,1057,1486121295,1,1058),(310,40,84,0,5100,'title',0,0,6,1058,1486121295,1,1040),(312,41,85,0,5610,'title',0,1689,0,0,1486121440,1,1688),(312,41,85,0,5611,'title',0,1690,1,1688,1486121440,1,1689),(312,41,85,0,5612,'title',0,1691,2,1689,1486121440,1,1690),(312,41,85,0,5613,'title',0,1692,3,1690,1486121440,1,1691),(312,41,85,0,5614,'title',0,1191,4,1691,1486121440,1,1692),(312,41,85,0,5615,'title',0,1053,5,1692,1486121440,1,1191),(312,41,85,0,5616,'title',0,1693,6,1191,1486121440,1,1053),(313,41,85,0,5617,'description',0,1694,7,1053,1486121440,1,1693),(313,41,85,0,5618,'description',0,1695,8,1693,1486121440,1,1694),(313,41,85,0,5619,'description',0,1696,9,1694,1486121440,1,1695),(313,41,85,0,5620,'description',0,1697,10,1695,1486121440,1,1696),(313,41,85,0,5621,'description',0,1698,11,1696,1486121440,1,1697),(313,41,85,0,5622,'description',0,1699,12,1697,1486121440,1,1698),(313,41,85,0,5623,'description',0,1700,13,1698,1486121440,1,1699),(313,41,85,0,5624,'description',0,1701,14,1699,1486121440,1,1700),(313,41,85,0,5625,'description',0,1702,15,1700,1486121440,1,1701),(313,41,85,0,5626,'description',0,1703,16,1701,1486121440,1,1702),(313,41,85,0,5627,'description',0,1704,17,1702,1486121440,1,1703),(313,41,85,0,5628,'description',0,1705,18,1703,1486121440,1,1704),(313,41,85,0,5629,'description',0,1706,19,1704,1486121440,1,1705),(313,41,85,0,5630,'description',0,1707,20,1705,1486121440,1,1706),(313,41,85,0,5631,'description',0,1708,21,1706,1486121440,1,1707),(313,41,85,0,5632,'description',0,1709,22,1707,1486121440,1,1708),(313,41,85,0,5633,'description',0,1710,23,1708,1486121440,1,1709),(313,41,85,0,5634,'description',0,1711,24,1709,1486121440,1,1710),(313,41,85,0,5635,'description',0,1712,25,1710,1486121440,1,1711),(313,41,85,0,5636,'description',0,1713,26,1711,1486121440,1,1712),(313,41,85,0,5637,'description',0,1714,27,1712,1486121440,1,1713),(313,41,85,0,5638,'description',0,1715,28,1713,1486121440,1,1714),(313,41,85,0,5639,'description',0,1716,29,1714,1486121440,1,1715),(313,41,85,0,5640,'description',0,1717,30,1715,1486121440,1,1716),(313,41,85,0,5641,'description',0,1718,31,1716,1486121440,1,1717),(313,41,85,0,5642,'description',0,1719,32,1717,1486121440,1,1718),(313,41,85,0,5643,'description',0,1720,33,1718,1486121440,1,1719),(313,41,85,0,5644,'description',0,1721,34,1719,1486121440,1,1720),(313,41,85,0,5645,'description',0,1722,35,1720,1486121440,1,1721),(313,41,85,0,5646,'description',0,1723,36,1721,1486121440,1,1722),(313,41,85,0,5647,'description',0,1724,37,1722,1486121440,1,1723),(313,41,85,0,5648,'description',0,1725,38,1723,1486121440,1,1724),(313,41,85,0,5649,'description',0,1726,39,1724,1486121440,1,1725),(313,41,85,0,5650,'description',0,1727,40,1725,1486121440,1,1726),(313,41,85,0,5651,'description',0,1728,41,1726,1486121440,1,1727),(313,41,85,0,5652,'description',0,1729,42,1727,1486121440,1,1728),(313,41,85,0,5653,'description',0,1730,43,1728,1486121440,1,1729),(313,41,85,0,5654,'description',0,1731,44,1729,1486121440,1,1730),(313,41,85,0,5655,'description',0,1732,45,1730,1486121440,1,1731),(313,41,85,0,5656,'description',0,1733,46,1731,1486121440,1,1732),(313,41,85,0,5657,'description',0,1734,47,1732,1486121440,1,1733),(313,41,85,0,5658,'description',0,1735,48,1733,1486121440,1,1734),(313,41,85,0,5659,'description',0,1736,49,1734,1486121440,1,1735),(313,41,85,0,5660,'description',0,1737,50,1735,1486121440,1,1736),(313,41,85,0,5661,'description',0,1738,51,1736,1486121440,1,1737),(313,41,85,0,5662,'description',0,1739,52,1737,1486121440,1,1738),(313,41,85,0,5663,'description',0,1740,53,1738,1486121440,1,1739),(313,41,85,0,5664,'description',0,1741,54,1739,1486121440,1,1740),(313,41,85,0,5665,'description',0,1742,55,1740,1486121440,1,1741),(313,41,85,0,5666,'description',0,1743,56,1741,1486121440,1,1742),(313,41,85,0,5667,'description',0,1744,57,1742,1486121440,1,1743),(313,41,85,0,5668,'description',0,1745,58,1743,1486121440,1,1744),(313,41,85,0,5669,'description',0,1746,59,1744,1486121440,1,1745),(313,41,85,0,5670,'description',0,1747,60,1745,1486121440,1,1746),(313,41,85,0,5671,'description',0,1748,61,1746,1486121440,1,1747),(313,41,85,0,5672,'description',0,1749,62,1747,1486121440,1,1748),(313,41,85,0,5673,'description',0,1750,63,1748,1486121440,1,1749),(313,41,85,0,5674,'description',0,1751,64,1749,1486121440,1,1750),(313,41,85,0,5675,'description',0,1752,65,1750,1486121440,1,1751),(313,41,85,0,5676,'description',0,1753,66,1751,1486121440,1,1752),(313,41,85,0,5677,'description',0,1754,67,1752,1486121440,1,1753),(313,41,85,0,5678,'description',0,1755,68,1753,1486121440,1,1754),(313,41,85,0,5679,'description',0,1756,69,1754,1486121440,1,1755),(313,41,85,0,5680,'description',0,1757,70,1755,1486121440,1,1756),(313,41,85,0,5681,'description',0,1758,71,1756,1486121440,1,1757),(313,41,85,0,5682,'description',0,1759,72,1757,1486121440,1,1758),(313,41,85,0,5683,'description',0,1760,73,1758,1486121440,1,1759),(313,41,85,0,5684,'description',0,1761,74,1759,1486121440,1,1760),(313,41,85,0,5685,'description',0,1762,75,1760,1486121440,1,1761),(313,41,85,0,5686,'description',0,1763,76,1761,1486121440,1,1762),(313,41,85,0,5687,'description',0,1764,77,1762,1486121440,1,1763),(313,41,85,0,5688,'description',0,1765,78,1763,1486121440,1,1764),(313,41,85,0,5689,'description',0,1766,79,1764,1486121440,1,1765),(313,41,85,0,5690,'description',0,1767,80,1765,1486121440,1,1766),(313,41,85,0,5691,'description',0,1768,81,1766,1486121440,1,1767),(313,41,85,0,5692,'description',0,1769,82,1767,1486121440,1,1768),(313,41,85,0,5693,'description',0,1770,83,1768,1486121440,1,1769),(313,41,85,0,5694,'description',0,1771,84,1769,1486121440,1,1770),(313,41,85,0,5695,'description',0,1772,85,1770,1486121440,1,1771),(313,41,85,0,5696,'description',0,1773,86,1771,1486121440,1,1772),(313,41,85,0,5697,'description',0,1774,87,1772,1486121440,1,1773),(313,41,85,0,5698,'description',0,1775,88,1773,1486121440,1,1774),(313,41,85,0,5699,'description',0,1776,89,1774,1486121440,1,1775),(313,41,85,0,5700,'description',0,1777,90,1775,1486121440,1,1776),(313,41,85,0,5701,'description',0,1778,91,1776,1486121440,1,1777),(313,41,85,0,5702,'description',0,1779,92,1777,1486121440,1,1778),(313,41,85,0,5703,'description',0,1780,93,1778,1486121440,1,1779),(313,41,85,0,5704,'description',0,1781,94,1779,1486121440,1,1780),(313,41,85,0,5705,'description',0,1782,95,1780,1486121440,1,1781),(313,41,85,0,5706,'description',0,1783,96,1781,1486121440,1,1782),(313,41,85,0,5707,'description',0,1784,97,1782,1486121440,1,1783),(313,41,85,0,5708,'description',0,1785,98,1783,1486121440,1,1784),(313,41,85,0,5709,'description',0,1786,99,1784,1486121440,1,1785),(313,41,85,0,5710,'description',0,1787,100,1785,1486121440,1,1786),(313,41,85,0,5711,'description',0,1788,101,1786,1486121440,1,1787),(313,41,85,0,5712,'description',0,1789,102,1787,1486121440,1,1788),(313,41,85,0,5713,'description',0,1790,103,1788,1486121440,1,1789),(313,41,85,0,5714,'description',0,1791,104,1789,1486121440,1,1790),(313,41,85,0,5715,'description',0,1792,105,1790,1486121440,1,1791),(313,41,85,0,5716,'description',0,1793,106,1791,1486121440,1,1792),(313,41,85,0,5717,'description',0,1794,107,1792,1486121440,1,1793),(313,41,85,0,5718,'description',0,1795,108,1793,1486121440,1,1794),(313,41,85,0,5719,'description',0,1796,109,1794,1486121440,1,1795),(313,41,85,0,5720,'description',0,1797,110,1795,1486121440,1,1796),(313,41,85,0,5721,'description',0,1798,111,1796,1486121440,1,1797),(313,41,85,0,5722,'description',0,1799,112,1797,1486121440,1,1798),(313,41,85,0,5723,'description',0,1800,113,1798,1486121440,1,1799),(313,41,85,0,5724,'description',0,1801,114,1799,1486121440,1,1800),(313,41,85,0,5725,'description',0,1802,115,1800,1486121440,1,1801),(313,41,85,0,5726,'description',0,1803,116,1801,1486121440,1,1802),(313,41,85,0,5727,'description',0,1804,117,1802,1486121440,1,1803),(313,41,85,0,5728,'description',0,1805,118,1803,1486121440,1,1804),(313,41,85,0,5729,'description',0,1806,119,1804,1486121440,1,1805),(313,41,85,0,5730,'description',0,1807,120,1805,1486121440,1,1806),(313,41,85,0,5731,'description',0,1808,121,1806,1486121440,1,1807),(313,41,85,0,5732,'description',0,1809,122,1807,1486121440,1,1808),(313,41,85,0,5733,'description',0,1810,123,1808,1486121440,1,1809),(313,41,85,0,5734,'description',0,1811,124,1809,1486121440,1,1810),(313,41,85,0,5735,'description',0,1812,125,1810,1486121440,1,1811),(313,41,85,0,5736,'description',0,1813,126,1811,1486121440,1,1812),(313,41,85,0,5737,'description',0,1814,127,1812,1486121440,1,1813),(313,41,85,0,5738,'description',0,1815,128,1813,1486121440,1,1814),(313,41,85,0,5739,'description',0,1816,129,1814,1486121440,1,1815),(313,41,85,0,5740,'description',0,1817,130,1815,1486121440,1,1816),(313,41,85,0,5741,'description',0,1818,131,1816,1486121440,1,1817),(313,41,85,0,5742,'description',0,1819,132,1817,1486121440,1,1818),(313,41,85,0,5743,'description',0,1820,133,1818,1486121440,1,1819),(313,41,85,0,5744,'description',0,1821,134,1819,1486121440,1,1820),(313,41,85,0,5745,'description',0,1822,135,1820,1486121440,1,1821),(313,41,85,0,5746,'description',0,1823,136,1821,1486121440,1,1822),(313,41,85,0,5747,'description',0,1824,137,1822,1486121440,1,1823),(313,41,85,0,5748,'description',0,1825,138,1823,1486121440,1,1824),(313,41,85,0,5749,'description',0,1826,139,1824,1486121440,1,1825),(313,41,85,0,5750,'description',0,1827,140,1825,1486121440,1,1826),(313,41,85,0,5751,'description',0,1828,141,1826,1486121440,1,1827),(313,41,85,0,5752,'description',0,1829,142,1827,1486121440,1,1828),(313,41,85,0,5753,'description',0,1830,143,1828,1486121440,1,1829),(313,41,85,0,5754,'description',0,1831,144,1829,1486121440,1,1830),(313,41,85,0,5755,'description',0,1832,145,1830,1486121440,1,1831),(313,41,85,0,5756,'description',0,1833,146,1831,1486121440,1,1832),(313,41,85,0,5757,'description',0,1834,147,1832,1486121440,1,1833),(313,41,85,0,5758,'description',0,1835,148,1833,1486121440,1,1834),(313,41,85,0,5759,'description',0,1836,149,1834,1486121440,1,1835),(313,41,85,0,5760,'description',0,1837,150,1835,1486121440,1,1836),(313,41,85,0,5761,'description',0,1838,151,1836,1486121440,1,1837),(313,41,85,0,5762,'description',0,1839,152,1837,1486121440,1,1838),(313,41,85,0,5763,'description',0,1840,153,1838,1486121440,1,1839),(313,41,85,0,5764,'description',0,1841,154,1839,1486121440,1,1840),(313,41,85,0,5765,'description',0,1842,155,1840,1486121440,1,1841),(313,41,85,0,5766,'description',0,1843,156,1841,1486121440,1,1842),(313,41,85,0,5767,'description',0,1844,157,1842,1486121440,1,1843),(313,41,85,0,5768,'description',0,1845,158,1843,1486121440,1,1844),(313,41,85,0,5769,'description',0,1846,159,1844,1486121440,1,1845),(313,41,85,0,5770,'description',0,1847,160,1845,1486121440,1,1846),(313,41,85,0,5771,'description',0,1848,161,1846,1486121440,1,1847),(313,41,85,0,5772,'description',0,1849,162,1847,1486121440,1,1848),(313,41,85,0,5773,'description',0,1850,163,1848,1486121440,1,1849),(313,41,85,0,5774,'description',0,1851,164,1849,1486121440,1,1850),(313,41,85,0,5775,'description',0,1852,165,1850,1486121440,1,1851),(313,41,85,0,5776,'description',0,1853,166,1851,1486121440,1,1852),(313,41,85,0,5777,'description',0,1854,167,1852,1486121440,1,1853),(313,41,85,0,5778,'description',0,1855,168,1853,1486121440,1,1854),(313,41,85,0,5779,'description',0,1856,169,1854,1486121440,1,1855),(313,41,85,0,5780,'description',0,1857,170,1855,1486121440,1,1856),(313,41,85,0,5781,'description',0,1858,171,1856,1486121440,1,1857),(313,41,85,0,5782,'description',0,1859,172,1857,1486121440,1,1858),(313,41,85,0,5783,'description',0,1860,173,1858,1486121440,1,1859),(313,41,85,0,5784,'description',0,1861,174,1859,1486121440,1,1860),(313,41,85,0,5785,'description',0,1862,175,1860,1486121440,1,1861),(313,41,85,0,5786,'description',0,1863,176,1861,1486121440,1,1862),(313,41,85,0,5787,'description',0,1864,177,1862,1486121440,1,1863),(313,41,85,0,5788,'description',0,1865,178,1863,1486121440,1,1864),(313,41,85,0,5789,'description',0,1866,179,1864,1486121440,1,1865),(313,41,85,0,5790,'description',0,1867,180,1865,1486121440,1,1866),(313,41,85,0,5791,'description',0,1868,181,1866,1486121440,1,1867),(313,41,85,0,5792,'description',0,1869,182,1867,1486121440,1,1868),(313,41,85,0,5793,'description',0,1870,183,1868,1486121440,1,1869),(313,41,85,0,5794,'description',0,1871,184,1869,1486121440,1,1870),(313,41,85,0,5795,'description',0,1872,185,1870,1486121440,1,1871),(313,41,85,0,5796,'description',0,1873,186,1871,1486121440,1,1872),(313,41,85,0,5797,'description',0,1874,187,1872,1486121440,1,1873),(313,41,85,0,5798,'description',0,1875,188,1873,1486121440,1,1874),(313,41,85,0,5799,'description',0,1876,189,1874,1486121440,1,1875),(313,41,85,0,5800,'description',0,1877,190,1875,1486121440,1,1876),(313,41,85,0,5801,'description',0,1878,191,1876,1486121440,1,1877),(313,41,85,0,5802,'description',0,1879,192,1877,1486121440,1,1878),(313,41,85,0,5803,'description',0,1880,193,1878,1486121440,1,1879),(313,41,85,0,5804,'description',0,1881,194,1879,1486121440,1,1880),(313,41,85,0,5805,'description',0,1882,195,1880,1486121440,1,1881),(313,41,85,0,5806,'description',0,1883,196,1881,1486121440,1,1882),(313,41,85,0,5807,'description',0,1884,197,1882,1486121440,1,1883),(313,41,85,0,5808,'description',0,1885,198,1883,1486121440,1,1884),(313,41,85,0,5809,'description',0,1886,199,1884,1486121440,1,1885),(313,41,85,0,5810,'description',0,1887,200,1885,1486121440,1,1886),(313,41,85,0,5811,'description',0,1888,201,1886,1486121440,1,1887),(313,41,85,0,5812,'description',0,1889,202,1887,1486121440,1,1888),(313,41,85,0,5813,'description',0,1890,203,1888,1486121440,1,1889),(313,41,85,0,5814,'description',0,1891,204,1889,1486121440,1,1890),(313,41,85,0,5815,'description',0,1892,205,1890,1486121440,1,1891),(313,41,85,0,5816,'description',0,1893,206,1891,1486121440,1,1892),(313,41,85,0,5817,'description',0,1894,207,1892,1486121440,1,1893),(313,41,85,0,5818,'description',0,1895,208,1893,1486121440,1,1894),(313,41,85,0,5819,'description',0,1896,209,1894,1486121440,1,1895),(313,41,85,0,5820,'description',0,1897,210,1895,1486121440,1,1896),(313,41,85,0,5821,'description',0,1898,211,1896,1486121440,1,1897),(313,41,85,0,5822,'description',0,1899,212,1897,1486121440,1,1898),(313,41,85,0,5823,'description',0,1900,213,1898,1486121440,1,1899),(313,41,85,0,5824,'description',0,1901,214,1899,1486121440,1,1900),(313,41,85,0,5825,'description',0,1902,215,1900,1486121440,1,1901),(313,41,85,0,5826,'description',0,1903,216,1901,1486121440,1,1902),(313,41,85,0,5827,'description',0,1904,217,1902,1486121440,1,1903),(313,41,85,0,5828,'description',0,1905,218,1903,1486121440,1,1904),(313,41,85,0,5829,'description',0,1906,219,1904,1486121440,1,1905),(313,41,85,0,5830,'description',0,1907,220,1905,1486121440,1,1906),(313,41,85,0,5831,'description',0,1908,221,1906,1486121440,1,1907),(313,41,85,0,5832,'description',0,1909,222,1907,1486121440,1,1908),(313,41,85,0,5833,'description',0,1172,223,1908,1486121440,1,1909),(313,41,85,0,5834,'description',0,1910,224,1909,1486121440,1,1172),(313,41,85,0,5835,'description',0,1911,225,1172,1486121440,1,1910),(313,41,85,0,5836,'description',0,1912,226,1910,1486121440,1,1911),(313,41,85,0,5837,'description',0,1913,227,1911,1486121440,1,1912),(313,41,85,0,5838,'description',0,1172,228,1912,1486121440,1,1913),(313,41,85,0,5839,'description',0,1914,229,1913,1486121440,1,1172),(313,41,85,0,5840,'description',0,1915,230,1172,1486121440,1,1914),(313,41,85,0,5841,'description',0,1916,231,1914,1486121440,1,1915),(313,41,85,0,5842,'description',0,1917,232,1915,1486121440,1,1916),(313,41,85,0,5843,'description',0,1918,233,1916,1486121440,1,1917),(313,41,85,0,5844,'description',0,1919,234,1917,1486121440,1,1918),(313,41,85,0,5845,'description',0,1920,235,1918,1486121440,1,1919),(313,41,85,0,5846,'description',0,1921,236,1919,1486121440,1,1920),(313,41,85,0,5847,'description',0,1922,237,1920,1486121440,1,1921),(313,41,85,0,5848,'description',0,1923,238,1921,1486121440,1,1922),(313,41,85,0,5849,'description',0,1924,239,1922,1486121440,1,1923),(313,41,85,0,5850,'description',0,1925,240,1923,1486121440,1,1924),(313,41,85,0,5851,'description',0,1926,241,1924,1486121440,1,1925),(313,41,85,0,5852,'description',0,1927,242,1925,1486121440,1,1926),(313,41,85,0,5853,'description',0,1928,243,1926,1486121440,1,1927),(313,41,85,0,5854,'description',0,1929,244,1927,1486121440,1,1928),(313,41,85,0,5855,'description',0,1930,245,1928,1486121440,1,1929),(313,41,85,0,5856,'description',0,1931,246,1929,1486121440,1,1930),(313,41,85,0,5857,'description',0,1932,247,1930,1486121440,1,1931),(313,41,85,0,5858,'description',0,1933,248,1931,1486121440,1,1932),(313,41,85,0,5859,'description',0,1934,249,1932,1486121440,1,1933),(313,41,85,0,5860,'description',0,1935,250,1933,1486121440,1,1934),(313,41,85,0,5861,'description',0,1936,251,1934,1486121440,1,1935),(313,41,85,0,5862,'description',0,1937,252,1935,1486121440,1,1936),(313,41,85,0,5863,'description',0,1938,253,1936,1486121440,1,1937),(313,41,85,0,5864,'description',0,1939,254,1937,1486121440,1,1938),(313,41,85,0,5865,'description',0,1940,255,1938,1486121440,1,1939),(313,41,85,0,5866,'description',0,1941,256,1939,1486121440,1,1940),(313,41,85,0,5867,'description',0,1942,257,1940,1486121440,1,1941),(313,41,85,0,5868,'description',0,1943,258,1941,1486121440,1,1942),(313,41,85,0,5869,'description',0,1944,259,1942,1486121440,1,1943),(313,41,85,0,5870,'description',0,1945,260,1943,1486121440,1,1944),(313,41,85,0,5871,'description',0,1946,261,1944,1486121440,1,1945),(313,41,85,0,5872,'description',0,1947,262,1945,1486121440,1,1946),(313,41,85,0,5873,'description',0,1948,263,1946,1486121440,1,1947),(313,41,85,0,5874,'description',0,1949,264,1947,1486121440,1,1948),(313,41,85,0,5875,'description',0,1950,265,1948,1486121440,1,1949),(313,41,85,0,5876,'description',0,1951,266,1949,1486121440,1,1950),(313,41,85,0,5877,'description',0,1952,267,1950,1486121440,1,1951),(313,41,85,0,5878,'description',0,1953,268,1951,1486121440,1,1952),(313,41,85,0,5879,'description',0,1954,269,1952,1486121440,1,1953),(313,41,85,0,5880,'description',0,1955,270,1953,1486121440,1,1954),(313,41,85,0,5881,'description',0,1956,271,1954,1486121440,1,1955),(313,41,85,0,5882,'description',0,1957,272,1955,1486121440,1,1956),(313,41,85,0,5883,'description',0,1958,273,1956,1486121440,1,1957),(313,41,85,0,5884,'description',0,1959,274,1957,1486121440,1,1958),(313,41,85,0,5885,'description',0,1960,275,1958,1486121440,1,1959),(313,41,85,0,5886,'description',0,1961,276,1959,1486121440,1,1960),(313,41,85,0,5887,'description',0,1962,277,1960,1486121440,1,1961),(313,41,85,0,5888,'description',0,1963,278,1961,1486121440,1,1962),(313,41,85,0,5889,'description',0,1964,279,1962,1486121440,1,1963),(313,41,85,0,5890,'description',0,1965,280,1963,1486121440,1,1964),(313,41,85,0,5891,'description',0,1966,281,1964,1486121440,1,1965),(313,41,85,0,5892,'description',0,1967,282,1965,1486121440,1,1966),(313,41,85,0,5893,'description',0,1968,283,1966,1486121440,1,1967),(313,41,85,0,5894,'description',0,1969,284,1967,1486121440,1,1968),(313,41,85,0,5895,'description',0,1970,285,1968,1486121440,1,1969),(313,41,85,0,5896,'description',0,1971,286,1969,1486121440,1,1970),(313,41,85,0,5897,'description',0,1972,287,1970,1486121440,1,1971),(313,41,85,0,5898,'description',0,1973,288,1971,1486121440,1,1972),(313,41,85,0,5899,'description',0,1974,289,1972,1486121440,1,1973),(313,41,85,0,5900,'description',0,1040,290,1973,1486121440,1,1974),(313,41,85,0,5901,'description',0,1975,291,1974,1486121440,1,1040),(313,41,85,0,5902,'description',0,1976,292,1040,1486121440,1,1975),(313,41,85,0,5903,'description',0,1977,293,1975,1486121440,1,1976),(313,41,85,0,5904,'description',0,1978,294,1976,1486121440,1,1977),(313,41,85,0,5905,'description',0,1979,295,1977,1486121440,1,1978),(313,41,85,0,5906,'description',0,1980,296,1978,1486121440,1,1979),(313,41,85,0,5907,'description',0,1981,297,1979,1486121440,1,1980),(313,41,85,0,5908,'description',0,1982,298,1980,1486121440,1,1981),(313,41,85,0,5909,'description',0,1983,299,1981,1486121440,1,1982),(313,41,85,0,5910,'description',0,1984,300,1982,1486121440,1,1983),(313,41,85,0,5911,'description',0,1985,301,1983,1486121440,1,1984),(313,41,85,0,5912,'description',0,1986,302,1984,1486121440,1,1985),(313,41,85,0,5913,'description',0,1987,303,1985,1486121440,1,1986),(313,41,85,0,5914,'description',0,1988,304,1986,1486121440,1,1987),(313,41,85,0,5915,'description',0,1989,305,1987,1486121440,1,1988),(313,41,85,0,5916,'description',0,1990,306,1988,1486121440,1,1989),(313,41,85,0,5917,'description',0,1991,307,1989,1486121440,1,1990),(313,41,85,0,5918,'description',0,1992,308,1990,1486121440,1,1991),(313,41,85,0,5919,'description',0,1993,309,1991,1486121440,1,1992),(313,41,85,0,5920,'description',0,1994,310,1992,1486121440,1,1993),(313,41,85,0,5921,'description',0,1995,311,1993,1486121440,1,1994),(313,41,85,0,5922,'description',0,1778,312,1994,1486121440,1,1995),(313,41,85,0,5923,'description',0,1996,313,1995,1486121440,1,1778),(313,41,85,0,5924,'description',0,1997,314,1778,1486121440,1,1996),(313,41,85,0,5925,'description',0,1998,315,1996,1486121440,1,1997),(313,41,85,0,5926,'description',0,1999,316,1997,1486121440,1,1998),(313,41,85,0,5927,'description',0,2000,317,1998,1486121440,1,1999),(313,41,85,0,5928,'description',0,2001,318,1999,1486121440,1,2000),(313,41,85,0,5929,'description',0,2002,319,2000,1486121440,1,2001),(313,41,85,0,5930,'description',0,2003,320,2001,1486121440,1,2002),(313,41,85,0,5931,'description',0,2004,321,2002,1486121440,1,2003),(313,41,85,0,5932,'description',0,2005,322,2003,1486121440,1,2004),(313,41,85,0,5933,'description',0,2006,323,2004,1486121440,1,2005),(313,41,85,0,5934,'description',0,2007,324,2005,1486121440,1,2006),(313,41,85,0,5935,'description',0,2008,325,2006,1486121440,1,2007),(313,41,85,0,5936,'description',0,1840,326,2007,1486121440,1,2008),(313,41,85,0,5937,'description',0,2009,327,2008,1486121440,1,1840),(313,41,85,0,5938,'description',0,2010,328,1840,1486121440,1,2009),(313,41,85,0,5939,'description',0,2011,329,2009,1486121440,1,2010),(313,41,85,0,5940,'description',0,2012,330,2010,1486121440,1,2011),(313,41,85,0,5941,'description',0,2013,331,2011,1486121440,1,2012),(313,41,85,0,5942,'description',0,2014,332,2012,1486121440,1,2013),(313,41,85,0,5943,'description',0,2015,333,2013,1486121440,1,2014),(313,41,85,0,5944,'description',0,2016,334,2014,1486121440,1,2015),(313,41,85,0,5945,'description',0,2017,335,2015,1486121440,1,2016),(313,41,85,0,5946,'description',0,2018,336,2016,1486121440,1,2017),(313,41,85,0,5947,'description',0,2019,337,2017,1486121440,1,2018),(313,41,85,0,5948,'description',0,2020,338,2018,1486121440,1,2019),(313,41,85,0,5949,'description',0,2021,339,2019,1486121440,1,2020),(313,41,85,0,5950,'description',0,2022,340,2020,1486121440,1,2021),(313,41,85,0,5951,'description',0,2023,341,2021,1486121440,1,2022),(313,41,85,0,5952,'description',0,2024,342,2022,1486121440,1,2023),(313,41,85,0,5953,'description',0,2025,343,2023,1486121440,1,2024),(313,41,85,0,5954,'description',0,2026,344,2024,1486121440,1,2025),(313,41,85,0,5955,'description',0,2027,345,2025,1486121440,1,2026),(313,41,85,0,5956,'description',0,2028,346,2026,1486121440,1,2027),(313,41,85,0,5957,'description',0,2029,347,2027,1486121440,1,2028),(313,41,85,0,5958,'description',0,2030,348,2028,1486121440,1,2029),(313,41,85,0,5959,'description',0,2031,349,2029,1486121440,1,2030),(313,41,85,0,5960,'description',0,2032,350,2030,1486121440,1,2031),(313,41,85,0,5961,'description',0,2033,351,2031,1486121440,1,2032),(313,41,85,0,5962,'description',0,2034,352,2032,1486121440,1,2033),(313,41,85,0,5963,'description',0,2035,353,2033,1486121440,1,2034),(313,41,85,0,5964,'description',0,2036,354,2034,1486121440,1,2035),(313,41,85,0,5965,'description',0,2037,355,2035,1486121440,1,2036),(313,41,85,0,5966,'description',0,2038,356,2036,1486121440,1,2037),(313,41,85,0,5967,'description',0,2039,357,2037,1486121440,1,2038),(313,41,85,0,5968,'description',0,2040,358,2038,1486121440,1,2039),(313,41,85,0,5969,'description',0,2041,359,2039,1486121440,1,2040),(313,41,85,0,5970,'description',0,2042,360,2040,1486121440,1,2041),(313,41,85,0,5971,'description',0,2043,361,2041,1486121440,1,2042),(313,41,85,0,5972,'description',0,2044,362,2042,1486121440,1,2043),(313,41,85,0,5973,'description',0,2045,363,2043,1486121440,1,2044),(313,41,85,0,5974,'description',0,2046,364,2044,1486121440,1,2045),(313,41,85,0,5975,'description',0,2047,365,2045,1486121440,1,2046),(313,41,85,0,5976,'description',0,2048,366,2046,1486121440,1,2047),(313,41,85,0,5977,'description',0,2049,367,2047,1486121440,1,2048),(313,41,85,0,5978,'description',0,2050,368,2048,1486121440,1,2049),(313,41,85,0,5979,'description',0,2051,369,2049,1486121440,1,2050),(313,41,85,0,5980,'description',0,2052,370,2050,1486121440,1,2051),(313,41,85,0,5981,'description',0,2053,371,2051,1486121440,1,2052),(313,41,85,0,5982,'description',0,2054,372,2052,1486121440,1,2053),(313,41,85,0,5983,'description',0,2055,373,2053,1486121440,1,2054),(313,41,85,0,5984,'description',0,2056,374,2054,1486121440,1,2055),(313,41,85,0,5985,'description',0,2057,375,2055,1486121440,1,2056),(313,41,85,0,5986,'description',0,2058,376,2056,1486121440,1,2057),(313,41,85,0,5987,'description',0,1840,377,2057,1486121440,1,2058),(313,41,85,0,5988,'description',0,1040,378,2058,1486121440,1,1840),(313,41,85,0,5989,'description',0,2059,379,1840,1486121440,1,1040),(313,41,85,0,5990,'description',0,2060,380,1040,1486121440,1,2059),(313,41,85,0,5991,'description',0,2061,381,2059,1486121440,1,2060),(313,41,85,0,5992,'description',0,2062,382,2060,1486121440,1,2061),(313,41,85,0,5993,'description',0,2063,383,2061,1486121440,1,2062),(313,41,85,0,5994,'description',0,2064,384,2062,1486121440,1,2063),(313,41,85,0,5995,'description',0,1172,385,2063,1486121440,1,2064),(313,41,85,0,5996,'description',0,2065,386,2064,1486121440,1,1172),(313,41,85,0,5997,'description',0,2066,387,1172,1486121440,1,2065),(313,41,85,0,5998,'description',0,2067,388,2065,1486121440,1,2066),(313,41,85,0,5999,'description',0,2068,389,2066,1486121440,1,2067),(313,41,85,0,6000,'description',0,2069,390,2067,1486121440,1,2068),(313,41,85,0,6001,'description',0,2070,391,2068,1486121440,1,2069),(313,41,85,0,6002,'description',0,2071,392,2069,1486121440,1,2070),(313,41,85,0,6003,'description',0,2072,393,2070,1486121440,1,2071),(313,41,85,0,6004,'description',0,2073,394,2071,1486121440,1,2072),(313,41,85,0,6005,'description',0,2074,395,2072,1486121440,1,2073),(313,41,85,0,6006,'description',0,2075,396,2073,1486121440,1,2074),(313,41,85,0,6007,'description',0,2076,397,2074,1486121440,1,2075),(313,41,85,0,6008,'description',0,2077,398,2075,1486121440,1,2076),(313,41,85,0,6009,'description',0,2078,399,2076,1486121440,1,2077),(313,41,85,0,6010,'description',0,2079,400,2077,1486121440,1,2078),(313,41,85,0,6011,'description',0,2080,401,2078,1486121440,1,2079),(313,41,85,0,6012,'description',0,2081,402,2079,1486121440,1,2080),(313,41,85,0,6013,'description',0,2082,403,2080,1486121440,1,2081),(313,41,85,0,6014,'description',0,2083,404,2081,1486121440,1,2082),(313,41,85,0,6015,'description',0,2084,405,2082,1486121440,1,2083),(313,41,85,0,6016,'description',0,2085,406,2083,1486121440,1,2084),(313,41,85,0,6017,'description',0,2086,407,2084,1486121440,1,2085),(313,41,85,0,6018,'description',0,2087,408,2085,1486121440,1,2086),(313,41,85,0,6019,'description',0,2088,409,2086,1486121440,1,2087),(313,41,85,0,6020,'description',0,2089,410,2087,1486121440,1,2088),(313,41,85,0,6021,'description',0,2090,411,2088,1486121440,1,2089),(313,41,85,0,6022,'description',0,2091,412,2089,1486121440,1,2090),(313,41,85,0,6023,'description',0,2092,413,2090,1486121440,1,2091),(313,41,85,0,6024,'description',0,2093,414,2091,1486121440,1,2092),(313,41,85,0,6025,'description',0,2094,415,2092,1486121440,1,2093),(313,41,85,0,6026,'description',0,2095,416,2093,1486121440,1,2094),(313,41,85,0,6027,'description',0,2096,417,2094,1486121440,1,2095),(313,41,85,0,6028,'description',0,2097,418,2095,1486121440,1,2096),(313,41,85,0,6029,'description',0,2098,419,2096,1486121440,1,2097),(313,41,85,0,6030,'description',0,2099,420,2097,1486121440,1,2098),(313,41,85,0,6031,'description',0,2100,421,2098,1486121440,1,2099),(313,41,85,0,6032,'description',0,2101,422,2099,1486121440,1,2100),(313,41,85,0,6033,'description',0,2102,423,2100,1486121440,1,2101),(313,41,85,0,6034,'description',0,2103,424,2101,1486121440,1,2102),(313,41,85,0,6035,'description',0,2104,425,2102,1486121440,1,2103),(313,41,85,0,6036,'description',0,2105,426,2103,1486121440,1,2104),(313,41,85,0,6037,'description',0,2106,427,2104,1486121440,1,2105),(313,41,85,0,6038,'description',0,2107,428,2105,1486121440,1,2106),(313,41,85,0,6039,'description',0,2108,429,2106,1486121440,1,2107),(313,41,85,0,6040,'description',0,2109,430,2107,1486121440,1,2108),(313,41,85,0,6041,'description',0,2110,431,2108,1486121440,1,2109),(313,41,85,0,6042,'description',0,2111,432,2109,1486121440,1,2110),(313,41,85,0,6043,'description',0,2112,433,2110,1486121440,1,2111),(313,41,85,0,6044,'description',0,2113,434,2111,1486121440,1,2112),(313,41,85,0,6045,'description',0,2114,435,2112,1486121440,1,2113),(313,41,85,0,6046,'description',0,2115,436,2113,1486121440,1,2114),(313,41,85,0,6047,'description',0,2018,437,2114,1486121440,1,2115),(313,41,85,0,6048,'description',0,2116,438,2115,1486121440,1,2018),(313,41,85,0,6049,'description',0,2117,439,2018,1486121440,1,2116),(313,41,85,0,6050,'description',0,2118,440,2116,1486121440,1,2117),(313,41,85,0,6051,'description',0,2119,441,2117,1486121440,1,2118),(313,41,85,0,6052,'description',0,2120,442,2118,1486121440,1,2119),(313,41,85,0,6053,'description',0,2121,443,2119,1486121440,1,2120),(313,41,85,0,6054,'description',0,2122,444,2120,1486121440,1,2121),(313,41,85,0,6055,'description',0,2123,445,2121,1486121440,1,2122),(313,41,85,0,6056,'description',0,2124,446,2122,1486121440,1,2123),(313,41,85,0,6057,'description',0,2125,447,2123,1486121440,1,2124),(313,41,85,0,6058,'description',0,2126,448,2124,1486121440,1,2125),(313,41,85,0,6059,'description',0,2127,449,2125,1486121440,1,2126),(313,41,85,0,6060,'description',0,2128,450,2126,1486121440,1,2127),(313,41,85,0,6061,'description',0,2129,451,2127,1486121440,1,2128),(313,41,85,0,6062,'description',0,2130,452,2128,1486121440,1,2129),(313,41,85,0,6063,'description',0,2131,453,2129,1486121440,1,2130),(313,41,85,0,6064,'description',0,2132,454,2130,1486121440,1,2131),(313,41,85,0,6065,'description',0,2133,455,2131,1486121440,1,2132),(313,41,85,0,6066,'description',0,2134,456,2132,1486121440,1,2133),(313,41,85,0,6067,'description',0,2135,457,2133,1486121440,1,2134),(313,41,85,0,6068,'description',0,2136,458,2134,1486121440,1,2135),(313,41,85,0,6069,'description',0,2137,459,2135,1486121440,1,2136),(313,41,85,0,6070,'description',0,2138,460,2136,1486121440,1,2137),(313,41,85,0,6071,'description',0,2139,461,2137,1486121440,1,2138),(313,41,85,0,6072,'description',0,2140,462,2138,1486121440,1,2139),(313,41,85,0,6073,'description',0,1978,463,2139,1486121440,1,2140),(313,41,85,0,6074,'description',0,2141,464,2140,1486121440,1,1978),(313,41,85,0,6075,'description',0,2142,465,1978,1486121440,1,2141),(313,41,85,0,6076,'description',0,2143,466,2141,1486121440,1,2142),(313,41,85,0,6077,'description',0,2144,467,2142,1486121440,1,2143),(313,41,85,0,6078,'description',0,2145,468,2143,1486121440,1,2144),(313,41,85,0,6079,'description',0,1062,469,2144,1486121440,1,2145),(313,41,85,0,6080,'description',0,2146,470,2145,1486121440,1,1062),(313,41,85,0,6081,'description',0,2147,471,1062,1486121440,1,2146),(313,41,85,0,6082,'description',0,2148,472,2146,1486121440,1,2147),(313,41,85,0,6083,'description',0,2149,473,2147,1486121440,1,2148),(313,41,85,0,6084,'description',0,2150,474,2148,1486121440,1,2149),(313,41,85,0,6085,'description',0,2151,475,2149,1486121440,1,2150),(313,41,85,0,6086,'description',0,2152,476,2150,1486121440,1,2151),(313,41,85,0,6087,'description',0,2153,477,2151,1486121440,1,2152),(313,41,85,0,6088,'description',0,2154,478,2152,1486121440,1,2153),(313,41,85,0,6089,'description',0,2155,479,2153,1486121440,1,2154),(313,41,85,0,6090,'description',0,2156,480,2154,1486121440,1,2155),(313,41,85,0,6091,'description',0,2157,481,2155,1486121440,1,2156),(313,41,85,0,6092,'description',0,2158,482,2156,1486121440,1,2157),(313,41,85,0,6093,'description',0,2159,483,2157,1486121440,1,2158),(313,41,85,0,6094,'description',0,2160,484,2158,1486121440,1,2159),(313,41,85,0,6095,'description',0,2161,485,2159,1486121440,1,2160),(313,41,85,0,6096,'description',0,2162,486,2160,1486121440,1,2161),(313,41,85,0,6097,'description',0,2163,487,2161,1486121440,1,2162),(313,41,85,0,6098,'description',0,2164,488,2162,1486121440,1,2163),(313,41,85,0,6099,'description',0,2165,489,2163,1486121440,1,2164),(313,41,85,0,6100,'description',0,2166,490,2164,1486121440,1,2165),(313,41,85,0,6101,'description',0,2167,491,2165,1486121440,1,2166),(313,41,85,0,6102,'description',0,2168,492,2166,1486121440,1,2167),(313,41,85,0,6103,'description',0,2169,493,2167,1486121440,1,2168),(313,41,85,0,6104,'description',0,2170,494,2168,1486121440,1,2169),(313,41,85,0,6105,'description',0,2171,495,2169,1486121440,1,2170),(313,41,85,0,6106,'description',0,2172,496,2170,1486121440,1,2171),(313,41,85,0,6107,'description',0,2173,497,2171,1486121440,1,2172),(313,41,85,0,6108,'description',0,2174,498,2172,1486121440,1,2173),(313,41,85,0,6109,'description',0,2175,499,2173,1486121440,1,2174),(313,41,85,0,6110,'description',0,2176,500,2174,1486121440,1,2175),(313,41,85,0,6111,'description',0,2177,501,2175,1486121440,1,2176),(313,41,85,0,6112,'description',0,2178,502,2176,1486121440,1,2177),(313,41,85,0,6113,'description',0,2179,503,2177,1486121440,1,2178),(313,41,85,0,6114,'description',0,2180,504,2178,1486121440,1,2179),(313,41,85,0,6115,'description',0,2181,505,2179,1486121440,1,2180),(313,41,85,0,6116,'description',0,2182,506,2180,1486121440,1,2181),(315,41,85,0,6117,'product_number',0,2183,507,2181,1486121440,1,2182),(315,41,85,0,6118,'product_number',0,0,508,2182,1486121440,1,2183),(312,41,64,0,6158,'title',0,2197,0,0,1485153514,1,1025),(312,41,64,0,6159,'title',0,1025,1,1025,1485153514,1,2197),(313,41,64,0,6160,'description',0,2197,2,2197,1485153514,1,1025),(313,41,64,0,6161,'description',0,2198,3,1025,1485153514,1,2197),(315,41,64,0,6162,'product_number',0,2199,4,2197,1485153514,1,2198),(315,41,64,0,6163,'product_number',0,0,5,2198,1485153514,1,2199),(312,41,68,0,7216,'title',0,3186,0,0,1485153762,1,1025),(312,41,68,0,7217,'title',0,2191,1,1025,1485153762,1,3186),(312,41,68,0,7218,'title',0,3187,2,3186,1485153762,1,2191),(312,41,68,0,7219,'title',0,3188,3,2191,1485153762,1,3187),(312,41,68,0,7220,'title',0,3189,4,3187,1485153762,1,3188),(312,41,68,0,7221,'title',0,1995,5,3188,1485153762,1,3189),(312,41,68,0,7222,'title',0,3190,6,3189,1485153762,1,1995),(312,41,68,0,7223,'title',0,3191,7,1995,1485153762,1,3190),(312,41,68,0,7224,'title',0,3192,8,3190,1485153762,1,3191),(312,41,68,0,7225,'title',0,3193,9,3191,1485153762,1,3192),(312,41,68,0,7226,'title',0,2191,10,3192,1485153762,1,3193),(312,41,68,0,7227,'title',0,3194,11,3193,1485153762,1,2191),(313,41,68,0,7228,'description',0,3195,12,2191,1485153762,1,3194),(313,41,68,0,7229,'description',0,1834,13,3194,1485153762,1,3195),(313,41,68,0,7230,'description',0,3196,14,3195,1485153762,1,1834),(313,41,68,0,7231,'description',0,3197,15,1834,1485153762,1,3196),(313,41,68,0,7232,'description',0,3198,16,3196,1485153762,1,3197),(313,41,68,0,7233,'description',0,3199,17,3197,1485153762,1,3198),(313,41,68,0,7234,'description',0,3200,18,3198,1485153762,1,3199),(313,41,68,0,7235,'description',0,3201,19,3199,1485153762,1,3200),(313,41,68,0,7236,'description',0,3202,20,3200,1485153762,1,3201),(313,41,68,0,7237,'description',0,3203,21,3201,1485153762,1,3202),(313,41,68,0,7238,'description',0,3204,22,3202,1485153762,1,3203),(313,41,68,0,7239,'description',0,3205,23,3203,1485153762,1,3204),(313,41,68,0,7240,'description',0,3206,24,3204,1485153762,1,3205),(313,41,68,0,7241,'description',0,3207,25,3205,1485153762,1,3206),(313,41,68,0,7242,'description',0,3208,26,3206,1485153762,1,3207),(313,41,68,0,7243,'description',0,3209,27,3207,1485153762,1,3208),(313,41,68,0,7244,'description',0,3210,28,3208,1485153762,1,3209),(313,41,68,0,7245,'description',0,3211,29,3209,1485153762,1,3210),(313,41,68,0,7246,'description',0,3212,30,3210,1485153762,1,3211),(313,41,68,0,7247,'description',0,3213,31,3211,1485153762,1,3212),(313,41,68,0,7248,'description',0,3214,32,3212,1485153762,1,3213),(313,41,68,0,7249,'description',0,3215,33,3213,1485153762,1,3214),(313,41,68,0,7250,'description',0,3216,34,3214,1485153762,1,3215),(313,41,68,0,7251,'description',0,3217,35,3215,1485153762,1,3216),(313,41,68,0,7252,'description',0,3218,36,3216,1485153762,1,3217),(313,41,68,0,7253,'description',0,3219,37,3217,1485153762,1,3218),(313,41,68,0,7254,'description',0,3220,38,3218,1485153762,1,3219),(313,41,68,0,7255,'description',0,3221,39,3219,1485153762,1,3220),(313,41,68,0,7256,'description',0,3222,40,3220,1485153762,1,3221),(313,41,68,0,7257,'description',0,3223,41,3221,1485153762,1,3222),(313,41,68,0,7258,'description',0,3224,42,3222,1485153762,1,3223),(313,41,68,0,7259,'description',0,3225,43,3223,1485153762,1,3224),(313,41,68,0,7260,'description',0,3226,44,3224,1485153762,1,3225),(313,41,68,0,7261,'description',0,3227,45,3225,1485153762,1,3226),(313,41,68,0,7262,'description',0,3228,46,3226,1485153762,1,3227),(313,41,68,0,7263,'description',0,3229,47,3227,1485153762,1,3228),(313,41,68,0,7264,'description',0,3230,48,3228,1485153762,1,3229),(313,41,68,0,7265,'description',0,3231,49,3229,1485153762,1,3230),(313,41,68,0,7266,'description',0,3232,50,3230,1485153762,1,3231),(313,41,68,0,7267,'description',0,3233,51,3231,1485153762,1,3232),(313,41,68,0,7268,'description',0,3234,52,3232,1485153762,1,3233),(313,41,68,0,7269,'description',0,1766,53,3233,1485153762,1,3234),(313,41,68,0,7270,'description',0,3235,54,3234,1485153762,1,1766),(313,41,68,0,7271,'description',0,3236,55,1766,1485153762,1,3235),(313,41,68,0,7272,'description',0,3237,56,3235,1485153762,1,3236),(313,41,68,0,7273,'description',0,3238,57,3236,1485153762,1,3237),(313,41,68,0,7274,'description',0,3239,58,3237,1485153762,1,3238),(313,41,68,0,7275,'description',0,3240,59,3238,1485153762,1,3239),(313,41,68,0,7276,'description',0,3241,60,3239,1485153762,1,3240),(313,41,68,0,7277,'description',0,3242,61,3240,1485153762,1,3241),(313,41,68,0,7278,'description',0,3243,62,3241,1485153762,1,3242),(313,41,68,0,7279,'description',0,3244,63,3242,1485153762,1,3243),(313,41,68,0,7280,'description',0,3245,64,3243,1485153762,1,3244),(313,41,68,0,7281,'description',0,3246,65,3244,1485153762,1,3245),(313,41,68,0,7282,'description',0,3247,66,3245,1485153762,1,3246),(313,41,68,0,7283,'description',0,3248,67,3246,1485153762,1,3247),(313,41,68,0,7284,'description',0,3249,68,3247,1485153762,1,3248),(313,41,68,0,7285,'description',0,3250,69,3248,1485153762,1,3249),(313,41,68,0,7286,'description',0,3251,70,3249,1485153762,1,3250),(313,41,68,0,7287,'description',0,3252,71,3250,1485153762,1,3251),(313,41,68,0,7288,'description',0,3253,72,3251,1485153762,1,3252),(313,41,68,0,7289,'description',0,3254,73,3252,1485153762,1,3253),(313,41,68,0,7290,'description',0,3255,74,3253,1485153762,1,3254),(313,41,68,0,7291,'description',0,3256,75,3254,1485153762,1,3255),(313,41,68,0,7292,'description',0,3257,76,3255,1485153762,1,3256),(313,41,68,0,7293,'description',0,1726,77,3256,1485153762,1,3257),(313,41,68,0,7294,'description',0,3258,78,3257,1485153762,1,1726),(313,41,68,0,7295,'description',0,3259,79,1726,1485153762,1,3258),(313,41,68,0,7296,'description',0,3260,80,3258,1485153762,1,3259),(313,41,68,0,7297,'description',0,3261,81,3259,1485153762,1,3260),(313,41,68,0,7298,'description',0,3262,82,3260,1485153762,1,3261),(313,41,68,0,7299,'description',0,3263,83,3261,1485153762,1,3262),(313,41,68,0,7300,'description',0,3264,84,3262,1485153762,1,3263),(313,41,68,0,7301,'description',0,3265,85,3263,1485153762,1,3264),(313,41,68,0,7302,'description',0,3266,86,3264,1485153762,1,3265),(313,41,68,0,7303,'description',0,3267,87,3265,1485153762,1,3266),(313,41,68,0,7304,'description',0,3268,88,3266,1485153762,1,3267),(313,41,68,0,7305,'description',0,3269,89,3267,1485153762,1,3268),(313,41,68,0,7306,'description',0,3270,90,3268,1485153762,1,3269),(313,41,68,0,7307,'description',0,3271,91,3269,1485153762,1,3270),(313,41,68,0,7308,'description',0,3272,92,3270,1485153762,1,3271),(313,41,68,0,7309,'description',0,3273,93,3271,1485153762,1,3272),(313,41,68,0,7310,'description',0,3274,94,3272,1485153762,1,3273),(313,41,68,0,7311,'description',0,1726,95,3273,1485153762,1,3274),(313,41,68,0,7312,'description',0,3275,96,3274,1485153762,1,1726),(313,41,68,0,7313,'description',0,3276,97,1726,1485153762,1,3275),(313,41,68,0,7314,'description',0,3277,98,3275,1485153762,1,3276),(313,41,68,0,7315,'description',0,3278,99,3276,1485153762,1,3277),(313,41,68,0,7316,'description',0,3279,100,3277,1485153762,1,3278),(313,41,68,0,7317,'description',0,3280,101,3278,1485153762,1,3279),(313,41,68,0,7318,'description',0,3281,102,3279,1485153762,1,3280),(313,41,68,0,7319,'description',0,3282,103,3280,1485153762,1,3281),(313,41,68,0,7320,'description',0,3283,104,3281,1485153762,1,3282),(313,41,68,0,7321,'description',0,3284,105,3282,1485153762,1,3283),(313,41,68,0,7322,'description',0,3285,106,3283,1485153762,1,3284),(313,41,68,0,7323,'description',0,3286,107,3284,1485153762,1,3285),(313,41,68,0,7324,'description',0,3287,108,3285,1485153762,1,3286),(313,41,68,0,7325,'description',0,3288,109,3286,1485153762,1,3287),(313,41,68,0,7326,'description',0,3289,110,3287,1485153762,1,3288),(313,41,68,0,7327,'description',0,3290,111,3288,1485153762,1,3289),(313,41,68,0,7328,'description',0,3291,112,3289,1485153762,1,3290),(313,41,68,0,7329,'description',0,3292,113,3290,1485153762,1,3291),(313,41,68,0,7330,'description',0,3293,114,3291,1485153762,1,3292),(313,41,68,0,7331,'description',0,3294,115,3292,1485153762,1,3293),(313,41,68,0,7332,'description',0,3295,116,3293,1485153762,1,3294),(313,41,68,0,7333,'description',0,2633,117,3294,1485153762,1,3295),(313,41,68,0,7334,'description',0,3296,118,3295,1485153762,1,2633),(313,41,68,0,7335,'description',0,3297,119,2633,1485153762,1,3296),(313,41,68,0,7336,'description',0,3298,120,3296,1485153762,1,3297),(313,41,68,0,7337,'description',0,3299,121,3297,1485153762,1,3298),(313,41,68,0,7338,'description',0,3300,122,3298,1485153762,1,3299),(313,41,68,0,7339,'description',0,3301,123,3299,1485153762,1,3300),(313,41,68,0,7340,'description',0,3302,124,3300,1485153762,1,3301),(313,41,68,0,7341,'description',0,3303,125,3301,1485153762,1,3302),(313,41,68,0,7342,'description',0,3304,126,3302,1485153762,1,3303),(313,41,68,0,7343,'description',0,3305,127,3303,1485153762,1,3304),(313,41,68,0,7344,'description',0,2191,128,3304,1485153762,1,3305),(313,41,68,0,7345,'description',0,3306,129,3305,1485153762,1,2191),(313,41,68,0,7346,'description',0,3127,130,2191,1485153762,1,3306),(313,41,68,0,7347,'description',0,3307,131,3306,1485153762,1,3127),(313,41,68,0,7348,'description',0,3308,132,3127,1485153762,1,3307),(313,41,68,0,7349,'description',0,3309,133,3307,1485153762,1,3308),(313,41,68,0,7350,'description',0,3310,134,3308,1485153762,1,3309),(313,41,68,0,7351,'description',0,3311,135,3309,1485153762,1,3310),(313,41,68,0,7352,'description',0,3312,136,3310,1485153762,1,3311),(313,41,68,0,7353,'description',0,3313,137,3311,1485153762,1,3312),(313,41,68,0,7354,'description',0,3314,138,3312,1485153762,1,3313),(313,41,68,0,7355,'description',0,3315,139,3313,1485153762,1,3314),(313,41,68,0,7356,'description',0,3316,140,3314,1485153762,1,3315),(313,41,68,0,7357,'description',0,3060,141,3315,1485153762,1,3316),(313,41,68,0,7358,'description',0,3317,142,3316,1485153762,1,3060),(313,41,68,0,7359,'description',0,3318,143,3060,1485153762,1,3317),(313,41,68,0,7360,'description',0,3319,144,3317,1485153762,1,3318),(313,41,68,0,7361,'description',0,3320,145,3318,1485153762,1,3319),(313,41,68,0,7362,'description',0,3321,146,3319,1485153762,1,3320),(313,41,68,0,7363,'description',0,3322,147,3320,1485153762,1,3321),(313,41,68,0,7364,'description',0,3323,148,3321,1485153762,1,3322),(313,41,68,0,7365,'description',0,3324,149,3322,1485153762,1,3323),(313,41,68,0,7366,'description',0,3325,150,3323,1485153762,1,3324),(313,41,68,0,7367,'description',0,3326,151,3324,1485153762,1,3325),(313,41,68,0,7368,'description',0,3327,152,3325,1485153762,1,3326),(313,41,68,0,7369,'description',0,3328,153,3326,1485153762,1,3327),(313,41,68,0,7370,'description',0,3329,154,3327,1485153762,1,3328),(313,41,68,0,7371,'description',0,3330,155,3328,1485153762,1,3329),(313,41,68,0,7372,'description',0,3331,156,3329,1485153762,1,3330),(313,41,68,0,7373,'description',0,3332,157,3330,1485153762,1,3331),(313,41,68,0,7374,'description',0,3333,158,3331,1485153762,1,3332),(313,41,68,0,7375,'description',0,3334,159,3332,1485153762,1,3333),(313,41,68,0,7376,'description',0,3335,160,3333,1485153762,1,3334),(313,41,68,0,7377,'description',0,3336,161,3334,1485153762,1,3335),(313,41,68,0,7378,'description',0,3337,162,3335,1485153762,1,3336),(313,41,68,0,7379,'description',0,3338,163,3336,1485153762,1,3337),(313,41,68,0,7380,'description',0,3305,164,3337,1485153762,1,3338),(313,41,68,0,7381,'description',0,3339,165,3338,1485153762,1,3305),(313,41,68,0,7382,'description',0,3340,166,3305,1485153762,1,3339),(313,41,68,0,7383,'description',0,3341,167,3339,1485153762,1,3340),(313,41,68,0,7384,'description',0,3342,168,3340,1485153762,1,3341),(313,41,68,0,7385,'description',0,3343,169,3341,1485153762,1,3342),(313,41,68,0,7386,'description',0,3344,170,3342,1485153762,1,3343),(313,41,68,0,7387,'description',0,3065,171,3343,1485153762,1,3344),(313,41,68,0,7388,'description',0,3345,172,3344,1485153762,1,3065),(313,41,68,0,7389,'description',0,3346,173,3065,1485153762,1,3345),(313,41,68,0,7390,'description',0,3347,174,3345,1485153762,1,3346),(313,41,68,0,7391,'description',0,3348,175,3346,1485153762,1,3347),(313,41,68,0,7392,'description',0,1840,176,3347,1485153762,1,3348),(313,41,68,0,7393,'description',0,3349,177,3348,1485153762,1,1840),(313,41,68,0,7394,'description',0,2396,178,1840,1485153762,1,3349),(313,41,68,0,7395,'description',0,3350,179,3349,1485153762,1,2396),(313,41,68,0,7396,'description',0,3351,180,2396,1485153762,1,3350),(313,41,68,0,7397,'description',0,3352,181,3350,1485153762,1,3351),(313,41,68,0,7398,'description',0,3353,182,3351,1485153762,1,3352),(313,41,68,0,7399,'description',0,3354,183,3352,1485153762,1,3353),(313,41,68,0,7400,'description',0,3355,184,3353,1485153762,1,3354),(313,41,68,0,7401,'description',0,3356,185,3354,1485153762,1,3355),(313,41,68,0,7402,'description',0,3357,186,3355,1485153762,1,3356),(313,41,68,0,7403,'description',0,3358,187,3356,1485153762,1,3357),(313,41,68,0,7404,'description',0,3359,188,3357,1485153762,1,3358),(313,41,68,0,7405,'description',0,3360,189,3358,1485153762,1,3359),(313,41,68,0,7406,'description',0,3361,190,3359,1485153762,1,3360),(313,41,68,0,7407,'description',0,3362,191,3360,1485153762,1,3361),(313,41,68,0,7408,'description',0,3363,192,3361,1485153762,1,3362),(313,41,68,0,7409,'description',0,3364,193,3362,1485153762,1,3363),(313,41,68,0,7410,'description',0,3365,194,3363,1485153762,1,3364),(313,41,68,0,7411,'description',0,3366,195,3364,1485153762,1,3365),(313,41,68,0,7412,'description',0,3367,196,3365,1485153762,1,3366),(313,41,68,0,7413,'description',0,3368,197,3366,1485153762,1,3367),(313,41,68,0,7414,'description',0,3369,198,3367,1485153762,1,3368),(313,41,68,0,7415,'description',0,3370,199,3368,1485153762,1,3369),(313,41,68,0,7416,'description',0,3371,200,3369,1485153762,1,3370),(313,41,68,0,7417,'description',0,3372,201,3370,1485153762,1,3371),(313,41,68,0,7418,'description',0,3373,202,3371,1485153762,1,3372),(313,41,68,0,7419,'description',0,3374,203,3372,1485153762,1,3373),(313,41,68,0,7420,'description',0,2384,204,3373,1485153762,1,3374),(313,41,68,0,7421,'description',0,3375,205,3374,1485153762,1,2384),(313,41,68,0,7422,'description',0,3376,206,2384,1485153762,1,3375),(313,41,68,0,7423,'description',0,3377,207,3375,1485153762,1,3376),(313,41,68,0,7424,'description',0,3378,208,3376,1485153762,1,3377),(313,41,68,0,7425,'description',0,3379,209,3377,1485153762,1,3378),(313,41,68,0,7426,'description',0,3380,210,3378,1485153762,1,3379),(313,41,68,0,7427,'description',0,3381,211,3379,1485153762,1,3380),(313,41,68,0,7428,'description',0,3382,212,3380,1485153762,1,3381),(313,41,68,0,7429,'description',0,3383,213,3381,1485153762,1,3382),(313,41,68,0,7430,'description',0,1952,214,3382,1485153762,1,3383),(313,41,68,0,7431,'description',0,3384,215,3383,1485153762,1,1952),(313,41,68,0,7432,'description',0,3385,216,1952,1485153762,1,3384),(313,41,68,0,7433,'description',0,3386,217,3384,1485153762,1,3385),(313,41,68,0,7434,'description',0,3387,218,3385,1485153762,1,3386),(313,41,68,0,7435,'description',0,3388,219,3386,1485153762,1,3387),(313,41,68,0,7436,'description',0,3389,220,3387,1485153762,1,3388),(313,41,68,0,7437,'description',0,3390,221,3388,1485153762,1,3389),(313,41,68,0,7438,'description',0,3391,222,3389,1485153762,1,3390),(313,41,68,0,7439,'description',0,3392,223,3390,1485153762,1,3391),(313,41,68,0,7440,'description',0,3393,224,3391,1485153762,1,3392),(313,41,68,0,7441,'description',0,3394,225,3392,1485153762,1,3393),(313,41,68,0,7442,'description',0,3395,226,3393,1485153762,1,3394),(313,41,68,0,7443,'description',0,3396,227,3394,1485153762,1,3395),(313,41,68,0,7444,'description',0,3397,228,3395,1485153762,1,3396),(313,41,68,0,7445,'description',0,3398,229,3396,1485153762,1,3397),(313,41,68,0,7446,'description',0,3399,230,3397,1485153762,1,3398),(313,41,68,0,7447,'description',0,3400,231,3398,1485153762,1,3399),(313,41,68,0,7448,'description',0,3401,232,3399,1485153762,1,3400),(313,41,68,0,7449,'description',0,3402,233,3400,1485153762,1,3401),(313,41,68,0,7450,'description',0,3403,234,3401,1485153762,1,3402),(313,41,68,0,7451,'description',0,3404,235,3402,1485153762,1,3403),(313,41,68,0,7452,'description',0,3405,236,3403,1485153762,1,3404),(313,41,68,0,7453,'description',0,3406,237,3404,1485153762,1,3405),(313,41,68,0,7454,'description',0,3407,238,3405,1485153762,1,3406),(313,41,68,0,7455,'description',0,3408,239,3406,1485153762,1,3407),(313,41,68,0,7456,'description',0,3409,240,3407,1485153762,1,3408),(313,41,68,0,7457,'description',0,3410,241,3408,1485153762,1,3409),(313,41,68,0,7458,'description',0,3411,242,3409,1485153762,1,3410),(313,41,68,0,7459,'description',0,3412,243,3410,1485153762,1,3411),(313,41,68,0,7460,'description',0,3413,244,3411,1485153762,1,3412),(313,41,68,0,7461,'description',0,3414,245,3412,1485153762,1,3413),(313,41,68,0,7462,'description',0,3415,246,3413,1485153762,1,3414),(313,41,68,0,7463,'description',0,3416,247,3414,1485153762,1,3415),(313,41,68,0,7464,'description',0,3417,248,3415,1485153762,1,3416),(313,41,68,0,7465,'description',0,3418,249,3416,1485153762,1,3417),(313,41,68,0,7466,'description',0,3419,250,3417,1485153762,1,3418),(313,41,68,0,7467,'description',0,3420,251,3418,1485153762,1,3419),(313,41,68,0,7468,'description',0,3421,252,3419,1485153762,1,3420),(313,41,68,0,7469,'description',0,3422,253,3420,1485153762,1,3421),(313,41,68,0,7470,'description',0,3423,254,3421,1485153762,1,3422),(313,41,68,0,7471,'description',0,3424,255,3422,1485153762,1,3423),(313,41,68,0,7472,'description',0,3425,256,3423,1485153762,1,3424),(313,41,68,0,7473,'description',0,3426,257,3424,1485153762,1,3425),(313,41,68,0,7474,'description',0,3427,258,3425,1485153762,1,3426),(313,41,68,0,7475,'description',0,3428,259,3426,1485153762,1,3427),(313,41,68,0,7476,'description',0,3429,260,3427,1485153762,1,3428),(313,41,68,0,7477,'description',0,3430,261,3428,1485153762,1,3429),(313,41,68,0,7478,'description',0,3431,262,3429,1485153762,1,3430),(313,41,68,0,7479,'description',0,3432,263,3430,1485153762,1,3431),(313,41,68,0,7480,'description',0,3433,264,3431,1485153762,1,3432),(313,41,68,0,7481,'description',0,3434,265,3432,1485153762,1,3433),(313,41,68,0,7482,'description',0,3435,266,3433,1485153762,1,3434),(313,41,68,0,7483,'description',0,3436,267,3434,1485153762,1,3435),(313,41,68,0,7484,'description',0,1696,268,3435,1485153762,1,3436),(313,41,68,0,7485,'description',0,3437,269,3436,1485153762,1,1696),(313,41,68,0,7486,'description',0,3438,270,1696,1485153762,1,3437),(313,41,68,0,7487,'description',0,3439,271,3437,1485153762,1,3438),(313,41,68,0,7488,'description',0,2365,272,3438,1485153762,1,3439),(313,41,68,0,7489,'description',0,3440,273,3439,1485153762,1,2365),(313,41,68,0,7490,'description',0,2038,274,2365,1485153762,1,3440),(313,41,68,0,7491,'description',0,3441,275,3440,1485153762,1,2038),(313,41,68,0,7492,'description',0,3442,276,2038,1485153762,1,3441),(313,41,68,0,7493,'description',0,3443,277,3441,1485153762,1,3442),(313,41,68,0,7494,'description',0,3444,278,3442,1485153762,1,3443),(313,41,68,0,7495,'description',0,3445,279,3443,1485153762,1,3444),(313,41,68,0,7496,'description',0,3446,280,3444,1485153762,1,3445),(313,41,68,0,7497,'description',0,3447,281,3445,1485153762,1,3446),(313,41,68,0,7498,'description',0,3448,282,3446,1485153762,1,3447),(313,41,68,0,7499,'description',0,3449,283,3447,1485153762,1,3448),(313,41,68,0,7500,'description',0,3450,284,3448,1485153762,1,3449),(313,41,68,0,7501,'description',0,3451,285,3449,1485153762,1,3450),(313,41,68,0,7502,'description',0,3452,286,3450,1485153762,1,3451),(313,41,68,0,7503,'description',0,3453,287,3451,1485153762,1,3452),(313,41,68,0,7504,'description',0,3454,288,3452,1485153762,1,3453),(313,41,68,0,7505,'description',0,3455,289,3453,1485153762,1,3454),(313,41,68,0,7506,'description',0,3456,290,3454,1485153762,1,3455),(313,41,68,0,7507,'description',0,3457,291,3455,1485153762,1,3456),(313,41,68,0,7508,'description',0,3458,292,3456,1485153762,1,3457),(313,41,68,0,7509,'description',0,3459,293,3457,1485153762,1,3458),(313,41,68,0,7510,'description',0,3460,294,3458,1485153762,1,3459),(313,41,68,0,7511,'description',0,3461,295,3459,1485153762,1,3460),(313,41,68,0,7512,'description',0,3462,296,3460,1485153762,1,3461),(313,41,68,0,7513,'description',0,3463,297,3461,1485153762,1,3462),(313,41,68,0,7514,'description',0,3464,298,3462,1485153762,1,3463),(313,41,68,0,7515,'description',0,3465,299,3463,1485153762,1,3464),(313,41,68,0,7516,'description',0,3466,300,3464,1485153762,1,3465),(313,41,68,0,7517,'description',0,3467,301,3465,1485153762,1,3466),(313,41,68,0,7518,'description',0,3468,302,3466,1485153762,1,3467),(313,41,68,0,7519,'description',0,3469,303,3467,1485153762,1,3468),(313,41,68,0,7520,'description',0,3470,304,3468,1485153762,1,3469),(313,41,68,0,7521,'description',0,3471,305,3469,1485153762,1,3470),(313,41,68,0,7522,'description',0,3472,306,3470,1485153762,1,3471),(313,41,68,0,7523,'description',0,3473,307,3471,1485153762,1,3472),(313,41,68,0,7524,'description',0,3474,308,3472,1485153762,1,3473),(313,41,68,0,7525,'description',0,3475,309,3473,1485153762,1,3474),(313,41,68,0,7526,'description',0,3476,310,3474,1485153762,1,3475),(313,41,68,0,7527,'description',0,3477,311,3475,1485153762,1,3476),(313,41,68,0,7528,'description',0,3478,312,3476,1485153762,1,3477),(313,41,68,0,7529,'description',0,3479,313,3477,1485153762,1,3478),(313,41,68,0,7530,'description',0,3480,314,3478,1485153762,1,3479),(313,41,68,0,7531,'description',0,3481,315,3479,1485153762,1,3480),(313,41,68,0,7532,'description',0,3482,316,3480,1485153762,1,3481),(313,41,68,0,7533,'description',0,3483,317,3481,1485153762,1,3482),(313,41,68,0,7534,'description',0,3484,318,3482,1485153762,1,3483),(313,41,68,0,7535,'description',0,3485,319,3483,1485153762,1,3484),(313,41,68,0,7536,'description',0,3486,320,3484,1485153762,1,3485),(313,41,68,0,7537,'description',0,3487,321,3485,1485153762,1,3486),(313,41,68,0,7538,'description',0,3488,322,3486,1485153762,1,3487),(313,41,68,0,7539,'description',0,3489,323,3487,1485153762,1,3488),(313,41,68,0,7540,'description',0,3490,324,3488,1485153762,1,3489),(313,41,68,0,7541,'description',0,3491,325,3489,1485153762,1,3490),(313,41,68,0,7542,'description',0,3492,326,3490,1485153762,1,3491),(313,41,68,0,7543,'description',0,3493,327,3491,1485153762,1,3492),(313,41,68,0,7544,'description',0,3494,328,3492,1485153762,1,3493),(313,41,68,0,7545,'description',0,3495,329,3493,1485153762,1,3494),(313,41,68,0,7546,'description',0,3496,330,3494,1485153762,1,3495),(313,41,68,0,7547,'description',0,3497,331,3495,1485153762,1,3496),(313,41,68,0,7548,'description',0,3498,332,3496,1485153762,1,3497),(313,41,68,0,7549,'description',0,3499,333,3497,1485153762,1,3498),(313,41,68,0,7550,'description',0,3500,334,3498,1485153762,1,3499),(313,41,68,0,7551,'description',0,3501,335,3499,1485153762,1,3500),(313,41,68,0,7552,'description',0,3502,336,3500,1485153762,1,3501),(313,41,68,0,7553,'description',0,3503,337,3501,1485153762,1,3502),(313,41,68,0,7554,'description',0,3504,338,3502,1485153762,1,3503),(313,41,68,0,7555,'description',0,3505,339,3503,1485153762,1,3504),(313,41,68,0,7556,'description',0,3506,340,3504,1485153762,1,3505),(313,41,68,0,7557,'description',0,3507,341,3505,1485153762,1,3506),(313,41,68,0,7558,'description',0,3508,342,3506,1485153762,1,3507),(313,41,68,0,7559,'description',0,3509,343,3507,1485153762,1,3508),(313,41,68,0,7560,'description',0,3510,344,3508,1485153762,1,3509),(313,41,68,0,7561,'description',0,3511,345,3509,1485153762,1,3510),(313,41,68,0,7562,'description',0,3512,346,3510,1485153762,1,3511),(313,41,68,0,7563,'description',0,1766,347,3511,1485153762,1,3512),(313,41,68,0,7564,'description',0,1840,348,3512,1485153762,1,1766),(313,41,68,0,7565,'description',0,3513,349,1766,1485153762,1,1840),(313,41,68,0,7566,'description',0,3514,350,1840,1485153762,1,3513),(313,41,68,0,7567,'description',0,3515,351,3513,1485153762,1,3514),(313,41,68,0,7568,'description',0,3516,352,3514,1485153762,1,3515),(313,41,68,0,7569,'description',0,3517,353,3515,1485153762,1,3516),(313,41,68,0,7570,'description',0,3518,354,3516,1485153762,1,3517),(313,41,68,0,7571,'description',0,3519,355,3517,1485153762,1,3518),(313,41,68,0,7572,'description',0,3520,356,3518,1485153762,1,3519),(313,41,68,0,7573,'description',0,3521,357,3519,1485153762,1,3520),(313,41,68,0,7574,'description',0,3522,358,3520,1485153762,1,3521),(313,41,68,0,7575,'description',0,3523,359,3521,1485153762,1,3522),(313,41,68,0,7576,'description',0,3524,360,3522,1485153762,1,3523),(313,41,68,0,7577,'description',0,3525,361,3523,1485153762,1,3524),(313,41,68,0,7578,'description',0,3526,362,3524,1485153762,1,3525),(313,41,68,0,7579,'description',0,3527,363,3525,1485153762,1,3526),(313,41,68,0,7580,'description',0,3528,364,3526,1485153762,1,3527),(313,41,68,0,7581,'description',0,3529,365,3527,1485153762,1,3528),(313,41,68,0,7582,'description',0,3530,366,3528,1485153762,1,3529),(313,41,68,0,7583,'description',0,3531,367,3529,1485153762,1,3530),(313,41,68,0,7584,'description',0,3532,368,3530,1485153762,1,3531),(313,41,68,0,7585,'description',0,3533,369,3531,1485153762,1,3532),(313,41,68,0,7586,'description',0,3534,370,3532,1485153762,1,3533),(313,41,68,0,7587,'description',0,3535,371,3533,1485153762,1,3534),(313,41,68,0,7588,'description',0,3536,372,3534,1485153762,1,3535),(313,41,68,0,7589,'description',0,3537,373,3535,1485153762,1,3536),(313,41,68,0,7590,'description',0,3538,374,3536,1485153762,1,3537),(313,41,68,0,7591,'description',0,3539,375,3537,1485153762,1,3538),(313,41,68,0,7592,'description',0,3241,376,3538,1485153762,1,3539),(313,41,68,0,7593,'description',0,3540,377,3539,1485153762,1,3241),(313,41,68,0,7594,'description',0,3541,378,3241,1485153762,1,3540),(313,41,68,0,7595,'description',0,3542,379,3540,1485153762,1,3541),(313,41,68,0,7596,'description',0,3543,380,3541,1485153762,1,3542),(313,41,68,0,7597,'description',0,3544,381,3542,1485153762,1,3543),(313,41,68,0,7598,'description',0,3545,382,3543,1485153762,1,3544),(313,41,68,0,7599,'description',0,3546,383,3544,1485153762,1,3545),(313,41,68,0,7600,'description',0,3547,384,3545,1485153762,1,3546),(313,41,68,0,7601,'description',0,3548,385,3546,1485153762,1,3547),(313,41,68,0,7602,'description',0,1040,386,3547,1485153762,1,3548),(313,41,68,0,7603,'description',0,3549,387,3548,1485153762,1,1040),(313,41,68,0,7604,'description',0,3550,388,1040,1485153762,1,3549),(313,41,68,0,7605,'description',0,3551,389,3549,1485153762,1,3550),(313,41,68,0,7606,'description',0,3552,390,3550,1485153762,1,3551),(313,41,68,0,7607,'description',0,3553,391,3551,1485153762,1,3552),(313,41,68,0,7608,'description',0,3554,392,3552,1485153762,1,3553),(313,41,68,0,7609,'description',0,3555,393,3553,1485153762,1,3554),(313,41,68,0,7610,'description',0,3556,394,3554,1485153762,1,3555),(313,41,68,0,7611,'description',0,3557,395,3555,1485153762,1,3556),(313,41,68,0,7612,'description',0,3558,396,3556,1485153762,1,3557),(313,41,68,0,7613,'description',0,3559,397,3557,1485153762,1,3558),(313,41,68,0,7614,'description',0,3560,398,3558,1485153762,1,3559),(313,41,68,0,7615,'description',0,3561,399,3559,1485153762,1,3560),(313,41,68,0,7616,'description',0,3562,400,3560,1485153762,1,3561),(313,41,68,0,7617,'description',0,3563,401,3561,1485153762,1,3562),(313,41,68,0,7618,'description',0,3564,402,3562,1485153762,1,3563),(313,41,68,0,7619,'description',0,3565,403,3563,1485153762,1,3564),(313,41,68,0,7620,'description',0,3566,404,3564,1485153762,1,3565),(313,41,68,0,7621,'description',0,3567,405,3565,1485153762,1,3566),(313,41,68,0,7622,'description',0,3568,406,3566,1485153762,1,3567),(313,41,68,0,7623,'description',0,3569,407,3567,1485153762,1,3568),(313,41,68,0,7624,'description',0,3570,408,3568,1485153762,1,3569),(313,41,68,0,7625,'description',0,3571,409,3569,1485153762,1,3570),(313,41,68,0,7626,'description',0,3572,410,3570,1485153762,1,3571),(313,41,68,0,7627,'description',0,3036,411,3571,1485153762,1,3572),(313,41,68,0,7628,'description',0,1965,412,3572,1485153762,1,3036),(313,41,68,0,7629,'description',0,3573,413,3036,1485153762,1,1965),(313,41,68,0,7630,'description',0,3574,414,1965,1485153762,1,3573),(313,41,68,0,7631,'description',0,3575,415,3573,1485153762,1,3574),(313,41,68,0,7632,'description',0,2782,416,3574,1485153762,1,3575),(313,41,68,0,7633,'description',0,3576,417,3575,1485153762,1,2782),(313,41,68,0,7634,'description',0,3577,418,2782,1485153762,1,3576),(313,41,68,0,7635,'description',0,3578,419,3576,1485153762,1,3577),(313,41,68,0,7636,'description',0,3579,420,3577,1485153762,1,3578),(313,41,68,0,7637,'description',0,3580,421,3578,1485153762,1,3579),(313,41,68,0,7638,'description',0,3581,422,3579,1485153762,1,3580),(313,41,68,0,7639,'description',0,3582,423,3580,1485153762,1,3581),(313,41,68,0,7640,'description',0,3583,424,3581,1485153762,1,3582),(313,41,68,0,7641,'description',0,3584,425,3582,1485153762,1,3583),(313,41,68,0,7642,'description',0,3585,426,3583,1485153762,1,3584),(313,41,68,0,7643,'description',0,3586,427,3584,1485153762,1,3585),(313,41,68,0,7644,'description',0,3587,428,3585,1485153762,1,3586),(313,41,68,0,7645,'description',0,3588,429,3586,1485153762,1,3587),(313,41,68,0,7646,'description',0,3589,430,3587,1485153762,1,3588),(313,41,68,0,7647,'description',0,3590,431,3588,1485153762,1,3589),(313,41,68,0,7648,'description',0,3591,432,3589,1485153762,1,3590),(313,41,68,0,7649,'description',0,3592,433,3590,1485153762,1,3591),(313,41,68,0,7650,'description',0,3593,434,3591,1485153762,1,3592),(313,41,68,0,7651,'description',0,3594,435,3592,1485153762,1,3593),(313,41,68,0,7652,'description',0,3595,436,3593,1485153762,1,3594),(313,41,68,0,7653,'description',0,3596,437,3594,1485153762,1,3595),(313,41,68,0,7654,'description',0,3597,438,3595,1485153762,1,3596),(313,41,68,0,7655,'description',0,3598,439,3596,1485153762,1,3597),(313,41,68,0,7656,'description',0,3599,440,3597,1485153762,1,3598),(313,41,68,0,7657,'description',0,3600,441,3598,1485153762,1,3599),(313,41,68,0,7658,'description',0,3601,442,3599,1485153762,1,3600),(313,41,68,0,7659,'description',0,2108,443,3600,1485153762,1,3601),(313,41,68,0,7660,'description',0,3602,444,3601,1485153762,1,2108),(313,41,68,0,7661,'description',0,3603,445,2108,1485153762,1,3602),(313,41,68,0,7662,'description',0,3604,446,3602,1485153762,1,3603),(313,41,68,0,7663,'description',0,3605,447,3603,1485153762,1,3604),(313,41,68,0,7664,'description',0,3606,448,3604,1485153762,1,3605),(313,41,68,0,7665,'description',0,3607,449,3605,1485153762,1,3606),(313,41,68,0,7666,'description',0,3608,450,3606,1485153762,1,3607),(313,41,68,0,7667,'description',0,3609,451,3607,1485153762,1,3608),(313,41,68,0,7668,'description',0,3610,452,3608,1485153762,1,3609),(313,41,68,0,7669,'description',0,3611,453,3609,1485153762,1,3610),(313,41,68,0,7670,'description',0,3612,454,3610,1485153762,1,3611),(313,41,68,0,7671,'description',0,3613,455,3611,1485153762,1,3612),(313,41,68,0,7672,'description',0,3614,456,3612,1485153762,1,3613),(313,41,68,0,7673,'description',0,3615,457,3613,1485153762,1,3614),(313,41,68,0,7674,'description',0,3616,458,3614,1485153762,1,3615),(313,41,68,0,7675,'description',0,3617,459,3615,1485153762,1,3616),(313,41,68,0,7676,'description',0,3618,460,3616,1485153762,1,3617),(313,41,68,0,7677,'description',0,3619,461,3617,1485153762,1,3618),(313,41,68,0,7678,'description',0,3620,462,3618,1485153762,1,3619),(313,41,68,0,7679,'description',0,3621,463,3619,1485153762,1,3620),(313,41,68,0,7680,'description',0,3622,464,3620,1485153762,1,3621),(313,41,68,0,7681,'description',0,3623,465,3621,1485153762,1,3622),(313,41,68,0,7682,'description',0,3624,466,3622,1485153762,1,3623),(313,41,68,0,7683,'description',0,3625,467,3623,1485153762,1,3624),(313,41,68,0,7684,'description',0,3626,468,3624,1485153762,1,3625),(313,41,68,0,7685,'description',0,3627,469,3625,1485153762,1,3626),(313,41,68,0,7686,'description',0,3628,470,3626,1485153762,1,3627),(313,41,68,0,7687,'description',0,3629,471,3627,1485153762,1,3628),(313,41,68,0,7688,'description',0,3630,472,3628,1485153762,1,3629),(313,41,68,0,7689,'description',0,3631,473,3629,1485153762,1,3630),(313,41,68,0,7690,'description',0,3632,474,3630,1485153762,1,3631),(313,41,68,0,7691,'description',0,3633,475,3631,1485153762,1,3632),(313,41,68,0,7692,'description',0,3634,476,3632,1485153762,1,3633),(313,41,68,0,7693,'description',0,3635,477,3633,1485153762,1,3634),(313,41,68,0,7694,'description',0,3636,478,3634,1485153762,1,3635),(313,41,68,0,7695,'description',0,3637,479,3635,1485153762,1,3636),(313,41,68,0,7696,'description',0,1784,480,3636,1485153762,1,3637),(313,41,68,0,7697,'description',0,3638,481,3637,1485153762,1,1784),(313,41,68,0,7698,'description',0,3639,482,1784,1485153762,1,3638),(313,41,68,0,7699,'description',0,3640,483,3638,1485153762,1,3639),(313,41,68,0,7700,'description',0,3641,484,3639,1485153762,1,3640),(313,41,68,0,7701,'description',0,1696,485,3640,1485153762,1,3641),(313,41,68,0,7702,'description',0,3642,486,3641,1485153762,1,1696),(313,41,68,0,7703,'description',0,3643,487,1696,1485153762,1,3642),(313,41,68,0,7704,'description',0,1882,488,3642,1485153762,1,3643),(313,41,68,0,7705,'description',0,3644,489,3643,1485153762,1,1882),(313,41,68,0,7706,'description',0,3645,490,1882,1485153762,1,3644),(313,41,68,0,7707,'description',0,3646,491,3644,1485153762,1,3645),(313,41,68,0,7708,'description',0,3647,492,3645,1485153762,1,3646),(313,41,68,0,7709,'description',0,3648,493,3646,1485153762,1,3647),(313,41,68,0,7710,'description',0,3649,494,3647,1485153762,1,3648),(313,41,68,0,7711,'description',0,3650,495,3648,1485153762,1,3649),(313,41,68,0,7712,'description',0,3651,496,3649,1485153762,1,3650),(313,41,68,0,7713,'description',0,3652,497,3650,1485153762,1,3651),(313,41,68,0,7714,'description',0,3653,498,3651,1485153762,1,3652),(313,41,68,0,7715,'description',0,3654,499,3652,1485153762,1,3653),(313,41,68,0,7716,'description',0,3655,500,3653,1485153762,1,3654),(313,41,68,0,7717,'description',0,3656,501,3654,1485153762,1,3655),(313,41,68,0,7718,'description',0,3657,502,3655,1485153762,1,3656),(313,41,68,0,7719,'description',0,3658,503,3656,1485153762,1,3657),(313,41,68,0,7720,'description',0,3659,504,3657,1485153762,1,3658),(313,41,68,0,7721,'description',0,3660,505,3658,1485153762,1,3659),(313,41,68,0,7722,'description',0,3661,506,3659,1485153762,1,3660),(313,41,68,0,7723,'description',0,3662,507,3660,1485153762,1,3661),(313,41,68,0,7724,'description',0,3663,508,3661,1485153762,1,3662),(313,41,68,0,7725,'description',0,3664,509,3662,1485153762,1,3663),(313,41,68,0,7726,'description',0,3665,510,3663,1485153762,1,3664),(313,41,68,0,7727,'description',0,3666,511,3664,1485153762,1,3665),(313,41,68,0,7728,'description',0,3667,512,3665,1485153762,1,3666),(313,41,68,0,7729,'description',0,3668,513,3666,1485153762,1,3667),(313,41,68,0,7730,'description',0,3669,514,3667,1485153762,1,3668),(313,41,68,0,7731,'description',0,3670,515,3668,1485153762,1,3669),(313,41,68,0,7732,'description',0,1040,516,3669,1485153762,1,3670),(313,41,68,0,7733,'description',0,3671,517,3670,1485153762,1,1040),(313,41,68,0,7734,'description',0,3672,518,1040,1485153762,1,3671),(313,41,68,0,7735,'description',0,3673,519,3671,1485153762,1,3672),(313,41,68,0,7736,'description',0,1772,520,3672,1485153762,1,3673),(313,41,68,0,7737,'description',0,3674,521,3673,1485153762,1,1772),(313,41,68,0,7738,'description',0,3675,522,1772,1485153762,1,3674),(313,41,68,0,7739,'description',0,3676,523,3674,1485153762,1,3675),(313,41,68,0,7740,'description',0,3677,524,3675,1485153762,1,3676),(313,41,68,0,7741,'description',0,3678,525,3676,1485153762,1,3677),(313,41,68,0,7742,'description',0,3679,526,3677,1485153762,1,3678),(313,41,68,0,7743,'description',0,3680,527,3678,1485153762,1,3679),(313,41,68,0,7744,'description',0,3681,528,3679,1485153762,1,3680),(315,41,68,0,7745,'product_number',0,3682,529,3680,1485153762,1,3681),(315,41,68,0,7746,'product_number',0,3681,530,3681,1485153762,1,3682),(315,41,68,0,7747,'product_number',0,3682,531,3682,1485153762,1,3681),(315,41,68,0,7748,'product_number',0,0,532,3681,1485153762,1,3682),(312,41,66,0,7749,'title',0,3683,0,0,1485153594,1,1025),(312,41,66,0,7750,'title',0,2201,1,1025,1485153594,1,3683),(312,41,66,0,7751,'title',0,2202,2,3683,1485153594,1,2201),(312,41,66,0,7752,'title',0,1172,3,2201,1485153594,1,2202),(312,41,66,0,7753,'title',0,1032,4,2202,1485153594,1,1172),(313,41,66,0,7754,'description',0,1033,5,1172,1485153594,1,1032),(313,41,66,0,7755,'description',0,1034,6,1032,1485153594,1,1033),(313,41,66,0,7756,'description',0,1035,7,1033,1485153594,1,1034),(313,41,66,0,7757,'description',0,1036,8,1034,1485153594,1,1035),(313,41,66,0,7758,'description',0,3684,9,1035,1485153594,1,1036),(313,41,66,0,7759,'description',0,3685,10,1036,1485153594,1,3684),(313,41,66,0,7760,'description',0,3686,11,3684,1485153594,1,3685),(313,41,66,0,7761,'description',0,3572,12,3685,1485153594,1,3686),(313,41,66,0,7762,'description',0,3687,13,3686,1485153594,1,3572),(313,41,66,0,7763,'description',0,3688,14,3572,1485153594,1,3687),(313,41,66,0,7764,'description',0,3689,15,3687,1485153594,1,3688),(313,41,66,0,7765,'description',0,3690,16,3688,1485153594,1,3689),(313,41,66,0,7766,'description',0,3691,17,3689,1485153594,1,3690),(313,41,66,0,7767,'description',0,3692,18,3690,1485153594,1,3691),(313,41,66,0,7768,'description',0,2601,19,3691,1485153594,1,3692),(313,41,66,0,7769,'description',0,3693,20,3692,1485153594,1,2601),(313,41,66,0,7770,'description',0,3694,21,2601,1485153594,1,3693),(313,41,66,0,7771,'description',0,1052,22,3693,1485153594,1,3694),(313,41,66,0,7772,'description',0,3695,23,3694,1485153594,1,1052),(313,41,66,0,7773,'description',0,3696,24,1052,1485153594,1,3695),(313,41,66,0,7774,'description',0,3697,25,3695,1485153594,1,3696),(313,41,66,0,7775,'description',0,3698,26,3696,1485153594,1,3697),(313,41,66,0,7776,'description',0,3699,27,3697,1485153594,1,3698),(313,41,66,0,7777,'description',0,3700,28,3698,1485153594,1,3699),(313,41,66,0,7778,'description',0,3701,29,3699,1485153594,1,3700),(313,41,66,0,7779,'description',0,3702,30,3700,1485153594,1,3701),(313,41,66,0,7780,'description',0,3703,31,3701,1485153594,1,3702),(313,41,66,0,7781,'description',0,3704,32,3702,1485153594,1,3703),(313,41,66,0,7782,'description',0,3705,33,3703,1485153594,1,3704),(313,41,66,0,7783,'description',0,3706,34,3704,1485153594,1,3705),(313,41,66,0,7784,'description',0,3707,35,3705,1485153594,1,3706),(313,41,66,0,7785,'description',0,3708,36,3706,1485153594,1,3707),(313,41,66,0,7786,'description',0,3709,37,3707,1485153594,1,3708),(313,41,66,0,7787,'description',0,3710,38,3708,1485153594,1,3709),(313,41,66,0,7788,'description',0,3711,39,3709,1485153594,1,3710),(313,41,66,0,7789,'description',0,3712,40,3710,1485153594,1,3711),(313,41,66,0,7790,'description',0,3713,41,3711,1485153594,1,3712),(313,41,66,0,7791,'description',0,3714,42,3712,1485153594,1,3713),(313,41,66,0,7792,'description',0,3715,43,3713,1485153594,1,3714),(313,41,66,0,7793,'description',0,3716,44,3714,1485153594,1,3715),(313,41,66,0,7794,'description',0,3717,45,3715,1485153594,1,3716),(313,41,66,0,7795,'description',0,3718,46,3716,1485153594,1,3717),(313,41,66,0,7796,'description',0,3719,47,3717,1485153594,1,3718),(313,41,66,0,7797,'description',0,3720,48,3718,1485153594,1,3719),(313,41,66,0,7798,'description',0,3721,49,3719,1485153594,1,3720),(313,41,66,0,7799,'description',0,3722,50,3720,1485153594,1,3721),(313,41,66,0,7800,'description',0,3723,51,3721,1485153594,1,3722),(313,41,66,0,7801,'description',0,3724,52,3722,1485153594,1,3723),(313,41,66,0,7802,'description',0,3725,53,3723,1485153594,1,3724),(313,41,66,0,7803,'description',0,3726,54,3724,1485153594,1,3725),(313,41,66,0,7804,'description',0,3727,55,3725,1485153594,1,3726),(313,41,66,0,7805,'description',0,3728,56,3726,1485153594,1,3727),(313,41,66,0,7806,'description',0,3729,57,3727,1485153594,1,3728),(313,41,66,0,7807,'description',0,3730,58,3728,1485153594,1,3729),(313,41,66,0,7808,'description',0,3731,59,3729,1485153594,1,3730),(313,41,66,0,7809,'description',0,3732,60,3730,1485153594,1,3731),(313,41,66,0,7810,'description',0,3733,61,3731,1485153594,1,3732),(313,41,66,0,7811,'description',0,3734,62,3732,1485153594,1,3733),(313,41,66,0,7812,'description',0,3735,63,3733,1485153594,1,3734),(313,41,66,0,7813,'description',0,3736,64,3734,1485153594,1,3735),(313,41,66,0,7814,'description',0,3737,65,3735,1485153594,1,3736),(313,41,66,0,7815,'description',0,3738,66,3736,1485153594,1,3737),(313,41,66,0,7816,'description',0,3739,67,3737,1485153594,1,3738),(313,41,66,0,7817,'description',0,3740,68,3738,1485153594,1,3739),(313,41,66,0,7818,'description',0,3741,69,3739,1485153594,1,3740),(313,41,66,0,7819,'description',0,3084,70,3740,1485153594,1,3741),(313,41,66,0,7820,'description',0,3742,71,3741,1485153594,1,3084),(313,41,66,0,7821,'description',0,3743,72,3084,1485153594,1,3742),(313,41,66,0,7822,'description',0,3744,73,3742,1485153594,1,3743),(313,41,66,0,7823,'description',0,3745,74,3743,1485153594,1,3744),(313,41,66,0,7824,'description',0,3746,75,3744,1485153594,1,3745),(313,41,66,0,7825,'description',0,3747,76,3745,1485153594,1,3746),(313,41,66,0,7826,'description',0,3748,77,3746,1485153594,1,3747),(313,41,66,0,7827,'description',0,3749,78,3747,1485153594,1,3748),(313,41,66,0,7828,'description',0,3750,79,3748,1485153594,1,3749),(313,41,66,0,7829,'description',0,3751,80,3749,1485153594,1,3750),(313,41,66,0,7830,'description',0,3752,81,3750,1485153594,1,3751),(313,41,66,0,7831,'description',0,3059,82,3751,1485153594,1,3752),(313,41,66,0,7832,'description',0,3753,83,3752,1485153594,1,3059),(313,41,66,0,7833,'description',0,3754,84,3059,1485153594,1,3753),(313,41,66,0,7834,'description',0,3755,85,3753,1485153594,1,3754),(313,41,66,0,7835,'description',0,3756,86,3754,1485153594,1,3755),(313,41,66,0,7836,'description',0,3757,87,3755,1485153594,1,3756),(313,41,66,0,7837,'description',0,3758,88,3756,1485153594,1,3757),(313,41,66,0,7838,'description',0,3759,89,3757,1485153594,1,3758),(313,41,66,0,7839,'description',0,3760,90,3758,1485153594,1,3759),(313,41,66,0,7840,'description',0,3761,91,3759,1485153594,1,3760),(313,41,66,0,7841,'description',0,3762,92,3760,1485153594,1,3761),(313,41,66,0,7842,'description',0,3763,93,3761,1485153594,1,3762),(313,41,66,0,7843,'description',0,3764,94,3762,1485153594,1,3763),(313,41,66,0,7844,'description',0,3765,95,3763,1485153594,1,3764),(313,41,66,0,7845,'description',0,3766,96,3764,1485153594,1,3765),(313,41,66,0,7846,'description',0,3767,97,3765,1485153594,1,3766),(313,41,66,0,7847,'description',0,3768,98,3766,1485153594,1,3767),(313,41,66,0,7848,'description',0,3769,99,3767,1485153594,1,3768),(313,41,66,0,7849,'description',0,3770,100,3768,1485153594,1,3769),(313,41,66,0,7850,'description',0,3771,101,3769,1485153594,1,3770),(313,41,66,0,7851,'description',0,3772,102,3770,1485153594,1,3771),(313,41,66,0,7852,'description',0,3773,103,3771,1485153594,1,3772),(313,41,66,0,7853,'description',0,3774,104,3772,1485153594,1,3773),(313,41,66,0,7854,'description',0,3775,105,3773,1485153594,1,3774),(313,41,66,0,7855,'description',0,3776,106,3774,1485153594,1,3775),(313,41,66,0,7856,'description',0,3777,107,3775,1485153594,1,3776),(313,41,66,0,7857,'description',0,3778,108,3776,1485153594,1,3777),(313,41,66,0,7858,'description',0,3779,109,3777,1485153594,1,3778),(313,41,66,0,7859,'description',0,3780,110,3778,1485153594,1,3779),(313,41,66,0,7860,'description',0,3781,111,3779,1485153594,1,3780),(313,41,66,0,7861,'description',0,3782,112,3780,1485153594,1,3781),(313,41,66,0,7862,'description',0,3783,113,3781,1485153594,1,3782),(313,41,66,0,7863,'description',0,3784,114,3782,1485153594,1,3783),(313,41,66,0,7864,'description',0,3785,115,3783,1485153594,1,3784),(313,41,66,0,7865,'description',0,3786,116,3784,1485153594,1,3785),(313,41,66,0,7866,'description',0,3787,117,3785,1485153594,1,3786),(313,41,66,0,7867,'description',0,3788,118,3786,1485153594,1,3787),(313,41,66,0,7868,'description',0,3789,119,3787,1485153594,1,3788),(313,41,66,0,7869,'description',0,3790,120,3788,1485153594,1,3789),(313,41,66,0,7870,'description',0,3791,121,3789,1485153594,1,3790),(313,41,66,0,7871,'description',0,3792,122,3790,1485153594,1,3791),(313,41,66,0,7872,'description',0,3793,123,3791,1485153594,1,3792),(313,41,66,0,7873,'description',0,3794,124,3792,1485153594,1,3793),(313,41,66,0,7874,'description',0,3795,125,3793,1485153594,1,3794),(313,41,66,0,7875,'description',0,3796,126,3794,1485153594,1,3795),(313,41,66,0,7876,'description',0,3797,127,3795,1485153594,1,3796),(313,41,66,0,7877,'description',0,3798,128,3796,1485153594,1,3797),(313,41,66,0,7878,'description',0,3799,129,3797,1485153594,1,3798),(313,41,66,0,7879,'description',0,3800,130,3798,1485153594,1,3799),(313,41,66,0,7880,'description',0,3801,131,3799,1485153594,1,3800),(313,41,66,0,7881,'description',0,2365,132,3800,1485153594,1,3801),(313,41,66,0,7882,'description',0,3802,133,3801,1485153594,1,2365),(313,41,66,0,7883,'description',0,3803,134,2365,1485153594,1,3802),(313,41,66,0,7884,'description',0,3804,135,3802,1485153594,1,3803),(313,41,66,0,7885,'description',0,3805,136,3803,1485153594,1,3804),(313,41,66,0,7886,'description',0,3806,137,3804,1485153594,1,3805),(313,41,66,0,7887,'description',0,3807,138,3805,1485153594,1,3806),(313,41,66,0,7888,'description',0,3808,139,3806,1485153594,1,3807),(313,41,66,0,7889,'description',0,3809,140,3807,1485153594,1,3808),(313,41,66,0,7890,'description',0,3810,141,3808,1485153594,1,3809),(313,41,66,0,7891,'description',0,3811,142,3809,1485153594,1,3810),(313,41,66,0,7892,'description',0,3812,143,3810,1485153594,1,3811),(313,41,66,0,7893,'description',0,3813,144,3811,1485153594,1,3812),(313,41,66,0,7894,'description',0,3814,145,3812,1485153594,1,3813),(313,41,66,0,7895,'description',0,3815,146,3813,1485153594,1,3814),(313,41,66,0,7896,'description',0,3816,147,3814,1485153594,1,3815),(313,41,66,0,7897,'description',0,3817,148,3815,1485153594,1,3816),(313,41,66,0,7898,'description',0,3818,149,3816,1485153594,1,3817),(313,41,66,0,7899,'description',0,3819,150,3817,1485153594,1,3818),(313,41,66,0,7900,'description',0,3820,151,3818,1485153594,1,3819),(313,41,66,0,7901,'description',0,3821,152,3819,1485153594,1,3820),(313,41,66,0,7902,'description',0,3822,153,3820,1485153594,1,3821),(313,41,66,0,7903,'description',0,3823,154,3821,1485153594,1,3822),(313,41,66,0,7904,'description',0,2365,155,3822,1485153594,1,3823),(313,41,66,0,7905,'description',0,3824,156,3823,1485153594,1,2365),(313,41,66,0,7906,'description',0,3825,157,2365,1485153594,1,3824),(313,41,66,0,7907,'description',0,3826,158,3824,1485153594,1,3825),(313,41,66,0,7908,'description',0,3827,159,3825,1485153594,1,3826),(313,41,66,0,7909,'description',0,3828,160,3826,1485153594,1,3827),(313,41,66,0,7910,'description',0,3829,161,3827,1485153594,1,3828),(313,41,66,0,7911,'description',0,3830,162,3828,1485153594,1,3829),(313,41,66,0,7912,'description',0,3262,163,3829,1485153594,1,3830),(313,41,66,0,7913,'description',0,2602,164,3830,1485153594,1,3262),(313,41,66,0,7914,'description',0,3831,165,3262,1485153594,1,2602),(313,41,66,0,7915,'description',0,3832,166,2602,1485153594,1,3831),(313,41,66,0,7916,'description',0,3833,167,3831,1485153594,1,3832),(313,41,66,0,7917,'description',0,3834,168,3832,1485153594,1,3833),(313,41,66,0,7918,'description',0,3835,169,3833,1485153594,1,3834),(313,41,66,0,7919,'description',0,3836,170,3834,1485153594,1,3835),(313,41,66,0,7920,'description',0,3837,171,3835,1485153594,1,3836),(313,41,66,0,7921,'description',0,3838,172,3836,1485153594,1,3837),(313,41,66,0,7922,'description',0,3839,173,3837,1485153594,1,3838),(313,41,66,0,7923,'description',0,3840,174,3838,1485153594,1,3839),(313,41,66,0,7924,'description',0,3841,175,3839,1485153594,1,3840),(313,41,66,0,7925,'description',0,3842,176,3840,1485153594,1,3841),(313,41,66,0,7926,'description',0,3843,177,3841,1485153594,1,3842),(313,41,66,0,7927,'description',0,3844,178,3842,1485153594,1,3843),(313,41,66,0,7928,'description',0,3845,179,3843,1485153594,1,3844),(313,41,66,0,7929,'description',0,3846,180,3844,1485153594,1,3845),(313,41,66,0,7930,'description',0,3847,181,3845,1485153594,1,3846),(313,41,66,0,7931,'description',0,3848,182,3846,1485153594,1,3847),(313,41,66,0,7932,'description',0,3849,183,3847,1485153594,1,3848),(313,41,66,0,7933,'description',0,3850,184,3848,1485153594,1,3849),(313,41,66,0,7934,'description',0,3851,185,3849,1485153594,1,3850),(313,41,66,0,7935,'description',0,3428,186,3850,1485153594,1,3851),(313,41,66,0,7936,'description',0,1840,187,3851,1485153594,1,3428),(313,41,66,0,7937,'description',0,3852,188,3428,1485153594,1,1840),(313,41,66,0,7938,'description',0,3853,189,1840,1485153594,1,3852),(313,41,66,0,7939,'description',0,3854,190,3852,1485153594,1,3853),(313,41,66,0,7940,'description',0,3855,191,3853,1485153594,1,3854),(313,41,66,0,7941,'description',0,3856,192,3854,1485153594,1,3855),(313,41,66,0,7942,'description',0,1052,193,3855,1485153594,1,3856),(313,41,66,0,7943,'description',0,3857,194,3856,1485153594,1,1052),(313,41,66,0,7944,'description',0,3858,195,1052,1485153594,1,3857),(313,41,66,0,7945,'description',0,3859,196,3857,1485153594,1,3858),(313,41,66,0,7946,'description',0,3860,197,3858,1485153594,1,3859),(313,41,66,0,7947,'description',0,3861,198,3859,1485153594,1,3860),(313,41,66,0,7948,'description',0,3862,199,3860,1485153594,1,3861),(313,41,66,0,7949,'description',0,3863,200,3861,1485153594,1,3862),(313,41,66,0,7950,'description',0,3864,201,3862,1485153594,1,3863),(313,41,66,0,7951,'description',0,3865,202,3863,1485153594,1,3864),(313,41,66,0,7952,'description',0,3866,203,3864,1485153594,1,3865),(313,41,66,0,7953,'description',0,3867,204,3865,1485153594,1,3866),(313,41,66,0,7954,'description',0,3868,205,3866,1485153594,1,3867),(313,41,66,0,7955,'description',0,3869,206,3867,1485153594,1,3868),(313,41,66,0,7956,'description',0,3870,207,3868,1485153594,1,3869),(313,41,66,0,7957,'description',0,3871,208,3869,1485153594,1,3870),(313,41,66,0,7958,'description',0,3872,209,3870,1485153594,1,3871),(313,41,66,0,7959,'description',0,3873,210,3871,1485153594,1,3872),(313,41,66,0,7960,'description',0,3874,211,3872,1485153594,1,3873),(313,41,66,0,7961,'description',0,3875,212,3873,1485153594,1,3874),(313,41,66,0,7962,'description',0,3876,213,3874,1485153594,1,3875),(313,41,66,0,7963,'description',0,3217,214,3875,1485153594,1,3876),(313,41,66,0,7964,'description',0,3877,215,3876,1485153594,1,3217),(313,41,66,0,7965,'description',0,3878,216,3217,1485153594,1,3877),(313,41,66,0,7966,'description',0,3879,217,3877,1485153594,1,3878),(313,41,66,0,7967,'description',0,3880,218,3878,1485153594,1,3879),(313,41,66,0,7968,'description',0,3881,219,3879,1485153594,1,3880),(313,41,66,0,7969,'description',0,3882,220,3880,1485153594,1,3881),(313,41,66,0,7970,'description',0,3883,221,3881,1485153594,1,3882),(313,41,66,0,7971,'description',0,3884,222,3882,1485153594,1,3883),(313,41,66,0,7972,'description',0,3885,223,3883,1485153594,1,3884),(313,41,66,0,7973,'description',0,3886,224,3884,1485153594,1,3885),(313,41,66,0,7974,'description',0,3887,225,3885,1485153594,1,3886),(313,41,66,0,7975,'description',0,3888,226,3886,1485153594,1,3887),(313,41,66,0,7976,'description',0,3889,227,3887,1485153594,1,3888),(313,41,66,0,7977,'description',0,3890,228,3888,1485153594,1,3889),(313,41,66,0,7978,'description',0,3891,229,3889,1485153594,1,3890),(313,41,66,0,7979,'description',0,3892,230,3890,1485153594,1,3891),(313,41,66,0,7980,'description',0,1840,231,3891,1485153594,1,3892),(313,41,66,0,7981,'description',0,3893,232,3892,1485153594,1,1840),(313,41,66,0,7982,'description',0,3894,233,1840,1485153594,1,3893),(313,41,66,0,7983,'description',0,3895,234,3893,1485153594,1,3894),(313,41,66,0,7984,'description',0,3896,235,3894,1485153594,1,3895),(313,41,66,0,7985,'description',0,3897,236,3895,1485153594,1,3896),(313,41,66,0,7986,'description',0,3898,237,3896,1485153594,1,3897),(313,41,66,0,7987,'description',0,3899,238,3897,1485153594,1,3898),(313,41,66,0,7988,'description',0,3900,239,3898,1485153594,1,3899),(313,41,66,0,7989,'description',0,3901,240,3899,1485153594,1,3900),(313,41,66,0,7990,'description',0,3902,241,3900,1485153594,1,3901),(313,41,66,0,7991,'description',0,3903,242,3901,1485153594,1,3902),(313,41,66,0,7992,'description',0,3904,243,3902,1485153594,1,3903),(313,41,66,0,7993,'description',0,3905,244,3903,1485153594,1,3904),(313,41,66,0,7994,'description',0,3906,245,3904,1485153594,1,3905),(313,41,66,0,7995,'description',0,3907,246,3905,1485153594,1,3906),(313,41,66,0,7996,'description',0,3908,247,3906,1485153594,1,3907),(313,41,66,0,7997,'description',0,3909,248,3907,1485153594,1,3908),(313,41,66,0,7998,'description',0,3910,249,3908,1485153594,1,3909),(313,41,66,0,7999,'description',0,3911,250,3909,1485153594,1,3910),(313,41,66,0,8000,'description',0,3912,251,3910,1485153594,1,3911),(313,41,66,0,8001,'description',0,3913,252,3911,1485153594,1,3912),(313,41,66,0,8002,'description',0,3914,253,3912,1485153594,1,3913),(313,41,66,0,8003,'description',0,3915,254,3913,1485153594,1,3914),(313,41,66,0,8004,'description',0,3916,255,3914,1485153594,1,3915),(313,41,66,0,8005,'description',0,3917,256,3915,1485153594,1,3916),(313,41,66,0,8006,'description',0,3918,257,3916,1485153594,1,3917),(313,41,66,0,8007,'description',0,3919,258,3917,1485153594,1,3918),(313,41,66,0,8008,'description',0,3920,259,3918,1485153594,1,3919),(313,41,66,0,8009,'description',0,3921,260,3919,1485153594,1,3920),(313,41,66,0,8010,'description',0,3922,261,3920,1485153594,1,3921),(313,41,66,0,8011,'description',0,3923,262,3921,1485153594,1,3922),(313,41,66,0,8012,'description',0,3924,263,3922,1485153594,1,3923),(313,41,66,0,8013,'description',0,3925,264,3923,1485153594,1,3924),(313,41,66,0,8014,'description',0,3926,265,3924,1485153594,1,3925),(313,41,66,0,8015,'description',0,3927,266,3925,1485153594,1,3926),(313,41,66,0,8016,'description',0,3928,267,3926,1485153594,1,3927),(313,41,66,0,8017,'description',0,2672,268,3927,1485153594,1,3928),(313,41,66,0,8018,'description',0,3929,269,3928,1485153594,1,2672),(313,41,66,0,8019,'description',0,3930,270,2672,1485153594,1,3929),(313,41,66,0,8020,'description',0,3931,271,3929,1485153594,1,3930),(313,41,66,0,8021,'description',0,3932,272,3930,1485153594,1,3931),(313,41,66,0,8022,'description',0,3933,273,3931,1485153594,1,3932),(313,41,66,0,8023,'description',0,3934,274,3932,1485153594,1,3933),(313,41,66,0,8024,'description',0,1052,275,3933,1485153594,1,3934),(313,41,66,0,8025,'description',0,3935,276,3934,1485153594,1,1052),(313,41,66,0,8026,'description',0,3936,277,1052,1485153594,1,3935),(313,41,66,0,8027,'description',0,3937,278,3935,1485153594,1,3936),(313,41,66,0,8028,'description',0,3938,279,3936,1485153594,1,3937),(313,41,66,0,8029,'description',0,3939,280,3937,1485153594,1,3938),(313,41,66,0,8030,'description',0,3940,281,3938,1485153594,1,3939),(313,41,66,0,8031,'description',0,3941,282,3939,1485153594,1,3940),(313,41,66,0,8032,'description',0,3942,283,3940,1485153594,1,3941),(313,41,66,0,8033,'description',0,3943,284,3941,1485153594,1,3942),(313,41,66,0,8034,'description',0,3944,285,3942,1485153594,1,3943),(313,41,66,0,8035,'description',0,3945,286,3943,1485153594,1,3944),(313,41,66,0,8036,'description',0,3946,287,3944,1485153594,1,3945),(313,41,66,0,8037,'description',0,3947,288,3945,1485153594,1,3946),(313,41,66,0,8038,'description',0,3948,289,3946,1485153594,1,3947),(313,41,66,0,8039,'description',0,3949,290,3947,1485153594,1,3948),(313,41,66,0,8040,'description',0,3950,291,3948,1485153594,1,3949),(313,41,66,0,8041,'description',0,3951,292,3949,1485153594,1,3950),(313,41,66,0,8042,'description',0,3952,293,3950,1485153594,1,3951),(313,41,66,0,8043,'description',0,3953,294,3951,1485153594,1,3952),(313,41,66,0,8044,'description',0,3954,295,3952,1485153594,1,3953),(313,41,66,0,8045,'description',0,3955,296,3953,1485153594,1,3954),(313,41,66,0,8046,'description',0,3956,297,3954,1485153594,1,3955),(313,41,66,0,8047,'description',0,3957,298,3955,1485153594,1,3956),(313,41,66,0,8048,'description',0,1883,299,3956,1485153594,1,3957),(313,41,66,0,8049,'description',0,1884,300,3957,1485153594,1,1883),(313,41,66,0,8050,'description',0,3958,301,1883,1485153594,1,1884),(313,41,66,0,8051,'description',0,3959,302,1884,1485153594,1,3958),(313,41,66,0,8052,'description',0,3960,303,3958,1485153594,1,3959),(313,41,66,0,8053,'description',0,3961,304,3959,1485153594,1,3960),(313,41,66,0,8054,'description',0,3962,305,3960,1485153594,1,3961),(313,41,66,0,8055,'description',0,3963,306,3961,1485153594,1,3962),(313,41,66,0,8056,'description',0,3964,307,3962,1485153594,1,3963),(313,41,66,0,8057,'description',0,3965,308,3963,1485153594,1,3964),(313,41,66,0,8058,'description',0,3966,309,3964,1485153594,1,3965),(313,41,66,0,8059,'description',0,3967,310,3965,1485153594,1,3966),(313,41,66,0,8060,'description',0,1052,311,3966,1485153594,1,3967),(313,41,66,0,8061,'description',0,3968,312,3967,1485153594,1,1052),(313,41,66,0,8062,'description',0,2601,313,1052,1485153594,1,3968),(313,41,66,0,8063,'description',0,3969,314,3968,1485153594,1,2601),(313,41,66,0,8064,'description',0,3970,315,2601,1485153594,1,3969),(313,41,66,0,8065,'description',0,3971,316,3969,1485153594,1,3970),(313,41,66,0,8066,'description',0,3972,317,3970,1485153594,1,3971),(313,41,66,0,8067,'description',0,3973,318,3971,1485153594,1,3972),(313,41,66,0,8068,'description',0,3974,319,3972,1485153594,1,3973),(313,41,66,0,8069,'description',0,3975,320,3973,1485153594,1,3974),(313,41,66,0,8070,'description',0,3976,321,3974,1485153594,1,3975),(313,41,66,0,8071,'description',0,3977,322,3975,1485153594,1,3976),(313,41,66,0,8072,'description',0,3978,323,3976,1485153594,1,3977),(313,41,66,0,8073,'description',0,3979,324,3977,1485153594,1,3978),(313,41,66,0,8074,'description',0,3980,325,3978,1485153594,1,3979),(313,41,66,0,8075,'description',0,3981,326,3979,1485153594,1,3980),(313,41,66,0,8076,'description',0,3982,327,3980,1485153594,1,3981),(313,41,66,0,8077,'description',0,3983,328,3981,1485153594,1,3982),(313,41,66,0,8078,'description',0,3984,329,3982,1485153594,1,3983),(313,41,66,0,8079,'description',0,2683,330,3983,1485153594,1,3984),(313,41,66,0,8080,'description',0,3985,331,3984,1485153594,1,2683),(313,41,66,0,8081,'description',0,3986,332,2683,1485153594,1,3985),(313,41,66,0,8082,'description',0,3987,333,3985,1485153594,1,3986),(313,41,66,0,8083,'description',0,3988,334,3986,1485153594,1,3987),(313,41,66,0,8084,'description',0,3989,335,3987,1485153594,1,3988),(313,41,66,0,8085,'description',0,3990,336,3988,1485153594,1,3989),(313,41,66,0,8086,'description',0,3991,337,3989,1485153594,1,3990),(313,41,66,0,8087,'description',0,3992,338,3990,1485153594,1,3991),(313,41,66,0,8088,'description',0,3993,339,3991,1485153594,1,3992),(313,41,66,0,8089,'description',0,3994,340,3992,1485153594,1,3993),(313,41,66,0,8090,'description',0,3995,341,3993,1485153594,1,3994),(313,41,66,0,8091,'description',0,3996,342,3994,1485153594,1,3995),(313,41,66,0,8092,'description',0,3997,343,3995,1485153594,1,3996),(313,41,66,0,8093,'description',0,3998,344,3996,1485153594,1,3997),(313,41,66,0,8094,'description',0,3999,345,3997,1485153594,1,3998),(313,41,66,0,8095,'description',0,4000,346,3998,1485153594,1,3999),(313,41,66,0,8096,'description',0,4001,347,3999,1485153594,1,4000),(313,41,66,0,8097,'description',0,4002,348,4000,1485153594,1,4001),(313,41,66,0,8098,'description',0,4003,349,4001,1485153594,1,4002),(313,41,66,0,8099,'description',0,4004,350,4002,1485153594,1,4003),(313,41,66,0,8100,'description',0,4005,351,4003,1485153594,1,4004),(313,41,66,0,8101,'description',0,4006,352,4004,1485153594,1,4005),(313,41,66,0,8102,'description',0,4007,353,4005,1485153594,1,4006),(313,41,66,0,8103,'description',0,4008,354,4006,1485153594,1,4007),(313,41,66,0,8104,'description',0,4009,355,4007,1485153594,1,4008),(313,41,66,0,8105,'description',0,4010,356,4008,1485153594,1,4009),(313,41,66,0,8106,'description',0,4011,357,4009,1485153594,1,4010),(313,41,66,0,8107,'description',0,4012,358,4010,1485153594,1,4011),(313,41,66,0,8108,'description',0,4013,359,4011,1485153594,1,4012),(313,41,66,0,8109,'description',0,4014,360,4012,1485153594,1,4013),(313,41,66,0,8110,'description',0,4015,361,4013,1485153594,1,4014),(313,41,66,0,8111,'description',0,4016,362,4014,1485153594,1,4015),(313,41,66,0,8112,'description',0,4017,363,4015,1485153594,1,4016),(313,41,66,0,8113,'description',0,4018,364,4016,1485153594,1,4017),(313,41,66,0,8114,'description',0,4019,365,4017,1485153594,1,4018),(313,41,66,0,8115,'description',0,4020,366,4018,1485153594,1,4019),(313,41,66,0,8116,'description',0,4021,367,4019,1485153594,1,4020),(313,41,66,0,8117,'description',0,4022,368,4020,1485153594,1,4021),(313,41,66,0,8118,'description',0,1840,369,4021,1485153594,1,4022),(313,41,66,0,8119,'description',0,4023,370,4022,1485153594,1,1840),(313,41,66,0,8120,'description',0,1822,371,1840,1485153594,1,4023),(313,41,66,0,8121,'description',0,4024,372,4023,1485153594,1,1822),(313,41,66,0,8122,'description',0,4025,373,1822,1485153594,1,4024),(313,41,66,0,8123,'description',0,4026,374,4024,1485153594,1,4025),(313,41,66,0,8124,'description',0,4027,375,4025,1485153594,1,4026),(313,41,66,0,8125,'description',0,4028,376,4026,1485153594,1,4027),(313,41,66,0,8126,'description',0,4029,377,4027,1485153594,1,4028),(313,41,66,0,8127,'description',0,4030,378,4028,1485153594,1,4029),(313,41,66,0,8128,'description',0,4031,379,4029,1485153594,1,4030),(313,41,66,0,8129,'description',0,4032,380,4030,1485153594,1,4031),(313,41,66,0,8130,'description',0,4033,381,4031,1485153594,1,4032),(313,41,66,0,8131,'description',0,4034,382,4032,1485153594,1,4033),(313,41,66,0,8132,'description',0,4035,383,4033,1485153594,1,4034),(313,41,66,0,8133,'description',0,4036,384,4034,1485153594,1,4035),(313,41,66,0,8134,'description',0,4037,385,4035,1485153594,1,4036),(313,41,66,0,8135,'description',0,2488,386,4036,1485153594,1,4037),(313,41,66,0,8136,'description',0,4038,387,4037,1485153594,1,2488),(313,41,66,0,8137,'description',0,4039,388,2488,1485153594,1,4038),(313,41,66,0,8138,'description',0,4040,389,4038,1485153594,1,4039),(313,41,66,0,8139,'description',0,4041,390,4039,1485153594,1,4040),(313,41,66,0,8140,'description',0,4042,391,4040,1485153594,1,4041),(313,41,66,0,8141,'description',0,4043,392,4041,1485153594,1,4042),(313,41,66,0,8142,'description',0,4044,393,4042,1485153594,1,4043),(313,41,66,0,8143,'description',0,4045,394,4043,1485153594,1,4044),(313,41,66,0,8144,'description',0,4046,395,4044,1485153594,1,4045),(313,41,66,0,8145,'description',0,4047,396,4045,1485153594,1,4046),(313,41,66,0,8146,'description',0,4048,397,4046,1485153594,1,4047),(313,41,66,0,8147,'description',0,4049,398,4047,1485153594,1,4048),(313,41,66,0,8148,'description',0,4050,399,4048,1485153594,1,4049),(313,41,66,0,8149,'description',0,4051,400,4049,1485153594,1,4050),(313,41,66,0,8150,'description',0,4052,401,4050,1485153594,1,4051),(313,41,66,0,8151,'description',0,4053,402,4051,1485153594,1,4052),(313,41,66,0,8152,'description',0,4054,403,4052,1485153594,1,4053),(313,41,66,0,8153,'description',0,4055,404,4053,1485153594,1,4054),(313,41,66,0,8154,'description',0,4056,405,4054,1485153594,1,4055),(313,41,66,0,8155,'description',0,4057,406,4055,1485153594,1,4056),(313,41,66,0,8156,'description',0,4058,407,4056,1485153594,1,4057),(313,41,66,0,8157,'description',0,4059,408,4057,1485153594,1,4058),(313,41,66,0,8158,'description',0,4060,409,4058,1485153594,1,4059),(313,41,66,0,8159,'description',0,4061,410,4059,1485153594,1,4060),(313,41,66,0,8160,'description',0,4062,411,4060,1485153594,1,4061),(313,41,66,0,8161,'description',0,4063,412,4061,1485153594,1,4062),(313,41,66,0,8162,'description',0,4064,413,4062,1485153594,1,4063),(313,41,66,0,8163,'description',0,4065,414,4063,1485153594,1,4064),(313,41,66,0,8164,'description',0,4066,415,4064,1485153594,1,4065),(313,41,66,0,8165,'description',0,4067,416,4065,1485153594,1,4066),(313,41,66,0,8166,'description',0,4068,417,4066,1485153594,1,4067),(313,41,66,0,8167,'description',0,2420,418,4067,1485153594,1,4068),(313,41,66,0,8168,'description',0,4069,419,4068,1485153594,1,2420),(313,41,66,0,8169,'description',0,4070,420,2420,1485153594,1,4069),(313,41,66,0,8170,'description',0,4071,421,4069,1485153594,1,4070),(313,41,66,0,8171,'description',0,4072,422,4070,1485153594,1,4071),(313,41,66,0,8172,'description',0,4073,423,4071,1485153594,1,4072),(313,41,66,0,8173,'description',0,4074,424,4072,1485153594,1,4073),(313,41,66,0,8174,'description',0,4075,425,4073,1485153594,1,4074),(313,41,66,0,8175,'description',0,4076,426,4074,1485153594,1,4075),(313,41,66,0,8176,'description',0,4077,427,4075,1485153594,1,4076),(313,41,66,0,8177,'description',0,4078,428,4076,1485153594,1,4077),(313,41,66,0,8178,'description',0,4079,429,4077,1485153594,1,4078),(313,41,66,0,8179,'description',0,4080,430,4078,1485153594,1,4079),(313,41,66,0,8180,'description',0,4081,431,4079,1485153594,1,4080),(313,41,66,0,8181,'description',0,4082,432,4080,1485153594,1,4081),(313,41,66,0,8182,'description',0,4083,433,4081,1485153594,1,4082),(313,41,66,0,8183,'description',0,4084,434,4082,1485153594,1,4083),(313,41,66,0,8184,'description',0,4085,435,4083,1485153594,1,4084),(313,41,66,0,8185,'description',0,4086,436,4084,1485153594,1,4085),(313,41,66,0,8186,'description',0,4087,437,4085,1485153594,1,4086),(313,41,66,0,8187,'description',0,4088,438,4086,1485153594,1,4087),(313,41,66,0,8188,'description',0,4089,439,4087,1485153594,1,4088),(313,41,66,0,8189,'description',0,3285,440,4088,1485153594,1,4089),(313,41,66,0,8190,'description',0,4090,441,4089,1485153594,1,3285),(313,41,66,0,8191,'description',0,4091,442,3285,1485153594,1,4090),(313,41,66,0,8192,'description',0,4092,443,4090,1485153594,1,4091),(313,41,66,0,8193,'description',0,3375,444,4091,1485153594,1,4092),(313,41,66,0,8194,'description',0,4093,445,4092,1485153594,1,3375),(313,41,66,0,8195,'description',0,4094,446,3375,1485153594,1,4093),(313,41,66,0,8196,'description',0,4095,447,4093,1485153594,1,4094),(313,41,66,0,8197,'description',0,4096,448,4094,1485153594,1,4095),(313,41,66,0,8198,'description',0,4097,449,4095,1485153594,1,4096),(313,41,66,0,8199,'description',0,4098,450,4096,1485153594,1,4097),(313,41,66,0,8200,'description',0,4099,451,4097,1485153594,1,4098),(313,41,66,0,8201,'description',0,1696,452,4098,1485153594,1,4099),(313,41,66,0,8202,'description',0,4100,453,4099,1485153594,1,1696),(313,41,66,0,8203,'description',0,4101,454,1696,1485153594,1,4100),(313,41,66,0,8204,'description',0,4102,455,4100,1485153594,1,4101),(313,41,66,0,8205,'description',0,4103,456,4101,1485153594,1,4102),(313,41,66,0,8206,'description',0,4104,457,4102,1485153594,1,4103),(313,41,66,0,8207,'description',0,4105,458,4103,1485153594,1,4104),(313,41,66,0,8208,'description',0,4106,459,4104,1485153594,1,4105),(313,41,66,0,8209,'description',0,4107,460,4105,1485153594,1,4106),(313,41,66,0,8210,'description',0,4108,461,4106,1485153594,1,4107),(313,41,66,0,8211,'description',0,4109,462,4107,1485153594,1,4108),(313,41,66,0,8212,'description',0,4110,463,4108,1485153594,1,4109),(313,41,66,0,8213,'description',0,4111,464,4109,1485153594,1,4110),(313,41,66,0,8214,'description',0,3738,465,4110,1485153594,1,4111),(313,41,66,0,8215,'description',0,4112,466,4111,1485153594,1,3738),(313,41,66,0,8216,'description',0,4113,467,3738,1485153594,1,4112),(313,41,66,0,8217,'description',0,4114,468,4112,1485153594,1,4113),(313,41,66,0,8218,'description',0,3125,469,4113,1485153594,1,4114),(313,41,66,0,8219,'description',0,3411,470,4114,1485153594,1,3125),(313,41,66,0,8220,'description',0,4115,471,3125,1485153594,1,3411),(313,41,66,0,8221,'description',0,4116,472,3411,1485153594,1,4115),(313,41,66,0,8222,'description',0,4117,473,4115,1485153594,1,4116),(313,41,66,0,8223,'description',0,4118,474,4116,1485153594,1,4117),(313,41,66,0,8224,'description',0,4119,475,4117,1485153594,1,4118),(313,41,66,0,8225,'description',0,4120,476,4118,1485153594,1,4119),(313,41,66,0,8226,'description',0,4121,477,4119,1485153594,1,4120),(313,41,66,0,8227,'description',0,4122,478,4120,1485153594,1,4121),(313,41,66,0,8228,'description',0,4123,479,4121,1485153594,1,4122),(313,41,66,0,8229,'description',0,4124,480,4122,1485153594,1,4123),(313,41,66,0,8230,'description',0,4125,481,4123,1485153594,1,4124),(313,41,66,0,8231,'description',0,4126,482,4124,1485153594,1,4125),(313,41,66,0,8232,'description',0,4127,483,4125,1485153594,1,4126),(313,41,66,0,8233,'description',0,4128,484,4126,1485153594,1,4127),(313,41,66,0,8234,'description',0,4129,485,4127,1485153594,1,4128),(313,41,66,0,8235,'description',0,2058,486,4128,1485153594,1,4129),(313,41,66,0,8236,'description',0,4130,487,4129,1485153594,1,2058),(313,41,66,0,8237,'description',0,4131,488,2058,1485153594,1,4130),(313,41,66,0,8238,'description',0,4132,489,4130,1485153594,1,4131),(313,41,66,0,8239,'description',0,4133,490,4131,1485153594,1,4132),(313,41,66,0,8240,'description',0,4134,491,4132,1485153594,1,4133),(313,41,66,0,8241,'description',0,4135,492,4133,1485153594,1,4134),(313,41,66,0,8242,'description',0,4136,493,4134,1485153594,1,4135),(313,41,66,0,8243,'description',0,4137,494,4135,1485153594,1,4136),(313,41,66,0,8244,'description',0,4138,495,4136,1485153594,1,4137),(313,41,66,0,8245,'description',0,4139,496,4137,1485153594,1,4138),(313,41,66,0,8246,'description',0,4140,497,4138,1485153594,1,4139),(313,41,66,0,8247,'description',0,4141,498,4139,1485153594,1,4140),(313,41,66,0,8248,'description',0,4142,499,4140,1485153594,1,4141),(313,41,66,0,8249,'description',0,4143,500,4141,1485153594,1,4142),(313,41,66,0,8250,'description',0,2662,501,4142,1485153594,1,4143),(313,41,66,0,8251,'description',0,4144,502,4143,1485153594,1,2662),(313,41,66,0,8252,'description',0,4145,503,2662,1485153594,1,4144),(313,41,66,0,8253,'description',0,4146,504,4144,1485153594,1,4145),(313,41,66,0,8254,'description',0,4147,505,4145,1485153594,1,4146),(313,41,66,0,8255,'description',0,4148,506,4146,1485153594,1,4147),(313,41,66,0,8256,'description',0,4149,507,4147,1485153594,1,4148),(313,41,66,0,8257,'description',0,4150,508,4148,1485153594,1,4149),(313,41,66,0,8258,'description',0,4151,509,4149,1485153594,1,4150),(313,41,66,0,8259,'description',0,4152,510,4150,1485153594,1,4151),(313,41,66,0,8260,'description',0,4153,511,4151,1485153594,1,4152),(313,41,66,0,8261,'description',0,4154,512,4152,1485153594,1,4153),(313,41,66,0,8262,'description',0,4155,513,4153,1485153594,1,4154),(313,41,66,0,8263,'description',0,4156,514,4154,1485153594,1,4155),(313,41,66,0,8264,'description',0,4157,515,4155,1485153594,1,4156),(313,41,66,0,8265,'description',0,1040,516,4156,1485153594,1,4157),(313,41,66,0,8266,'description',0,4158,517,4157,1485153594,1,1040),(313,41,66,0,8267,'description',0,1710,518,1040,1485153594,1,4158),(313,41,66,0,8268,'description',0,2322,519,4158,1485153594,1,1710),(313,41,66,0,8269,'description',0,4159,520,1710,1485153594,1,2322),(313,41,66,0,8270,'description',0,4160,521,2322,1485153594,1,4159),(313,41,66,0,8271,'description',0,4161,522,4159,1485153594,1,4160),(313,41,66,0,8272,'description',0,4162,523,4160,1485153594,1,4161),(313,41,66,0,8273,'description',0,4163,524,4161,1485153594,1,4162),(313,41,66,0,8274,'description',0,4164,525,4162,1485153594,1,4163),(313,41,66,0,8275,'description',0,4165,526,4163,1485153594,1,4164),(313,41,66,0,8276,'description',0,4166,527,4164,1485153594,1,4165),(313,41,66,0,8277,'description',0,4167,528,4165,1485153594,1,4166),(313,41,66,0,8278,'description',0,4168,529,4166,1485153594,1,4167),(313,41,66,0,8279,'description',0,4169,530,4167,1485153594,1,4168),(313,41,66,0,8280,'description',0,4170,531,4168,1485153594,1,4169),(313,41,66,0,8281,'description',0,4171,532,4169,1485153594,1,4170),(313,41,66,0,8282,'description',0,4172,533,4170,1485153594,1,4171),(313,41,66,0,8283,'description',0,4173,534,4171,1485153594,1,4172),(313,41,66,0,8284,'description',0,4174,535,4172,1485153594,1,4173),(313,41,66,0,8285,'description',0,4175,536,4173,1485153594,1,4174),(313,41,66,0,8286,'description',0,4176,537,4174,1485153594,1,4175),(313,41,66,0,8287,'description',0,4177,538,4175,1485153594,1,4176),(313,41,66,0,8288,'description',0,4178,539,4176,1485153594,1,4177),(313,41,66,0,8289,'description',0,2732,540,4177,1485153594,1,4178),(313,41,66,0,8290,'description',0,4179,541,4178,1485153594,1,2732),(315,41,66,0,8291,'product_number',0,4180,542,2732,1485153594,1,4179),(315,41,66,0,8292,'product_number',0,4179,543,4179,1485153594,1,4180),(315,41,66,0,8293,'product_number',0,4180,544,4180,1485153594,1,4179),(315,41,66,0,8294,'product_number',0,0,545,4179,1485153594,1,4180),(319,43,71,0,8295,'title',0,4182,0,0,1485522423,1,4181),(319,43,71,0,8296,'title',0,4183,1,4181,1485522423,1,4182),(319,43,71,0,8297,'title',0,4184,2,4182,1485522423,1,4183),(319,43,71,0,8298,'title',0,4185,3,4183,1485522423,1,4184),(319,43,71,0,8299,'title',0,4186,4,4184,1485522423,1,4185),(319,43,71,0,8300,'title',0,4187,5,4185,1485522423,1,4186),(319,43,71,0,8301,'title',0,4188,6,4186,1485522423,1,4187),(319,43,71,0,8302,'title',0,4189,7,4187,1485522423,1,4188),(319,43,71,0,8303,'title',0,4190,8,4188,1485522423,1,4189),(320,43,71,0,8304,'description',0,4191,9,4189,1485522423,1,4190),(320,43,71,0,8305,'description',0,4192,10,4190,1485522423,1,4191),(320,43,71,0,8306,'description',0,4193,11,4191,1485522423,1,4192),(320,43,71,0,8307,'description',0,4194,12,4192,1485522423,1,4193),(320,43,71,0,8308,'description',0,4195,13,4193,1485522423,1,4194),(320,43,71,0,8309,'description',0,4196,14,4194,1485522423,1,4195),(320,43,71,0,8310,'description',0,4197,15,4195,1485522423,1,4196),(320,43,71,0,8311,'description',0,4198,16,4196,1485522423,1,4197),(320,43,71,0,8312,'description',0,4199,17,4197,1485522423,1,4198),(320,43,71,0,8313,'description',0,4200,18,4198,1485522423,1,4199),(320,43,71,0,8314,'description',0,4201,19,4199,1485522423,1,4200),(320,43,71,0,8315,'description',0,4202,20,4200,1485522423,1,4201),(320,43,71,0,8316,'description',0,4203,21,4201,1485522423,1,4202),(320,43,71,0,8317,'description',0,4204,22,4202,1485522423,1,4203),(320,43,71,0,8318,'description',0,4205,23,4203,1485522423,1,4204),(320,43,71,0,8319,'description',0,4206,24,4204,1485522423,1,4205),(320,43,71,0,8320,'description',0,4207,25,4205,1485522423,1,4206),(320,43,71,0,8321,'description',0,4208,26,4206,1485522423,1,4207),(320,43,71,0,8322,'description',0,4209,27,4207,1485522423,1,4208),(320,43,71,0,8323,'description',0,814,28,4208,1485522423,1,4209),(320,43,71,0,8324,'description',0,4210,29,4209,1485522423,1,814),(320,43,71,0,8325,'description',0,4211,30,814,1485522423,1,4210),(320,43,71,0,8326,'description',0,4212,31,4210,1485522423,1,4211),(320,43,71,0,8327,'description',0,4213,32,4211,1485522423,1,4212),(320,43,71,0,8328,'description',0,4214,33,4212,1485522423,1,4213),(320,43,71,0,8329,'description',0,4192,34,4213,1485522423,1,4214),(320,43,71,0,8330,'description',0,814,35,4214,1485522423,1,4192),(320,43,71,0,8331,'description',0,4215,36,4192,1485522423,1,814),(320,43,71,0,8332,'description',0,4216,37,814,1485522423,1,4215),(320,43,71,0,8333,'description',0,816,38,4215,1485522423,1,4216),(320,43,71,0,8334,'description',0,4217,39,4216,1485522423,1,816),(320,43,71,0,8335,'description',0,4218,40,816,1485522423,1,4217),(320,43,71,0,8336,'description',0,4219,41,4217,1485522423,1,4218),(320,43,71,0,8337,'description',0,4220,42,4218,1485522423,1,4219),(320,43,71,0,8338,'description',0,4221,43,4219,1485522423,1,4220),(320,43,71,0,8339,'description',0,816,44,4220,1485522423,1,4221),(320,43,71,0,8340,'description',0,4222,45,4221,1485522423,1,816),(320,43,71,0,8341,'description',0,4223,46,816,1485522423,1,4222),(320,43,71,0,8342,'description',0,4219,47,4222,1485522423,1,4223),(320,43,71,0,8343,'description',0,4224,48,4223,1485522423,1,4219),(320,43,71,0,8344,'description',0,4199,49,4219,1485522423,1,4224),(320,43,71,0,8345,'description',0,4204,50,4224,1485522423,1,4199),(320,43,71,0,8346,'description',0,4190,51,4199,1485522423,1,4204),(320,43,71,0,8347,'description',0,4225,52,4204,1485522423,1,4190),(320,43,71,0,8348,'description',0,4226,53,4190,1485522423,1,4225),(320,43,71,0,8349,'description',0,4203,54,4225,1485522423,1,4226),(320,43,71,0,8350,'description',0,4227,55,4226,1485522423,1,4203),(320,43,71,0,8351,'description',0,4228,56,4203,1485522423,1,4227),(320,43,71,0,8352,'description',0,4229,57,4227,1485522423,1,4228),(320,43,71,0,8353,'description',0,4230,58,4228,1485522423,1,4229),(320,43,71,0,8354,'description',0,4228,59,4229,1485522423,1,4230),(320,43,71,0,8355,'description',0,4231,60,4230,1485522423,1,4228),(320,43,71,0,8356,'description',0,4232,61,4228,1485522423,1,4231),(320,43,71,0,8357,'description',0,814,62,4231,1485522423,1,4232),(320,43,71,0,8358,'description',0,4233,63,4232,1485522423,1,814),(320,43,71,0,8359,'description',0,4234,64,814,1485522423,1,4233),(320,43,71,0,8360,'description',0,4235,65,4233,1485522423,1,4234),(320,43,71,0,8361,'description',0,4236,66,4234,1485522423,1,4235),(320,43,71,0,8362,'description',0,4237,67,4235,1485522423,1,4236),(320,43,71,0,8363,'description',0,4238,68,4236,1485522423,1,4237),(320,43,71,0,8364,'description',0,4239,69,4237,1485522423,1,4238),(320,43,71,0,8365,'description',0,4240,70,4238,1485522423,1,4239),(320,43,71,0,8366,'description',0,4203,71,4239,1485522423,1,4240),(320,43,71,0,8367,'description',0,4241,72,4240,1485522423,1,4203),(320,43,71,0,8368,'description',0,4242,73,4203,1485522423,1,4241),(320,43,71,0,8369,'description',0,4243,74,4241,1485522423,1,4242),(320,43,71,0,8370,'description',0,4199,75,4242,1485522423,1,4243),(320,43,71,0,8371,'description',0,4204,76,4243,1485522423,1,4199),(320,43,71,0,8372,'description',0,4202,77,4199,1485522423,1,4204),(320,43,71,0,8373,'description',0,4244,78,4204,1485522423,1,4202),(320,43,71,0,8374,'description',0,4245,79,4202,1485522423,1,4244),(320,43,71,0,8375,'description',0,4246,80,4244,1485522423,1,4245),(320,43,71,0,8376,'description',0,4247,81,4245,1485522423,1,4246),(320,43,71,0,8377,'description',0,4248,82,4246,1485522423,1,4247),(320,43,71,0,8378,'description',0,814,83,4247,1485522423,1,4248),(320,43,71,0,8379,'description',0,4249,84,4248,1485522423,1,814),(320,43,71,0,8380,'description',0,4234,85,814,1485522423,1,4249),(320,43,71,0,8381,'description',0,4250,86,4249,1485522423,1,4234),(320,43,71,0,8382,'description',0,4199,87,4234,1485522423,1,4250),(320,43,71,0,8383,'description',0,4204,88,4250,1485522423,1,4199),(320,43,71,0,8384,'description',0,4190,89,4199,1485522423,1,4204),(320,43,71,0,8385,'description',0,4251,90,4204,1485522423,1,4190),(320,43,71,0,8386,'description',0,4191,91,4190,1485522423,1,4251),(320,43,71,0,8387,'description',0,4252,92,4251,1485522423,1,4191),(320,43,71,0,8388,'description',0,4253,93,4191,1485522423,1,4252),(320,43,71,0,8389,'description',0,4203,94,4252,1485522423,1,4253),(320,43,71,0,8390,'description',0,4227,95,4253,1485522423,1,4203),(320,43,71,0,8391,'description',0,4215,96,4203,1485522423,1,4227),(320,43,71,0,8392,'description',0,4216,97,4227,1485522423,1,4215),(320,43,71,0,8393,'description',0,4248,98,4215,1485522423,1,4216),(320,43,71,0,8394,'description',0,4254,99,4216,1485522423,1,4248),(320,43,71,0,8395,'description',0,4255,100,4248,1485522423,1,4254),(320,43,71,0,8396,'description',0,4256,101,4254,1485522423,1,4255),(320,43,71,0,8397,'description',0,4257,102,4255,1485522423,1,4256),(320,43,71,0,8398,'description',0,814,103,4256,1485522423,1,4257),(320,43,71,0,8399,'description',0,4258,104,4257,1485522423,1,814),(320,43,71,0,8400,'description',0,4259,105,814,1485522423,1,4258),(320,43,71,0,8401,'description',0,4260,106,4258,1485522423,1,4259),(320,43,71,0,8402,'description',0,4261,107,4259,1485522423,1,4260),(320,43,71,0,8403,'description',0,4199,108,4260,1485522423,1,4261),(320,43,71,0,8404,'description',0,814,109,4261,1485522423,1,4199),(320,43,71,0,8405,'description',0,4262,110,4199,1485522423,1,814),(320,43,71,0,8406,'description',0,4263,111,814,1485522423,1,4262),(320,43,71,0,8407,'description',0,4199,112,4262,1485522423,1,4263),(320,43,71,0,8408,'description',0,4264,113,4263,1485522423,1,4199),(320,43,71,0,8409,'description',0,4265,114,4199,1485522423,1,4264),(320,43,71,0,8410,'description',0,4248,115,4264,1485522423,1,4265),(320,43,71,0,8411,'description',0,4254,116,4265,1485522423,1,4248),(320,43,71,0,8412,'description',0,4255,117,4248,1485522423,1,4254),(320,43,71,0,8413,'description',0,4256,118,4254,1485522423,1,4255),(320,43,71,0,8414,'description',0,4266,119,4255,1485522423,1,4256),(320,43,71,0,8415,'description',0,4267,120,4256,1485522423,1,4266),(320,43,71,0,8416,'description',0,4268,121,4266,1485522423,1,4267),(320,43,71,0,8417,'description',0,4228,122,4267,1485522423,1,4268),(320,43,71,0,8418,'description',0,4269,123,4268,1485522423,1,4228),(320,43,71,0,8419,'description',0,4270,124,4228,1485522423,1,4269),(320,43,71,0,8420,'description',0,4254,125,4269,1485522423,1,4270),(320,43,71,0,8421,'description',0,4224,126,4270,1485522423,1,4254),(320,43,71,0,8422,'description',0,4228,127,4254,1485522423,1,4224),(320,43,71,0,8423,'description',0,4271,128,4224,1485522423,1,4228),(320,43,71,0,8424,'description',0,4272,129,4228,1485522423,1,4271),(320,43,71,0,8425,'description',0,4199,130,4271,1485522423,1,4272),(320,43,71,0,8426,'description',0,4204,131,4272,1485522423,1,4199),(320,43,71,0,8427,'description',0,4273,132,4199,1485522423,1,4204),(320,43,71,0,8428,'description',0,4234,133,4204,1485522423,1,4273),(320,43,71,0,8429,'description',0,4274,134,4273,1485522423,1,4234),(320,43,71,0,8430,'description',0,4275,135,4234,1485522423,1,4274),(320,43,71,0,8431,'description',0,4276,136,4274,1485522423,1,4275),(320,43,71,0,8432,'description',0,4239,137,4275,1485522423,1,4276),(320,43,71,0,8433,'description',0,4190,138,4276,1485522423,1,4239),(320,43,71,0,8434,'description',0,4277,139,4239,1485522423,1,4190),(320,43,71,0,8435,'description',0,4278,140,4190,1485522423,1,4277),(320,43,71,0,8436,'description',0,4194,141,4277,1485522423,1,4278),(320,43,71,0,8437,'description',0,4204,142,4278,1485522423,1,4194),(320,43,71,0,8438,'description',0,4279,143,4194,1485522423,1,4204),(320,43,71,0,8439,'description',0,4280,144,4204,1485522423,1,4279),(320,43,71,0,8440,'description',0,4281,145,4279,1485522423,1,4280),(320,43,71,0,8441,'description',0,4282,146,4280,1485522423,1,4281),(320,43,71,0,8442,'description',0,4283,147,4281,1485522423,1,4282),(320,43,71,0,8443,'description',0,4284,148,4282,1485522423,1,4283),(320,43,71,0,8444,'description',0,4199,149,4283,1485522423,1,4284),(320,43,71,0,8445,'description',0,4204,150,4284,1485522423,1,4199),(320,43,71,0,8446,'description',0,4285,151,4199,1485522423,1,4204),(320,43,71,0,8447,'description',0,4234,152,4204,1485522423,1,4285),(320,43,71,0,8448,'description',0,4235,153,4285,1485522423,1,4234),(320,43,71,0,8449,'description',0,4286,154,4234,1485522423,1,4235),(320,43,71,0,8450,'description',0,4223,155,4235,1485522423,1,4286),(320,43,71,0,8451,'description',0,4230,156,4286,1485522423,1,4223),(320,43,71,0,8452,'description',0,4248,157,4223,1485522423,1,4230),(320,43,71,0,8453,'description',0,4287,158,4230,1485522423,1,4248),(320,43,71,0,8454,'description',0,4288,159,4248,1485522423,1,4287),(320,43,71,0,8455,'description',0,4289,160,4287,1485522423,1,4288),(320,43,71,0,8456,'description',0,4290,161,4288,1485522423,1,4289),(320,43,71,0,8457,'description',0,4291,162,4289,1485522423,1,4290),(320,43,71,0,8458,'description',0,4292,163,4290,1485522423,1,4291),(320,43,71,0,8459,'description',0,4293,164,4291,1485522423,1,4292),(320,43,71,0,8460,'description',0,4294,165,4292,1485522423,1,4293),(320,43,71,0,8461,'description',0,4295,166,4293,1485522423,1,4294),(320,43,71,0,8462,'description',0,4296,167,4294,1485522423,1,4295),(320,43,71,0,8463,'description',0,4297,168,4295,1485522423,1,4296),(320,43,71,0,8464,'description',0,4298,169,4296,1485522423,1,4297),(320,43,71,0,8465,'description',0,4299,170,4297,1485522423,1,4298),(320,43,71,0,8466,'description',0,4300,171,4298,1485522423,1,4299),(320,43,71,0,8467,'description',0,4301,172,4299,1485522423,1,4300),(320,43,71,0,8468,'description',0,4302,173,4300,1485522423,1,4301),(320,43,71,0,8469,'description',0,4303,174,4301,1485522423,1,4302),(320,43,71,0,8470,'description',0,4304,175,4302,1485522423,1,4303),(320,43,71,0,8471,'description',0,4305,176,4303,1485522423,1,4304),(320,43,71,0,8472,'description',0,4306,177,4304,1485522423,1,4305),(320,43,71,0,8473,'description',0,4307,178,4305,1485522423,1,4306),(320,43,71,0,8474,'description',0,4308,179,4306,1485522423,1,4307),(320,43,71,0,8475,'description',0,4309,180,4307,1485522423,1,4308),(320,43,71,0,8476,'description',0,4310,181,4308,1485522423,1,4309),(320,43,71,0,8477,'description',0,4311,182,4309,1485522423,1,4310),(320,43,71,0,8478,'description',0,4312,183,4310,1485522423,1,4311),(320,43,71,0,8479,'description',0,4313,184,4311,1485522423,1,4312),(320,43,71,0,8480,'description',0,4314,185,4312,1485522423,1,4313),(320,43,71,0,8481,'description',0,4315,186,4313,1485522423,1,4314),(320,43,71,0,8482,'description',0,2743,187,4314,1485522423,1,4315),(320,43,71,0,8483,'description',0,4316,188,4315,1485522423,1,2743),(320,43,71,0,8484,'description',0,4317,189,2743,1485522423,1,4316),(320,43,71,0,8485,'description',0,4318,190,4316,1485522423,1,4317),(320,43,71,0,8486,'description',0,4319,191,4317,1485522423,1,4318),(320,43,71,0,8487,'description',0,4320,192,4318,1485522423,1,4319),(320,43,71,0,8488,'description',0,4321,193,4319,1485522423,1,4320),(320,43,71,0,8489,'description',0,4322,194,4320,1485522423,1,4321),(320,43,71,0,8490,'description',0,4323,195,4321,1485522423,1,4322),(320,43,71,0,8491,'description',0,2304,196,4322,1485522423,1,4323),(320,43,71,0,8492,'description',0,4324,197,4323,1485522423,1,2304),(320,43,71,0,8493,'description',0,4325,198,2304,1485522423,1,4324),(320,43,71,0,8494,'description',0,4326,199,4324,1485522423,1,4325),(320,43,71,0,8495,'description',0,2879,200,4325,1485522423,1,4326),(320,43,71,0,8496,'description',0,4327,201,4326,1485522423,1,2879),(320,43,71,0,8497,'description',0,4328,202,2879,1485522423,1,4327),(320,43,71,0,8498,'description',0,4329,203,4327,1485522423,1,4328),(320,43,71,0,8499,'description',0,4330,204,4328,1485522423,1,4329),(320,43,71,0,8500,'description',0,4331,205,4329,1485522423,1,4330),(320,43,71,0,8501,'description',0,4332,206,4330,1485522423,1,4331),(320,43,71,0,8502,'description',0,4333,207,4331,1485522423,1,4332),(320,43,71,0,8503,'description',0,4334,208,4332,1485522423,1,4333),(320,43,71,0,8504,'description',0,4335,209,4333,1485522423,1,4334),(320,43,71,0,8505,'description',0,4336,210,4334,1485522423,1,4335),(320,43,71,0,8506,'description',0,4337,211,4335,1485522423,1,4336),(320,43,71,0,8507,'description',0,4338,212,4336,1485522423,1,4337),(320,43,71,0,8508,'description',0,4339,213,4337,1485522423,1,4338),(320,43,71,0,8509,'description',0,4340,214,4338,1485522423,1,4339),(320,43,71,0,8510,'description',0,4341,215,4339,1485522423,1,4340),(320,43,71,0,8511,'description',0,4342,216,4340,1485522423,1,4341),(320,43,71,0,8512,'description',0,4343,217,4341,1485522423,1,4342),(320,43,71,0,8513,'description',0,4344,218,4342,1485522423,1,4343),(320,43,71,0,8514,'description',0,4345,219,4343,1485522423,1,4344),(320,43,71,0,8515,'description',0,4346,220,4344,1485522423,1,4345),(320,43,71,0,8516,'description',0,4347,221,4345,1485522423,1,4346),(320,43,71,0,8517,'description',0,4348,222,4346,1485522423,1,4347),(320,43,71,0,8518,'description',0,4349,223,4347,1485522423,1,4348),(320,43,71,0,8519,'description',0,4350,224,4348,1485522423,1,4349),(320,43,71,0,8520,'description',0,4351,225,4349,1485522423,1,4350),(320,43,71,0,8521,'description',0,4352,226,4350,1485522423,1,4351),(320,43,71,0,8522,'description',0,4353,227,4351,1485522423,1,4352),(320,43,71,0,8523,'description',0,4354,228,4352,1485522423,1,4353),(320,43,71,0,8524,'description',0,4355,229,4353,1485522423,1,4354),(320,43,71,0,8525,'description',0,4356,230,4354,1485522423,1,4355),(320,43,71,0,8526,'description',0,4357,231,4355,1485522423,1,4356),(320,43,71,0,8527,'description',0,4358,232,4356,1485522423,1,4357),(320,43,71,0,8528,'description',0,4359,233,4357,1485522423,1,4358),(320,43,71,0,8529,'description',0,4360,234,4358,1485522423,1,4359),(320,43,71,0,8530,'description',0,4361,235,4359,1485522423,1,4360),(320,43,71,0,8531,'description',0,4362,236,4360,1485522423,1,4361),(320,43,71,0,8532,'description',0,4363,237,4361,1485522423,1,4362),(320,43,71,0,8533,'description',0,4364,238,4362,1485522423,1,4363),(320,43,71,0,8534,'description',0,4365,239,4363,1485522423,1,4364),(320,43,71,0,8535,'description',0,4366,240,4364,1485522423,1,4365),(320,43,71,0,8536,'description',0,4367,241,4365,1485522423,1,4366),(320,43,71,0,8537,'description',0,4368,242,4366,1485522423,1,4367),(320,43,71,0,8538,'description',0,4369,243,4367,1485522423,1,4368),(320,43,71,0,8539,'description',0,4370,244,4368,1485522423,1,4369),(320,43,71,0,8540,'description',0,4371,245,4369,1485522423,1,4370),(320,43,71,0,8541,'description',0,4372,246,4370,1485522423,1,4371),(320,43,71,0,8542,'description',0,4373,247,4371,1485522423,1,4372),(320,43,71,0,8543,'description',0,3410,248,4372,1485522423,1,4373),(320,43,71,0,8544,'description',0,4374,249,4373,1485522423,1,3410),(320,43,71,0,8545,'description',0,4375,250,3410,1485522423,1,4374),(320,43,71,0,8546,'description',0,4376,251,4374,1485522423,1,4375),(320,43,71,0,8547,'description',0,4377,252,4375,1485522423,1,4376),(320,43,71,0,8548,'description',0,4378,253,4376,1485522423,1,4377),(320,43,71,0,8549,'description',0,4379,254,4377,1485522423,1,4378),(320,43,71,0,8550,'description',0,4380,255,4378,1485522423,1,4379),(320,43,71,0,8551,'description',0,4381,256,4379,1485522423,1,4380),(320,43,71,0,8552,'description',0,4382,257,4380,1485522423,1,4381),(320,43,71,0,8553,'description',0,4383,258,4381,1485522423,1,4382),(320,43,71,0,8554,'description',0,4384,259,4382,1485522423,1,4383),(320,43,71,0,8555,'description',0,4385,260,4383,1485522423,1,4384),(320,43,71,0,8556,'description',0,4386,261,4384,1485522423,1,4385),(320,43,71,0,8557,'description',0,4387,262,4385,1485522423,1,4386),(320,43,71,0,8558,'description',0,4388,263,4386,1485522423,1,4387),(320,43,71,0,8559,'description',0,4389,264,4387,1485522423,1,4388),(320,43,71,0,8560,'description',0,4390,265,4388,1485522423,1,4389),(320,43,71,0,8561,'description',0,4391,266,4389,1485522423,1,4390),(320,43,71,0,8562,'description',0,4392,267,4390,1485522423,1,4391),(320,43,71,0,8563,'description',0,4393,268,4391,1485522423,1,4392),(320,43,71,0,8564,'description',0,4394,269,4392,1485522423,1,4393),(320,43,71,0,8565,'description',0,4395,270,4393,1485522423,1,4394),(320,43,71,0,8566,'description',0,4396,271,4394,1485522423,1,4395),(320,43,71,0,8567,'description',0,4397,272,4395,1485522423,1,4396),(320,43,71,0,8568,'description',0,4398,273,4396,1485522423,1,4397),(320,43,71,0,8569,'description',0,4399,274,4397,1485522423,1,4398),(320,43,71,0,8570,'description',0,4400,275,4398,1485522423,1,4399),(320,43,71,0,8571,'description',0,4401,276,4399,1485522423,1,4400),(320,43,71,0,8572,'description',0,4402,277,4400,1485522423,1,4401),(320,43,71,0,8573,'description',0,4403,278,4401,1485522423,1,4402),(320,43,71,0,8574,'description',0,4404,279,4402,1485522423,1,4403),(320,43,71,0,8575,'description',0,4405,280,4403,1485522423,1,4404),(320,43,71,0,8576,'description',0,4406,281,4404,1485522423,1,4405),(320,43,71,0,8577,'description',0,4407,282,4405,1485522423,1,4406),(320,43,71,0,8578,'description',0,4408,283,4406,1485522423,1,4407),(320,43,71,0,8579,'description',0,4409,284,4407,1485522423,1,4408),(320,43,71,0,8580,'description',0,4410,285,4408,1485522423,1,4409),(320,43,71,0,8581,'description',0,4411,286,4409,1485522423,1,4410),(320,43,71,0,8582,'description',0,4412,287,4410,1485522423,1,4411),(320,43,71,0,8583,'description',0,4413,288,4411,1485522423,1,4412),(320,43,71,0,8584,'description',0,4414,289,4412,1485522423,1,4413),(320,43,71,0,8585,'description',0,4415,290,4413,1485522423,1,4414),(320,43,71,0,8586,'description',0,4416,291,4414,1485522423,1,4415),(320,43,71,0,8587,'description',0,4417,292,4415,1485522423,1,4416),(320,43,71,0,8588,'description',0,4418,293,4416,1485522423,1,4417),(320,43,71,0,8589,'description',0,4419,294,4417,1485522423,1,4418),(320,43,71,0,8590,'description',0,4420,295,4418,1485522423,1,4419),(320,43,71,0,8591,'description',0,4421,296,4419,1485522423,1,4420),(320,43,71,0,8592,'description',0,4422,297,4420,1485522423,1,4421),(320,43,71,0,8593,'description',0,3992,298,4421,1485522423,1,4422),(320,43,71,0,8594,'description',0,4423,299,4422,1485522423,1,3992),(320,43,71,0,8595,'description',0,4424,300,3992,1485522423,1,4423),(320,43,71,0,8596,'description',0,4425,301,4423,1485522423,1,4424),(320,43,71,0,8597,'description',0,4426,302,4424,1485522423,1,4425),(320,43,71,0,8598,'description',0,4427,303,4425,1485522423,1,4426),(320,43,71,0,8599,'description',0,4428,304,4426,1485522423,1,4427),(320,43,71,0,8600,'description',0,4429,305,4427,1485522423,1,4428),(320,43,71,0,8601,'description',0,4430,306,4428,1485522423,1,4429),(320,43,71,0,8602,'description',0,4431,307,4429,1485522423,1,4430),(320,43,71,0,8603,'description',0,4432,308,4430,1485522423,1,4431),(320,43,71,0,8604,'description',0,4433,309,4431,1485522423,1,4432),(320,43,71,0,8605,'description',0,4434,310,4432,1485522423,1,4433),(320,43,71,0,8606,'description',0,4435,311,4433,1485522423,1,4434),(320,43,71,0,8607,'description',0,4436,312,4434,1485522423,1,4435),(320,43,71,0,8608,'description',0,4437,313,4435,1485522423,1,4436),(320,43,71,0,8609,'description',0,4438,314,4436,1485522423,1,4437),(320,43,71,0,8610,'description',0,4439,315,4437,1485522423,1,4438),(320,43,71,0,8611,'description',0,4440,316,4438,1485522423,1,4439),(320,43,71,0,8612,'description',0,4441,317,4439,1485522423,1,4440),(320,43,71,0,8613,'description',0,4442,318,4440,1485522423,1,4441),(320,43,71,0,8614,'description',0,4443,319,4441,1485522423,1,4442),(320,43,71,0,8615,'description',0,4444,320,4442,1485522423,1,4443),(320,43,71,0,8616,'description',0,4445,321,4443,1485522423,1,4444),(320,43,71,0,8617,'description',0,4446,322,4444,1485522423,1,4445),(320,43,71,0,8618,'description',0,4447,323,4445,1485522423,1,4446),(320,43,71,0,8619,'description',0,4448,324,4446,1485522423,1,4447),(320,43,71,0,8620,'description',0,4449,325,4447,1485522423,1,4448),(320,43,71,0,8621,'description',0,4450,326,4448,1485522423,1,4449),(320,43,71,0,8622,'description',0,4451,327,4449,1485522423,1,4450),(320,43,71,0,8623,'description',0,4452,328,4450,1485522423,1,4451),(320,43,71,0,8624,'description',0,1840,329,4451,1485522423,1,4452),(320,43,71,0,8625,'description',0,4453,330,4452,1485522423,1,1840),(320,43,71,0,8626,'description',0,4454,331,1840,1485522423,1,4453),(320,43,71,0,8627,'description',0,1805,332,4453,1485522423,1,4454),(320,43,71,0,8628,'description',0,4455,333,4454,1485522423,1,1805),(320,43,71,0,8629,'description',0,4456,334,1805,1485522423,1,4455),(320,43,71,0,8630,'description',0,4457,335,4455,1485522423,1,4456),(320,43,71,0,8631,'description',0,4458,336,4456,1485522423,1,4457),(320,43,71,0,8632,'description',0,4459,337,4457,1485522423,1,4458),(320,43,71,0,8633,'description',0,4460,338,4458,1485522423,1,4459),(320,43,71,0,8634,'description',0,4461,339,4459,1485522423,1,4460),(320,43,71,0,8635,'description',0,4462,340,4460,1485522423,1,4461),(320,43,71,0,8636,'description',0,4463,341,4461,1485522423,1,4462),(320,43,71,0,8637,'description',0,4464,342,4462,1485522423,1,4463),(320,43,71,0,8638,'description',0,4465,343,4463,1485522423,1,4464),(320,43,71,0,8639,'description',0,4466,344,4464,1485522423,1,4465),(320,43,71,0,8640,'description',0,4467,345,4465,1485522423,1,4466),(320,43,71,0,8641,'description',0,4468,346,4466,1485522423,1,4467),(320,43,71,0,8642,'description',0,4469,347,4467,1485522423,1,4468),(320,43,71,0,8643,'description',0,4470,348,4468,1485522423,1,4469),(320,43,71,0,8644,'description',0,4471,349,4469,1485522423,1,4470),(320,43,71,0,8645,'description',0,4472,350,4470,1485522423,1,4471),(320,43,71,0,8646,'description',0,4473,351,4471,1485522423,1,4472),(320,43,71,0,8647,'description',0,4474,352,4472,1485522423,1,4473),(320,43,71,0,8648,'description',0,4475,353,4473,1485522423,1,4474),(320,43,71,0,8649,'description',0,4476,354,4474,1485522423,1,4475),(320,43,71,0,8650,'description',0,4477,355,4475,1485522423,1,4476),(320,43,71,0,8651,'description',0,4478,356,4476,1485522423,1,4477),(320,43,71,0,8652,'description',0,4479,357,4477,1485522423,1,4478),(320,43,71,0,8653,'description',0,4480,358,4478,1485522423,1,4479),(320,43,71,0,8654,'description',0,4481,359,4479,1485522423,1,4480),(320,43,71,0,8655,'description',0,4482,360,4480,1485522423,1,4481),(320,43,71,0,8656,'description',0,4483,361,4481,1485522423,1,4482),(320,43,71,0,8657,'description',0,4484,362,4482,1485522423,1,4483),(320,43,71,0,8658,'description',0,4485,363,4483,1485522423,1,4484),(320,43,71,0,8659,'description',0,4486,364,4484,1485522423,1,4485),(320,43,71,0,8660,'description',0,4487,365,4485,1485522423,1,4486),(320,43,71,0,8661,'description',0,4488,366,4486,1485522423,1,4487),(320,43,71,0,8662,'description',0,4489,367,4487,1485522423,1,4488),(320,43,71,0,8663,'description',0,4490,368,4488,1485522423,1,4489),(320,43,71,0,8664,'description',0,4491,369,4489,1485522423,1,4490),(320,43,71,0,8665,'description',0,4492,370,4490,1485522423,1,4491),(320,43,71,0,8666,'description',0,4493,371,4491,1485522423,1,4492),(320,43,71,0,8667,'description',0,4494,372,4492,1485522423,1,4493),(320,43,71,0,8668,'description',0,4495,373,4493,1485522423,1,4494),(320,43,71,0,8669,'description',0,4496,374,4494,1485522423,1,4495),(320,43,71,0,8670,'description',0,4497,375,4495,1485522423,1,4496),(320,43,71,0,8671,'description',0,4498,376,4496,1485522423,1,4497),(320,43,71,0,8672,'description',0,4499,377,4497,1485522423,1,4498),(320,43,71,0,8673,'description',0,4500,378,4498,1485522423,1,4499),(320,43,71,0,8674,'description',0,4501,379,4499,1485522423,1,4500),(320,43,71,0,8675,'description',0,4502,380,4500,1485522423,1,4501),(320,43,71,0,8676,'description',0,4503,381,4501,1485522423,1,4502),(320,43,71,0,8677,'description',0,4504,382,4502,1485522423,1,4503),(320,43,71,0,8678,'description',0,4505,383,4503,1485522423,1,4504),(320,43,71,0,8679,'description',0,4506,384,4504,1485522423,1,4505),(320,43,71,0,8680,'description',0,4507,385,4505,1485522423,1,4506),(320,43,71,0,8681,'description',0,4508,386,4506,1485522423,1,4507),(320,43,71,0,8682,'description',0,4509,387,4507,1485522423,1,4508),(320,43,71,0,8683,'description',0,4510,388,4508,1485522423,1,4509),(320,43,71,0,8684,'description',0,4511,389,4509,1485522423,1,4510),(320,43,71,0,8685,'description',0,4512,390,4510,1485522423,1,4511),(320,43,71,0,8686,'description',0,4513,391,4511,1485522423,1,4512),(320,43,71,0,8687,'description',0,4514,392,4512,1485522423,1,4513),(320,43,71,0,8688,'description',0,4515,393,4513,1485522423,1,4514),(320,43,71,0,8689,'description',0,4516,394,4514,1485522423,1,4515),(320,43,71,0,8690,'description',0,4517,395,4515,1485522423,1,4516),(320,43,71,0,8691,'description',0,1839,396,4516,1485522423,1,4517),(320,43,71,0,8692,'description',0,4518,397,4517,1485522423,1,1839),(320,43,71,0,8693,'description',0,4519,398,1839,1485522423,1,4518),(320,43,71,0,8694,'description',0,4520,399,4518,1485522423,1,4519),(320,43,71,0,8695,'description',0,4521,400,4519,1485522423,1,4520),(320,43,71,0,8696,'description',0,4522,401,4520,1485522423,1,4521),(320,43,71,0,8697,'description',0,4523,402,4521,1485522423,1,4522),(320,43,71,0,8698,'description',0,4524,403,4522,1485522423,1,4523),(320,43,71,0,8699,'description',0,4525,404,4523,1485522423,1,4524),(320,43,71,0,8700,'description',0,4526,405,4524,1485522423,1,4525),(320,43,71,0,8701,'description',0,4527,406,4525,1485522423,1,4526),(320,43,71,0,8702,'description',0,4085,407,4526,1485522423,1,4527),(320,43,71,0,8703,'description',0,4528,408,4527,1485522423,1,4085),(320,43,71,0,8704,'description',0,4529,409,4085,1485522423,1,4528),(320,43,71,0,8705,'description',0,4530,410,4528,1485522423,1,4529),(320,43,71,0,8706,'description',0,4531,411,4529,1485522423,1,4530),(320,43,71,0,8707,'description',0,4532,412,4530,1485522423,1,4531),(320,43,71,0,8708,'description',0,4533,413,4531,1485522423,1,4532),(320,43,71,0,8709,'description',0,4534,414,4532,1485522423,1,4533),(320,43,71,0,8710,'description',0,4535,415,4533,1485522423,1,4534),(320,43,71,0,8711,'description',0,4536,416,4534,1485522423,1,4535),(320,43,71,0,8712,'description',0,4537,417,4535,1485522423,1,4536),(320,43,71,0,8713,'description',0,4538,418,4536,1485522423,1,4537),(320,43,71,0,8714,'description',0,4539,419,4537,1485522423,1,4538),(320,43,71,0,8715,'description',0,4540,420,4538,1485522423,1,4539),(320,43,71,0,8716,'description',0,4541,421,4539,1485522423,1,4540),(320,43,71,0,8717,'description',0,4542,422,4540,1485522423,1,4541),(320,43,71,0,8718,'description',0,4543,423,4541,1485522423,1,4542),(320,43,71,0,8719,'description',0,4544,424,4542,1485522423,1,4543),(320,43,71,0,8720,'description',0,4545,425,4543,1485522423,1,4544),(320,43,71,0,8721,'description',0,4546,426,4544,1485522423,1,4545),(320,43,71,0,8722,'description',0,4547,427,4545,1485522423,1,4546),(320,43,71,0,8723,'description',0,4548,428,4546,1485522423,1,4547),(320,43,71,0,8724,'description',0,4549,429,4547,1485522423,1,4548),(320,43,71,0,8725,'description',0,4550,430,4548,1485522423,1,4549),(320,43,71,0,8726,'description',0,4551,431,4549,1485522423,1,4550),(320,43,71,0,8727,'description',0,4552,432,4550,1485522423,1,4551),(320,43,71,0,8728,'description',0,4553,433,4551,1485522423,1,4552),(320,43,71,0,8729,'description',0,4554,434,4552,1485522423,1,4553),(320,43,71,0,8730,'description',0,4555,435,4553,1485522423,1,4554),(320,43,71,0,8731,'description',0,4556,436,4554,1485522423,1,4555),(320,43,71,0,8732,'description',0,4557,437,4555,1485522423,1,4556),(320,43,71,0,8733,'description',0,4558,438,4556,1485522423,1,4557),(320,43,71,0,8734,'description',0,4559,439,4557,1485522423,1,4558),(320,43,71,0,8735,'description',0,4560,440,4558,1485522423,1,4559),(320,43,71,0,8736,'description',0,4561,441,4559,1485522423,1,4560),(320,43,71,0,8737,'description',0,2959,442,4560,1485522423,1,4561),(320,43,71,0,8738,'description',0,4562,443,4561,1485522423,1,2959),(320,43,71,0,8739,'description',0,4563,444,2959,1485522423,1,4562),(320,43,71,0,8740,'description',0,2365,445,4562,1485522423,1,4563),(320,43,71,0,8741,'description',0,4564,446,4563,1485522423,1,2365),(320,43,71,0,8742,'description',0,4565,447,2365,1485522423,1,4564),(320,43,71,0,8743,'description',0,4566,448,4564,1485522423,1,4565),(320,43,71,0,8744,'description',0,4567,449,4565,1485522423,1,4566),(320,43,71,0,8745,'description',0,4568,450,4566,1485522423,1,4567),(320,43,71,0,8746,'description',0,3280,451,4567,1485522423,1,4568),(320,43,71,0,8747,'description',0,3184,452,4568,1485522423,1,3280),(320,43,71,0,8748,'description',0,4569,453,3280,1485522423,1,3184),(320,43,71,0,8749,'description',0,4570,454,3184,1485522423,1,4569),(320,43,71,0,8750,'description',0,4571,455,4569,1485522423,1,4570),(320,43,71,0,8751,'description',0,4572,456,4570,1485522423,1,4571),(320,43,71,0,8752,'description',0,4573,457,4571,1485522423,1,4572),(320,43,71,0,8753,'description',0,4574,458,4572,1485522423,1,4573),(320,43,71,0,8754,'description',0,4575,459,4573,1485522423,1,4574),(320,43,71,0,8755,'description',0,4576,460,4574,1485522423,1,4575),(320,43,71,0,8756,'description',0,4577,461,4575,1485522423,1,4576),(320,43,71,0,8757,'description',0,4578,462,4576,1485522423,1,4577),(320,43,71,0,8758,'description',0,4579,463,4577,1485522423,1,4578),(320,43,71,0,8759,'description',0,3139,464,4578,1485522423,1,4579),(320,43,71,0,8760,'description',0,4580,465,4579,1485522423,1,3139),(320,43,71,0,8761,'description',0,4581,466,3139,1485522423,1,4580),(320,43,71,0,8762,'description',0,4582,467,4580,1485522423,1,4581),(320,43,71,0,8763,'description',0,1986,468,4581,1485522423,1,4582),(320,43,71,0,8764,'description',0,4583,469,4582,1485522423,1,1986),(320,43,71,0,8765,'description',0,4584,470,1986,1485522423,1,4583),(320,43,71,0,8766,'description',0,4585,471,4583,1485522423,1,4584),(320,43,71,0,8767,'description',0,4586,472,4584,1485522423,1,4585),(320,43,71,0,8768,'description',0,4587,473,4585,1485522423,1,4586),(320,43,71,0,8769,'description',0,4588,474,4586,1485522423,1,4587),(320,43,71,0,8770,'description',0,4589,475,4587,1485522423,1,4588),(320,43,71,0,8771,'description',0,4590,476,4588,1485522423,1,4589),(320,43,71,0,8772,'description',0,4591,477,4589,1485522423,1,4590),(320,43,71,0,8773,'description',0,4592,478,4590,1485522423,1,4591),(320,43,71,0,8774,'description',0,4593,479,4591,1485522423,1,4592),(320,43,71,0,8775,'description',0,4594,480,4592,1485522423,1,4593),(320,43,71,0,8776,'description',0,4595,481,4593,1485522423,1,4594),(320,43,71,0,8777,'description',0,4596,482,4594,1485522423,1,4595),(320,43,71,0,8778,'description',0,4597,483,4595,1485522423,1,4596),(320,43,71,0,8779,'description',0,4598,484,4596,1485522423,1,4597),(320,43,71,0,8780,'description',0,2499,485,4597,1485522423,1,4598),(320,43,71,0,8781,'description',0,4599,486,4598,1485522423,1,2499),(320,43,71,0,8782,'description',0,4600,487,2499,1485522423,1,4599),(320,43,71,0,8783,'description',0,4601,488,4599,1485522423,1,4600),(320,43,71,0,8784,'description',0,4602,489,4600,1485522423,1,4601),(320,43,71,0,8785,'description',0,4603,490,4601,1485522423,1,4602),(320,43,71,0,8786,'description',0,4604,491,4602,1485522423,1,4603),(320,43,71,0,8787,'description',0,3605,492,4603,1485522423,1,4604),(320,43,71,0,8788,'description',0,4605,493,4604,1485522423,1,3605),(320,43,71,0,8789,'description',0,4606,494,3605,1485522423,1,4605),(320,43,71,0,8790,'description',0,1040,495,4605,1485522423,1,4606),(320,43,71,0,8791,'description',0,4607,496,4606,1485522423,1,1040),(320,43,71,0,8792,'description',0,4608,497,1040,1485522423,1,4607),(320,43,71,0,8793,'description',0,4609,498,4607,1485522423,1,4608),(320,43,71,0,8794,'description',0,4610,499,4608,1485522423,1,4609),(320,43,71,0,8795,'description',0,4611,500,4609,1485522423,1,4610),(320,43,71,0,8796,'description',0,4612,501,4610,1485522423,1,4611),(320,43,71,0,8797,'description',0,4613,502,4611,1485522423,1,4612),(320,43,71,0,8798,'description',0,4614,503,4612,1485522423,1,4613),(320,43,71,0,8799,'description',0,4615,504,4613,1485522423,1,4614),(320,43,71,0,8800,'description',0,4616,505,4614,1485522423,1,4615),(320,43,71,0,8801,'description',0,4617,506,4615,1485522423,1,4616),(320,43,71,0,8802,'description',0,4618,507,4616,1485522423,1,4617),(320,43,71,0,8803,'description',0,4619,508,4617,1485522423,1,4618),(320,43,71,0,8804,'description',0,4620,509,4618,1485522423,1,4619),(320,43,71,0,8805,'description',0,1840,510,4619,1485522423,1,4620),(320,43,71,0,8806,'description',0,4621,511,4620,1485522423,1,1840),(320,43,71,0,8807,'description',0,4622,512,1840,1485522423,1,4621),(320,43,71,0,8808,'description',0,4623,513,4621,1485522423,1,4622),(320,43,71,0,8809,'description',0,4624,514,4622,1485522423,1,4623),(320,43,71,0,8810,'description',0,4625,515,4623,1485522423,1,4624),(320,43,71,0,8811,'description',0,4626,516,4624,1485522423,1,4625),(320,43,71,0,8812,'description',0,4627,517,4625,1485522423,1,4626),(320,43,71,0,8813,'description',0,4628,518,4626,1485522423,1,4627),(320,43,71,0,8814,'description',0,4629,519,4627,1485522423,1,4628),(320,43,71,0,8815,'description',0,4630,520,4628,1485522423,1,4629),(320,43,71,0,8816,'description',0,4631,521,4629,1485522423,1,4630),(320,43,71,0,8817,'description',0,4632,522,4630,1485522423,1,4631),(320,43,71,0,8818,'description',0,4633,523,4631,1485522423,1,4632),(320,43,71,0,8819,'description',0,4634,524,4632,1485522423,1,4633),(320,43,71,0,8820,'description',0,4635,525,4633,1485522423,1,4634),(320,43,71,0,8821,'description',0,4636,526,4634,1485522423,1,4635),(320,43,71,0,8822,'description',0,4637,527,4635,1485522423,1,4636),(320,43,71,0,8823,'description',0,4638,528,4636,1485522423,1,4637),(320,43,71,0,8824,'description',0,4639,529,4637,1485522423,1,4638),(320,43,71,0,8825,'description',0,4640,530,4638,1485522423,1,4639),(320,43,71,0,8826,'description',0,4641,531,4639,1485522423,1,4640),(320,43,71,0,8827,'description',0,4642,532,4640,1485522423,1,4641),(320,43,71,0,8828,'description',0,4643,533,4641,1485522423,1,4642),(320,43,71,0,8829,'description',0,4644,534,4642,1485522423,1,4643),(320,43,71,0,8830,'description',0,4645,535,4643,1485522423,1,4644),(320,43,71,0,8831,'description',0,4646,536,4644,1485522423,1,4645),(320,43,71,0,8832,'description',0,4647,537,4645,1485522423,1,4646),(320,43,71,0,8833,'description',0,4648,538,4646,1485522423,1,4647),(320,43,71,0,8834,'description',0,4649,539,4647,1485522423,1,4648),(320,43,71,0,8835,'description',0,4650,540,4648,1485522423,1,4649),(320,43,71,0,8836,'description',0,2924,541,4649,1485522423,1,4650),(320,43,71,0,8837,'description',0,4651,542,4650,1485522423,1,2924),(320,43,71,0,8838,'description',0,4652,543,2924,1485522423,1,4651),(320,43,71,0,8839,'description',0,4653,544,4651,1485522423,1,4652),(320,43,71,0,8840,'description',0,4654,545,4652,1485522423,1,4653),(320,43,71,0,8841,'description',0,4655,546,4653,1485522423,1,4654),(320,43,71,0,8842,'description',0,4656,547,4654,1485522423,1,4655),(320,43,71,0,8843,'description',0,4657,548,4655,1485522423,1,4656),(320,43,71,0,8844,'description',0,4658,549,4656,1485522423,1,4657),(320,43,71,0,8845,'description',0,4659,550,4657,1485522423,1,4658),(320,43,71,0,8846,'description',0,4660,551,4658,1485522423,1,4659),(320,43,71,0,8847,'description',0,4661,552,4659,1485522423,1,4660),(320,43,71,0,8848,'description',0,4662,553,4660,1485522423,1,4661),(320,43,71,0,8849,'description',0,4663,554,4661,1485522423,1,4662),(320,43,71,0,8850,'description',0,4664,555,4662,1485522423,1,4663),(320,43,71,0,8851,'description',0,4665,556,4663,1485522423,1,4664),(320,43,71,0,8852,'description',0,4666,557,4664,1485522423,1,4665),(320,43,71,0,8853,'description',0,4667,558,4665,1485522423,1,4666),(320,43,71,0,8854,'description',0,4668,559,4666,1485522423,1,4667),(320,43,71,0,8855,'description',0,4669,560,4667,1485522423,1,4668),(320,43,71,0,8856,'description',0,4670,561,4668,1485522423,1,4669),(320,43,71,0,8857,'description',0,4671,562,4669,1485522423,1,4670),(320,43,71,0,8858,'description',0,3561,563,4670,1485522423,1,4671),(320,43,71,0,8859,'description',0,4672,564,4671,1485522423,1,3561),(320,43,71,0,8860,'description',0,4673,565,3561,1485522423,1,4672),(320,43,71,0,8861,'description',0,3513,566,4672,1485522423,1,4673),(320,43,71,0,8862,'description',0,4674,567,4673,1485522423,1,3513),(320,43,71,0,8863,'description',0,4675,568,3513,1485522423,1,4674),(320,43,71,0,8864,'description',0,4676,569,4674,1485522423,1,4675),(320,43,71,0,8865,'description',0,4677,570,4675,1485522423,1,4676),(320,43,71,0,8866,'description',0,4678,571,4676,1485522423,1,4677),(320,43,71,0,8867,'description',0,4679,572,4677,1485522423,1,4678),(320,43,71,0,8868,'description',0,2511,573,4678,1485522423,1,4679),(320,43,71,0,8869,'description',0,4680,574,4679,1485522423,1,2511),(320,43,71,0,8870,'description',0,4681,575,2511,1485522423,1,4680),(320,43,71,0,8871,'description',0,4682,576,4680,1485522423,1,4681),(320,43,71,0,8872,'description',0,4683,577,4681,1485522423,1,4682),(320,43,71,0,8873,'description',0,4684,578,4682,1485522423,1,4683),(320,43,71,0,8874,'description',0,4685,579,4683,1485522423,1,4684),(320,43,71,0,8875,'description',0,4686,580,4684,1485522423,1,4685),(320,43,71,0,8876,'description',0,4687,581,4685,1485522423,1,4686),(320,43,71,0,8877,'description',0,4688,582,4686,1485522423,1,4687),(320,43,71,0,8878,'description',0,4689,583,4687,1485522423,1,4688),(320,43,71,0,8879,'description',0,4690,584,4688,1485522423,1,4689),(320,43,71,0,8880,'description',0,4691,585,4689,1485522423,1,4690),(320,43,71,0,8881,'description',0,4692,586,4690,1485522423,1,4691),(320,43,71,0,8882,'description',0,4693,587,4691,1485522423,1,4692),(320,43,71,0,8883,'description',0,4694,588,4692,1485522423,1,4693),(320,43,71,0,8884,'description',0,4695,589,4693,1485522423,1,4694),(320,43,71,0,8885,'description',0,4696,590,4694,1485522423,1,4695),(320,43,71,0,8886,'description',0,4697,591,4695,1485522423,1,4696),(320,43,71,0,8887,'description',0,4698,592,4696,1485522423,1,4697),(320,43,71,0,8888,'description',0,4699,593,4697,1485522423,1,4698),(320,43,71,0,8889,'description',0,4700,594,4698,1485522423,1,4699),(320,43,71,0,8890,'description',0,4701,595,4699,1485522423,1,4700),(320,43,71,0,8891,'description',0,4702,596,4700,1485522423,1,4701),(320,43,71,0,8892,'description',0,4703,597,4701,1485522423,1,4702),(320,43,71,0,8893,'description',0,4704,598,4702,1485522423,1,4703),(320,43,71,0,8894,'description',0,4705,599,4703,1485522423,1,4704),(320,43,71,0,8895,'description',0,4706,600,4704,1485522423,1,4705),(320,43,71,0,8896,'description',0,4707,601,4705,1485522423,1,4706),(320,43,71,0,8897,'description',0,4708,602,4706,1485522423,1,4707),(320,43,71,0,8898,'description',0,4709,603,4707,1485522423,1,4708),(320,43,71,0,8899,'description',0,4710,604,4708,1485522423,1,4709),(320,43,71,0,8900,'description',0,4711,605,4709,1485522423,1,4710),(320,43,71,0,8901,'description',0,4712,606,4710,1485522423,1,4711),(320,43,71,0,8902,'description',0,4713,607,4711,1485522423,1,4712),(320,43,71,0,8903,'description',0,4714,608,4712,1485522423,1,4713),(320,43,71,0,8904,'description',0,4715,609,4713,1485522423,1,4714),(320,43,71,0,8905,'description',0,4716,610,4714,1485522423,1,4715),(320,43,71,0,8906,'description',0,4717,611,4715,1485522423,1,4716),(320,43,71,0,8907,'description',0,4718,612,4716,1485522423,1,4717),(320,43,71,0,8908,'description',0,4719,613,4717,1485522423,1,4718),(320,43,71,0,8909,'description',0,1040,614,4718,1485522423,1,4719),(320,43,71,0,8910,'description',0,4720,615,4719,1485522423,1,1040),(320,43,71,0,8911,'description',0,4721,616,1040,1485522423,1,4720),(320,43,71,0,8912,'description',0,3957,617,4720,1485522423,1,4721),(320,43,71,0,8913,'description',0,4722,618,4721,1485522423,1,3957),(320,43,71,0,8914,'description',0,4723,619,3957,1485522423,1,4722),(320,43,71,0,8915,'description',0,4724,620,4722,1485522423,1,4723),(320,43,71,0,8916,'description',0,1052,621,4723,1485522423,1,4724),(320,43,71,0,8917,'description',0,4725,622,4724,1485522423,1,1052),(320,43,71,0,8918,'description',0,2525,623,1052,1485522423,1,4725),(320,43,71,0,8919,'description',0,4619,624,4725,1485522423,1,2525),(320,43,71,0,8920,'description',0,4726,625,2525,1485522423,1,4619),(320,43,71,0,8921,'description',0,4727,626,4619,1485522423,1,4726),(320,43,71,0,8922,'description',0,4728,627,4726,1485522423,1,4727),(320,43,71,0,8923,'description',0,4729,628,4727,1485522423,1,4728),(320,43,71,0,8924,'description',0,4730,629,4728,1485522423,1,4729),(320,43,71,0,8925,'description',0,4731,630,4729,1485522423,1,4730),(320,43,71,0,8926,'description',0,4732,631,4730,1485522423,1,4731),(320,43,71,0,8927,'description',0,4733,632,4731,1485522423,1,4732),(320,43,71,0,8928,'description',0,4734,633,4732,1485522423,1,4733),(320,43,71,0,8929,'description',0,4735,634,4733,1485522423,1,4734),(320,43,71,0,8930,'description',0,1052,635,4734,1485522423,1,4735),(320,43,71,0,8931,'description',0,4736,636,4735,1485522423,1,1052),(320,43,71,0,8932,'description',0,4737,637,1052,1485522423,1,4736),(320,43,71,0,8933,'description',0,4738,638,4736,1485522423,1,4737),(320,43,71,0,8934,'description',0,4739,639,4737,1485522423,1,4738),(320,43,71,0,8935,'description',0,4740,640,4738,1485522423,1,4739),(320,43,71,0,8936,'description',0,4741,641,4739,1485522423,1,4740),(320,43,71,0,8937,'description',0,4742,642,4740,1485522423,1,4741),(320,43,71,0,8938,'description',0,4743,643,4741,1485522423,1,4742),(320,43,71,0,8939,'description',0,4744,644,4742,1485522423,1,4743),(320,43,71,0,8940,'description',0,4745,645,4743,1485522423,1,4744),(320,43,71,0,8941,'description',0,4746,646,4744,1485522423,1,4745),(320,43,71,0,8942,'description',0,2879,647,4745,1485522423,1,4746),(320,43,71,0,8943,'description',0,0,648,4746,1485522423,1,2879),(245,27,74,0,8944,'name',0,1171,0,0,1485715347,3,1025),(245,27,74,0,8945,'name',0,1040,1,1025,1485715347,3,1171),(245,27,74,0,8946,'name',0,0,2,1171,1485715347,3,1040),(4,1,86,0,9987,'name',0,5685,0,0,1486123295,3,1025),(4,1,86,0,9988,'name',0,0,1,1025,1486123295,3,5685),(245,27,87,0,9989,'name',0,1171,0,0,1486123347,3,1025),(245,27,87,0,9990,'name',0,1052,1,1025,1486123347,3,1171),(245,27,87,0,9991,'name',0,0,2,1171,1486123347,3,1052),(245,27,89,0,10533,'name',0,1171,0,0,1486123377,3,1025),(245,27,89,0,10534,'name',0,1172,1,1025,1486123377,3,1171),(245,27,89,0,10535,'name',0,0,2,1171,1486123377,3,1172),(245,27,88,0,10536,'name',0,1171,0,0,1486123361,3,1025),(245,27,88,0,10537,'name',0,1040,1,1025,1486123361,3,1171),(245,27,88,0,10538,'name',0,2201,2,1171,1486123361,3,1040),(245,27,88,0,10539,'name',0,6155,3,1040,1486123361,3,2201),(245,27,88,0,10540,'name',0,6156,4,2201,1486123361,3,6155),(245,27,88,0,10541,'name',0,6157,5,6155,1486123361,3,6156),(245,27,88,0,10542,'name',0,1040,6,6156,1486123361,3,6157),(245,27,88,0,10543,'name',0,0,7,6157,1486123361,3,1040),(230,23,79,0,10544,'name',0,3186,0,0,1486121030,1,6158),(230,23,79,0,10545,'name',0,6159,1,6158,1486121030,1,3186),(230,23,79,0,10546,'name',0,6160,2,3186,1486121030,1,6159),(230,23,79,0,10547,'name',0,0,3,6159,1486121030,1,6160),(310,40,90,0,10548,'title',0,1026,0,0,1486366668,1,1025),(310,40,90,0,10549,'title',0,1172,1,1025,1486366668,1,1026),(310,40,90,0,10550,'title',0,1025,2,1026,1486366668,1,1172),(311,40,90,0,10551,'description',0,1026,3,1172,1486366668,1,1025),(311,40,90,0,10552,'description',0,1172,4,1025,1486366668,1,1026),(311,40,90,0,10553,'description',0,1025,5,1026,1486366668,1,1172),(311,40,90,0,10554,'description',0,1026,6,1172,1486366668,1,1025),(311,40,90,0,10555,'description',0,1172,7,1025,1486366668,1,1026),(311,40,90,0,10556,'description',0,0,8,1026,1486366668,1,1172),(312,41,67,0,10557,'title',0,6161,0,0,1485153594,1,1025),(312,41,67,0,10558,'title',0,1053,1,1025,1485153594,1,6161),(312,41,67,0,10559,'title',0,2202,2,6161,1485153594,1,1053),(312,41,67,0,10560,'title',0,1696,3,1053,1485153594,1,2202),(312,41,67,0,10561,'title',0,1025,4,2202,1485153594,1,1696),(313,41,67,0,10562,'description',0,6161,5,1696,1485153594,1,1025),(313,41,67,0,10563,'description',0,6162,6,1025,1485153594,1,6161),(313,41,67,0,10564,'description',0,6163,7,6161,1485153594,1,6162),(313,41,67,0,10565,'description',0,2662,8,6162,1485153594,1,6163),(313,41,67,0,10566,'description',0,6164,9,6163,1485153594,1,2662),(313,41,67,0,10567,'description',0,6165,10,2662,1485153594,1,6164),(313,41,67,0,10568,'description',0,6166,11,6164,1485153594,1,6165),(313,41,67,0,10569,'description',0,6167,12,6165,1485153594,1,6166),(313,41,67,0,10570,'description',0,6168,13,6166,1485153594,1,6167),(313,41,67,0,10571,'description',0,6169,14,6167,1485153594,1,6168),(313,41,67,0,10572,'description',0,6170,15,6168,1485153594,1,6169),(313,41,67,0,10573,'description',0,6171,16,6169,1485153594,1,6170),(313,41,67,0,10574,'description',0,6172,17,6170,1485153594,1,6171),(313,41,67,0,10575,'description',0,6173,18,6171,1485153594,1,6172),(313,41,67,0,10576,'description',0,6174,19,6172,1485153594,1,6173),(313,41,67,0,10577,'description',0,6175,20,6173,1485153594,1,6174),(313,41,67,0,10578,'description',0,6176,21,6174,1485153594,1,6175),(313,41,67,0,10579,'description',0,6177,22,6175,1485153594,1,6176),(313,41,67,0,10580,'description',0,6178,23,6176,1485153594,1,6177),(313,41,67,0,10581,'description',0,6179,24,6177,1485153594,1,6178),(313,41,67,0,10582,'description',0,6180,25,6178,1485153594,1,6179),(313,41,67,0,10583,'description',0,6181,26,6179,1485153594,1,6180),(313,41,67,0,10584,'description',0,6182,27,6180,1485153594,1,6181),(313,41,67,0,10585,'description',0,6183,28,6181,1485153594,1,6182),(313,41,67,0,10586,'description',0,6184,29,6182,1485153594,1,6183),(313,41,67,0,10587,'description',0,6185,30,6183,1485153594,1,6184),(313,41,67,0,10588,'description',0,6186,31,6184,1485153594,1,6185),(313,41,67,0,10589,'description',0,6187,32,6185,1485153594,1,6186),(313,41,67,0,10590,'description',0,6188,33,6186,1485153594,1,6187),(313,41,67,0,10591,'description',0,6189,34,6187,1485153594,1,6188),(313,41,67,0,10592,'description',0,6190,35,6188,1485153594,1,6189),(313,41,67,0,10593,'description',0,6191,36,6189,1485153594,1,6190),(313,41,67,0,10594,'description',0,6192,37,6190,1485153594,1,6191),(313,41,67,0,10595,'description',0,6193,38,6191,1485153594,1,6192),(313,41,67,0,10596,'description',0,6194,39,6192,1485153594,1,6193),(313,41,67,0,10597,'description',0,6195,40,6193,1485153594,1,6194),(313,41,67,0,10598,'description',0,6196,41,6194,1485153594,1,6195),(313,41,67,0,10599,'description',0,6197,42,6195,1485153594,1,6196),(313,41,67,0,10600,'description',0,6198,43,6196,1485153594,1,6197),(313,41,67,0,10601,'description',0,6199,44,6197,1485153594,1,6198),(313,41,67,0,10602,'description',0,2732,45,6198,1485153594,1,6199),(313,41,67,0,10603,'description',0,6200,46,6199,1485153594,1,2732),(313,41,67,0,10604,'description',0,6201,47,2732,1485153594,1,6200),(313,41,67,0,10605,'description',0,6202,48,6200,1485153594,1,6201),(313,41,67,0,10606,'description',0,6203,49,6201,1485153594,1,6202),(313,41,67,0,10607,'description',0,6204,50,6202,1485153594,1,6203),(313,41,67,0,10608,'description',0,6205,51,6203,1485153594,1,6204),(313,41,67,0,10609,'description',0,6206,52,6204,1485153594,1,6205),(313,41,67,0,10610,'description',0,6207,53,6205,1485153594,1,6206),(313,41,67,0,10611,'description',0,6208,54,6206,1485153594,1,6207),(313,41,67,0,10612,'description',0,6209,55,6207,1485153594,1,6208),(313,41,67,0,10613,'description',0,2743,56,6208,1485153594,1,6209),(313,41,67,0,10614,'description',0,6210,57,6209,1485153594,1,2743),(313,41,67,0,10615,'description',0,6211,58,2743,1485153594,1,6210),(313,41,67,0,10616,'description',0,6212,59,6210,1485153594,1,6211),(313,41,67,0,10617,'description',0,6213,60,6211,1485153594,1,6212),(313,41,67,0,10618,'description',0,6214,61,6212,1485153594,1,6213),(313,41,67,0,10619,'description',0,6215,62,6213,1485153594,1,6214),(313,41,67,0,10620,'description',0,6216,63,6214,1485153594,1,6215),(313,41,67,0,10621,'description',0,6217,64,6215,1485153594,1,6216),(313,41,67,0,10622,'description',0,6218,65,6216,1485153594,1,6217),(313,41,67,0,10623,'description',0,6219,66,6217,1485153594,1,6218),(313,41,67,0,10624,'description',0,6220,67,6218,1485153594,1,6219),(313,41,67,0,10625,'description',0,6221,68,6219,1485153594,1,6220),(313,41,67,0,10626,'description',0,6222,69,6220,1485153594,1,6221),(313,41,67,0,10627,'description',0,6223,70,6221,1485153594,1,6222),(313,41,67,0,10628,'description',0,6224,71,6222,1485153594,1,6223),(313,41,67,0,10629,'description',0,6225,72,6223,1485153594,1,6224),(313,41,67,0,10630,'description',0,6226,73,6224,1485153594,1,6225),(313,41,67,0,10631,'description',0,6227,74,6225,1485153594,1,6226),(313,41,67,0,10632,'description',0,6228,75,6226,1485153594,1,6227),(313,41,67,0,10633,'description',0,6229,76,6227,1485153594,1,6228),(313,41,67,0,10634,'description',0,6230,77,6228,1485153594,1,6229),(313,41,67,0,10635,'description',0,2321,78,6229,1485153594,1,6230),(313,41,67,0,10636,'description',0,6231,79,6230,1485153594,1,2321),(313,41,67,0,10637,'description',0,6232,80,2321,1485153594,1,6231),(313,41,67,0,10638,'description',0,6233,81,6231,1485153594,1,6232),(313,41,67,0,10639,'description',0,6234,82,6232,1485153594,1,6233),(313,41,67,0,10640,'description',0,6235,83,6233,1485153594,1,6234),(313,41,67,0,10641,'description',0,6236,84,6234,1485153594,1,6235),(313,41,67,0,10642,'description',0,6237,85,6235,1485153594,1,6236),(313,41,67,0,10643,'description',0,6238,86,6236,1485153594,1,6237),(313,41,67,0,10644,'description',0,6239,87,6237,1485153594,1,6238),(313,41,67,0,10645,'description',0,6240,88,6238,1485153594,1,6239),(313,41,67,0,10646,'description',0,6241,89,6239,1485153594,1,6240),(313,41,67,0,10647,'description',0,6242,90,6240,1485153594,1,6241),(313,41,67,0,10648,'description',0,6243,91,6241,1485153594,1,6242),(313,41,67,0,10649,'description',0,6244,92,6242,1485153594,1,6243),(313,41,67,0,10650,'description',0,6245,93,6243,1485153594,1,6244),(313,41,67,0,10651,'description',0,6246,94,6244,1485153594,1,6245),(313,41,67,0,10652,'description',0,6247,95,6245,1485153594,1,6246),(313,41,67,0,10653,'description',0,2782,96,6246,1485153594,1,6247),(313,41,67,0,10654,'description',0,6248,97,6247,1485153594,1,2782),(313,41,67,0,10655,'description',0,6249,98,2782,1485153594,1,6248),(313,41,67,0,10656,'description',0,6250,99,6248,1485153594,1,6249),(313,41,67,0,10657,'description',0,6251,100,6249,1485153594,1,6250),(313,41,67,0,10658,'description',0,6252,101,6250,1485153594,1,6251),(313,41,67,0,10659,'description',0,6253,102,6251,1485153594,1,6252),(313,41,67,0,10660,'description',0,6254,103,6252,1485153594,1,6253),(313,41,67,0,10661,'description',0,6255,104,6253,1485153594,1,6254),(313,41,67,0,10662,'description',0,6256,105,6254,1485153594,1,6255),(313,41,67,0,10663,'description',0,6257,106,6255,1485153594,1,6256),(313,41,67,0,10664,'description',0,6258,107,6256,1485153594,1,6257),(313,41,67,0,10665,'description',0,6259,108,6257,1485153594,1,6258),(313,41,67,0,10666,'description',0,6260,109,6258,1485153594,1,6259),(313,41,67,0,10667,'description',0,6261,110,6259,1485153594,1,6260),(313,41,67,0,10668,'description',0,6262,111,6260,1485153594,1,6261),(313,41,67,0,10669,'description',0,6263,112,6261,1485153594,1,6262),(313,41,67,0,10670,'description',0,6264,113,6262,1485153594,1,6263),(313,41,67,0,10671,'description',0,6265,114,6263,1485153594,1,6264),(313,41,67,0,10672,'description',0,6266,115,6264,1485153594,1,6265),(313,41,67,0,10673,'description',0,6267,116,6265,1485153594,1,6266),(313,41,67,0,10674,'description',0,6268,117,6266,1485153594,1,6267),(313,41,67,0,10675,'description',0,6269,118,6267,1485153594,1,6268),(313,41,67,0,10676,'description',0,6270,119,6268,1485153594,1,6269),(313,41,67,0,10677,'description',0,6271,120,6269,1485153594,1,6270),(313,41,67,0,10678,'description',0,6272,121,6270,1485153594,1,6271),(313,41,67,0,10679,'description',0,6273,122,6271,1485153594,1,6272),(313,41,67,0,10680,'description',0,6274,123,6272,1485153594,1,6273),(313,41,67,0,10681,'description',0,6275,124,6273,1485153594,1,6274),(313,41,67,0,10682,'description',0,6276,125,6274,1485153594,1,6275),(313,41,67,0,10683,'description',0,6277,126,6275,1485153594,1,6276),(313,41,67,0,10684,'description',0,6278,127,6276,1485153594,1,6277),(313,41,67,0,10685,'description',0,6279,128,6277,1485153594,1,6278),(313,41,67,0,10686,'description',0,6280,129,6278,1485153594,1,6279),(313,41,67,0,10687,'description',0,6281,130,6279,1485153594,1,6280),(313,41,67,0,10688,'description',0,6282,131,6280,1485153594,1,6281),(313,41,67,0,10689,'description',0,6283,132,6281,1485153594,1,6282),(313,41,67,0,10690,'description',0,6284,133,6282,1485153594,1,6283),(313,41,67,0,10691,'description',0,6285,134,6283,1485153594,1,6284),(313,41,67,0,10692,'description',0,6286,135,6284,1485153594,1,6285),(313,41,67,0,10693,'description',0,6287,136,6285,1485153594,1,6286),(313,41,67,0,10694,'description',0,6288,137,6286,1485153594,1,6287),(313,41,67,0,10695,'description',0,6289,138,6287,1485153594,1,6288),(313,41,67,0,10696,'description',0,6290,139,6288,1485153594,1,6289),(313,41,67,0,10697,'description',0,6291,140,6289,1485153594,1,6290),(313,41,67,0,10698,'description',0,6292,141,6290,1485153594,1,6291),(313,41,67,0,10699,'description',0,6293,142,6291,1485153594,1,6292),(313,41,67,0,10700,'description',0,6294,143,6292,1485153594,1,6293),(313,41,67,0,10701,'description',0,6295,144,6293,1485153594,1,6294),(313,41,67,0,10702,'description',0,6296,145,6294,1485153594,1,6295),(313,41,67,0,10703,'description',0,6297,146,6295,1485153594,1,6296),(313,41,67,0,10704,'description',0,6298,147,6296,1485153594,1,6297),(313,41,67,0,10705,'description',0,6299,148,6297,1485153594,1,6298),(313,41,67,0,10706,'description',0,6300,149,6298,1485153594,1,6299),(313,41,67,0,10707,'description',0,6301,150,6299,1485153594,1,6300),(313,41,67,0,10708,'description',0,6302,151,6300,1485153594,1,6301),(313,41,67,0,10709,'description',0,6303,152,6301,1485153594,1,6302),(313,41,67,0,10710,'description',0,6304,153,6302,1485153594,1,6303),(313,41,67,0,10711,'description',0,6305,154,6303,1485153594,1,6304),(313,41,67,0,10712,'description',0,6306,155,6304,1485153594,1,6305),(313,41,67,0,10713,'description',0,6307,156,6305,1485153594,1,6306),(313,41,67,0,10714,'description',0,6308,157,6306,1485153594,1,6307),(313,41,67,0,10715,'description',0,6309,158,6307,1485153594,1,6308),(313,41,67,0,10716,'description',0,6310,159,6308,1485153594,1,6309),(313,41,67,0,10717,'description',0,6311,160,6309,1485153594,1,6310),(313,41,67,0,10718,'description',0,6312,161,6310,1485153594,1,6311),(313,41,67,0,10719,'description',0,6313,162,6311,1485153594,1,6312),(313,41,67,0,10720,'description',0,6314,163,6312,1485153594,1,6313),(313,41,67,0,10721,'description',0,6315,164,6313,1485153594,1,6314),(313,41,67,0,10722,'description',0,6316,165,6314,1485153594,1,6315),(313,41,67,0,10723,'description',0,6317,166,6315,1485153594,1,6316),(313,41,67,0,10724,'description',0,6318,167,6316,1485153594,1,6317),(313,41,67,0,10725,'description',0,6319,168,6317,1485153594,1,6318),(313,41,67,0,10726,'description',0,6320,169,6318,1485153594,1,6319),(313,41,67,0,10727,'description',0,6321,170,6319,1485153594,1,6320),(313,41,67,0,10728,'description',0,6322,171,6320,1485153594,1,6321),(313,41,67,0,10729,'description',0,6323,172,6321,1485153594,1,6322),(313,41,67,0,10730,'description',0,6324,173,6322,1485153594,1,6323),(313,41,67,0,10731,'description',0,6325,174,6323,1485153594,1,6324),(313,41,67,0,10732,'description',0,6326,175,6324,1485153594,1,6325),(313,41,67,0,10733,'description',0,6327,176,6325,1485153594,1,6326),(313,41,67,0,10734,'description',0,6328,177,6326,1485153594,1,6327),(313,41,67,0,10735,'description',0,6329,178,6327,1485153594,1,6328),(313,41,67,0,10736,'description',0,1696,179,6328,1485153594,1,6329),(313,41,67,0,10737,'description',0,6330,180,6329,1485153594,1,1696),(313,41,67,0,10738,'description',0,6331,181,1696,1485153594,1,6330),(313,41,67,0,10739,'description',0,6332,182,6330,1485153594,1,6331),(313,41,67,0,10740,'description',0,6333,183,6331,1485153594,1,6332),(313,41,67,0,10741,'description',0,6334,184,6332,1485153594,1,6333),(313,41,67,0,10742,'description',0,6335,185,6333,1485153594,1,6334),(313,41,67,0,10743,'description',0,6336,186,6334,1485153594,1,6335),(313,41,67,0,10744,'description',0,6337,187,6335,1485153594,1,6336),(313,41,67,0,10745,'description',0,6338,188,6336,1485153594,1,6337),(313,41,67,0,10746,'description',0,6339,189,6337,1485153594,1,6338),(313,41,67,0,10747,'description',0,2191,190,6338,1485153594,1,6339),(313,41,67,0,10748,'description',0,6340,191,6339,1485153594,1,2191),(313,41,67,0,10749,'description',0,6341,192,2191,1485153594,1,6340),(313,41,67,0,10750,'description',0,6342,193,6340,1485153594,1,6341),(313,41,67,0,10751,'description',0,6343,194,6341,1485153594,1,6342),(313,41,67,0,10752,'description',0,2879,195,6342,1485153594,1,6343),(313,41,67,0,10753,'description',0,1840,196,6343,1485153594,1,2879),(313,41,67,0,10754,'description',0,6344,197,2879,1485153594,1,1840),(313,41,67,0,10755,'description',0,6345,198,1840,1485153594,1,6344),(313,41,67,0,10756,'description',0,6346,199,6344,1485153594,1,6345),(313,41,67,0,10757,'description',0,6347,200,6345,1485153594,1,6346),(313,41,67,0,10758,'description',0,6348,201,6346,1485153594,1,6347),(313,41,67,0,10759,'description',0,2191,202,6347,1485153594,1,6348),(313,41,67,0,10760,'description',0,6349,203,6348,1485153594,1,2191),(313,41,67,0,10761,'description',0,6350,204,2191,1485153594,1,6349),(313,41,67,0,10762,'description',0,6351,205,6349,1485153594,1,6350),(313,41,67,0,10763,'description',0,6352,206,6350,1485153594,1,6351),(313,41,67,0,10764,'description',0,6353,207,6351,1485153594,1,6352),(313,41,67,0,10765,'description',0,6354,208,6352,1485153594,1,6353),(313,41,67,0,10766,'description',0,6355,209,6353,1485153594,1,6354),(313,41,67,0,10767,'description',0,6356,210,6354,1485153594,1,6355),(313,41,67,0,10768,'description',0,6357,211,6355,1485153594,1,6356),(313,41,67,0,10769,'description',0,6358,212,6356,1485153594,1,6357),(313,41,67,0,10770,'description',0,6359,213,6357,1485153594,1,6358),(313,41,67,0,10771,'description',0,6360,214,6358,1485153594,1,6359),(313,41,67,0,10772,'description',0,6361,215,6359,1485153594,1,6360),(313,41,67,0,10773,'description',0,6362,216,6360,1485153594,1,6361),(313,41,67,0,10774,'description',0,6363,217,6361,1485153594,1,6362),(313,41,67,0,10775,'description',0,6364,218,6362,1485153594,1,6363),(313,41,67,0,10776,'description',0,6365,219,6363,1485153594,1,6364),(313,41,67,0,10777,'description',0,6366,220,6364,1485153594,1,6365),(313,41,67,0,10778,'description',0,6367,221,6365,1485153594,1,6366),(313,41,67,0,10779,'description',0,6368,222,6366,1485153594,1,6367),(313,41,67,0,10780,'description',0,6369,223,6367,1485153594,1,6368),(313,41,67,0,10781,'description',0,6370,224,6368,1485153594,1,6369),(313,41,67,0,10782,'description',0,6371,225,6369,1485153594,1,6370),(313,41,67,0,10783,'description',0,6372,226,6370,1485153594,1,6371),(313,41,67,0,10784,'description',0,6373,227,6371,1485153594,1,6372),(313,41,67,0,10785,'description',0,6374,228,6372,1485153594,1,6373),(313,41,67,0,10786,'description',0,6375,229,6373,1485153594,1,6374),(313,41,67,0,10787,'description',0,6376,230,6374,1485153594,1,6375),(313,41,67,0,10788,'description',0,6377,231,6375,1485153594,1,6376),(313,41,67,0,10789,'description',0,6378,232,6376,1485153594,1,6377),(313,41,67,0,10790,'description',0,6379,233,6377,1485153594,1,6378),(313,41,67,0,10791,'description',0,6380,234,6378,1485153594,1,6379),(313,41,67,0,10792,'description',0,6381,235,6379,1485153594,1,6380),(313,41,67,0,10793,'description',0,6382,236,6380,1485153594,1,6381),(313,41,67,0,10794,'description',0,6383,237,6381,1485153594,1,6382),(313,41,67,0,10795,'description',0,6384,238,6382,1485153594,1,6383),(313,41,67,0,10796,'description',0,6385,239,6383,1485153594,1,6384),(313,41,67,0,10797,'description',0,6386,240,6384,1485153594,1,6385),(313,41,67,0,10798,'description',0,6387,241,6385,1485153594,1,6386),(313,41,67,0,10799,'description',0,2924,242,6386,1485153594,1,6387),(313,41,67,0,10800,'description',0,2523,243,6387,1485153594,1,2924),(313,41,67,0,10801,'description',0,6388,244,2924,1485153594,1,2523),(313,41,67,0,10802,'description',0,6389,245,2523,1485153594,1,6388),(313,41,67,0,10803,'description',0,6390,246,6388,1485153594,1,6389),(313,41,67,0,10804,'description',0,6391,247,6389,1485153594,1,6390),(313,41,67,0,10805,'description',0,6392,248,6390,1485153594,1,6391),(313,41,67,0,10806,'description',0,6393,249,6391,1485153594,1,6392),(313,41,67,0,10807,'description',0,6394,250,6392,1485153594,1,6393),(313,41,67,0,10808,'description',0,6395,251,6393,1485153594,1,6394),(313,41,67,0,10809,'description',0,6396,252,6394,1485153594,1,6395),(313,41,67,0,10810,'description',0,6397,253,6395,1485153594,1,6396),(313,41,67,0,10811,'description',0,2647,254,6396,1485153594,1,6397),(313,41,67,0,10812,'description',0,6398,255,6397,1485153594,1,2647),(313,41,67,0,10813,'description',0,6399,256,2647,1485153594,1,6398),(313,41,67,0,10814,'description',0,6400,257,6398,1485153594,1,6399),(313,41,67,0,10815,'description',0,6401,258,6399,1485153594,1,6400),(313,41,67,0,10816,'description',0,6402,259,6400,1485153594,1,6401),(313,41,67,0,10817,'description',0,6403,260,6401,1485153594,1,6402),(313,41,67,0,10818,'description',0,6404,261,6402,1485153594,1,6403),(313,41,67,0,10819,'description',0,6405,262,6403,1485153594,1,6404),(313,41,67,0,10820,'description',0,2478,263,6404,1485153594,1,6405),(313,41,67,0,10821,'description',0,6406,264,6405,1485153594,1,2478),(313,41,67,0,10822,'description',0,6407,265,2478,1485153594,1,6406),(313,41,67,0,10823,'description',0,6408,266,6406,1485153594,1,6407),(313,41,67,0,10824,'description',0,6409,267,6407,1485153594,1,6408),(313,41,67,0,10825,'description',0,6410,268,6408,1485153594,1,6409),(313,41,67,0,10826,'description',0,6411,269,6409,1485153594,1,6410),(313,41,67,0,10827,'description',0,6412,270,6410,1485153594,1,6411),(313,41,67,0,10828,'description',0,6413,271,6411,1485153594,1,6412),(313,41,67,0,10829,'description',0,6414,272,6412,1485153594,1,6413),(313,41,67,0,10830,'description',0,6199,273,6413,1485153594,1,6414),(313,41,67,0,10831,'description',0,6415,274,6414,1485153594,1,6199),(313,41,67,0,10832,'description',0,6416,275,6199,1485153594,1,6415),(313,41,67,0,10833,'description',0,6417,276,6415,1485153594,1,6416),(313,41,67,0,10834,'description',0,6418,277,6416,1485153594,1,6417),(313,41,67,0,10835,'description',0,6419,278,6417,1485153594,1,6418),(313,41,67,0,10836,'description',0,6420,279,6418,1485153594,1,6419),(313,41,67,0,10837,'description',0,6421,280,6419,1485153594,1,6420),(313,41,67,0,10838,'description',0,2959,281,6420,1485153594,1,6421),(313,41,67,0,10839,'description',0,6422,282,6421,1485153594,1,2959),(313,41,67,0,10840,'description',0,6423,283,2959,1485153594,1,6422),(313,41,67,0,10841,'description',0,6424,284,6422,1485153594,1,6423),(313,41,67,0,10842,'description',0,6425,285,6423,1485153594,1,6424),(313,41,67,0,10843,'description',0,6426,286,6424,1485153594,1,6425),(313,41,67,0,10844,'description',0,6427,287,6425,1485153594,1,6426),(313,41,67,0,10845,'description',0,6428,288,6426,1485153594,1,6427),(313,41,67,0,10846,'description',0,6429,289,6427,1485153594,1,6428),(313,41,67,0,10847,'description',0,6430,290,6428,1485153594,1,6429),(313,41,67,0,10848,'description',0,6431,291,6429,1485153594,1,6430),(313,41,67,0,10849,'description',0,6432,292,6430,1485153594,1,6431),(313,41,67,0,10850,'description',0,6433,293,6431,1485153594,1,6432),(313,41,67,0,10851,'description',0,6434,294,6432,1485153594,1,6433),(313,41,67,0,10852,'description',0,6435,295,6433,1485153594,1,6434),(313,41,67,0,10853,'description',0,6436,296,6434,1485153594,1,6435),(313,41,67,0,10854,'description',0,6437,297,6435,1485153594,1,6436),(313,41,67,0,10855,'description',0,6438,298,6436,1485153594,1,6437),(313,41,67,0,10856,'description',0,6439,299,6437,1485153594,1,6438),(313,41,67,0,10857,'description',0,6440,300,6438,1485153594,1,6439),(313,41,67,0,10858,'description',0,6441,301,6439,1485153594,1,6440),(313,41,67,0,10859,'description',0,6442,302,6440,1485153594,1,6441),(313,41,67,0,10860,'description',0,6443,303,6441,1485153594,1,6442),(313,41,67,0,10861,'description',0,6444,304,6442,1485153594,1,6443),(313,41,67,0,10862,'description',0,6445,305,6443,1485153594,1,6444),(313,41,67,0,10863,'description',0,6446,306,6444,1485153594,1,6445),(313,41,67,0,10864,'description',0,6447,307,6445,1485153594,1,6446),(313,41,67,0,10865,'description',0,6448,308,6446,1485153594,1,6447),(313,41,67,0,10866,'description',0,6449,309,6447,1485153594,1,6448),(313,41,67,0,10867,'description',0,6450,310,6448,1485153594,1,6449),(313,41,67,0,10868,'description',0,6451,311,6449,1485153594,1,6450),(313,41,67,0,10869,'description',0,6452,312,6450,1485153594,1,6451),(313,41,67,0,10870,'description',0,6453,313,6451,1485153594,1,6452),(313,41,67,0,10871,'description',0,6454,314,6452,1485153594,1,6453),(313,41,67,0,10872,'description',0,1746,315,6453,1485153594,1,6454),(313,41,67,0,10873,'description',0,6455,316,6454,1485153594,1,1746),(313,41,67,0,10874,'description',0,6456,317,1746,1485153594,1,6455),(313,41,67,0,10875,'description',0,6457,318,6455,1485153594,1,6456),(313,41,67,0,10876,'description',0,6458,319,6456,1485153594,1,6457),(313,41,67,0,10877,'description',0,6459,320,6457,1485153594,1,6458),(313,41,67,0,10878,'description',0,6460,321,6458,1485153594,1,6459),(313,41,67,0,10879,'description',0,6461,322,6459,1485153594,1,6460),(313,41,67,0,10880,'description',0,6462,323,6460,1485153594,1,6461),(313,41,67,0,10881,'description',0,6463,324,6461,1485153594,1,6462),(313,41,67,0,10882,'description',0,6464,325,6462,1485153594,1,6463),(313,41,67,0,10883,'description',0,6465,326,6463,1485153594,1,6464),(313,41,67,0,10884,'description',0,6466,327,6464,1485153594,1,6465),(313,41,67,0,10885,'description',0,6178,328,6465,1485153594,1,6466),(313,41,67,0,10886,'description',0,6467,329,6466,1485153594,1,6178),(313,41,67,0,10887,'description',0,6468,330,6178,1485153594,1,6467),(313,41,67,0,10888,'description',0,6469,331,6467,1485153594,1,6468),(313,41,67,0,10889,'description',0,6470,332,6468,1485153594,1,6469),(313,41,67,0,10890,'description',0,6471,333,6469,1485153594,1,6470),(313,41,67,0,10891,'description',0,6472,334,6470,1485153594,1,6471),(313,41,67,0,10892,'description',0,6473,335,6471,1485153594,1,6472),(313,41,67,0,10893,'description',0,6474,336,6472,1485153594,1,6473),(313,41,67,0,10894,'description',0,6475,337,6473,1485153594,1,6474),(313,41,67,0,10895,'description',0,6476,338,6474,1485153594,1,6475),(313,41,67,0,10896,'description',0,6477,339,6475,1485153594,1,6476),(313,41,67,0,10897,'description',0,6478,340,6476,1485153594,1,6477),(313,41,67,0,10898,'description',0,6479,341,6477,1485153594,1,6478),(313,41,67,0,10899,'description',0,6480,342,6478,1485153594,1,6479),(313,41,67,0,10900,'description',0,6481,343,6479,1485153594,1,6480),(313,41,67,0,10901,'description',0,6482,344,6480,1485153594,1,6481),(313,41,67,0,10902,'description',0,2356,345,6481,1485153594,1,6482),(313,41,67,0,10903,'description',0,6483,346,6482,1485153594,1,2356),(313,41,67,0,10904,'description',0,6484,347,2356,1485153594,1,6483),(313,41,67,0,10905,'description',0,6485,348,6483,1485153594,1,6484),(313,41,67,0,10906,'description',0,6486,349,6484,1485153594,1,6485),(313,41,67,0,10907,'description',0,6487,350,6485,1485153594,1,6486),(313,41,67,0,10908,'description',0,6488,351,6486,1485153594,1,6487),(313,41,67,0,10909,'description',0,6489,352,6487,1485153594,1,6488),(313,41,67,0,10910,'description',0,6490,353,6488,1485153594,1,6489),(313,41,67,0,10911,'description',0,6491,354,6489,1485153594,1,6490),(313,41,67,0,10912,'description',0,6492,355,6490,1485153594,1,6491),(313,41,67,0,10913,'description',0,6493,356,6491,1485153594,1,6492),(313,41,67,0,10914,'description',0,6494,357,6492,1485153594,1,6493),(313,41,67,0,10915,'description',0,6495,358,6493,1485153594,1,6494),(313,41,67,0,10916,'description',0,6496,359,6494,1485153594,1,6495),(313,41,67,0,10917,'description',0,6497,360,6495,1485153594,1,6496),(313,41,67,0,10918,'description',0,3036,361,6496,1485153594,1,6497),(313,41,67,0,10919,'description',0,2029,362,6497,1485153594,1,3036),(313,41,67,0,10920,'description',0,6498,363,3036,1485153594,1,2029),(313,41,67,0,10921,'description',0,6499,364,2029,1485153594,1,6498),(313,41,67,0,10922,'description',0,6500,365,6498,1485153594,1,6499),(313,41,67,0,10923,'description',0,6501,366,6499,1485153594,1,6500),(313,41,67,0,10924,'description',0,6502,367,6500,1485153594,1,6501),(313,41,67,0,10925,'description',0,6503,368,6501,1485153594,1,6502),(313,41,67,0,10926,'description',0,6504,369,6502,1485153594,1,6503),(313,41,67,0,10927,'description',0,6505,370,6503,1485153594,1,6504),(313,41,67,0,10928,'description',0,6506,371,6504,1485153594,1,6505),(313,41,67,0,10929,'description',0,6507,372,6505,1485153594,1,6506),(313,41,67,0,10930,'description',0,1840,373,6506,1485153594,1,6507),(313,41,67,0,10931,'description',0,6508,374,6507,1485153594,1,1840),(313,41,67,0,10932,'description',0,6509,375,1840,1485153594,1,6508),(313,41,67,0,10933,'description',0,6510,376,6508,1485153594,1,6509),(313,41,67,0,10934,'description',0,6511,377,6509,1485153594,1,6510),(313,41,67,0,10935,'description',0,6512,378,6510,1485153594,1,6511),(313,41,67,0,10936,'description',0,6513,379,6511,1485153594,1,6512),(313,41,67,0,10937,'description',0,6514,380,6512,1485153594,1,6513),(313,41,67,0,10938,'description',0,6515,381,6513,1485153594,1,6514),(313,41,67,0,10939,'description',0,6516,382,6514,1485153594,1,6515),(313,41,67,0,10940,'description',0,6517,383,6515,1485153594,1,6516),(313,41,67,0,10941,'description',0,6518,384,6516,1485153594,1,6517),(313,41,67,0,10942,'description',0,6519,385,6517,1485153594,1,6518),(313,41,67,0,10943,'description',0,3059,386,6518,1485153594,1,6519),(313,41,67,0,10944,'description',0,3060,387,6519,1485153594,1,3059),(313,41,67,0,10945,'description',0,6520,388,3059,1485153594,1,3060),(313,41,67,0,10946,'description',0,6521,389,3060,1485153594,1,6520),(313,41,67,0,10947,'description',0,6522,390,6520,1485153594,1,6521),(313,41,67,0,10948,'description',0,6523,391,6521,1485153594,1,6522),(313,41,67,0,10949,'description',0,3065,392,6522,1485153594,1,6523),(313,41,67,0,10950,'description',0,6524,393,6523,1485153594,1,3065),(313,41,67,0,10951,'description',0,6525,394,3065,1485153594,1,6524),(313,41,67,0,10952,'description',0,6526,395,6524,1485153594,1,6525),(313,41,67,0,10953,'description',0,6527,396,6525,1485153594,1,6526),(313,41,67,0,10954,'description',0,6528,397,6526,1485153594,1,6527),(313,41,67,0,10955,'description',0,6529,398,6527,1485153594,1,6528),(313,41,67,0,10956,'description',0,6530,399,6528,1485153594,1,6529),(313,41,67,0,10957,'description',0,6531,400,6529,1485153594,1,6530),(313,41,67,0,10958,'description',0,6532,401,6530,1485153594,1,6531),(313,41,67,0,10959,'description',0,6533,402,6531,1485153594,1,6532),(313,41,67,0,10960,'description',0,6534,403,6532,1485153594,1,6533),(313,41,67,0,10961,'description',0,6535,404,6533,1485153594,1,6534),(313,41,67,0,10962,'description',0,6536,405,6534,1485153594,1,6535),(313,41,67,0,10963,'description',0,6537,406,6535,1485153594,1,6536),(313,41,67,0,10964,'description',0,6538,407,6536,1485153594,1,6537),(313,41,67,0,10965,'description',0,6539,408,6537,1485153594,1,6538),(313,41,67,0,10966,'description',0,6540,409,6538,1485153594,1,6539),(313,41,67,0,10967,'description',0,6541,410,6539,1485153594,1,6540),(313,41,67,0,10968,'description',0,3084,411,6540,1485153594,1,6541),(313,41,67,0,10969,'description',0,6542,412,6541,1485153594,1,3084),(313,41,67,0,10970,'description',0,6543,413,3084,1485153594,1,6542),(313,41,67,0,10971,'description',0,6544,414,6542,1485153594,1,6543),(313,41,67,0,10972,'description',0,6545,415,6543,1485153594,1,6544),(313,41,67,0,10973,'description',0,6546,416,6544,1485153594,1,6545),(313,41,67,0,10974,'description',0,6547,417,6545,1485153594,1,6546),(313,41,67,0,10975,'description',0,6548,418,6546,1485153594,1,6547),(313,41,67,0,10976,'description',0,6549,419,6547,1485153594,1,6548),(313,41,67,0,10977,'description',0,6550,420,6548,1485153594,1,6549),(313,41,67,0,10978,'description',0,6551,421,6549,1485153594,1,6550),(313,41,67,0,10979,'description',0,6552,422,6550,1485153594,1,6551),(313,41,67,0,10980,'description',0,6553,423,6551,1485153594,1,6552),(313,41,67,0,10981,'description',0,6554,424,6552,1485153594,1,6553),(313,41,67,0,10982,'description',0,6555,425,6553,1485153594,1,6554),(313,41,67,0,10983,'description',0,6556,426,6554,1485153594,1,6555),(313,41,67,0,10984,'description',0,6557,427,6555,1485153594,1,6556),(313,41,67,0,10985,'description',0,6558,428,6556,1485153594,1,6557),(313,41,67,0,10986,'description',0,6559,429,6557,1485153594,1,6558),(313,41,67,0,10987,'description',0,6560,430,6558,1485153594,1,6559),(313,41,67,0,10988,'description',0,6561,431,6559,1485153594,1,6560),(313,41,67,0,10989,'description',0,6562,432,6560,1485153594,1,6561),(313,41,67,0,10990,'description',0,6563,433,6561,1485153594,1,6562),(313,41,67,0,10991,'description',0,6564,434,6562,1485153594,1,6563),(313,41,67,0,10992,'description',0,6565,435,6563,1485153594,1,6564),(313,41,67,0,10993,'description',0,6566,436,6564,1485153594,1,6565),(313,41,67,0,10994,'description',0,6567,437,6565,1485153594,1,6566),(313,41,67,0,10995,'description',0,6568,438,6566,1485153594,1,6567),(313,41,67,0,10996,'description',0,6569,439,6567,1485153594,1,6568),(313,41,67,0,10997,'description',0,6570,440,6568,1485153594,1,6569),(313,41,67,0,10998,'description',0,6571,441,6569,1485153594,1,6570),(313,41,67,0,10999,'description',0,6572,442,6570,1485153594,1,6571),(313,41,67,0,11000,'description',0,6573,443,6571,1485153594,1,6572),(313,41,67,0,11001,'description',0,6574,444,6572,1485153594,1,6573),(313,41,67,0,11002,'description',0,6575,445,6573,1485153594,1,6574),(313,41,67,0,11003,'description',0,6576,446,6574,1485153594,1,6575),(313,41,67,0,11004,'description',0,6577,447,6575,1485153594,1,6576),(313,41,67,0,11005,'description',0,6578,448,6576,1485153594,1,6577),(313,41,67,0,11006,'description',0,6579,449,6577,1485153594,1,6578),(313,41,67,0,11007,'description',0,1840,450,6578,1485153594,1,6579),(313,41,67,0,11008,'description',0,1726,451,6579,1485153594,1,1840),(313,41,67,0,11009,'description',0,6580,452,1840,1485153594,1,1726),(313,41,67,0,11010,'description',0,6581,453,1726,1485153594,1,6580),(313,41,67,0,11011,'description',0,3125,454,6580,1485153594,1,6581),(313,41,67,0,11012,'description',0,2365,455,6581,1485153594,1,3125),(313,41,67,0,11013,'description',0,6582,456,3125,1485153594,1,2365),(313,41,67,0,11014,'description',0,3127,457,2365,1485153594,1,6582),(313,41,67,0,11015,'description',0,6583,458,6582,1485153594,1,3127),(313,41,67,0,11016,'description',0,6584,459,3127,1485153594,1,6583),(313,41,67,0,11017,'description',0,6585,460,6583,1485153594,1,6584),(313,41,67,0,11018,'description',0,6586,461,6584,1485153594,1,6585),(313,41,67,0,11019,'description',0,6587,462,6585,1485153594,1,6586),(313,41,67,0,11020,'description',0,6588,463,6586,1485153594,1,6587),(313,41,67,0,11021,'description',0,6589,464,6587,1485153594,1,6588),(313,41,67,0,11022,'description',0,6590,465,6588,1485153594,1,6589),(313,41,67,0,11023,'description',0,6591,466,6589,1485153594,1,6590),(313,41,67,0,11024,'description',0,6592,467,6590,1485153594,1,6591),(313,41,67,0,11025,'description',0,6593,468,6591,1485153594,1,6592),(313,41,67,0,11026,'description',0,3139,469,6592,1485153594,1,6593),(313,41,67,0,11027,'description',0,6594,470,6593,1485153594,1,3139),(313,41,67,0,11028,'description',0,2412,471,3139,1485153594,1,6594),(313,41,67,0,11029,'description',0,6595,472,6594,1485153594,1,2412),(313,41,67,0,11030,'description',0,6596,473,2412,1485153594,1,6595),(313,41,67,0,11031,'description',0,6597,474,6595,1485153594,1,6596),(313,41,67,0,11032,'description',0,6598,475,6596,1485153594,1,6597),(313,41,67,0,11033,'description',0,6599,476,6597,1485153594,1,6598),(313,41,67,0,11034,'description',0,6600,477,6598,1485153594,1,6599),(313,41,67,0,11035,'description',0,6601,478,6599,1485153594,1,6600),(313,41,67,0,11036,'description',0,6602,479,6600,1485153594,1,6601),(313,41,67,0,11037,'description',0,6603,480,6601,1485153594,1,6602),(313,41,67,0,11038,'description',0,6604,481,6602,1485153594,1,6603),(313,41,67,0,11039,'description',0,6605,482,6603,1485153594,1,6604),(313,41,67,0,11040,'description',0,6606,483,6604,1485153594,1,6605),(313,41,67,0,11041,'description',0,6607,484,6605,1485153594,1,6606),(313,41,67,0,11042,'description',0,6608,485,6606,1485153594,1,6607),(313,41,67,0,11043,'description',0,6609,486,6607,1485153594,1,6608),(313,41,67,0,11044,'description',0,6610,487,6608,1485153594,1,6609),(313,41,67,0,11045,'description',0,6611,488,6609,1485153594,1,6610),(313,41,67,0,11046,'description',0,6612,489,6610,1485153594,1,6611),(313,41,67,0,11047,'description',0,6613,490,6611,1485153594,1,6612),(313,41,67,0,11048,'description',0,2191,491,6612,1485153594,1,6613),(313,41,67,0,11049,'description',0,6614,492,6613,1485153594,1,2191),(313,41,67,0,11050,'description',0,6615,493,2191,1485153594,1,6614),(313,41,67,0,11051,'description',0,6616,494,6614,1485153594,1,6615),(313,41,67,0,11052,'description',0,6617,495,6615,1485153594,1,6616),(313,41,67,0,11053,'description',0,2078,496,6616,1485153594,1,6617),(313,41,67,0,11054,'description',0,6618,497,6617,1485153594,1,2078),(313,41,67,0,11055,'description',0,6619,498,2078,1485153594,1,6618),(313,41,67,0,11056,'description',0,6620,499,6618,1485153594,1,6619),(313,41,67,0,11057,'description',0,6621,500,6619,1485153594,1,6620),(313,41,67,0,11058,'description',0,6622,501,6620,1485153594,1,6621),(313,41,67,0,11059,'description',0,6623,502,6621,1485153594,1,6622),(313,41,67,0,11060,'description',0,6624,503,6622,1485153594,1,6623),(313,41,67,0,11061,'description',0,6625,504,6623,1485153594,1,6624),(313,41,67,0,11062,'description',0,6626,505,6624,1485153594,1,6625),(313,41,67,0,11063,'description',0,6627,506,6625,1485153594,1,6626),(313,41,67,0,11064,'description',0,6628,507,6626,1485153594,1,6627),(313,41,67,0,11065,'description',0,6629,508,6627,1485153594,1,6628),(313,41,67,0,11066,'description',0,6630,509,6628,1485153594,1,6629),(313,41,67,0,11067,'description',0,6631,510,6629,1485153594,1,6630),(313,41,67,0,11068,'description',0,6632,511,6630,1485153594,1,6631),(313,41,67,0,11069,'description',0,6633,512,6631,1485153594,1,6632),(313,41,67,0,11070,'description',0,6634,513,6632,1485153594,1,6633),(313,41,67,0,11071,'description',0,6635,514,6633,1485153594,1,6634),(313,41,67,0,11072,'description',0,6636,515,6634,1485153594,1,6635),(313,41,67,0,11073,'description',0,6637,516,6635,1485153594,1,6636),(313,41,67,0,11074,'description',0,3184,517,6636,1485153594,1,6637),(313,41,67,0,11075,'description',0,2185,518,6637,1485153594,1,3184),(315,41,67,0,11076,'product_number',0,6638,519,3184,1485153594,1,2185),(315,41,67,0,11077,'product_number',0,2185,520,2185,1485153594,1,6638),(315,41,67,0,11078,'product_number',0,6638,521,6638,1485153594,1,2185),(315,41,67,0,11079,'product_number',0,0,522,2185,1485153594,1,6638),(312,41,63,0,11080,'title',0,6639,0,0,1485153471,1,1025),(312,41,63,0,11081,'title',0,2201,1,1025,1485153471,1,6639),(312,41,63,0,11082,'title',0,2202,2,6639,1485153471,1,2201),(312,41,63,0,11083,'title',0,1052,3,2201,1485153471,1,2202),(312,41,63,0,11084,'title',0,1032,4,2202,1485153471,1,1052),(313,41,63,0,11085,'description',0,1033,5,1052,1485153471,1,1032),(313,41,63,0,11086,'description',0,1034,6,1032,1485153471,1,1033),(313,41,63,0,11087,'description',0,1035,7,1033,1485153471,1,1034),(313,41,63,0,11088,'description',0,1036,8,1034,1485153471,1,1035),(313,41,63,0,11089,'description',0,2191,9,1035,1485153471,1,1036),(313,41,63,0,11090,'description',0,6640,10,1036,1485153471,1,2191),(313,41,63,0,11091,'description',0,6641,11,2191,1485153471,1,6640),(313,41,63,0,11092,'description',0,6642,12,6640,1485153471,1,6641),(313,41,63,0,11093,'description',0,6643,13,6641,1485153471,1,6642),(313,41,63,0,11094,'description',0,6644,14,6642,1485153471,1,6643),(313,41,63,0,11095,'description',0,6645,15,6643,1485153471,1,6644),(313,41,63,0,11096,'description',0,6646,16,6644,1485153471,1,6645),(313,41,63,0,11097,'description',0,6647,17,6645,1485153471,1,6646),(313,41,63,0,11098,'description',0,6648,18,6646,1485153471,1,6647),(313,41,63,0,11099,'description',0,6649,19,6647,1485153471,1,6648),(313,41,63,0,11100,'description',0,6650,20,6648,1485153471,1,6649),(313,41,63,0,11101,'description',0,6651,21,6649,1485153471,1,6650),(313,41,63,0,11102,'description',0,6652,22,6650,1485153471,1,6651),(313,41,63,0,11103,'description',0,6653,23,6651,1485153471,1,6652),(313,41,63,0,11104,'description',0,6654,24,6652,1485153471,1,6653),(313,41,63,0,11105,'description',0,6655,25,6653,1485153471,1,6654),(313,41,63,0,11106,'description',0,6656,26,6654,1485153471,1,6655),(313,41,63,0,11107,'description',0,6657,27,6655,1485153471,1,6656),(313,41,63,0,11108,'description',0,6658,28,6656,1485153471,1,6657),(313,41,63,0,11109,'description',0,6659,29,6657,1485153471,1,6658),(313,41,63,0,11110,'description',0,6660,30,6658,1485153471,1,6659),(313,41,63,0,11111,'description',0,6661,31,6659,1485153471,1,6660),(313,41,63,0,11112,'description',0,6662,32,6660,1485153471,1,6661),(313,41,63,0,11113,'description',0,6663,33,6661,1485153471,1,6662),(313,41,63,0,11114,'description',0,6664,34,6662,1485153471,1,6663),(313,41,63,0,11115,'description',0,6665,35,6663,1485153471,1,6664),(313,41,63,0,11116,'description',0,6666,36,6664,1485153471,1,6665),(313,41,63,0,11117,'description',0,6667,37,6665,1485153471,1,6666),(313,41,63,0,11118,'description',0,6668,38,6666,1485153471,1,6667),(313,41,63,0,11119,'description',0,6669,39,6667,1485153471,1,6668),(313,41,63,0,11120,'description',0,6670,40,6668,1485153471,1,6669),(313,41,63,0,11121,'description',0,6671,41,6669,1485153471,1,6670),(313,41,63,0,11122,'description',0,6672,42,6670,1485153471,1,6671),(313,41,63,0,11123,'description',0,6673,43,6671,1485153471,1,6672),(313,41,63,0,11124,'description',0,6674,44,6672,1485153471,1,6673),(313,41,63,0,11125,'description',0,6675,45,6673,1485153471,1,6674),(313,41,63,0,11126,'description',0,6676,46,6674,1485153471,1,6675),(313,41,63,0,11127,'description',0,6677,47,6675,1485153471,1,6676),(313,41,63,0,11128,'description',0,6678,48,6676,1485153471,1,6677),(313,41,63,0,11129,'description',0,6679,49,6677,1485153471,1,6678),(313,41,63,0,11130,'description',0,6680,50,6678,1485153471,1,6679),(313,41,63,0,11131,'description',0,6681,51,6679,1485153471,1,6680),(313,41,63,0,11132,'description',0,6682,52,6680,1485153471,1,6681),(313,41,63,0,11133,'description',0,6683,53,6681,1485153471,1,6682),(313,41,63,0,11134,'description',0,6684,54,6682,1485153471,1,6683),(313,41,63,0,11135,'description',0,6685,55,6683,1485153471,1,6684),(313,41,63,0,11136,'description',0,6686,56,6684,1485153471,1,6685),(313,41,63,0,11137,'description',0,6687,57,6685,1485153471,1,6686),(313,41,63,0,11138,'description',0,6688,58,6686,1485153471,1,6687),(313,41,63,0,11139,'description',0,6689,59,6687,1485153471,1,6688),(313,41,63,0,11140,'description',0,6690,60,6688,1485153471,1,6689),(313,41,63,0,11141,'description',0,6691,61,6689,1485153471,1,6690),(313,41,63,0,11142,'description',0,6692,62,6690,1485153471,1,6691),(313,41,63,0,11143,'description',0,6693,63,6691,1485153471,1,6692),(313,41,63,0,11144,'description',0,6694,64,6692,1485153471,1,6693),(313,41,63,0,11145,'description',0,6695,65,6693,1485153471,1,6694),(313,41,63,0,11146,'description',0,6696,66,6694,1485153471,1,6695),(313,41,63,0,11147,'description',0,6697,67,6695,1485153471,1,6696),(313,41,63,0,11148,'description',0,6698,68,6696,1485153471,1,6697),(313,41,63,0,11149,'description',0,6699,69,6697,1485153471,1,6698),(313,41,63,0,11150,'description',0,6700,70,6698,1485153471,1,6699),(313,41,63,0,11151,'description',0,6701,71,6699,1485153471,1,6700),(313,41,63,0,11152,'description',0,6702,72,6700,1485153471,1,6701),(313,41,63,0,11153,'description',0,6703,73,6701,1485153471,1,6702),(313,41,63,0,11154,'description',0,6704,74,6702,1485153471,1,6703),(313,41,63,0,11155,'description',0,6705,75,6703,1485153471,1,6704),(313,41,63,0,11156,'description',0,6706,76,6704,1485153471,1,6705),(313,41,63,0,11157,'description',0,6707,77,6705,1485153471,1,6706),(313,41,63,0,11158,'description',0,6708,78,6706,1485153471,1,6707),(313,41,63,0,11159,'description',0,6649,79,6707,1485153471,1,6708),(313,41,63,0,11160,'description',0,6709,80,6708,1485153471,1,6649),(313,41,63,0,11161,'description',0,6710,81,6649,1485153471,1,6709),(313,41,63,0,11162,'description',0,6711,82,6709,1485153471,1,6710),(313,41,63,0,11163,'description',0,6712,83,6710,1485153471,1,6711),(313,41,63,0,11164,'description',0,6713,84,6711,1485153471,1,6712),(313,41,63,0,11165,'description',0,6714,85,6712,1485153471,1,6713),(313,41,63,0,11166,'description',0,6715,86,6713,1485153471,1,6714),(313,41,63,0,11167,'description',0,6716,87,6714,1485153471,1,6715),(313,41,63,0,11168,'description',0,6717,88,6715,1485153471,1,6716),(313,41,63,0,11169,'description',0,6718,89,6716,1485153471,1,6717),(313,41,63,0,11170,'description',0,6719,90,6717,1485153471,1,6718),(313,41,63,0,11171,'description',0,6720,91,6718,1485153471,1,6719),(313,41,63,0,11172,'description',0,6721,92,6719,1485153471,1,6720),(313,41,63,0,11173,'description',0,6722,93,6720,1485153471,1,6721),(313,41,63,0,11174,'description',0,6723,94,6721,1485153471,1,6722),(313,41,63,0,11175,'description',0,6724,95,6722,1485153471,1,6723),(313,41,63,0,11176,'description',0,6725,96,6723,1485153471,1,6724),(313,41,63,0,11177,'description',0,6726,97,6724,1485153471,1,6725),(313,41,63,0,11178,'description',0,6727,98,6725,1485153471,1,6726),(313,41,63,0,11179,'description',0,6728,99,6726,1485153471,1,6727),(313,41,63,0,11180,'description',0,6729,100,6727,1485153471,1,6728),(313,41,63,0,11181,'description',0,6730,101,6728,1485153471,1,6729),(313,41,63,0,11182,'description',0,6731,102,6729,1485153471,1,6730),(313,41,63,0,11183,'description',0,6732,103,6730,1485153471,1,6731),(313,41,63,0,11184,'description',0,6733,104,6731,1485153471,1,6732),(313,41,63,0,11185,'description',0,6734,105,6732,1485153471,1,6733),(313,41,63,0,11186,'description',0,6735,106,6733,1485153471,1,6734),(313,41,63,0,11187,'description',0,6736,107,6734,1485153471,1,6735),(313,41,63,0,11188,'description',0,6737,108,6735,1485153471,1,6736),(313,41,63,0,11189,'description',0,6738,109,6736,1485153471,1,6737),(313,41,63,0,11190,'description',0,6739,110,6737,1485153471,1,6738),(313,41,63,0,11191,'description',0,6740,111,6738,1485153471,1,6739),(313,41,63,0,11192,'description',0,2304,112,6739,1485153471,1,6740),(313,41,63,0,11193,'description',0,6741,113,6740,1485153471,1,2304),(313,41,63,0,11194,'description',0,6742,114,2304,1485153471,1,6741),(313,41,63,0,11195,'description',0,6743,115,6741,1485153471,1,6742),(313,41,63,0,11196,'description',0,6744,116,6742,1485153471,1,6743),(313,41,63,0,11197,'description',0,6745,117,6743,1485153471,1,6744),(313,41,63,0,11198,'description',0,6746,118,6744,1485153471,1,6745),(313,41,63,0,11199,'description',0,6747,119,6745,1485153471,1,6746),(313,41,63,0,11200,'description',0,1827,120,6746,1485153471,1,6747),(313,41,63,0,11201,'description',0,6748,121,6747,1485153471,1,1827),(313,41,63,0,11202,'description',0,6749,122,1827,1485153471,1,6748),(313,41,63,0,11203,'description',0,6750,123,6748,1485153471,1,6749),(313,41,63,0,11204,'description',0,6751,124,6749,1485153471,1,6750),(313,41,63,0,11205,'description',0,6752,125,6750,1485153471,1,6751),(313,41,63,0,11206,'description',0,6753,126,6751,1485153471,1,6752),(313,41,63,0,11207,'description',0,6754,127,6752,1485153471,1,6753),(313,41,63,0,11208,'description',0,6755,128,6753,1485153471,1,6754),(313,41,63,0,11209,'description',0,6756,129,6754,1485153471,1,6755),(313,41,63,0,11210,'description',0,2321,130,6755,1485153471,1,6756),(313,41,63,0,11211,'description',0,2322,131,6756,1485153471,1,2321),(313,41,63,0,11212,'description',0,6757,132,2321,1485153471,1,2322),(313,41,63,0,11213,'description',0,6758,133,2322,1485153471,1,6757),(313,41,63,0,11214,'description',0,6759,134,6757,1485153471,1,6758),(313,41,63,0,11215,'description',0,6760,135,6758,1485153471,1,6759),(313,41,63,0,11216,'description',0,6761,136,6759,1485153471,1,6760),(313,41,63,0,11217,'description',0,6762,137,6760,1485153471,1,6761),(313,41,63,0,11218,'description',0,6763,138,6761,1485153471,1,6762),(313,41,63,0,11219,'description',0,6764,139,6762,1485153471,1,6763),(313,41,63,0,11220,'description',0,6765,140,6763,1485153471,1,6764),(313,41,63,0,11221,'description',0,6766,141,6764,1485153471,1,6765),(313,41,63,0,11222,'description',0,6767,142,6765,1485153471,1,6766),(313,41,63,0,11223,'description',0,6768,143,6766,1485153471,1,6767),(313,41,63,0,11224,'description',0,6769,144,6767,1485153471,1,6768),(313,41,63,0,11225,'description',0,6770,145,6768,1485153471,1,6769),(313,41,63,0,11226,'description',0,6771,146,6769,1485153471,1,6770),(313,41,63,0,11227,'description',0,6772,147,6770,1485153471,1,6771),(313,41,63,0,11228,'description',0,6773,148,6771,1485153471,1,6772),(313,41,63,0,11229,'description',0,6774,149,6772,1485153471,1,6773),(313,41,63,0,11230,'description',0,6775,150,6773,1485153471,1,6774),(313,41,63,0,11231,'description',0,6776,151,6774,1485153471,1,6775),(313,41,63,0,11232,'description',0,6777,152,6775,1485153471,1,6776),(313,41,63,0,11233,'description',0,6778,153,6776,1485153471,1,6777),(313,41,63,0,11234,'description',0,6779,154,6777,1485153471,1,6778),(313,41,63,0,11235,'description',0,6780,155,6778,1485153471,1,6779),(313,41,63,0,11236,'description',0,6781,156,6779,1485153471,1,6780),(313,41,63,0,11237,'description',0,6782,157,6780,1485153471,1,6781),(313,41,63,0,11238,'description',0,6783,158,6781,1485153471,1,6782),(313,41,63,0,11239,'description',0,6784,159,6782,1485153471,1,6783),(313,41,63,0,11240,'description',0,6785,160,6783,1485153471,1,6784),(313,41,63,0,11241,'description',0,6786,161,6784,1485153471,1,6785),(313,41,63,0,11242,'description',0,6787,162,6785,1485153471,1,6786),(313,41,63,0,11243,'description',0,6788,163,6786,1485153471,1,6787),(313,41,63,0,11244,'description',0,6789,164,6787,1485153471,1,6788),(313,41,63,0,11245,'description',0,2356,165,6788,1485153471,1,6789),(313,41,63,0,11246,'description',0,6790,166,6789,1485153471,1,2356),(313,41,63,0,11247,'description',0,6791,167,2356,1485153471,1,6790),(313,41,63,0,11248,'description',0,6792,168,6790,1485153471,1,6791),(313,41,63,0,11249,'description',0,6793,169,6791,1485153471,1,6792),(313,41,63,0,11250,'description',0,6794,170,6792,1485153471,1,6793),(313,41,63,0,11251,'description',0,6795,171,6793,1485153471,1,6794),(313,41,63,0,11252,'description',0,6796,172,6794,1485153471,1,6795),(313,41,63,0,11253,'description',0,6797,173,6795,1485153471,1,6796),(313,41,63,0,11254,'description',0,1840,174,6796,1485153471,1,6797),(313,41,63,0,11255,'description',0,2365,175,6797,1485153471,1,1840),(313,41,63,0,11256,'description',0,6798,176,1840,1485153471,1,2365),(313,41,63,0,11257,'description',0,6799,177,2365,1485153471,1,6798),(313,41,63,0,11258,'description',0,6800,178,6798,1485153471,1,6799),(313,41,63,0,11259,'description',0,6801,179,6799,1485153471,1,6800),(313,41,63,0,11260,'description',0,6802,180,6800,1485153471,1,6801),(313,41,63,0,11261,'description',0,6803,181,6801,1485153471,1,6802),(313,41,63,0,11262,'description',0,6804,182,6802,1485153471,1,6803),(313,41,63,0,11263,'description',0,6805,183,6803,1485153471,1,6804),(313,41,63,0,11264,'description',0,6806,184,6804,1485153471,1,6805),(313,41,63,0,11265,'description',0,6807,185,6805,1485153471,1,6806),(313,41,63,0,11266,'description',0,6808,186,6806,1485153471,1,6807),(313,41,63,0,11267,'description',0,6809,187,6807,1485153471,1,6808),(313,41,63,0,11268,'description',0,6810,188,6808,1485153471,1,6809),(313,41,63,0,11269,'description',0,6811,189,6809,1485153471,1,6810),(313,41,63,0,11270,'description',0,6812,190,6810,1485153471,1,6811),(313,41,63,0,11271,'description',0,6813,191,6811,1485153471,1,6812),(313,41,63,0,11272,'description',0,6814,192,6812,1485153471,1,6813),(313,41,63,0,11273,'description',0,6815,193,6813,1485153471,1,6814),(313,41,63,0,11274,'description',0,2384,194,6814,1485153471,1,6815),(313,41,63,0,11275,'description',0,6816,195,6815,1485153471,1,2384),(313,41,63,0,11276,'description',0,6817,196,2384,1485153471,1,6816),(313,41,63,0,11277,'description',0,6818,197,6816,1485153471,1,6817),(313,41,63,0,11278,'description',0,6819,198,6817,1485153471,1,6818),(313,41,63,0,11279,'description',0,6820,199,6818,1485153471,1,6819),(313,41,63,0,11280,'description',0,6821,200,6819,1485153471,1,6820),(313,41,63,0,11281,'description',0,6822,201,6820,1485153471,1,6821),(313,41,63,0,11282,'description',0,6823,202,6821,1485153471,1,6822),(313,41,63,0,11283,'description',0,6824,203,6822,1485153471,1,6823),(313,41,63,0,11284,'description',0,6825,204,6823,1485153471,1,6824),(313,41,63,0,11285,'description',0,6826,205,6824,1485153471,1,6825),(313,41,63,0,11286,'description',0,2396,206,6825,1485153471,1,6826),(313,41,63,0,11287,'description',0,6827,207,6826,1485153471,1,2396),(313,41,63,0,11288,'description',0,6828,208,2396,1485153471,1,6827),(313,41,63,0,11289,'description',0,6829,209,6827,1485153471,1,6828),(313,41,63,0,11290,'description',0,6830,210,6828,1485153471,1,6829),(313,41,63,0,11291,'description',0,6831,211,6829,1485153471,1,6830),(313,41,63,0,11292,'description',0,6832,212,6830,1485153471,1,6831),(313,41,63,0,11293,'description',0,6833,213,6831,1485153471,1,6832),(313,41,63,0,11294,'description',0,6834,214,6832,1485153471,1,6833),(313,41,63,0,11295,'description',0,6835,215,6833,1485153471,1,6834),(313,41,63,0,11296,'description',0,6836,216,6834,1485153471,1,6835),(313,41,63,0,11297,'description',0,6837,217,6835,1485153471,1,6836),(313,41,63,0,11298,'description',0,6838,218,6836,1485153471,1,6837),(313,41,63,0,11299,'description',0,6839,219,6837,1485153471,1,6838),(313,41,63,0,11300,'description',0,6840,220,6838,1485153471,1,6839),(313,41,63,0,11301,'description',0,6841,221,6839,1485153471,1,6840),(313,41,63,0,11302,'description',0,2412,222,6840,1485153471,1,6841),(313,41,63,0,11303,'description',0,6842,223,6841,1485153471,1,2412),(313,41,63,0,11304,'description',0,6843,224,2412,1485153471,1,6842),(313,41,63,0,11305,'description',0,6844,225,6842,1485153471,1,6843),(313,41,63,0,11306,'description',0,6845,226,6843,1485153471,1,6844),(313,41,63,0,11307,'description',0,6846,227,6844,1485153471,1,6845),(313,41,63,0,11308,'description',0,6847,228,6845,1485153471,1,6846),(313,41,63,0,11309,'description',0,6848,229,6846,1485153471,1,6847),(313,41,63,0,11310,'description',0,2420,230,6847,1485153471,1,6848),(313,41,63,0,11311,'description',0,6849,231,6848,1485153471,1,2420),(313,41,63,0,11312,'description',0,6850,232,2420,1485153471,1,6849),(313,41,63,0,11313,'description',0,6851,233,6849,1485153471,1,6850),(313,41,63,0,11314,'description',0,6852,234,6850,1485153471,1,6851),(313,41,63,0,11315,'description',0,6853,235,6851,1485153471,1,6852),(313,41,63,0,11316,'description',0,6854,236,6852,1485153471,1,6853),(313,41,63,0,11317,'description',0,6855,237,6853,1485153471,1,6854),(313,41,63,0,11318,'description',0,6856,238,6854,1485153471,1,6855),(313,41,63,0,11319,'description',0,6857,239,6855,1485153471,1,6856),(313,41,63,0,11320,'description',0,6858,240,6856,1485153471,1,6857),(313,41,63,0,11321,'description',0,6859,241,6857,1485153471,1,6858),(313,41,63,0,11322,'description',0,6860,242,6858,1485153471,1,6859),(313,41,63,0,11323,'description',0,6861,243,6859,1485153471,1,6860),(313,41,63,0,11324,'description',0,6862,244,6860,1485153471,1,6861),(313,41,63,0,11325,'description',0,6863,245,6861,1485153471,1,6862),(313,41,63,0,11326,'description',0,6864,246,6862,1485153471,1,6863),(313,41,63,0,11327,'description',0,6865,247,6863,1485153471,1,6864),(313,41,63,0,11328,'description',0,6866,248,6864,1485153471,1,6865),(313,41,63,0,11329,'description',0,6867,249,6865,1485153471,1,6866),(313,41,63,0,11330,'description',0,6868,250,6866,1485153471,1,6867),(313,41,63,0,11331,'description',0,6869,251,6867,1485153471,1,6868),(313,41,63,0,11332,'description',0,6870,252,6868,1485153471,1,6869),(313,41,63,0,11333,'description',0,1040,253,6869,1485153471,1,6870),(313,41,63,0,11334,'description',0,6871,254,6870,1485153471,1,1040),(313,41,63,0,11335,'description',0,6872,255,1040,1485153471,1,6871),(313,41,63,0,11336,'description',0,6873,256,6871,1485153471,1,6872),(313,41,63,0,11337,'description',0,6874,257,6872,1485153471,1,6873),(313,41,63,0,11338,'description',0,1772,258,6873,1485153471,1,6874),(313,41,63,0,11339,'description',0,6875,259,6874,1485153471,1,1772),(313,41,63,0,11340,'description',0,6876,260,1772,1485153471,1,6875),(313,41,63,0,11341,'description',0,6877,261,6875,1485153471,1,6876),(313,41,63,0,11342,'description',0,6878,262,6876,1485153471,1,6877),(313,41,63,0,11343,'description',0,6879,263,6877,1485153471,1,6878),(313,41,63,0,11344,'description',0,6880,264,6878,1485153471,1,6879),(313,41,63,0,11345,'description',0,6881,265,6879,1485153471,1,6880),(313,41,63,0,11346,'description',0,1946,266,6880,1485153471,1,6881),(313,41,63,0,11347,'description',0,6882,267,6881,1485153471,1,1946),(313,41,63,0,11348,'description',0,6883,268,1946,1485153471,1,6882),(313,41,63,0,11349,'description',0,6884,269,6882,1485153471,1,6883),(313,41,63,0,11350,'description',0,6885,270,6883,1485153471,1,6884),(313,41,63,0,11351,'description',0,6886,271,6884,1485153471,1,6885),(313,41,63,0,11352,'description',0,6887,272,6885,1485153471,1,6886),(313,41,63,0,11353,'description',0,6888,273,6886,1485153471,1,6887),(313,41,63,0,11354,'description',0,6889,274,6887,1485153471,1,6888),(313,41,63,0,11355,'description',0,6890,275,6888,1485153471,1,6889),(313,41,63,0,11356,'description',0,6891,276,6889,1485153471,1,6890),(313,41,63,0,11357,'description',0,6892,277,6890,1485153471,1,6891),(313,41,63,0,11358,'description',0,6893,278,6891,1485153471,1,6892),(313,41,63,0,11359,'description',0,6894,279,6892,1485153471,1,6893),(313,41,63,0,11360,'description',0,6895,280,6893,1485153471,1,6894),(313,41,63,0,11361,'description',0,2008,281,6894,1485153471,1,6895),(313,41,63,0,11362,'description',0,6896,282,6895,1485153471,1,2008),(313,41,63,0,11363,'description',0,6897,283,2008,1485153471,1,6896),(313,41,63,0,11364,'description',0,6898,284,6896,1485153471,1,6897),(313,41,63,0,11365,'description',0,6899,285,6897,1485153471,1,6898),(313,41,63,0,11366,'description',0,6900,286,6898,1485153471,1,6899),(313,41,63,0,11367,'description',0,6901,287,6899,1485153471,1,6900),(313,41,63,0,11368,'description',0,6902,288,6900,1485153471,1,6901),(313,41,63,0,11369,'description',0,6903,289,6901,1485153471,1,6902),(313,41,63,0,11370,'description',0,6904,290,6902,1485153471,1,6903),(313,41,63,0,11371,'description',0,6905,291,6903,1485153471,1,6904),(313,41,63,0,11372,'description',0,2478,292,6904,1485153471,1,6905),(313,41,63,0,11373,'description',0,6906,293,6905,1485153471,1,2478),(313,41,63,0,11374,'description',0,6907,294,2478,1485153471,1,6906),(313,41,63,0,11375,'description',0,6908,295,6906,1485153471,1,6907),(313,41,63,0,11376,'description',0,6909,296,6907,1485153471,1,6908),(313,41,63,0,11377,'description',0,6910,297,6908,1485153471,1,6909),(313,41,63,0,11378,'description',0,6911,298,6909,1485153471,1,6910),(313,41,63,0,11379,'description',0,6912,299,6910,1485153471,1,6911),(313,41,63,0,11380,'description',0,6913,300,6911,1485153471,1,6912),(313,41,63,0,11381,'description',0,6914,301,6912,1485153471,1,6913),(313,41,63,0,11382,'description',0,2488,302,6913,1485153471,1,6914),(313,41,63,0,11383,'description',0,6915,303,6914,1485153471,1,2488),(313,41,63,0,11384,'description',0,6916,304,2488,1485153471,1,6915),(313,41,63,0,11385,'description',0,6917,305,6915,1485153471,1,6916),(313,41,63,0,11386,'description',0,6918,306,6916,1485153471,1,6917),(313,41,63,0,11387,'description',0,6919,307,6917,1485153471,1,6918),(313,41,63,0,11388,'description',0,6920,308,6918,1485153471,1,6919),(313,41,63,0,11389,'description',0,6921,309,6919,1485153471,1,6920),(313,41,63,0,11390,'description',0,6922,310,6920,1485153471,1,6921),(313,41,63,0,11391,'description',0,6903,311,6921,1485153471,1,6922),(313,41,63,0,11392,'description',0,6923,312,6922,1485153471,1,6903),(313,41,63,0,11393,'description',0,1728,313,6903,1485153471,1,6923),(313,41,63,0,11394,'description',0,6924,314,6923,1485153471,1,1728),(313,41,63,0,11395,'description',0,2499,315,1728,1485153471,1,6924),(313,41,63,0,11396,'description',0,6925,316,6924,1485153471,1,2499),(313,41,63,0,11397,'description',0,6926,317,2499,1485153471,1,6925),(313,41,63,0,11398,'description',0,6927,318,6925,1485153471,1,6926),(313,41,63,0,11399,'description',0,6928,319,6926,1485153471,1,6927),(313,41,63,0,11400,'description',0,6929,320,6927,1485153471,1,6928),(313,41,63,0,11401,'description',0,6930,321,6928,1485153471,1,6929),(313,41,63,0,11402,'description',0,6931,322,6929,1485153471,1,6930),(313,41,63,0,11403,'description',0,6932,323,6930,1485153471,1,6931),(313,41,63,0,11404,'description',0,6933,324,6931,1485153471,1,6932),(313,41,63,0,11405,'description',0,6934,325,6932,1485153471,1,6933),(313,41,63,0,11406,'description',0,6935,326,6933,1485153471,1,6934),(313,41,63,0,11407,'description',0,2511,327,6934,1485153471,1,6935),(313,41,63,0,11408,'description',0,6936,328,6935,1485153471,1,2511),(313,41,63,0,11409,'description',0,6937,329,2511,1485153471,1,6936),(313,41,63,0,11410,'description',0,6938,330,6936,1485153471,1,6937),(313,41,63,0,11411,'description',0,6939,331,6937,1485153471,1,6938),(313,41,63,0,11412,'description',0,6940,332,6938,1485153471,1,6939),(313,41,63,0,11413,'description',0,6941,333,6939,1485153471,1,6940),(313,41,63,0,11414,'description',0,6942,334,6940,1485153471,1,6941),(313,41,63,0,11415,'description',0,6943,335,6941,1485153471,1,6942),(313,41,63,0,11416,'description',0,6944,336,6942,1485153471,1,6943),(313,41,63,0,11417,'description',0,6945,337,6943,1485153471,1,6944),(313,41,63,0,11418,'description',0,6946,338,6944,1485153471,1,6945),(313,41,63,0,11419,'description',0,2523,339,6945,1485153471,1,6946),(313,41,63,0,11420,'description',0,1840,340,6946,1485153471,1,2523),(313,41,63,0,11421,'description',0,6947,341,2523,1485153471,1,1840),(313,41,63,0,11422,'description',0,2525,342,1840,1485153471,1,6947),(313,41,63,0,11423,'description',0,6948,343,6947,1485153471,1,2525),(313,41,63,0,11424,'description',0,6949,344,2525,1485153471,1,6948),(313,41,63,0,11425,'description',0,6950,345,6948,1485153471,1,6949),(313,41,63,0,11426,'description',0,6951,346,6949,1485153471,1,6950),(313,41,63,0,11427,'description',0,6952,347,6950,1485153471,1,6951),(313,41,63,0,11428,'description',0,6953,348,6951,1485153471,1,6952),(313,41,63,0,11429,'description',0,6954,349,6952,1485153471,1,6953),(313,41,63,0,11430,'description',0,6955,350,6953,1485153471,1,6954),(313,41,63,0,11431,'description',0,6956,351,6954,1485153471,1,6955),(313,41,63,0,11432,'description',0,6957,352,6955,1485153471,1,6956),(313,41,63,0,11433,'description',0,6958,353,6956,1485153471,1,6957),(313,41,63,0,11434,'description',0,6959,354,6957,1485153471,1,6958),(313,41,63,0,11435,'description',0,6960,355,6958,1485153471,1,6959),(313,41,63,0,11436,'description',0,6961,356,6959,1485153471,1,6960),(313,41,63,0,11437,'description',0,6962,357,6960,1485153471,1,6961),(313,41,63,0,11438,'description',0,6963,358,6961,1485153471,1,6962),(313,41,63,0,11439,'description',0,6964,359,6962,1485153471,1,6963),(313,41,63,0,11440,'description',0,6965,360,6963,1485153471,1,6964),(313,41,63,0,11441,'description',0,6966,361,6964,1485153471,1,6965),(313,41,63,0,11442,'description',0,6967,362,6965,1485153471,1,6966),(313,41,63,0,11443,'description',0,6968,363,6966,1485153471,1,6967),(313,41,63,0,11444,'description',0,6902,364,6967,1485153471,1,6968),(313,41,63,0,11445,'description',0,2191,365,6968,1485153471,1,6902),(313,41,63,0,11446,'description',0,6969,366,6902,1485153471,1,2191),(313,41,63,0,11447,'description',0,6970,367,2191,1485153471,1,6969),(313,41,63,0,11448,'description',0,6971,368,6969,1485153471,1,6970),(313,41,63,0,11449,'description',0,6972,369,6970,1485153471,1,6971),(313,41,63,0,11450,'description',0,6973,370,6971,1485153471,1,6972),(313,41,63,0,11451,'description',0,6974,371,6972,1485153471,1,6973),(313,41,63,0,11452,'description',0,6975,372,6973,1485153471,1,6974),(313,41,63,0,11453,'description',0,6976,373,6974,1485153471,1,6975),(313,41,63,0,11454,'description',0,6977,374,6975,1485153471,1,6976),(313,41,63,0,11455,'description',0,6978,375,6976,1485153471,1,6977),(313,41,63,0,11456,'description',0,6979,376,6977,1485153471,1,6978),(313,41,63,0,11457,'description',0,6980,377,6978,1485153471,1,6979),(313,41,63,0,11458,'description',0,6981,378,6979,1485153471,1,6980),(313,41,63,0,11459,'description',0,6982,379,6980,1485153471,1,6981),(313,41,63,0,11460,'description',0,6983,380,6981,1485153471,1,6982),(313,41,63,0,11461,'description',0,6984,381,6982,1485153471,1,6983),(313,41,63,0,11462,'description',0,6985,382,6983,1485153471,1,6984),(313,41,63,0,11463,'description',0,6986,383,6984,1485153471,1,6985),(313,41,63,0,11464,'description',0,6987,384,6985,1485153471,1,6986),(313,41,63,0,11465,'description',0,6988,385,6986,1485153471,1,6987),(313,41,63,0,11466,'description',0,6989,386,6987,1485153471,1,6988),(313,41,63,0,11467,'description',0,6990,387,6988,1485153471,1,6989),(313,41,63,0,11468,'description',0,6991,388,6989,1485153471,1,6990),(313,41,63,0,11469,'description',0,6770,389,6990,1485153471,1,6991),(313,41,63,0,11470,'description',0,6992,390,6991,1485153471,1,6770),(313,41,63,0,11471,'description',0,6993,391,6770,1485153471,1,6992),(313,41,63,0,11472,'description',0,6994,392,6992,1485153471,1,6993),(313,41,63,0,11473,'description',0,6995,393,6993,1485153471,1,6994),(313,41,63,0,11474,'description',0,6996,394,6994,1485153471,1,6995),(313,41,63,0,11475,'description',0,6997,395,6995,1485153471,1,6996),(313,41,63,0,11476,'description',0,6998,396,6996,1485153471,1,6997),(313,41,63,0,11477,'description',0,6999,397,6997,1485153471,1,6998),(313,41,63,0,11478,'description',0,7000,398,6998,1485153471,1,6999),(313,41,63,0,11479,'description',0,7001,399,6999,1485153471,1,7000),(313,41,63,0,11480,'description',0,7002,400,7000,1485153471,1,7001),(313,41,63,0,11481,'description',0,7003,401,7001,1485153471,1,7002),(313,41,63,0,11482,'description',0,7004,402,7002,1485153471,1,7003),(313,41,63,0,11483,'description',0,7005,403,7003,1485153471,1,7004),(313,41,63,0,11484,'description',0,7006,404,7004,1485153471,1,7005),(313,41,63,0,11485,'description',0,7007,405,7005,1485153471,1,7006),(313,41,63,0,11486,'description',0,7008,406,7006,1485153471,1,7007),(313,41,63,0,11487,'description',0,7009,407,7007,1485153471,1,7008),(313,41,63,0,11488,'description',0,7010,408,7008,1485153471,1,7009),(313,41,63,0,11489,'description',0,7011,409,7009,1485153471,1,7010),(313,41,63,0,11490,'description',0,7012,410,7010,1485153471,1,7011),(313,41,63,0,11491,'description',0,7013,411,7011,1485153471,1,7012),(313,41,63,0,11492,'description',0,7014,412,7012,1485153471,1,7013),(313,41,63,0,11493,'description',0,7015,413,7013,1485153471,1,7014),(313,41,63,0,11494,'description',0,7016,414,7014,1485153471,1,7015),(313,41,63,0,11495,'description',0,7017,415,7015,1485153471,1,7016),(313,41,63,0,11496,'description',0,7018,416,7016,1485153471,1,7017),(313,41,63,0,11497,'description',0,7019,417,7017,1485153471,1,7018),(313,41,63,0,11498,'description',0,7020,418,7018,1485153471,1,7019),(313,41,63,0,11499,'description',0,7021,419,7019,1485153471,1,7020),(313,41,63,0,11500,'description',0,7022,420,7020,1485153471,1,7021),(313,41,63,0,11501,'description',0,2601,421,7021,1485153471,1,7022),(313,41,63,0,11502,'description',0,2602,422,7022,1485153471,1,2601),(313,41,63,0,11503,'description',0,7023,423,2601,1485153471,1,2602),(313,41,63,0,11504,'description',0,7024,424,2602,1485153471,1,7023),(313,41,63,0,11505,'description',0,7025,425,7023,1485153471,1,7024),(313,41,63,0,11506,'description',0,7026,426,7024,1485153471,1,7025),(313,41,63,0,11507,'description',0,7027,427,7025,1485153471,1,7026),(313,41,63,0,11508,'description',0,7028,428,7026,1485153471,1,7027),(313,41,63,0,11509,'description',0,7029,429,7027,1485153471,1,7028),(313,41,63,0,11510,'description',0,7030,430,7028,1485153471,1,7029),(313,41,63,0,11511,'description',0,7031,431,7029,1485153471,1,7030),(313,41,63,0,11512,'description',0,7032,432,7030,1485153471,1,7031),(313,41,63,0,11513,'description',0,7033,433,7031,1485153471,1,7032),(313,41,63,0,11514,'description',0,7034,434,7032,1485153471,1,7033),(313,41,63,0,11515,'description',0,7035,435,7033,1485153471,1,7034),(313,41,63,0,11516,'description',0,7036,436,7034,1485153471,1,7035),(313,41,63,0,11517,'description',0,7037,437,7035,1485153471,1,7036),(313,41,63,0,11518,'description',0,1892,438,7036,1485153471,1,7037),(313,41,63,0,11519,'description',0,7038,439,7037,1485153471,1,1892),(313,41,63,0,11520,'description',0,7039,440,1892,1485153471,1,7038),(313,41,63,0,11521,'description',0,7040,441,7038,1485153471,1,7039),(313,41,63,0,11522,'description',0,7041,442,7039,1485153471,1,7040),(313,41,63,0,11523,'description',0,7042,443,7040,1485153471,1,7041),(313,41,63,0,11524,'description',0,7043,444,7041,1485153471,1,7042),(313,41,63,0,11525,'description',0,7044,445,7042,1485153471,1,7043),(313,41,63,0,11526,'description',0,7045,446,7043,1485153471,1,7044),(313,41,63,0,11527,'description',0,7046,447,7044,1485153471,1,7045),(313,41,63,0,11528,'description',0,7047,448,7045,1485153471,1,7046),(313,41,63,0,11529,'description',0,7048,449,7046,1485153471,1,7047),(313,41,63,0,11530,'description',0,7049,450,7047,1485153471,1,7048),(313,41,63,0,11531,'description',0,7050,451,7048,1485153471,1,7049),(313,41,63,0,11532,'description',0,7051,452,7049,1485153471,1,7050),(313,41,63,0,11533,'description',0,7052,453,7050,1485153471,1,7051),(313,41,63,0,11534,'description',0,2633,454,7051,1485153471,1,7052),(313,41,63,0,11535,'description',0,7053,455,7052,1485153471,1,2633),(313,41,63,0,11536,'description',0,7054,456,2633,1485153471,1,7053),(313,41,63,0,11537,'description',0,7055,457,7053,1485153471,1,7054),(313,41,63,0,11538,'description',0,7056,458,7054,1485153471,1,7055),(313,41,63,0,11539,'description',0,7057,459,7055,1485153471,1,7056),(313,41,63,0,11540,'description',0,7058,460,7056,1485153471,1,7057),(313,41,63,0,11541,'description',0,7059,461,7057,1485153471,1,7058),(313,41,63,0,11542,'description',0,7060,462,7058,1485153471,1,7059),(313,41,63,0,11543,'description',0,7061,463,7059,1485153471,1,7060),(313,41,63,0,11544,'description',0,7062,464,7060,1485153471,1,7061),(313,41,63,0,11545,'description',0,7063,465,7061,1485153471,1,7062),(313,41,63,0,11546,'description',0,7064,466,7062,1485153471,1,7063),(313,41,63,0,11547,'description',0,7065,467,7063,1485153471,1,7064),(313,41,63,0,11548,'description',0,2647,468,7064,1485153471,1,7065),(313,41,63,0,11549,'description',0,7066,469,7065,1485153471,1,2647),(313,41,63,0,11550,'description',0,7067,470,2647,1485153471,1,7066),(313,41,63,0,11551,'description',0,7068,471,7066,1485153471,1,7067),(313,41,63,0,11552,'description',0,7069,472,7067,1485153471,1,7068),(313,41,63,0,11553,'description',0,7070,473,7068,1485153471,1,7069),(313,41,63,0,11554,'description',0,7071,474,7069,1485153471,1,7070),(313,41,63,0,11555,'description',0,7072,475,7070,1485153471,1,7071),(313,41,63,0,11556,'description',0,7073,476,7071,1485153471,1,7072),(313,41,63,0,11557,'description',0,7074,477,7072,1485153471,1,7073),(313,41,63,0,11558,'description',0,1905,478,7073,1485153471,1,7074),(313,41,63,0,11559,'description',0,7075,479,7074,1485153471,1,1905),(313,41,63,0,11560,'description',0,7076,480,1905,1485153471,1,7075),(313,41,63,0,11561,'description',0,7077,481,7075,1485153471,1,7076),(313,41,63,0,11562,'description',0,7078,482,7076,1485153471,1,7077),(313,41,63,0,11563,'description',0,7079,483,7077,1485153471,1,7078),(313,41,63,0,11564,'description',0,2662,484,7078,1485153471,1,7079),(313,41,63,0,11565,'description',0,7080,485,7079,1485153471,1,2662),(313,41,63,0,11566,'description',0,7081,486,2662,1485153471,1,7080),(313,41,63,0,11567,'description',0,7082,487,7080,1485153471,1,7081),(313,41,63,0,11568,'description',0,7083,488,7081,1485153471,1,7082),(313,41,63,0,11569,'description',0,7084,489,7082,1485153471,1,7083),(313,41,63,0,11570,'description',0,7085,490,7083,1485153471,1,7084),(313,41,63,0,11571,'description',0,7086,491,7084,1485153471,1,7085),(313,41,63,0,11572,'description',0,7087,492,7085,1485153471,1,7086),(313,41,63,0,11573,'description',0,7088,493,7086,1485153471,1,7087),(313,41,63,0,11574,'description',0,2672,494,7087,1485153471,1,7088),(313,41,63,0,11575,'description',0,7089,495,7088,1485153471,1,2672),(313,41,63,0,11576,'description',0,7090,496,2672,1485153471,1,7089),(313,41,63,0,11577,'description',0,7091,497,7089,1485153471,1,7090),(313,41,63,0,11578,'description',0,7092,498,7090,1485153471,1,7091),(313,41,63,0,11579,'description',0,7093,499,7091,1485153471,1,7092),(313,41,63,0,11580,'description',0,7094,500,7092,1485153471,1,7093),(313,41,63,0,11581,'description',0,7095,501,7093,1485153471,1,7094),(313,41,63,0,11582,'description',0,7096,502,7094,1485153471,1,7095),(313,41,63,0,11583,'description',0,7097,503,7095,1485153471,1,7096),(313,41,63,0,11584,'description',0,7098,504,7096,1485153471,1,7097),(313,41,63,0,11585,'description',0,2683,505,7097,1485153471,1,7098),(313,41,63,0,11586,'description',0,7099,506,7098,1485153471,1,2683),(313,41,63,0,11587,'description',0,7100,507,2683,1485153471,1,7099),(313,41,63,0,11588,'description',0,7101,508,7099,1485153471,1,7100),(313,41,63,0,11589,'description',0,7102,509,7100,1485153471,1,7101),(313,41,63,0,11590,'description',0,7103,510,7101,1485153471,1,7102),(313,41,63,0,11591,'description',0,7104,511,7102,1485153471,1,7103),(313,41,63,0,11592,'description',0,7105,512,7103,1485153471,1,7104),(313,41,63,0,11593,'description',0,7106,513,7104,1485153471,1,7105),(313,41,63,0,11594,'description',0,2420,514,7105,1485153471,1,7106),(313,41,63,0,11595,'description',0,2185,515,7106,1485153471,1,2420),(315,41,63,0,11596,'product_number',0,7107,516,2420,1485153471,1,2185),(315,41,63,0,11597,'product_number',0,2185,517,2185,1485153471,1,7107),(315,41,63,0,11598,'product_number',0,7107,518,7107,1485153471,1,2185),(315,41,63,0,11599,'product_number',0,1025,519,2185,1485153471,1,7107),(316,41,63,0,11600,'',0,1171,520,7107,1485153471,1,1025),(316,41,63,0,11601,'',0,1052,521,1025,1485153471,1,1171),(316,41,63,0,11602,'',0,1025,522,1171,1485153471,1,1052),(316,41,63,0,11603,'',0,1171,523,1052,1485153471,1,1025),(316,41,63,0,11604,'',0,1040,524,1025,1485153471,1,1171),(316,41,63,0,11605,'',0,1025,525,1171,1485153471,1,1040),(316,41,63,0,11606,'',0,1171,526,1040,1485153471,1,1025),(316,41,63,0,11607,'',0,1172,527,1025,1485153471,1,1171),(316,41,63,0,11608,'',0,0,528,1171,1485153471,1,1172),(6,3,127,0,11619,'name',0,959,0,0,1486995248,2,7114),(6,3,127,0,11620,'name',0,0,1,7114,1486995248,2,959),(6,3,128,0,11621,'name',0,0,0,0,1486995248,2,7115),(6,3,129,0,11622,'name',0,0,0,0,1486995248,2,7116),(6,3,130,0,11623,'name',0,0,0,0,1486995248,2,6158),(6,3,133,0,11624,'name',0,930,0,0,1486996262,2,7117),(6,3,133,0,11625,'name',0,0,1,7117,1486996262,2,930),(8,4,135,0,11626,'first_name',0,1017,0,0,1486997214,2,7118),(9,4,135,0,11627,'last_name',0,7118,1,7118,1486997214,2,1017),(12,4,135,0,11628,'user_account',0,7118,2,1017,1486997214,2,7118),(12,4,135,0,11629,'user_account',0,1019,3,7118,1486997214,2,7118),(12,4,135,0,11630,'user_account',0,0,4,7118,1486997214,2,1019);
/*!40000 ALTER TABLE `ezsearch_object_word_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_search_phrase`
--

DROP TABLE IF EXISTS `ezsearch_search_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_search_phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` varchar(250) DEFAULT NULL,
  `phrase_count` int(11) DEFAULT '0',
  `result_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ezsearch_search_phrase_phrase` (`phrase`),
  KEY `ezsearch_search_phrase_count` (`phrase_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_search_phrase`
--

LOCK TABLES `ezsearch_search_phrase` WRITE;
/*!40000 ALTER TABLE `ezsearch_search_phrase` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsearch_search_phrase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsearch_word`
--

DROP TABLE IF EXISTS `ezsearch_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsearch_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_count` int(11) NOT NULL DEFAULT '0',
  `word` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezsearch_word_obj_count` (`object_count`),
  KEY `ezsearch_word_word_i` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=7119 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsearch_word`
--

LOCK TABLES `ezsearch_word` WRITE;
/*!40000 ALTER TABLE `ezsearch_word` DISABLE KEYS */;
INSERT INTO `ezsearch_word` VALUES (812,1,'setup'),(814,3,'the'),(816,3,'for'),(877,1,'common'),(927,1,'ez.no'),(930,4,'users'),(951,1,'main'),(952,2,'group'),(953,2,'anonymous'),(954,3,'user'),(955,1,'nospam'),(958,2,'administrator'),(959,1,'editors'),(961,1,'media'),(962,1,'images'),(963,1,'files'),(964,1,'multimedia'),(965,1,'ini'),(966,1,'settings'),(967,1,'sitestyle_identifier'),(968,1,'design'),(1017,2,'admin'),(1018,1,'foo'),(1019,2,'example.com'),(1025,24,'product'),(1026,3,'category'),(1032,8,'qwej'),(1033,8,'hqwkjehkjqwhe'),(1034,8,'kjqwehk'),(1035,8,'qhwekjhqjwkejhq'),(1036,8,'wkjhekjqwhekj'),(1040,19,'2'),(1041,1,'askdljalskd'),(1042,1,'jalksdjlkasdasd'),(1052,14,'1'),(1053,10,'製品'),(1054,3,'品カ'),(1055,3,'カテ'),(1056,3,'テゴ'),(1057,3,'ゴリ'),(1058,3,'リ'),(1059,1,'asdh'),(1060,1,'kjhaskdjhakjsdhajksdhjkaksdhakj'),(1061,1,'haskjdhsad'),(1062,4,'こん'),(1063,1,'んに'),(1064,1,'にち'),(1065,1,'ちは'),(1066,1,'は。'),(1070,2,'products'),(1171,7,'photo'),(1172,9,'3'),(1174,1,'all'),(1175,1,'asd'),(1176,2,'subcategory'),(1177,1,'home'),(1179,1,'サイ'),(1180,1,'イト'),(1181,1,'トの'),(1182,1,'のト'),(1183,1,'トッ'),(1184,1,'ップ'),(1185,1,'プペ'),(1186,1,'ペー'),(1187,1,'ージ'),(1188,1,'すべ'),(1189,1,'べて'),(1190,1,'ての'),(1191,4,'の製'),(1688,1,'日本'),(1689,1,'本の'),(1690,1,'の唯'),(1691,1,'唯一'),(1692,1,'一の'),(1693,1,'球ヱ'),(1694,1,'ヱ試'),(1695,1,'試'),(1696,8,'4'),(1697,1,'教図'),(1698,1,'図ケ'),(1699,1,'ケ済'),(1700,1,'済袋'),(1701,1,'袋ヌ'),(1702,1,'ヌ当'),(1703,1,'当利'),(1704,1,'利づ'),(1705,1,'づさ'),(1706,1,'さぞ'),(1707,1,'ぞ朗'),(1708,1,'朗野'),(1709,1,'野ニ'),(1710,2,'ニセ'),(1711,1,'セヨ'),(1712,1,'ヨ草'),(1713,1,'草述'),(1714,1,'述わ'),(1715,1,'わぎ'),(1716,1,'ぎつ'),(1717,1,'つく'),(1718,1,'く利'),(1719,1,'利固'),(1720,1,'固ス'),(1721,1,'スレ'),(1722,1,'レカ'),(1723,1,'カロ'),(1724,1,'ロ帯'),(1725,1,'帯'),(1726,6,'5'),(1727,1,'幌む'),(1728,7,'むぴ'),(1729,1,'ぴ浪'),(1730,1,'浪技'),(1731,1,'技ぐ'),(1732,1,'ぐず'),(1733,1,'ずぶ'),(1734,1,'ぶさ'),(1735,1,'さ少'),(1736,1,'少改'),(1737,1,'改よ'),(1738,1,'よッ'),(1739,1,'ッ航'),(1740,1,'航報'),(1741,1,'報ろ'),(1742,1,'ろび'),(1743,1,'び軽'),(1744,1,'軽棋'),(1745,1,'棋ミ'),(1746,4,'ミチ'),(1747,1,'チ傑'),(1748,1,'傑著'),(1749,1,'著ム'),(1750,1,'ム前'),(1751,1,'前告'),(1752,1,'告や'),(1753,1,'やに'),(1754,1,'に黒'),(1755,1,'黒対'),(1756,1,'対く'),(1757,1,'くづ'),(1758,1,'づち'),(1759,1,'ち海'),(1760,1,'海参'),(1761,1,'参せ'),(1762,1,'せみ'),(1763,1,'み目'),(1764,1,'目音'),(1765,1,'音ゅ'),(1766,3,'ゅ。'),(1767,1,'。鼓'),(1768,1,'鼓イ'),(1769,1,'イミ'),(1770,1,'ミ日'),(1771,1,'日'),(1772,8,'13'),(1773,1,'新そ'),(1774,1,'そフ'),(1775,1,'フ際'),(1776,1,'際撤'),(1777,1,'撤ト'),(1778,1,'トへ'),(1779,1,'へも'),(1780,1,'もぼ'),(1781,1,'ぼ一'),(1782,1,'一無'),(1783,1,'無ウ'),(1784,3,'ウシ'),(1785,1,'シレ'),(1786,1,'レラ'),(1787,1,'ラ発'),(1788,1,'発置'),(1789,1,'置や'),(1790,1,'や条'),(1791,1,'条北'),(1792,1,'北ニ'),(1793,1,'ニチ'),(1794,1,'チヨ'),(1795,1,'ヨ公'),(1796,1,'公印'),(1797,1,'印ゆ'),(1798,1,'ゆき'),(1799,1,'きぶ'),(1800,1,'ぶ述'),(1801,1,'述透'),(1802,1,'透ょ'),(1803,1,'ょゆ'),(1804,1,'ゆづ'),(1805,3,'づた'),(1806,1,'た闘'),(1807,1,'闘能'),(1808,1,'能ヌ'),(1809,1,'ヌエ'),(1810,1,'エ数'),(1811,1,'数政'),(1812,1,'政キ'),(1813,1,'キチ'),(1814,1,'チテ'),(1815,1,'テサ'),(1816,1,'サ付'),(1817,1,'付援'),(1818,1,'援和'),(1819,1,'和府'),(1820,1,'府ウ'),(1821,1,'ウナ'),(1822,3,'ナト'),(1823,1,'ト治'),(1824,1,'治九'),(1825,1,'九聞'),(1826,1,'聞び'),(1827,7,'びぞ'),(1828,1,'ぞも'),(1829,1,'もせ'),(1830,1,'せ表'),(1831,1,'表定'),(1832,1,'定れ'),(1833,1,'れイ'),(1834,3,'イー'),(1835,1,'ー策'),(1836,1,'策僕'),(1837,1,'僕が'),(1838,1,'がゆ'),(1839,3,'ゆ。'),(1840,16,'。'),(1841,1,'葉観'),(1842,1,'観サ'),(1843,1,'サ属'),(1844,1,'属主'),(1845,1,'主通'),(1846,1,'通ロ'),(1847,1,'ロ鳥'),(1848,1,'鳥覧'),(1849,1,'覧ン'),(1850,1,'ンあ'),(1851,1,'あぜ'),(1852,1,'ぜて'),(1853,1,'て市'),(1854,1,'市報'),(1855,1,'報ょ'),(1856,1,'ょを'),(1857,1,'をス'),(1858,1,'ス点'),(1859,1,'点一'),(1860,1,'一ぱ'),(1861,1,'ぱぶ'),(1862,1,'ぶみ'),(1863,1,'み居'),(1864,1,'居稿'),(1865,1,'稿わ'),(1866,1,'わ間'),(1867,1,'間務'),(1868,1,'務ン'),(1869,1,'ン銭'),(1870,1,'銭大'),(1871,1,'大ぱ'),(1872,1,'ぱド'),(1873,1,'ド条'),(1874,1,'条長'),(1875,1,'長ネ'),(1876,1,'ネ所'),(1877,1,'所誕'),(1878,1,'誕暫'),(1879,1,'暫械'),(1880,1,'械ラ'),(1881,1,'ラづ'),(1882,3,'づき'),(1883,3,'きぴ'),(1884,3,'ぴ。'),(1885,1,'。覧'),(1886,1,'覧キ'),(1887,1,'キカ'),(1888,1,'カ牛'),(1889,1,'牛紀'),(1890,1,'紀ぴ'),(1891,1,'ぴつ'),(1892,7,'つほ'),(1893,1,'ほげ'),(1894,1,'げ室'),(1895,1,'室護'),(1896,1,'護ワ'),(1897,1,'ワナ'),(1898,1,'ナ辞'),(1899,1,'辞神'),(1900,1,'神ラ'),(1901,1,'ラ停'),(1902,1,'停労'),(1903,1,'労輸'),(1904,1,'輸ウ'),(1905,7,'ウロ'),(1906,1,'ロヌ'),(1907,1,'ヌリ'),(1908,1,'リ爆'),(1909,1,'爆'),(1910,1,'鮮着'),(1911,1,'着ぐ'),(1912,1,'ぐ感'),(1913,1,'感'),(1914,1,'奈オ'),(1915,1,'オレ'),(1916,1,'レ困'),(1917,1,'困史'),(1918,1,'史り'),(1919,1,'り町'),(1920,1,'町'),(1921,1,'16'),(1922,1,'意ヘ'),(1923,1,'ヘク'),(1924,1,'ク事'),(1925,1,'事慶'),(1926,1,'慶い'),(1927,1,'いく'),(1928,1,'くょ'),(1929,1,'ょほ'),(1930,1,'ほ。'),(1931,1,'。防'),(1932,1,'防で'),(1933,1,'でん'),(1934,1,'んざ'),(1935,1,'ざひ'),(1936,1,'ひ託'),(1937,1,'託税'),(1938,1,'税済'),(1939,1,'済す'),(1940,1,'すむ'),(1941,1,'むざ'),(1942,1,'ざほ'),(1943,1,'ほ経'),(1944,1,'経切'),(1945,1,'切あ'),(1946,7,'あず'),(1947,1,'ずこ'),(1948,1,'こ内'),(1949,1,'内説'),(1950,1,'説発'),(1951,1,'発ヤ'),(1952,3,'ヤヨ'),(1953,1,'ヨチ'),(1954,1,'チ齢'),(1955,1,'齢地'),(1956,1,'地ド'),(1957,1,'ドな'),(1958,1,'なげ'),(1959,1,'げえ'),(1960,1,'え型'),(1961,1,'型情'),(1962,1,'情ヱ'),(1963,1,'ヱス'),(1964,1,'スヌ'),(1965,3,'ヌメ'),(1966,1,'メ方'),(1967,1,'方要'),(1968,1,'要よ'),(1969,1,'よね'),(1970,1,'ねル'),(1971,1,'ルク'),(1972,1,'ク的'),(1973,1,'的対'),(1974,1,'対'),(1975,1,'新カ'),(1976,1,'カ衆'),(1977,1,'衆'),(1978,1,'82'),(1979,1,'朴さ'),(1980,1,'さぶ'),(1981,1,'ぶド'),(1982,1,'ドみ'),(1983,1,'み真'),(1984,1,'真株'),(1985,1,'株ハ'),(1986,3,'ハネ'),(1987,1,'ネア'),(1988,1,'ア発'),(1989,1,'発弟'),(1990,1,'弟ヨ'),(1991,1,'ヨフ'),(1992,1,'フ百'),(1993,1,'百代'),(1994,1,'代ク'),(1995,3,'クト'),(1996,1,'へ木'),(1997,1,'木世'),(1998,1,'世ず'),(1999,1,'ず読'),(2000,1,'読記'),(2001,1,'記う'),(2002,1,'うぴ'),(2003,1,'ぴ巻'),(2004,1,'巻民'),(2005,1,'民皇'),(2006,1,'皇あ'),(2007,1,'あぱ'),(2008,7,'ぱ。'),(2009,1,'判レ'),(2010,1,'レせ'),(2011,1,'せッ'),(2012,1,'ッ内'),(2013,1,'内教'),(2014,1,'教ヨ'),(2015,1,'ヨ航'),(2016,1,'航将'),(2017,1,'将の'),(2018,1,'のゃ'),(2019,1,'ゃん'),(2020,1,'ん網'),(2021,1,'網透'),(2022,1,'透オ'),(2023,1,'オ末'),(2024,1,'末元'),(2025,1,'元べ'),(2026,1,'べ歳'),(2027,1,'歳企'),(2028,1,'企ヌ'),(2029,4,'ヌ際'),(2030,1,'際少'),(2031,1,'少題'),(2032,1,'題ゃ'),(2033,1,'ゃろ'),(2034,1,'ろま'),(2035,1,'ま守'),(2036,1,'守紛'),(2037,1,'紛カ'),(2038,3,'カミ'),(2039,1,'ミヘ'),(2040,1,'ヘ堅'),(2041,1,'堅死'),(2042,1,'死ナ'),(2043,1,'ナヨ'),(2044,1,'ヨラ'),(2045,1,'ラ鎖'),(2046,1,'鎖多'),(2047,1,'多北'),(2048,1,'北関'),(2049,1,'関め'),(2050,1,'め第'),(2051,1,'第仲'),(2052,1,'仲倫'),(2053,1,'倫剖'),(2054,1,'剖卵'),(2055,1,'卵ぽ'),(2056,1,'ぽレ'),(2057,1,'レド'),(2058,3,'ド。'),(2059,1,'済ロ'),(2060,1,'ロタ'),(2061,1,'タサ'),(2062,1,'サホ'),(2063,1,'ホ豊'),(2064,1,'豊'),(2065,1,'向ド'),(2066,1,'ドざ'),(2067,1,'ざぴ'),(2068,1,'ぴ紀'),(2069,1,'紀促'),(2070,1,'促利'),(2071,1,'利け'),(2072,1,'けた'),(2073,1,'たレ'),(2074,1,'レ献'),(2075,1,'献厩'),(2076,1,'厩授'),(2077,1,'授ツ'),(2078,4,'ツ見'),(2079,1,'見公'),(2080,1,'公イ'),(2081,1,'イコ'),(2082,1,'コセ'),(2083,1,'セヘ'),(2084,1,'ヘ洋'),(2085,1,'洋稿'),(2086,1,'稿ト'),(2087,1,'トエ'),(2088,1,'エユ'),(2089,1,'ユツ'),(2090,1,'ツ交'),(2091,1,'交特'),(2092,1,'特リ'),(2093,1,'リが'),(2094,1,'がめ'),(2095,1,'める'),(2096,1,'る容'),(2097,1,'容時'),(2098,1,'時め'),(2099,1,'めな'),(2100,1,'なん'),(2101,1,'ん理'),(2102,1,'理番'),(2103,1,'番ヤ'),(2104,1,'ヤス'),(2105,1,'スカ'),(2106,1,'カ空'),(2107,1,'空俳'),(2108,3,'俳寺'),(2109,1,'寺豊'),(2110,1,'豊打'),(2111,1,'打ン'),(2112,1,'ン。'),(2113,1,'。気'),(2114,1,'気な'),(2115,1,'なの'),(2116,1,'ゃ言'),(2117,1,'言断'),(2118,1,'断け'),(2119,1,'けル'),(2120,1,'ルま'),(2121,1,'ま数'),(2122,1,'数済'),(2123,1,'済ウ'),(2124,1,'ウネ'),(2125,1,'ネ経'),(2126,1,'経政'),(2127,1,'政そ'),(2128,1,'そぞ'),(2129,1,'ぞー'),(2130,1,'ーフ'),(2131,1,'フ季'),(2132,1,'季主'),(2133,1,'主ほ'),(2134,1,'ほル'),(2135,1,'ル上'),(2136,1,'上院'),(2137,1,'院れ'),(2138,1,'れッ'),(2139,1,'ッ票'),(2140,1,'票'),(2141,1,'教く'),(2142,1,'くわ'),(2143,1,'わ報'),(2144,1,'報西'),(2145,1,'西こ'),(2146,1,'ん祝'),(2147,1,'祝歳'),(2148,1,'歳比'),(2149,1,'比ー'),(2150,1,'ーっ'),(2151,1,'っド'),(2152,1,'ドぎ'),(2153,1,'ぎ本'),(2154,1,'本'),(2155,1,'62'),(2156,1,'座ハ'),(2157,1,'ハイ'),(2158,1,'イリ'),(2159,1,'リ触'),(2160,1,'触目'),(2161,1,'目ば'),(2162,1,'ばむ'),(2163,1,'むし'),(2164,1,'し験'),(2165,1,'験釧'),(2166,1,'釧ホ'),(2167,1,'ホ紹'),(2168,1,'紹券'),(2169,1,'券集'),(2170,1,'集リ'),(2171,1,'リメ'),(2172,1,'メイ'),(2173,1,'イ教'),(2174,1,'教動'),(2175,1,'動乱'),(2176,1,'乱皇'),(2177,1,'皇さ'),(2178,1,'さや'),(2179,1,'やぜ'),(2180,1,'ぜこ'),(2181,1,'こ。'),(2182,1,'jp'),(2183,1,'271'),(2185,2,'pr'),(2191,11,'9'),(2197,1,'two'),(2198,1,'xy'),(2199,1,'827'),(2201,8,'商品'),(2202,10,'品'),(2304,7,'業理'),(2321,8,'オセ'),(2322,6,'セム'),(2356,8,'サミ'),(2365,14,'6'),(2384,7,'イノ'),(2396,7,'ムメ'),(2412,8,'ユマ'),(2420,7,'の。'),(2478,8,'るね'),(2488,7,'フや'),(2499,7,'けざ'),(2511,6,'ヲト'),(2523,8,'き。'),(2525,6,'っち'),(2601,7,'コツ'),(2602,7,'ツメ'),(2633,7,'ウレ'),(2647,8,'ワシ'),(2662,10,'をめ'),(2672,7,'ルヱ'),(2683,3,'ハチ'),(2732,3,'フ。'),(2743,4,'りあ'),(2782,4,'ヨヌ'),(2879,4,'ず。'),(2924,4,'びき'),(2959,4,'51'),(3036,4,'ウヌ'),(3059,4,'エル'),(3060,4,'ルナ'),(3065,4,'ルヲ'),(3084,4,'セク'),(3125,4,'応'),(3127,4,'ゆイ'),(3139,4,'ソキ'),(3184,3,'け。'),(3186,2,'hello'),(3187,1,'プロ'),(3188,1,'ロダ'),(3189,1,'ダク'),(3190,1,'トハ'),(3191,1,'ハロ'),(3192,1,'ロー'),(3193,1,'ー'),(3194,1,'lkasjdlkjasdlkj'),(3195,1,'富イ'),(3196,1,'ーえ'),(3197,1,'え物'),(3198,1,'物透'),(3199,1,'透ホ'),(3200,1,'ホ割'),(3201,1,'割小'),(3202,1,'小折'),(3203,1,'折ん'),(3204,1,'ん小'),(3205,1,'小中'),(3206,1,'中エ'),(3207,1,'エメ'),(3208,1,'メ読'),(3209,1,'読'),(3210,1,'39'),(3211,1,'約か'),(3212,1,'かぞ'),(3213,1,'ぞ見'),(3214,1,'見準'),(3215,1,'準読'),(3216,1,'読レ'),(3217,3,'レぱ'),(3218,1,'ぱさ'),(3219,1,'さぎ'),(3220,1,'ぎ付'),(3221,1,'付旦'),(3222,1,'旦エ'),(3223,1,'エマ'),(3224,1,'マ件'),(3225,1,'件福'),(3226,1,'福偽'),(3227,1,'偽サ'),(3228,1,'サカ'),(3229,1,'カ活'),(3230,1,'活買'),(3231,1,'買指'),(3232,1,'指亡'),(3233,1,'亡好'),(3234,1,'好ゅ'),(3235,1,'。画'),(3236,1,'画す'),(3237,1,'す議'),(3238,1,'議目'),(3239,1,'目ソ'),(3240,1,'ソサ'),(3241,1,'サヲ'),(3242,1,'ヲヱ'),(3243,1,'ヱ新'),(3244,1,'新全'),(3245,1,'全ヘ'),(3246,1,'ヘケ'),(3247,1,'ケ泥'),(3248,1,'泥埼'),(3249,1,'埼ん'),(3250,1,'んよ'),(3251,1,'よン'),(3252,1,'ンろ'),(3253,1,'ろ棋'),(3254,1,'棋支'),(3255,1,'支お'),(3256,1,'お務'),(3257,1,'務'),(3258,1,'時ー'),(3259,1,'ー所'),(3260,1,'所硬'),(3261,1,'硬ナ'),(3262,3,'ナツ'),(3263,1,'ツユ'),(3264,1,'ユイ'),(3265,1,'イ止'),(3266,1,'止当'),(3267,1,'当ド'),(3268,1,'ドご'),(3269,1,'ごク'),(3270,1,'クて'),(3271,1,'て表'),(3272,1,'表関'),(3273,1,'関前'),(3274,1,'前'),(3275,1,'稿ん'),(3276,1,'んや'),(3277,1,'や査'),(3278,1,'査県'),(3279,1,'県レ'),(3280,3,'レけ'),(3281,1,'けむ'),(3282,1,'む線'),(3283,1,'線界'),(3284,1,'界よ'),(3285,3,'よつ'),(3286,1,'つ球'),(3287,1,'球見'),(3288,1,'見く'),(3289,1,'くへ'),(3290,1,'へ資'),(3291,1,'資色'),(3292,1,'色ひ'),(3293,1,'ひ。'),(3294,1,'。記'),(3295,1,'記ウ'),(3296,1,'レ済'),(3297,1,'済襲'),(3298,1,'襲両'),(3299,1,'両テ'),(3300,1,'テ表'),(3301,1,'表老'),(3302,1,'老び'),(3303,1,'びぼ'),(3304,1,'ぼ子'),(3305,1,'子'),(3306,1,'離ゆ'),(3307,1,'イぎ'),(3308,1,'ぎ古'),(3309,1,'古文'),(3310,1,'文ぼ'),(3311,1,'ぼで'),(3312,1,'でが'),(3313,1,'がリ'),(3314,1,'リ性'),(3315,1,'性裁'),(3316,1,'裁ル'),(3317,1,'ナヘ'),(3318,1,'ヘト'),(3319,1,'ト投'),(3320,1,'投湯'),(3321,1,'湯ヘ'),(3322,1,'ヘキ'),(3323,1,'キエ'),(3324,1,'エミ'),(3325,1,'ミ眠'),(3326,1,'眠参'),(3327,1,'参ヌ'),(3328,1,'ヌク'),(3329,1,'ク能'),(3330,1,'能院'),(3331,1,'院備'),(3332,1,'備ツ'),(3333,1,'ツノ'),(3334,1,'ノ親'),(3335,1,'親告'),(3336,1,'告ゆ'),(3337,1,'ゆ手'),(3338,1,'手子'),(3339,1,'85'),(3340,1,'書カ'),(3341,1,'カ初'),(3342,1,'初報'),(3343,1,'報ム'),(3344,1,'ムル'),(3345,1,'ヲ鉢'),(3346,1,'鉢尚'),(3347,1,'尚い'),(3348,1,'い。'),(3349,1,'吉ム'),(3350,1,'メル'),(3351,1,'ルネ'),(3352,1,'ネ賞'),(3353,1,'賞滞'),(3354,1,'滞ど'),(3355,1,'どい'),(3356,1,'いも'),(3357,1,'も制'),(3358,1,'制高'),(3359,1,'高エ'),(3360,1,'エソ'),(3361,1,'ソ弘'),(3362,1,'弘秘'),(3363,1,'秘ホ'),(3364,1,'ホス'),(3365,1,'スヨ'),(3366,1,'ヨノ'),(3367,1,'ノ否'),(3368,1,'否数'),(3369,1,'数と'),(3370,1,'とあ'),(3371,1,'あも'),(3372,1,'も兵'),(3373,1,'兵申'),(3374,1,'申イ'),(3375,3,'ノヱ'),(3376,1,'ヱ商'),(3377,1,'商日'),(3378,1,'日う'),(3379,1,'うぱ'),(3380,1,'ぱ知'),(3381,1,'知臓'),(3382,1,'臓テ'),(3383,1,'テヤ'),(3384,1,'ヨソ'),(3385,1,'ソ覚'),(3386,1,'覚富'),(3387,1,'富ケ'),(3388,1,'ケト'),(3389,1,'ト山'),(3390,1,'山投'),(3391,1,'投ら'),(3392,1,'ら疎'),(3393,1,'疎点'),(3394,1,'点広'),(3395,1,'広め'),(3396,1,'めお'),(3397,1,'おい'),(3398,1,'い約'),(3399,1,'約十'),(3400,1,'十ホ'),(3401,1,'ホテ'),(3402,1,'テチ'),(3403,1,'チク'),(3404,1,'ク知'),(3405,1,'知'),(3406,1,'25'),(3407,1,'更べ'),(3408,1,'べず'),(3409,1,'ず東'),(3410,3,'東'),(3411,3,'7'),(3412,1,'談リ'),(3413,1,'リ員'),(3414,1,'員族'),(3415,1,'族イ'),(3416,1,'イド'),(3417,1,'ドえ'),(3418,1,'えゆ'),(3419,1,'ゆ筋'),(3420,1,'筋保'),(3421,1,'保わ'),(3422,1,'わ絶'),(3423,1,'絶不'),(3424,1,'不挙'),(3425,1,'挙尾'),(3426,1,'尾滋'),(3427,1,'滋ご'),(3428,3,'ご。'),(3429,1,'。家'),(3430,1,'家レ'),(3431,1,'レ団'),(3432,1,'団恵'),(3433,1,'恵メ'),(3434,1,'メ応'),(3435,1,'応指'),(3436,1,'指'),(3437,1,'信ぜ'),(3438,1,'ぜ念'),(3439,1,'念'),(3440,1,'少カ'),(3441,1,'ミ場'),(3442,1,'場相'),(3443,1,'相や'),(3444,1,'や対'),(3445,1,'対相'),(3446,1,'相人'),(3447,1,'人ろ'),(3448,1,'ろの'),(3449,1,'のル'),(3450,1,'ルお'),(3451,1,'お界'),(3452,1,'界罪'),(3453,1,'罪み'),(3454,1,'みま'),(3455,1,'まば'),(3456,1,'ばど'),(3457,1,'ど権'),(3458,1,'権描'),(3459,1,'描フ'),(3460,1,'フ治'),(3461,1,'治羅'),(3462,1,'羅誘'),(3463,1,'誘逮'),(3464,1,'逮ぶ'),(3465,1,'ぶ。'),(3466,1,'。録'),(3467,1,'録メ'),(3468,1,'メク'),(3469,1,'ク用'),(3470,1,'用女'),(3471,1,'女社'),(3472,1,'社ア'),(3473,1,'ア告'),(3474,1,'告聞'),(3475,1,'聞ル'),(3476,1,'ルマ'),(3477,1,'マオ'),(3478,1,'オ氏'),(3479,1,'氏使'),(3480,1,'使版'),(3481,1,'版ま'),(3482,1,'まご'),(3483,1,'ごン'),(3484,1,'ンし'),(3485,1,'し泉'),(3486,1,'泉選'),(3487,1,'選さ'),(3488,1,'さリ'),(3489,1,'リ獣'),(3490,1,'獣愛'),(3491,1,'愛伊'),(3492,1,'伊ぼ'),(3493,1,'ぼそ'),(3494,1,'そっ'),(3495,1,'っあ'),(3496,1,'あ際'),(3497,1,'際棋'),(3498,1,'棋員'),(3499,1,'員ん'),(3500,1,'んぽ'),(3501,1,'ぽ録'),(3502,1,'録転'),(3503,1,'転リ'),(3504,1,'リ取'),(3505,1,'取良'),(3506,1,'良ミ'),(3507,1,'ミ搭'),(3508,1,'搭法'),(3509,1,'法誠'),(3510,1,'誠苦'),(3511,1,'苦省'),(3512,1,'省ゅ'),(3513,2,'53'),(3514,1,'量術'),(3515,1,'術ひ'),(3516,1,'ひち'),(3517,1,'ち挙'),(3518,1,'挙作'),(3519,1,'作ロ'),(3520,1,'ロ前'),(3521,1,'前作'),(3522,1,'作ろ'),(3523,1,'ろ担'),(3524,1,'担合'),(3525,1,'合ク'),(3526,1,'クす'),(3527,1,'すめ'),(3528,1,'め班'),(3529,1,'班他'),(3530,1,'他イ'),(3531,1,'イモ'),(3532,1,'モ棒'),(3533,1,'棒略'),(3534,1,'略へ'),(3535,1,'へぐ'),(3536,1,'ぐ牡'),(3537,1,'牡総'),(3538,1,'総ス'),(3539,1,'スサ'),(3540,1,'ヲ済'),(3541,1,'済平'),(3542,1,'平界'),(3543,1,'界ふ'),(3544,1,'ふ高'),(3545,1,'高昨'),(3546,1,'昨ひ'),(3547,1,'ひ画'),(3548,1,'画'),(3549,1,'月な'),(3550,1,'な住'),(3551,1,'住村'),(3552,1,'村ぎ'),(3553,1,'ぎ信'),(3554,1,'信乾'),(3555,1,'乾凍'),(3556,1,'凍刷'),(3557,1,'刷往'),(3558,1,'往フ'),(3559,1,'フみ'),(3560,1,'みあ'),(3561,2,'あめ'),(3562,1,'め。'),(3563,1,'。速'),(3564,1,'速チ'),(3565,1,'チナ'),(3566,1,'ナケ'),(3567,1,'ケ愛'),(3568,1,'愛残'),(3569,1,'残事'),(3570,1,'事済'),(3571,1,'済ニ'),(3572,3,'ニウ'),(3573,1,'メ小'),(3574,1,'小転'),(3575,1,'転ヨ'),(3576,1,'ヌ剤'),(3577,1,'剤神'),(3578,1,'神ら'),(3579,1,'ら輔'),(3580,1,'輔南'),(3581,1,'南づ'),(3582,1,'づん'),(3583,1,'んゅ'),(3584,1,'ゅと'),(3585,1,'と氏'),(3586,1,'氏克'),(3587,1,'克つ'),(3588,1,'つと'),(3589,1,'と抗'),(3590,1,'抗計'),(3591,1,'計企'),(3592,1,'企セ'),(3593,1,'セヲ'),(3594,1,'ヲム'),(3595,1,'ムロ'),(3596,1,'ロ思'),(3597,1,'思公'),(3598,1,'公重'),(3599,1,'重大'),(3600,1,'大的'),(3601,1,'的俳'),(3602,1,'寺尊'),(3603,1,'尊き'),(3604,1,'きぞ'),(3605,3,'ぞゃ'),(3606,1,'ゃ。'),(3607,1,'。紙'),(3608,1,'紙ヨ'),(3609,1,'ヨ軍'),(3610,1,'軍営'),(3611,1,'営ク'),(3612,1,'クな'),(3613,1,'な観'),(3614,1,'観用'),(3615,1,'用ま'),(3616,1,'まぴ'),(3617,1,'ぴめ'),(3618,1,'めゆ'),(3619,1,'ゆ申'),(3620,1,'申住'),(3621,1,'住み'),(3622,1,'みぴ'),(3623,1,'ぴ暮'),(3624,1,'暮給'),(3625,1,'給ヨ'),(3626,1,'ヨヲ'),(3627,1,'ヲレ'),(3628,1,'レテ'),(3629,1,'テ祝'),(3630,1,'祝画'),(3631,1,'画ナ'),(3632,1,'ナヲ'),(3633,1,'ヲチ'),(3634,1,'チ再'),(3635,1,'再学'),(3636,1,'学ロ'),(3637,1,'ロウ'),(3638,1,'シツ'),(3639,1,'ツ造'),(3640,1,'造定'),(3641,1,'定'),(3642,1,'一す'),(3643,1,'すづ'),(3644,1,'きさ'),(3645,1,'さ終'),(3646,1,'終母'),(3647,1,'母ド'),(3648,1,'ド近'),(3649,1,'近比'),(3650,1,'比ト'),(3651,1,'ト活'),(3652,1,'活旅'),(3653,1,'旅は'),(3654,1,'はじ'),(3655,1,'じ市'),(3656,1,'市壮'),(3657,1,'壮フ'),(3658,1,'フそ'),(3659,1,'そけ'),(3660,1,'け果'),(3661,1,'果保'),(3662,1,'保ソ'),(3663,1,'ソヱ'),(3664,1,'ヱウ'),(3665,1,'ウイ'),(3666,1,'イ談'),(3667,1,'談新'),(3668,1,'新キ'),(3669,1,'キ保'),(3670,1,'保'),(3671,1,'戸リ'),(3672,1,'リ皆'),(3673,1,'皆'),(3674,1,'務英'),(3675,1,'英傍'),(3676,1,'傍や'),(3677,1,'やげ'),(3678,1,'げつ'),(3679,1,'つイ'),(3680,1,'イ。'),(3681,1,'xx'),(3682,1,'837'),(3683,1,'three'),(3684,1,'期誕'),(3685,1,'誕的'),(3686,1,'的ニ'),(3687,1,'ウ界'),(3688,1,'界特'),(3689,1,'特慎'),(3690,1,'慎ミ'),(3691,1,'ミス'),(3692,1,'スコ'),(3693,1,'ツ暮'),(3694,1,'暮'),(3695,1,'広そ'),(3696,1,'そひ'),(3697,1,'ひ丁'),(3698,1,'丁応'),(3699,1,'応裏'),(3700,1,'裏ケ'),(3701,1,'ケネ'),(3702,1,'ネ左'),(3703,1,'左回'),(3704,1,'回イ'),(3705,1,'イ限'),(3706,1,'限間'),(3707,1,'間す'),(3708,1,'す約'),(3709,1,'約主'),(3710,1,'主に'),(3711,1,'にへ'),(3712,1,'へ能'),(3713,1,'能'),(3714,1,'67'),(3715,1,'遺ば'),(3716,1,'ばじ'),(3717,1,'じひ'),(3718,1,'ひ稿'),(3719,1,'稿原'),(3720,1,'原が'),(3721,1,'が愛'),(3722,1,'愛'),(3723,1,'27'),(3724,1,'楽漏'),(3725,1,'漏頂'),(3726,1,'頂て'),(3727,1,'て。'),(3728,1,'。境'),(3729,1,'境テ'),(3730,1,'テレ'),(3731,1,'レ健'),(3732,1,'健無'),(3733,1,'無づ'),(3734,1,'づ葉'),(3735,1,'葉談'),(3736,1,'談は'),(3737,1,'はゃ'),(3738,1,'ゃづ'),(3739,1,'づ先'),(3740,1,'先八'),(3741,1,'八セ'),(3742,1,'ク調'),(3743,1,'調売'),(3744,1,'売で'),(3745,1,'でゅ'),(3746,1,'ゅレ'),(3747,1,'レ四'),(3748,1,'四載'),(3749,1,'載書'),(3750,1,'書ヘ'),(3751,1,'ヘス'),(3752,1,'スエ'),(3753,1,'ル姫'),(3754,1,'姫株'),(3755,1,'株ぱ'),(3756,1,'ぱろ'),(3757,1,'ろめ'),(3758,1,'めづ'),(3759,1,'づ岸'),(3760,1,'岸装'),(3761,1,'装ク'),(3762,1,'クふ'),(3763,1,'ふ中'),(3764,1,'中途'),(3765,1,'途ス'),(3766,1,'ス縦'),(3767,1,'縦督'),(3768,1,'督略'),(3769,1,'略っ'),(3770,1,'っべ'),(3771,1,'べー'),(3772,1,'ーう'),(3773,1,'う吾'),(3774,1,'吾属'),(3775,1,'属よ'),(3776,1,'よそ'),(3777,1,'そリ'),(3778,1,'リど'),(3779,1,'ど初'),(3780,1,'初更'),(3781,1,'更と'),(3782,1,'とせ'),(3783,1,'せあ'),(3784,1,'あび'),(3785,1,'び受'),(3786,1,'受佐'),(3787,1,'佐又'),(3788,1,'又で'),(3789,1,'でみ'),(3790,1,'みわ'),(3791,1,'わ。'),(3792,1,'。極'),(3793,1,'極え'),(3794,1,'え遺'),(3795,1,'遺展'),(3796,1,'展児'),(3797,1,'児ン'),(3798,1,'ンせ'),(3799,1,'せけ'),(3800,1,'け地'),(3801,1,'地'),(3802,1,'好ム'),(3803,1,'ムケ'),(3804,1,'ケル'),(3805,1,'ルシ'),(3806,1,'シ秋'),(3807,1,'秋都'),(3808,1,'都ぐ'),(3809,1,'ぐっ'),(3810,1,'っ研'),(3811,1,'研検'),(3812,1,'検ツ'),(3813,1,'ツス'),(3814,1,'ス韓'),(3815,1,'韓圧'),(3816,1,'圧カ'),(3817,1,'カヌ'),(3818,1,'ヌ断'),(3819,1,'断前'),(3820,1,'前リ'),(3821,1,'リ府'),(3822,1,'府因'),(3823,1,'因'),(3824,1,'司ハ'),(3825,1,'ハコ'),(3826,1,'コチ'),(3827,1,'チ運'),(3828,1,'運査'),(3829,1,'査セ'),(3830,1,'セナ'),(3831,1,'メ域'),(3832,1,'域優'),(3833,1,'優サ'),(3834,1,'サヱ'),(3835,1,'ヱラ'),(3836,1,'ラソ'),(3837,1,'ソ月'),(3838,1,'月斐'),(3839,1,'斐ケ'),(3840,1,'ケ商'),(3841,1,'商下'),(3842,1,'下ト'),(3843,1,'トサ'),(3844,1,'サヤ'),(3845,1,'ヤ終'),(3846,1,'終育'),(3847,1,'育娘'),(3848,1,'娘汁'),(3849,1,'汁班'),(3850,1,'班ょ'),(3851,1,'ょご'),(3852,1,'20'),(3853,1,'妹廷'),(3854,1,'廷訓'),(3855,1,'訓誘'),(3856,1,'誘'),(3857,1,'皇ホ'),(3858,1,'ホフ'),(3859,1,'フ住'),(3860,1,'住會'),(3861,1,'會覧'),(3862,1,'覧雄'),(3863,1,'雄ぎ'),(3864,1,'ぎ費'),(3865,1,'費真'),(3866,1,'真ん'),(3867,1,'んれ'),(3868,1,'れ分'),(3869,1,'分演'),(3870,1,'演イ'),(3871,1,'イ軽'),(3872,1,'軽経'),(3873,1,'経ト'),(3874,1,'ト無'),(3875,1,'無碁'),(3876,1,'碁レ'),(3877,1,'ぱぽ'),(3878,1,'ぽ表'),(3879,1,'表警'),(3880,1,'警ほ'),(3881,1,'ほ強'),(3882,1,'強進'),(3883,1,'進ヨ'),(3884,1,'ヨホ'),(3885,1,'ホヤ'),(3886,1,'ヤ勝'),(3887,1,'勝後'),(3888,1,'後午'),(3889,1,'午鮮'),(3890,1,'鮮果'),(3891,1,'果あ'),(3892,1,'あ。'),(3893,1,'95'),(3894,1,'賞キ'),(3895,1,'キマ'),(3896,1,'マサ'),(3897,1,'サウ'),(3898,1,'ウ地'),(3899,1,'地源'),(3900,1,'源ぼ'),(3901,1,'ぼも'),(3902,1,'もれ'),(3903,1,'れ字'),(3904,1,'字前'),(3905,1,'前ヤ'),(3906,1,'ヤリ'),(3907,1,'リソ'),(3908,1,'ソ作'),(3909,1,'作一'),(3910,1,'一町'),(3911,1,'町え'),(3912,1,'えは'),(3913,1,'はば'),(3914,1,'ばっ'),(3915,1,'っ有'),(3916,1,'有障'),(3917,1,'障レ'),(3918,1,'レ水'),(3919,1,'水打'),(3920,1,'打ぽ'),(3921,1,'ぽ松'),(3922,1,'松場'),(3923,1,'場だ'),(3924,1,'だる'),(3925,1,'るゆ'),(3926,1,'ゆ物'),(3927,1,'物毎'),(3928,1,'毎ル'),(3929,1,'ヱ賞'),(3930,1,'賞半'),(3931,1,'半く'),(3932,1,'くね'),(3933,1,'ね覧'),(3934,1,'覧'),(3935,1,'決ミ'),(3936,1,'ミヌ'),(3937,1,'ヌ年'),(3938,1,'年今'),(3939,1,'今と'),(3940,1,'とざ'),(3941,1,'ざづ'),(3942,1,'づ木'),(3943,1,'木分'),(3944,1,'分ゃ'),(3945,1,'ゃ浩'),(3946,1,'浩択'),(3947,1,'択ど'),(3948,1,'どへ'),(3949,1,'へか'),(3950,1,'かひ'),(3951,1,'ひ応'),(3952,1,'応住'),(3953,1,'住本'),(3954,1,'本族'),(3955,1,'族被'),(3956,1,'被な'),(3957,2,'なき'),(3958,1,'。出'),(3959,1,'出ケ'),(3960,1,'ケ来'),(3961,1,'来断'),(3962,1,'断り'),(3963,1,'りも'),(3964,1,'もば'),(3965,1,'ばく'),(3966,1,'く質'),(3967,1,'質'),(3968,1,'出コ'),(3969,1,'ツレ'),(3970,1,'レア'),(3971,1,'ア高'),(3972,1,'高黒'),(3973,1,'黒レ'),(3974,1,'レぽ'),(3975,1,'ぽ展'),(3976,1,'展総'),(3977,1,'総メ'),(3978,1,'メユ'),(3979,1,'ユソ'),(3980,1,'ソセ'),(3981,1,'セ合'),(3982,1,'合鮮'),(3983,1,'鮮努'),(3984,1,'努ハ'),(3985,1,'チ岡'),(3986,1,'岡載'),(3987,1,'載そ'),(3988,1,'そん'),(3989,1,'んレ'),(3990,1,'レつ'),(3991,1,'つ最'),(3992,3,'最掲'),(3993,1,'掲ッ'),(3994,1,'ッン'),(3995,1,'ンク'),(3996,1,'ク歩'),(3997,1,'歩人'),(3998,1,'人オ'),(3999,1,'オヌ'),(4000,1,'ヌ読'),(4001,1,'読使'),(4002,1,'使ヤ'),(4003,1,'ヤメ'),(4004,1,'メ当'),(4005,1,'当酒'),(4006,1,'酒文'),(4007,1,'文来'),(4008,1,'来機'),(4009,1,'機習'),(4010,1,'習テ'),(4011,1,'テ長'),(4012,1,'長泉'),(4013,1,'泉ナ'),(4014,1,'ナ星'),(4015,1,'星果'),(4016,1,'果寿'),(4017,1,'寿崩'),(4018,1,'崩な'),(4019,1,'なぜ'),(4020,1,'ぜり'),(4021,1,'りと'),(4022,1,'と。'),(4023,1,'程ナ'),(4024,1,'トシ'),(4025,1,'シキ'),(4026,1,'キ時'),(4027,1,'時路'),(4028,1,'路だ'),(4029,1,'だほ'),(4030,1,'ほず'),(4031,1,'ずろ'),(4032,1,'ろ億'),(4033,1,'億告'),(4034,1,'告物'),(4035,1,'物ね'),(4036,1,'ねむ'),(4037,1,'むフ'),(4038,1,'や争'),(4039,1,'争故'),(4040,1,'故ぼ'),(4041,1,'ぼ究'),(4042,1,'究西'),(4043,1,'西リ'),(4044,1,'リワ'),(4045,1,'ワ紙'),(4046,1,'紙新'),(4047,1,'新添'),(4048,1,'添ト'),(4049,1,'トを'),(4050,1,'をど'),(4051,1,'ど閣'),(4052,1,'閣害'),(4053,1,'害ご'),(4054,1,'ご輔'),(4055,1,'輔告'),(4056,1,'告ヤ'),(4057,1,'ヤ実'),(4058,1,'実展'),(4059,1,'展チ'),(4060,1,'チ身'),(4061,1,'身春'),(4062,1,'春み'),(4063,1,'みど'),(4064,1,'どめ'),(4065,1,'め暮'),(4066,1,'暮具'),(4067,1,'具辺'),(4068,1,'辺の'),(4069,1,'。右'),(4070,1,'右ぎ'),(4071,1,'ぎせ'),(4072,1,'せ庭'),(4073,1,'庭雲'),(4074,1,'雲ロ'),(4075,1,'ロア'),(4076,1,'アオ'),(4077,1,'オ近'),(4078,1,'近成'),(4079,1,'成つ'),(4080,1,'つず'),(4081,1,'ず行'),(4082,1,'行'),(4083,1,'64'),(4084,1,'夜テ'),(4085,3,'テナ'),(4086,1,'ナ供'),(4087,1,'供和'),(4088,1,'和ち'),(4089,1,'ちよ'),(4090,1,'つ際'),(4091,1,'際提'),(4092,1,'提ノ'),(4093,1,'ヱカ'),(4094,1,'カエ'),(4095,1,'エ決'),(4096,1,'決入'),(4097,1,'入ヱ'),(4098,1,'ヱ慮'),(4099,1,'慮'),(4100,1,'困つ'),(4101,1,'つよ'),(4102,1,'よ電'),(4103,1,'電響'),(4104,1,'響た'),(4105,1,'たン'),(4106,1,'ンリ'),(4107,1,'リ動'),(4108,1,'動惑'),(4109,1,'惑室'),(4110,1,'室乳'),(4111,1,'乳ゃ'),(4112,1,'づぐ'),(4113,1,'ぐろ'),(4114,1,'ろ応'),(4115,1,'開び'),(4116,1,'びず'),(4117,1,'ずー'),(4118,1,'ーレ'),(4119,1,'レ輪'),(4120,1,'輪工'),(4121,1,'工ウ'),(4122,1,'ウヘ'),(4123,1,'ヘ名'),(4124,1,'名川'),(4125,1,'川舎'),(4126,1,'舎賛'),(4127,1,'賛こ'),(4128,1,'こけ'),(4129,1,'けド'),(4130,1,'。催'),(4131,1,'催む'),(4132,1,'むこ'),(4133,1,'こ変'),(4134,1,'変済'),(4135,1,'済高'),(4136,1,'高設'),(4137,1,'設な'),(4138,1,'なゆ'),(4139,1,'ゆら'),(4140,1,'らの'),(4141,1,'の訪'),(4142,1,'訪縮'),(4143,1,'縮を'),(4144,1,'めが'),(4145,1,'が老'),(4146,1,'老脳'),(4147,1,'脳掲'),(4148,1,'掲ス'),(4149,1,'スじ'),(4150,1,'じ部'),(4151,1,'部最'),(4152,1,'最文'),(4153,1,'文ア'),(4154,1,'アハ'),(4155,1,'ハメ'),(4156,1,'メ歩'),(4157,1,'歩'),(4158,1,'対ニ'),(4159,1,'ム止'),(4160,1,'止車'),(4161,1,'車ク'),(4162,1,'クヒ'),(4163,1,'ヒワ'),(4164,1,'ワ健'),(4165,1,'健多'),(4166,1,'多で'),(4167,1,'でっ'),(4168,1,'っト'),(4169,1,'トフ'),(4170,1,'フ環'),(4171,1,'環'),(4172,1,'73'),(4173,1,'正幸'),(4174,1,'幸績'),(4175,1,'績仕'),(4176,1,'仕ん'),(4177,1,'んぶ'),(4178,1,'ぶフ'),(4179,1,'cj'),(4180,1,'831'),(4181,1,'about'),(4182,1,'us'),(4183,1,'私た'),(4184,1,'たち'),(4185,1,'ちに'),(4186,1,'に関'),(4187,1,'関し'),(4188,1,'して'),(4189,1,'ては'),(4190,1,'javascript'),(4191,1,'code'),(4192,1,'with'),(4193,1,'focus'),(4194,1,'on'),(4195,1,'improved'),(4196,1,'data'),(4197,1,'model'),(4198,1,'chai'),(4199,1,'is'),(4200,1,'an'),(4201,1,'assertion'),(4202,1,'library'),(4203,1,'to'),(4204,1,'a'),(4205,1,'task'),(4206,1,'runner'),(4207,1,'aiming'),(4208,1,'at'),(4209,1,'explaining'),(4210,1,'increasing'),(4211,1,'speed'),(4212,1,'up'),(4213,1,'consecutive'),(4214,1,'function'),(4215,1,'host'),(4216,1,'environment'),(4217,1,'most'),(4218,1,'popular'),(4219,1,'browsers'),(4220,1,'share'),(4221,1,'support'),(4222,1,'creating'),(4223,1,'web'),(4224,1,'it'),(4225,1,'transformation'),(4226,1,'toolkit'),(4227,1,'create'),(4228,1,'and'),(4229,1,'mobile'),(4230,1,'applications'),(4231,1,'interact'),(4232,1,'despite'),(4233,1,'process'),(4234,1,'of'),(4235,1,'any'),(4236,1,'state'),(4237,1,'changes'),(4238,1,'usually'),(4239,1,'by'),(4240,1,'analogy'),(4241,1,'transform'),(4242,1,'css'),(4243,1,'gulp'),(4244,1,'that'),(4245,1,'they'),(4246,1,'are'),(4247,1,'embedded'),(4248,1,'in'),(4249,1,'popularity'),(4250,1,'v8'),(4251,1,'source'),(4252,1,'can'),(4253,1,'respond'),(4254,1,'which'),(4255,1,'could'),(4256,1,'also'),(4257,1,'increased'),(4258,1,'subject'),(4259,1,'maintains'),(4260,1,'mongodb'),(4261,1,'xhr'),(4262,1,'concept'),(4263,1,'vanilla'),(4264,1,'not'),(4265,1,'available'),(4266,1,'known'),(4267,1,'immediately'),(4268,1,'pages'),(4269,1,'response'),(4270,1,'objects'),(4271,1,'node'),(4272,1,'iife'),(4273,1,'list'),(4274,1,'software'),(4275,1,'modules'),(4276,1,'defined'),(4277,1,'framework'),(4278,1,'based'),(4279,1,'simple'),(4280,1,'pluggable'),(4281,1,'static'),(4282,1,'site'),(4283,1,'generator'),(4284,1,'coffeescript'),(4285,1,'way'),(4286,1,'compatible'),(4287,1,'c'),(4288,1,'奉'),(4289,1,'32'),(4290,1,'供フ'),(4291,1,'フカ'),(4292,1,'カ校'),(4293,1,'校作'),(4294,1,'作め'),(4295,1,'めぱ'),(4296,1,'ぱら'),(4297,1,'らみ'),(4298,1,'み取'),(4299,1,'取若'),(4300,1,'若競'),(4301,1,'競ス'),(4302,1,'ス殺'),(4303,1,'殺右'),(4304,1,'右れ'),(4305,1,'れ長'),(4306,1,'長'),(4307,1,'36'),(4308,1,'今テ'),(4309,1,'テロ'),(4310,1,'ロサ'),(4311,1,'サク'),(4312,1,'ク本'),(4313,1,'本著'),(4314,1,'著ン'),(4315,1,'ンり'),(4316,1,'あ読'),(4317,1,'読能'),(4318,1,'能オ'),(4319,1,'オホ'),(4320,1,'ホモ'),(4321,1,'モ良'),(4322,1,'良嫌'),(4323,1,'嫌業'),(4324,1,'理音'),(4325,1,'音設'),(4326,1,'設ず'),(4327,1,'。存'),(4328,1,'存転'),(4329,1,'転ゃ'),(4330,1,'ゃス'),(4331,1,'スぽ'),(4332,1,'ぽう'),(4333,1,'う主'),(4334,1,'主勝'),(4335,1,'勝件'),(4336,1,'件ド'),(4337,1,'ドゅ'),(4338,1,'ゅび'),(4339,1,'び付'),(4340,1,'付甲'),(4341,1,'甲ぼ'),(4342,1,'ぼ売'),(4343,1,'売士'),(4344,1,'士東'),(4345,1,'東ラ'),(4346,1,'ラせ'),(4347,1,'せ式'),(4348,1,'式算'),(4349,1,'算ヒ'),(4350,1,'ヒエ'),(4351,1,'エリ'),(4352,1,'リキ'),(4353,1,'キ首'),(4354,1,'首天'),(4355,1,'天ヱ'),(4356,1,'ヱヌ'),(4357,1,'ヌ達'),(4358,1,'達約'),(4359,1,'約際'),(4360,1,'際マ'),(4361,1,'マノ'),(4362,1,'ノオ'),(4363,1,'オメ'),(4364,1,'メ震'),(4365,1,'震千'),(4366,1,'千セ'),(4367,1,'セケ'),(4368,1,'ケ能'),(4369,1,'能密'),(4370,1,'密ね'),(4371,1,'ねだ'),(4372,1,'だラ'),(4373,1,'ラ東'),(4374,1,'46'),(4375,1,'愛ね'),(4376,1,'ねら'),(4377,1,'ら掲'),(4378,1,'掲成'),(4379,1,'成恋'),(4380,1,'恋ち'),(4381,1,'ち。'),(4382,1,'。全'),(4383,1,'全フ'),(4384,1,'フ見'),(4385,1,'見作'),(4386,1,'作チ'),(4387,1,'チヘ'),(4388,1,'ヘ型'),(4389,1,'型軽'),(4390,1,'軽じ'),(4391,1,'じま'),(4392,1,'まづ'),(4393,1,'づ一'),(4394,1,'一優'),(4395,1,'優べ'),(4396,1,'べょ'),(4397,1,'ょも'),(4398,1,'も介'),(4399,1,'介方'),(4400,1,'方マ'),(4401,1,'マラ'),(4402,1,'ラチ'),(4403,1,'チル'),(4404,1,'ル候'),(4405,1,'候行'),(4406,1,'行み'),(4407,1,'みら'),(4408,1,'ら九'),(4409,1,'九断'),(4410,1,'断ノ'),(4411,1,'ノツ'),(4412,1,'ツ家'),(4413,1,'家阪'),(4414,1,'阪ウ'),(4415,1,'ウヤ'),(4416,1,'ヤハ'),(4417,1,'ハツ'),(4418,1,'ツ挙'),(4419,1,'挙岡'),(4420,1,'岡は'),(4421,1,'は辺'),(4422,1,'辺最'),(4423,1,'掲じ'),(4424,1,'じ気'),(4425,1,'気盛'),(4426,1,'盛ヌ'),(4427,1,'ヌ外'),(4428,1,'外宅'),(4429,1,'宅ワ'),(4430,1,'ワオ'),(4431,1,'オ注'),(4432,1,'注真'),(4433,1,'真ト'),(4434,1,'ト一'),(4435,1,'一芝'),(4436,1,'芝づ'),(4437,1,'づふ'),(4438,1,'ふじ'),(4439,1,'じ無'),(4440,1,'無'),(4441,1,'24'),(4442,1,'税あ'),(4443,1,'あ向'),(4444,1,'向原'),(4445,1,'原を'),(4446,1,'を行'),(4447,1,'行祖'),(4448,1,'祖直'),(4449,1,'直護'),(4450,1,'護め'),(4451,1,'めす'),(4452,1,'す。'),(4453,1,'大ゅ'),(4454,1,'ゅづ'),(4455,1,'た病'),(4456,1,'病客'),(4457,1,'客転'),(4458,1,'転ヌ'),(4459,1,'ヌフ'),(4460,1,'フ可'),(4461,1,'可道'),(4462,1,'道ぜ'),(4463,1,'ぜび'),(4464,1,'びれ'),(4465,1,'れ味'),(4466,1,'味者'),(4467,1,'者景'),(4468,1,'景応'),(4469,1,'応ノ'),(4470,1,'ノカ'),(4471,1,'カヱ'),(4472,1,'ヱモ'),(4473,1,'モ相'),(4474,1,'相悦'),(4475,1,'悦エ'),(4476,1,'エレ'),(4477,1,'レス'),(4478,1,'ス度'),(4479,1,'度面'),(4480,1,'面フ'),(4481,1,'フ停'),(4482,1,'停司'),(4483,1,'司に'),(4484,1,'にみ'),(4485,1,'みょ'),(4486,1,'ょし'),(4487,1,'し断'),(4488,1,'断勢'),(4489,1,'勢き'),(4490,1,'きル'),(4491,1,'ルに'),(4492,1,'にイ'),(4493,1,'イ左'),(4494,1,'左密'),(4495,1,'密ぱ'),(4496,1,'ぱざ'),(4497,1,'ざそ'),(4498,1,'そぜ'),(4499,1,'ぜ秒'),(4500,1,'秒間'),(4501,1,'間西'),(4502,1,'西よ'),(4503,1,'よ津'),(4504,1,'津回'),(4505,1,'回ッ'),(4506,1,'ッド'),(4507,1,'ドぽ'),(4508,1,'ぽ部'),(4509,1,'部票'),(4510,1,'票み'),(4511,1,'みク'),(4512,1,'クい'),(4513,1,'いそ'),(4514,1,'そ突'),(4515,1,'突覚'),(4516,1,'覚布'),(4517,1,'布ゆ'),(4518,1,'。視'),(4519,1,'視ゆ'),(4520,1,'ゆひ'),(4521,1,'ひぱ'),(4522,1,'ぱ勝'),(4523,1,'勝貴'),(4524,1,'貴マ'),(4525,1,'マ運'),(4526,1,'運提'),(4527,1,'提テ'),(4528,1,'ナ科'),(4529,1,'科'),(4530,1,'93'),(4531,1,'香ケ'),(4532,1,'ケス'),(4533,1,'スマ'),(4534,1,'マセ'),(4535,1,'セ質'),(4536,1,'質府'),(4537,1,'府ゃ'),(4538,1,'ゃむ'),(4539,1,'むク'),(4540,1,'ク替'),(4541,1,'替外'),(4542,1,'外タ'),(4543,1,'タフ'),(4544,1,'フウ'),(4545,1,'ウ兵'),(4546,1,'兵改'),(4547,1,'改ヤ'),(4548,1,'ヤ投'),(4549,1,'投旅'),(4550,1,'旅合'),(4551,1,'合シ'),(4552,1,'シ表'),(4553,1,'表不'),(4554,1,'不ノ'),(4555,1,'ノル'),(4556,1,'ル支'),(4557,1,'支聞'),(4558,1,'聞ヒ'),(4559,1,'ヒテ'),(4560,1,'テ亡'),(4561,1,'亡'),(4562,1,'重演'),(4563,1,'演'),(4564,1,'次購'),(4565,1,'購詳'),(4566,1,'詳列'),(4567,1,'列岸'),(4568,1,'岸レ'),(4569,1,'。離'),(4570,1,'離ぱ'),(4571,1,'ぱ我'),(4572,1,'我盗'),(4573,1,'盗ム'),(4574,1,'ムト'),(4575,1,'トテ'),(4576,1,'テ査'),(4577,1,'査感'),(4578,1,'感テ'),(4579,1,'テソ'),(4580,1,'キ週'),(4581,1,'週高'),(4582,1,'高ハ'),(4583,1,'ネヘ'),(4584,1,'ヘ進'),(4585,1,'進北'),(4586,1,'北づ'),(4587,1,'づ作'),(4588,1,'作試'),(4589,1,'試ぜ'),(4590,1,'ぜべ'),(4591,1,'べた'),(4592,1,'た済'),(4593,1,'済利'),(4594,1,'利ば'),(4595,1,'ばこ'),(4596,1,'こ広'),(4597,1,'広単'),(4598,1,'単け'),(4599,1,'ざし'),(4600,1,'しえ'),(4601,1,'え主'),(4602,1,'主著'),(4603,1,'著ざ'),(4604,1,'ざぞ'),(4605,1,'ゃ臨'),(4606,1,'臨'),(4607,1,'政だ'),(4608,1,'だク'),(4609,1,'ク主'),(4610,1,'主白'),(4611,1,'白ヲ'),(4612,1,'ヲケ'),(4613,1,'ケ費'),(4614,1,'費'),(4615,1,'96'),(4616,1,'克即'),(4617,1,'即橋'),(4618,1,'橋ち'),(4619,1,'ちリ'),(4620,1,'リ。'),(4621,1,'新死'),(4622,1,'死ヌ'),(4623,1,'ヌル'),(4624,1,'ル業'),(4625,1,'業編'),(4626,1,'編で'),(4627,1,'で都'),(4628,1,'都追'),(4629,1,'追へ'),(4630,1,'へべ'),(4631,1,'べう'),(4632,1,'う指'),(4633,1,'指村'),(4634,1,'村か'),(4635,1,'かぎ'),(4636,1,'ぎす'),(4637,1,'すげ'),(4638,1,'げ足'),(4639,1,'足発'),(4640,1,'発ち'),(4641,1,'ちス'),(4642,1,'スげ'),(4643,1,'げ補'),(4644,1,'補未'),(4645,1,'未セ'),(4646,1,'セ第'),(4647,1,'第故'),(4648,1,'故達'),(4649,1,'達も'),(4650,1,'もび'),(4651,1,'き選'),(4652,1,'選統'),(4653,1,'統ミ'),(4654,1,'ミ権'),(4655,1,'権沿'),(4656,1,'沿名'),(4657,1,'名ス'),(4658,1,'ス熱'),(4659,1,'熱左'),(4660,1,'左ニ'),(4661,1,'ニ面'),(4662,1,'面供'),(4663,1,'供ト'),(4664,1,'ト門'),(4665,1,'門力'),(4666,1,'力経'),(4667,1,'経ア'),(4668,1,'アタ'),(4669,1,'タ訴'),(4670,1,'訴北'),(4671,1,'北あ'),(4672,1,'め載'),(4673,1,'載'),(4674,1,'鉄ト'),(4675,1,'トム'),(4676,1,'ム人'),(4677,1,'人総'),(4678,1,'総イ'),(4679,1,'イヲ'),(4680,1,'ト三'),(4681,1,'三輸'),(4682,1,'輸回'),(4683,1,'回ら'),(4684,1,'らべ'),(4685,1,'べ。'),(4686,1,'。妓'),(4687,1,'妓て'),(4688,1,'てう'),(4689,1,'うド'),(4690,1,'ド島'),(4691,1,'島蓄'),(4692,1,'蓄ヱ'),(4693,1,'ヱケ'),(4694,1,'ケラ'),(4695,1,'ラ合'),(4696,1,'合抑'),(4697,1,'抑の'),(4698,1,'のあ'),(4699,1,'あひ'),(4700,1,'ひ店'),(4701,1,'店韓'),(4702,1,'韓ケ'),(4703,1,'ケミ'),(4704,1,'ミヤ'),(4705,1,'ヤラ'),(4706,1,'ラ最'),(4707,1,'最年'),(4708,1,'年ニ'),(4709,1,'ニエ'),(4710,1,'エシ'),(4711,1,'シ担'),(4712,1,'担左'),(4713,1,'左み'),(4714,1,'みじ'),(4715,1,'じ教'),(4716,1,'教誠'),(4717,1,'誠さ'),(4718,1,'さ的'),(4719,1,'的'),(4720,1,'地元'),(4721,1,'元な'),(4722,1,'きげ'),(4723,1,'げ浴'),(4724,1,'浴'),(4725,1,'覧っ'),(4726,1,'リさ'),(4727,1,'さ講'),(4728,1,'講止'),(4729,1,'止コ'),(4730,1,'コタ'),(4731,1,'タ評'),(4732,1,'評正'),(4733,1,'正ヱ'),(4734,1,'ヱ医'),(4735,1,'医'),(4736,1,'組そ'),(4737,1,'そづ'),(4738,1,'づぽ'),(4739,1,'ぽ連'),(4740,1,'連共'),(4741,1,'共セ'),(4742,1,'セ載'),(4743,1,'載天'),(4744,1,'天始'),(4745,1,'始謙'),(4746,1,'謙ず'),(5685,1,'photos'),(6155,1,'品写'),(6156,1,'写真'),(6157,1,'真'),(6158,2,'english'),(6159,1,'new'),(6160,1,'zealand'),(6161,1,'four'),(6162,1,'館に'),(6163,1,'にを'),(6164,1,'めッ'),(6165,1,'ッ都'),(6166,1,'都雪'),(6167,1,'雪当'),(6168,1,'当え'),(6169,1,'えん'),(6170,1,'んこ'),(6171,1,'こリ'),(6172,1,'リ勝'),(6173,1,'勝請'),(6174,1,'請て'),(6175,1,'て載'),(6176,1,'載構'),(6177,1,'構シ'),(6178,1,'シヨ'),(6179,1,'ヨア'),(6180,1,'アリ'),(6181,1,'リ作'),(6182,1,'作育'),(6183,1,'育野'),(6184,1,'野必'),(6185,1,'必朝'),(6186,1,'朝テ'),(6187,1,'テ氏'),(6188,1,'氏状'),(6189,1,'状れ'),(6190,1,'れぴ'),(6191,1,'ぴざ'),(6192,1,'ざ児'),(6193,1,'児'),(6194,1,'43'),(6195,1,'岩亨'),(6196,1,'亨ほ'),(6197,1,'ほた'),(6198,1,'たラ'),(6199,1,'ラフ'),(6200,1,'。雑'),(6201,1,'雑げ'),(6202,1,'げ社'),(6203,1,'社聞'),(6204,1,'聞際'),(6205,1,'際ス'),(6206,1,'ステ'),(6207,1,'テ誌'),(6208,1,'誌早'),(6209,1,'早り'),(6210,1,'あろ'),(6211,1,'ろ断'),(6212,1,'断毎'),(6213,1,'毎る'),(6214,1,'るで'),(6215,1,'でご'),(6216,1,'ご浜'),(6217,1,'浜業'),(6218,1,'業ソ'),(6219,1,'ソテ'),(6220,1,'テ運'),(6221,1,'運検'),(6222,1,'検く'),(6223,1,'くト'),(6224,1,'ト内'),(6225,1,'内通'),(6226,1,'通シ'),(6227,1,'シ主'),(6228,1,'主大'),(6229,1,'大ニ'),(6230,1,'ニオ'),(6231,1,'セユ'),(6232,1,'ユ年'),(6233,1,'年東'),(6234,1,'東大'),(6235,1,'大コ'),(6236,1,'コヱ'),(6237,1,'ヱホ'),(6238,1,'ホニ'),(6239,1,'ニ画'),(6240,1,'画機'),(6241,1,'機た'),(6242,1,'たよ'),(6243,1,'よて'),(6244,1,'て通'),(6245,1,'通受'),(6246,1,'受ノ'),(6247,1,'ノヨ'),(6248,1,'ヌシ'),(6249,1,'シ方'),(6250,1,'方禁'),(6251,1,'禁セ'),(6252,1,'セカ'),(6253,1,'カ紙'),(6254,1,'紙購'),(6255,1,'購別'),(6256,1,'別碁'),(6257,1,'碁候'),(6258,1,'候法'),(6259,1,'法ぼ'),(6260,1,'ぼつ'),(6261,1,'つ。'),(6262,1,'。暁'),(6263,1,'暁ヲ'),(6264,1,'ヲユ'),(6265,1,'ユキ'),(6266,1,'キ真'),(6267,1,'真円'),(6268,1,'円ッ'),(6269,1,'ッべ'),(6270,1,'べら'),(6271,1,'ら氏'),(6272,1,'氏著'),(6273,1,'著メ'),(6274,1,'メオ'),(6275,1,'オテ'),(6276,1,'テ謀'),(6277,1,'謀始'),(6278,1,'始む'),(6279,1,'むけ'),(6280,1,'け分'),(6281,1,'分映'),(6282,1,'映ぴ'),(6283,1,'ぴや'),(6284,1,'やせ'),(6285,1,'せな'),(6286,1,'な公'),(6287,1,'公道'),(6288,1,'道ぎ'),(6289,1,'ぎー'),(6290,1,'ーざ'),(6291,1,'ざ部'),(6292,1,'部応'),(6293,1,'応入'),(6294,1,'入ざ'),(6295,1,'ざっ'),(6296,1,'っら'),(6297,1,'ら国'),(6298,1,'国図'),(6299,1,'図イ'),(6300,1,'イへ'),(6301,1,'へみ'),(6302,1,'み鳥'),(6303,1,'鳥表'),(6304,1,'表す'),(6305,1,'すざ'),(6306,1,'ざぐ'),(6307,1,'ぐよ'),(6308,1,'よ女'),(6309,1,'女断'),(6310,1,'断連'),(6311,1,'連テ'),(6312,1,'テキ'),(6313,1,'キ演'),(6314,1,'演女'),(6315,1,'女を'),(6316,1,'をや'),(6317,1,'やべ'),(6318,1,'べ今'),(6319,1,'今場'),(6320,1,'場ラ'),(6321,1,'ラク'),(6322,1,'クロ'),(6323,1,'ロ開'),(6324,1,'開記'),(6325,1,'記べ'),(6326,1,'べを'),(6327,1,'をリ'),(6328,1,'リ禁'),(6329,1,'禁'),(6330,1,'進ス'),(6331,1,'スモ'),(6332,1,'モソ'),(6333,1,'ソワ'),(6334,1,'ワ社'),(6335,1,'社九'),(6336,1,'九ミ'),(6337,1,'ミセ'),(6338,1,'セ敬'),(6339,1,'敬'),(6340,1,'好術'),(6341,1,'術ク'),(6342,1,'クぱ'),(6343,1,'ぱず'),(6344,1,'弾ッ'),(6345,1,'ッで'),(6346,1,'でき'),(6347,1,'き黒'),(6348,1,'黒'),(6349,1,'取ぞ'),(6350,1,'ぞれ'),(6351,1,'れん'),(6352,1,'ん投'),(6353,1,'投頂'),(6354,1,'頂ぱ'),(6355,1,'ぱお'),(6356,1,'お谷'),(6357,1,'谷舞'),(6358,1,'舞ノ'),(6359,1,'ノ残'),(6360,1,'残傘'),(6361,1,'傘ヌ'),(6362,1,'ヌタ'),(6363,1,'タ傾'),(6364,1,'傾'),(6365,1,'40'),(6366,1,'備カ'),(6367,1,'カフ'),(6368,1,'フ看'),(6369,1,'看玲'),(6370,1,'玲メ'),(6371,1,'メケ'),(6372,1,'ケヱ'),(6373,1,'ヱ能'),(6374,1,'能権'),(6375,1,'権ざ'),(6376,1,'ざ無'),(6377,1,'無事'),(6378,1,'事取'),(6379,1,'取項'),(6380,1,'項ー'),(6381,1,'ーょ'),(6382,1,'ょ転'),(6383,1,'転科'),(6384,1,'科幅'),(6385,1,'幅採'),(6386,1,'採洋'),(6387,1,'洋び'),(6388,1,'。京'),(6389,1,'京も'),(6390,1,'も購'),(6391,1,'購祀'),(6392,1,'祀チ'),(6393,1,'チ率'),(6394,1,'率東'),(6395,1,'東俳'),(6396,1,'俳テ'),(6397,1,'テワ'),(6398,1,'シ受'),(6399,1,'受銀'),(6400,1,'銀ヌ'),(6401,1,'ヌ通'),(6402,1,'通多'),(6403,1,'多著'),(6404,1,'著情'),(6405,1,'情る'),(6406,1,'ねと'),(6407,1,'とへ'),(6408,1,'へ幌'),(6409,1,'幌図'),(6410,1,'図ハ'),(6411,1,'ハ申'),(6412,1,'申台'),(6413,1,'台ひ'),(6414,1,'ひラ'),(6415,1,'フ株'),(6416,1,'株'),(6417,1,'68'),(6418,1,'輸さ'),(6419,1,'さわ'),(6420,1,'わ冬'),(6421,1,'冬'),(6422,1,'両ど'),(6423,1,'どを'),(6424,1,'をて'),(6425,1,'て八'),(6426,1,'八作'),(6427,1,'作で'),(6428,1,'でた'),(6429,1,'たト'),(6430,1,'トり'),(6431,1,'り客'),(6432,1,'客無'),(6433,1,'無へ'),(6434,1,'へば'),(6435,1,'ば見'),(6436,1,'見着'),(6437,1,'着み'),(6438,1,'みほ'),(6439,1,'ほト'),(6440,1,'ト経'),(6441,1,'経倉'),(6442,1,'倉セ'),(6443,1,'セチ'),(6444,1,'チユ'),(6445,1,'ユヒ'),(6446,1,'ヒ競'),(6447,1,'競暮'),(6448,1,'暮亜'),(6449,1,'亜才'),(6450,1,'才滅'),(6451,1,'滅れ'),(6452,1,'れ。'),(6453,1,'。合'),(6454,1,'合ミ'),(6455,1,'チメ'),(6456,1,'メ的'),(6457,1,'的熊'),(6458,1,'熊ス'),(6459,1,'スあ'),(6460,1,'あ命'),(6461,1,'命検'),(6462,1,'検阜'),(6463,1,'阜正'),(6464,1,'正任'),(6465,1,'任セ'),(6466,1,'セシ'),(6467,1,'ヨヒ'),(6468,1,'ヒ同'),(6469,1,'同速'),(6470,1,'速芸'),(6471,1,'芸ね'),(6472,1,'ね必'),(6473,1,'必適'),(6474,1,'適と'),(6475,1,'とー'),(6476,1,'ー日'),(6477,1,'日供'),(6478,1,'供ン'),(6479,1,'ン力'),(6480,1,'力'),(6481,1,'94'),(6482,1,'関サ'),(6483,1,'ミワ'),(6484,1,'ワ題'),(6485,1,'題前'),(6486,1,'前ラ'),(6487,1,'ラれ'),(6488,1,'れ国'),(6489,1,'国来'),(6490,1,'来ホ'),(6491,1,'ホア'),(6492,1,'アヌ'),(6493,1,'ヌ聴'),(6494,1,'聴料'),(6495,1,'料コ'),(6496,1,'コケ'),(6497,1,'ケウ'),(6498,1,'際竹'),(6499,1,'竹マ'),(6500,1,'マシ'),(6501,1,'シ八'),(6502,1,'八除'),(6503,1,'除ろ'),(6504,1,'ろで'),(6505,1,'でを'),(6506,1,'をえ'),(6507,1,'え。'),(6508,1,'針ろ'),(6509,1,'ろせ'),(6510,1,'せぼ'),(6511,1,'ぼ調'),(6512,1,'調係'),(6513,1,'係テ'),(6514,1,'テ周'),(6515,1,'周止'),(6516,1,'止イ'),(6517,1,'イ党'),(6518,1,'党最'),(6519,1,'最エ'),(6520,1,'ナ米'),(6521,1,'米報'),(6522,1,'報ア'),(6523,1,'アル'),(6524,1,'ヲ知'),(6525,1,'知測'),(6526,1,'測れ'),(6527,1,'れば'),(6528,1,'ば甲'),(6529,1,'甲祭'),(6530,1,'祭ぐ'),(6531,1,'ぐご'),(6532,1,'ごむ'),(6533,1,'む手'),(6534,1,'手市'),(6535,1,'市た'),(6536,1,'た来'),(6537,1,'来題'),(6538,1,'題ぱ'),(6539,1,'ぱ最'),(6540,1,'最細'),(6541,1,'細セ'),(6542,1,'ク毎'),(6543,1,'毎約'),(6544,1,'約じ'),(6545,1,'じー'),(6546,1,'ーら'),(6547,1,'ら題'),(6548,1,'題第'),(6549,1,'第設'),(6550,1,'設エ'),(6551,1,'エ睦'),(6552,1,'睦木'),(6553,1,'木ソ'),(6554,1,'ソウ'),(6555,1,'ウ断'),(6556,1,'断勝'),(6557,1,'勝う'),(6558,1,'う稿'),(6559,1,'稿告'),(6560,1,'告介'),(6561,1,'介カ'),(6562,1,'カア'),(6563,1,'ア安'),(6564,1,'安各'),(6565,1,'各ワ'),(6566,1,'ワ受'),(6567,1,'受要'),(6568,1,'要げ'),(6569,1,'げひ'),(6570,1,'ひド'),(6571,1,'ドは'),(6572,1,'は岡'),(6573,1,'岡率'),(6574,1,'率憲'),(6575,1,'憲切'),(6576,1,'切子'),(6577,1,'子る'),(6578,1,'るら'),(6579,1,'ら。'),(6580,1,'熊や'),(6581,1,'や応'),(6582,1,'日ゆ'),(6583,1,'イは'),(6584,1,'はン'),(6585,1,'ン康'),(6586,1,'康読'),(6587,1,'読行'),(6588,1,'行の'),(6589,1,'のッ'),(6590,1,'ッど'),(6591,1,'ど転'),(6592,1,'転従'),(6593,1,'従ソ'),(6594,1,'キユ'),(6595,1,'マ講'),(6596,1,'講'),(6597,1,'12'),(6598,1,'滞延'),(6599,1,'延ア'),(6600,1,'アセ'),(6601,1,'セメ'),(6602,1,'メ縁'),(6603,1,'縁訟'),(6604,1,'訟題'),(6605,1,'題ナ'),(6606,1,'ナヒ'),(6607,1,'ヒ汚'),(6608,1,'汚化'),(6609,1,'化ク'),(6610,1,'クぎ'),(6611,1,'ぎぜ'),(6612,1,'ぜ録'),(6613,1,'録'),(6614,1,'景ク'),(6615,1,'クキ'),(6616,1,'キモ'),(6617,1,'モツ'),(6618,1,'見鹿'),(6619,1,'鹿お'),(6620,1,'おる'),(6621,1,'る池'),(6622,1,'池垣'),(6623,1,'垣ぜ'),(6624,1,'ぜな'),(6625,1,'なて'),(6626,1,'てが'),(6627,1,'が決'),(6628,1,'決間'),(6629,1,'間ヘ'),(6630,1,'ヘ為'),(6631,1,'為歯'),(6632,1,'歯没'),(6633,1,'没牧'),(6634,1,'牧ン'),(6635,1,'ンじ'),(6636,1,'じべ'),(6637,1,'べけ'),(6638,1,'228'),(6639,1,'one'),(6640,1,'都ホ'),(6641,1,'ホ政'),(6642,1,'政行'),(6643,1,'行レ'),(6644,1,'レ名'),(6645,1,'名棄'),(6646,1,'棄ル'),(6647,1,'ルち'),(6648,1,'ちが'),(6649,1,'がと'),(6650,1,'と無'),(6651,1,'無申'),(6652,1,'申月'),(6653,1,'月ぎ'),(6654,1,'ぎぶ'),(6655,1,'ぶ舞'),(6656,1,'舞承'),(6657,1,'承載'),(6658,1,'載ま'),(6659,1,'まれ'),(6660,1,'れむ'),(6661,1,'む品'),(6662,1,'品読'),(6663,1,'読ロ'),(6664,1,'ロヲ'),(6665,1,'ヲツ'),(6666,1,'ツエ'),(6667,1,'エ浜'),(6668,1,'浜'),(6669,1,'57'),(6670,1,'起み'),(6671,1,'みラ'),(6672,1,'ラ国'),(6673,1,'国国'),(6674,1,'国読'),(6675,1,'読う'),(6676,1,'うび'),(6677,1,'び持'),(6678,1,'持岡'),(6679,1,'岡び'),(6680,1,'び。'),(6681,1,'。採'),(6682,1,'採問'),(6683,1,'問ヤ'),(6684,1,'ヤタ'),(6685,1,'タテ'),(6686,1,'テ日'),(6687,1,'日略'),(6688,1,'略フ'),(6689,1,'フた'),(6690,1,'たお'),(6691,1,'おわ'),(6692,1,'わ頭'),(6693,1,'頭治'),(6694,1,'治オ'),(6695,1,'オヒ'),(6696,1,'ヒ文'),(6697,1,'文然'),(6698,1,'然な'),(6699,1,'な真'),(6700,1,'真知'),(6701,1,'知ロ'),(6702,1,'ロ保'),(6703,1,'保担'),(6704,1,'担ぽ'),(6705,1,'ぽへ'),(6706,1,'へ他'),(6707,1,'他善'),(6708,1,'善が'),(6709,1,'とず'),(6710,1,'ず際'),(6711,1,'際消'),(6712,1,'消ヘ'),(6713,1,'ヘホ'),(6714,1,'ホキ'),(6715,1,'キ修'),(6716,1,'修意'),(6717,1,'意京'),(6718,1,'京形'),(6719,1,'形マ'),(6720,1,'マヒ'),(6721,1,'ヒ大'),(6722,1,'大舵'),(6723,1,'舵る'),(6724,1,'るレ'),(6725,1,'レむ'),(6726,1,'むぜ'),(6727,1,'ぜ原'),(6728,1,'原書'),(6729,1,'書と'),(6730,1,'と疑'),(6731,1,'疑刊'),(6732,1,'刊ラ'),(6733,1,'ラめ'),(6734,1,'め付'),(6735,1,'付史'),(6736,1,'史が'),(6737,1,'がひ'),(6738,1,'ひ無'),(6739,1,'無明'),(6740,1,'明業'),(6741,1,'理ぶ'),(6742,1,'ぶま'),(6743,1,'まク'),(6744,1,'クる'),(6745,1,'る。'),(6746,1,'。済'),(6747,1,'済び'),(6748,1,'ぞ案'),(6749,1,'案意'),(6750,1,'意カ'),(6751,1,'カ速'),(6752,1,'速属'),(6753,1,'属ぴ'),(6754,1,'ぴ輪'),(6755,1,'輪更'),(6756,1,'更オ'),(6757,1,'ムワ'),(6758,1,'ワ証'),(6759,1,'証輪'),(6760,1,'輪相'),(6761,1,'相ニ'),(6762,1,'ニレ'),(6763,1,'レフ'),(6764,1,'フ作'),(6765,1,'作情'),(6766,1,'情む'),(6767,1,'むれ'),(6768,1,'れ戦'),(6769,1,'戦'),(6770,1,'8'),(6771,1,'集ラ'),(6772,1,'ラリ'),(6773,1,'リ用'),(6774,1,'用高'),(6775,1,'高碁'),(6776,1,'碁な'),(6777,1,'なへ'),(6778,1,'へい'),(6779,1,'い君'),(6780,1,'君広'),(6781,1,'広ち'),(6782,1,'ちレ'),(6783,1,'レて'),(6784,1,'て新'),(6785,1,'新'),(6786,1,'86'),(6787,1,'静ヱ'),(6788,1,'ヱキ'),(6789,1,'キサ'),(6790,1,'ミ市'),(6791,1,'市庁'),(6792,1,'庁課'),(6793,1,'課さ'),(6794,1,'さふ'),(6795,1,'ふだ'),(6796,1,'だよ'),(6797,1,'よ。'),(6798,1,'青治'),(6799,1,'治ク'),(6800,1,'クヘ'),(6801,1,'ヘサ'),(6802,1,'サフ'),(6803,1,'フ市'),(6804,1,'市京'),(6805,1,'京本'),(6806,1,'本に'),(6807,1,'にょ'),(6808,1,'ょ局'),(6809,1,'局童'),(6810,1,'童ナ'),(6811,1,'ナコ'),(6812,1,'コ代'),(6813,1,'代治'),(6814,1,'治ヨ'),(6815,1,'ヨイ'),(6816,1,'ノ胴'),(6817,1,'胴封'),(6818,1,'封意'),(6819,1,'意ゆ'),(6820,1,'ゆつ'),(6821,1,'つゅ'),(6822,1,'ゅだ'),(6823,1,'だ投'),(6824,1,'投岡'),(6825,1,'岡断'),(6826,1,'断ム'),(6827,1,'メラ'),(6828,1,'ラミ'),(6829,1,'ミ千'),(6830,1,'千込'),(6831,1,'込責'),(6832,1,'責ラ'),(6833,1,'ラマ'),(6834,1,'マ機'),(6835,1,'機状'),(6836,1,'状や'),(6837,1,'や作'),(6838,1,'作他'),(6839,1,'他略'),(6840,1,'略ス'),(6841,1,'スユ'),(6842,1,'マ学'),(6843,1,'学松'),(6844,1,'松ス'),(6845,1,'ス光'),(6846,1,'光律'),(6847,1,'律泊'),(6848,1,'泊の'),(6849,1,'。化'),(6850,1,'化ラ'),(6851,1,'ラな'),(6852,1,'なえ'),(6853,1,'えつ'),(6854,1,'つ始'),(6855,1,'始秋'),(6856,1,'秋ゅ'),(6857,1,'ゅト'),(6858,1,'ト横'),(6859,1,'横院'),(6860,1,'院ぐ'),(6861,1,'ぐ学'),(6862,1,'学発'),(6863,1,'発シ'),(6864,1,'シヤ'),(6865,1,'ヤ校'),(6866,1,'校人'),(6867,1,'人ス'),(6868,1,'スヲ'),(6869,1,'ヲ好'),(6870,1,'好'),(6871,1,'見俸'),(6872,1,'俸ワ'),(6873,1,'ワ基'),(6874,1,'基'),(6875,1,'手よ'),(6876,1,'よべ'),(6877,1,'べぼ'),(6878,1,'ぼけ'),(6879,1,'け庭'),(6880,1,'庭方'),(6881,1,'方あ'),(6882,1,'ずだ'),(6883,1,'だ迫'),(6884,1,'迫画'),(6885,1,'画着'),(6886,1,'着む'),(6887,1,'むび'),(6888,1,'び変'),(6889,1,'変冷'),(6890,1,'冷勧'),(6891,1,'勧封'),(6892,1,'封慮'),(6893,1,'慮た'),(6894,1,'たて'),(6895,1,'てぱ'),(6896,1,'。保'),(6897,1,'保ム'),(6898,1,'ムサ'),(6899,1,'サリ'),(6900,1,'リヌ'),(6901,1,'ヌ円'),(6902,1,'円'),(6903,1,'81'),(6904,1,'米配'),(6905,1,'配る'),(6906,1,'ね図'),(6907,1,'図索'),(6908,1,'索ぽ'),(6909,1,'ぽイ'),(6910,1,'イば'),(6911,1,'ば帝'),(6912,1,'帝社'),(6913,1,'社き'),(6914,1,'きフ'),(6915,1,'やレ'),(6916,1,'レ検'),(6917,1,'検聞'),(6918,1,'聞か'),(6919,1,'かへ'),(6920,1,'へす'),(6921,1,'す事'),(6922,1,'事'),(6923,1,'主む'),(6924,1,'ぴけ'),(6925,1,'ざ樹'),(6926,1,'樹信'),(6927,1,'信タ'),(6928,1,'タ情'),(6929,1,'情百'),(6930,1,'百ゆ'),(6931,1,'ゆ滝'),(6932,1,'滝洗'),(6933,1,'洗ヌ'),(6934,1,'ヌウ'),(6935,1,'ウヲ'),(6936,1,'ト南'),(6937,1,'南歩'),(6938,1,'歩け'),(6939,1,'けぐ'),(6940,1,'ぐさ'),(6941,1,'さ間'),(6942,1,'間備'),(6943,1,'備ま'),(6944,1,'まみ'),(6945,1,'みも'),(6946,1,'もき'),(6947,1,'食っ'),(6948,1,'ち乗'),(6949,1,'乗稿'),(6950,1,'稿メ'),(6951,1,'メ校'),(6952,1,'校稿'),(6953,1,'稿形'),(6954,1,'形け'),(6955,1,'けぴ'),(6956,1,'ぴ防'),(6957,1,'防関'),(6958,1,'関ル'),(6959,1,'ルた'),(6960,1,'たッ'),(6961,1,'ッ再'),(6962,1,'再孝'),(6963,1,'孝キ'),(6964,1,'キハ'),(6965,1,'ハサ'),(6966,1,'サメ'),(6967,1,'メ帯'),(6968,1,'帯円'),(6969,1,'逆ア'),(6970,1,'アソ'),(6971,1,'ソ志'),(6972,1,'志電'),(6973,1,'電だ'),(6974,1,'だス'),(6975,1,'ス堀'),(6976,1,'堀維'),(6977,1,'維オ'),(6978,1,'オヲ'),(6979,1,'ヲソ'),(6980,1,'ソレ'),(6981,1,'レ優'),(6982,1,'優定'),(6983,1,'定か'),(6984,1,'かま'),(6985,1,'まげ'),(6986,1,'げな'),(6987,1,'な供'),(6988,1,'供'),(6989,1,'92'),(6990,1,'向光'),(6991,1,'光'),(6992,1,'慎ど'),(6993,1,'ど。'),(6994,1,'。産'),(6995,1,'産操'),(6996,1,'操す'),(6997,1,'すご'),(6998,1,'ごぞ'),(6999,1,'ぞし'),(7000,1,'し改'),(7001,1,'改市'),(7002,1,'市ホ'),(7003,1,'ホ困'),(7004,1,'困供'),(7005,1,'供ね'),(7006,1,'ねず'),(7007,1,'ずど'),(7008,1,'どぞ'),(7009,1,'ぞ治'),(7010,1,'治魔'),(7011,1,'魔ノ'),(7012,1,'ノワ'),(7013,1,'ワ担'),(7014,1,'担対'),(7015,1,'対一'),(7016,1,'一供'),(7017,1,'供ゃ'),(7018,1,'ゃレ'),(7019,1,'レ峡'),(7020,1,'峡論'),(7021,1,'論ロ'),(7022,1,'ロコ'),(7023,1,'メ給'),(7024,1,'給宝'),(7025,1,'宝こ'),(7026,1,'こゅ'),(7027,1,'ゅえ'),(7028,1,'え以'),(7029,1,'以治'),(7030,1,'治時'),(7031,1,'時ニ'),(7032,1,'ニヨ'),(7033,1,'ヨス'),(7034,1,'ス交'),(7035,1,'交'),(7036,1,'18'),(7037,1,'従つ'),(7038,1,'ほぞ'),(7039,1,'ぞ的'),(7040,1,'的埋'),(7041,1,'埋め'),(7042,1,'めあ'),(7043,1,'あげ'),(7044,1,'げ。'),(7045,1,'。泊'),(7046,1,'泊ヘ'),(7047,1,'ヘハ'),(7048,1,'ハリ'),(7049,1,'リム'),(7050,1,'ム激'),(7051,1,'激覧'),(7052,1,'覧ウ'),(7053,1,'レ料'),(7054,1,'料済'),(7055,1,'済ハ'),(7056,1,'ハナ'),(7057,1,'ナヤ'),(7058,1,'ヤ市'),(7059,1,'市賞'),(7060,1,'賞べ'),(7061,1,'べ属'),(7062,1,'属'),(7063,1,'87'),(7064,1,'奮開'),(7065,1,'開ワ'),(7066,1,'シモ'),(7067,1,'モナ'),(7068,1,'ナ貢'),(7069,1,'貢真'),(7070,1,'真掲'),(7071,1,'掲ぽ'),(7072,1,'ぽ無'),(7073,1,'無相'),(7074,1,'相ウ'),(7075,1,'ロヱ'),(7076,1,'ヱハ'),(7077,1,'ハ事'),(7078,1,'事松'),(7079,1,'松を'),(7080,1,'め家'),(7081,1,'家権'),(7082,1,'権ゆ'),(7083,1,'ゆク'),(7084,1,'クお'),(7085,1,'おだ'),(7086,1,'だ然'),(7087,1,'然文'),(7088,1,'文ル'),(7089,1,'ヱメ'),(7090,1,'メ板'),(7091,1,'板策'),(7092,1,'策ぱ'),(7093,1,'ぱと'),(7094,1,'とぴ'),(7095,1,'ぴ山'),(7096,1,'山連'),(7097,1,'連ヒ'),(7098,1,'ヒハ'),(7099,1,'チケ'),(7100,1,'ケ和'),(7101,1,'和態'),(7102,1,'態奏'),(7103,1,'奏訳'),(7104,1,'訳う'),(7105,1,'うほ'),(7106,1,'ほの'),(7107,1,'192'),(7114,1,'country'),(7115,1,'japanese'),(7116,1,'chinese'),(7117,1,'approving'),(7118,1,'second');
/*!40000 ALTER TABLE `ezsearch_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsection`
--

DROP TABLE IF EXISTS `ezsection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `navigation_part_identifier` varchar(100) DEFAULT 'ezcontentnavigationpart',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsection`
--

LOCK TABLES `ezsection` WRITE;
/*!40000 ALTER TABLE `ezsection` DISABLE KEYS */;
INSERT INTO `ezsection` VALUES (1,'standard','','Standard','ezcontentnavigationpart'),(2,'users','','Users','ezusernavigationpart'),(3,'media','','Media','ezmedianavigationpart'),(4,'setup','','Setup','ezsetupnavigationpart'),(5,'design','','Design','ezvisualnavigationpart'),(6,'','','Restricted','ezcontentnavigationpart');
/*!40000 ALTER TABLE `ezsection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsession`
--

DROP TABLE IF EXISTS `ezsession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsession` (
  `data` longtext NOT NULL,
  `expiration_time` int(11) NOT NULL DEFAULT '0',
  `session_key` varchar(32) NOT NULL DEFAULT '',
  `user_hash` varchar(32) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_key`),
  KEY `expiration_time` (`expiration_time`),
  KEY `ezsession_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsession`
--

LOCK TABLES `ezsession` WRITE;
/*!40000 ALTER TABLE `ezsession` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsite_data`
--

DROP TABLE IF EXISTS `ezsite_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsite_data` (
  `name` varchar(60) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsite_data`
--

LOCK TABLES `ezsite_data` WRITE;
/*!40000 ALTER TABLE `ezsite_data` DISABLE KEYS */;
INSERT INTO `ezsite_data` VALUES ('ezdemo','2.0'),('ezpublish-release','1'),('ezpublish-version','5.4.0');
/*!40000 ALTER TABLE `ezsite_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezstarrating`
--

DROP TABLE IF EXISTS `ezstarrating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezstarrating` (
  `contentobject_id` int(11) NOT NULL,
  `contentobject_attribute_id` int(11) NOT NULL,
  `rating_average` float NOT NULL,
  `rating_count` int(11) NOT NULL,
  PRIMARY KEY (`contentobject_id`,`contentobject_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezstarrating`
--

LOCK TABLES `ezstarrating` WRITE;
/*!40000 ALTER TABLE `ezstarrating` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezstarrating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezstarrating_data`
--

DROP TABLE IF EXISTS `ezstarrating_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezstarrating_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_key` varchar(32) NOT NULL,
  `rating` float NOT NULL,
  `contentobject_id` int(11) NOT NULL,
  `contentobject_attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_session_key` (`user_id`,`session_key`),
  KEY `contentobject_id_contentobject_attribute_id` (`contentobject_id`,`contentobject_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezstarrating_data`
--

LOCK TABLES `ezstarrating_data` WRITE;
/*!40000 ALTER TABLE `ezstarrating_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezstarrating_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezsubtree_notification_rule`
--

DROP TABLE IF EXISTS `ezsubtree_notification_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezsubtree_notification_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT '0',
  `use_digest` int(11) DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezsubtree_notification_rule_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezsubtree_notification_rule`
--

LOCK TABLES `ezsubtree_notification_rule` WRITE;
/*!40000 ALTER TABLE `ezsubtree_notification_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezsubtree_notification_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztipafriend_counter`
--

DROP TABLE IF EXISTS `eztipafriend_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztipafriend_counter` (
  `count` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `requested` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`,`requested`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztipafriend_counter`
--

LOCK TABLES `eztipafriend_counter` WRITE;
/*!40000 ALTER TABLE `eztipafriend_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `eztipafriend_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztipafriend_request`
--

DROP TABLE IF EXISTS `eztipafriend_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztipafriend_request` (
  `created` int(11) NOT NULL DEFAULT '0',
  `email_receiver` varchar(100) NOT NULL DEFAULT '',
  KEY `eztipafriend_request_created` (`created`),
  KEY `eztipafriend_request_email_rec` (`email_receiver`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztipafriend_request`
--

LOCK TABLES `eztipafriend_request` WRITE;
/*!40000 ALTER TABLE `eztipafriend_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `eztipafriend_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eztrigger`
--

DROP TABLE IF EXISTS `eztrigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eztrigger` (
  `connect_type` char(1) NOT NULL DEFAULT '',
  `function_name` varchar(200) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(200) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eztrigger_def_id` (`module_name`(50),`function_name`(50),`connect_type`),
  KEY `eztrigger_fetch` (`name`(25),`module_name`(50),`function_name`(50))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eztrigger`
--

LOCK TABLES `eztrigger` WRITE;
/*!40000 ALTER TABLE `eztrigger` DISABLE KEYS */;
INSERT INTO `eztrigger` VALUES ('b','publish',1,'content','pre_publish',2);
/*!40000 ALTER TABLE `eztrigger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl`
--

DROP TABLE IF EXISTS `ezurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl` (
  `created` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_valid` int(11) NOT NULL DEFAULT '1',
  `last_checked` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `original_url_md5` varchar(32) NOT NULL DEFAULT '',
  `url` longtext,
  PRIMARY KEY (`id`),
  KEY `ezurl_url` (`url`(255))
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl`
--

LOCK TABLES `ezurl` WRITE;
/*!40000 ALTER TABLE `ezurl` DISABLE KEYS */;
INSERT INTO `ezurl` VALUES (1484920003,29,1,0,1484920003,'9b492048041e95b32de08bafc86d759b','/content/view/sitemap/2'),(1484920003,30,1,0,1484920003,'c86bcb109d8e70f9db65c803baafd550','/content/view/tagcloud/2');
/*!40000 ALTER TABLE `ezurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurl_object_link`
--

DROP TABLE IF EXISTS `ezurl_object_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurl_object_link` (
  `contentobject_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentobject_attribute_version` int(11) NOT NULL DEFAULT '0',
  `url_id` int(11) NOT NULL DEFAULT '0',
  KEY `ezurl_ol_coa_id` (`contentobject_attribute_id`),
  KEY `ezurl_ol_coa_version` (`contentobject_attribute_version`),
  KEY `ezurl_ol_url_id` (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurl_object_link`
--

LOCK TABLES `ezurl_object_link` WRITE;
/*!40000 ALTER TABLE `ezurl_object_link` DISABLE KEYS */;
INSERT INTO `ezurl_object_link` VALUES (200,2,29),(201,2,30);
/*!40000 ALTER TABLE `ezurl_object_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias`
--

DROP TABLE IF EXISTS `ezurlalias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias` (
  `destination_url` longtext NOT NULL,
  `forward_to_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_imported` int(11) NOT NULL DEFAULT '0',
  `is_internal` int(11) NOT NULL DEFAULT '1',
  `is_wildcard` int(11) NOT NULL DEFAULT '0',
  `source_md5` varchar(32) DEFAULT NULL,
  `source_url` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ezurlalias_desturl` (`destination_url`(200)),
  KEY `ezurlalias_forward_to_id` (`forward_to_id`),
  KEY `ezurlalias_imp_wcard_fwd` (`is_imported`,`is_wildcard`,`forward_to_id`),
  KEY `ezurlalias_source_md5` (`source_md5`),
  KEY `ezurlalias_source_url` (`source_url`(255)),
  KEY `ezurlalias_wcard_fwd` (`is_wildcard`,`forward_to_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias`
--

LOCK TABLES `ezurlalias` WRITE;
/*!40000 ALTER TABLE `ezurlalias` DISABLE KEYS */;
INSERT INTO `ezurlalias` VALUES ('content/view/full/2',0,12,1,1,0,'d41d8cd98f00b204e9800998ecf8427e',''),('content/view/full/5',0,13,1,1,0,'9bc65c2abec141778ffaa729489f3e87','users'),('content/view/full/12',0,15,1,1,0,'02d4e844e3a660857a3f81585995ffe1','users/guest_accounts'),('content/view/full/13',0,16,1,1,0,'1b1d79c16700fd6003ea7be233e754ba','users/administrator_users'),('content/view/full/14',0,17,1,1,0,'0bb9dd665c96bbc1cf36b79180786dea','users/editors'),('content/view/full/15',0,18,1,1,0,'f1305ac5f327a19b451d82719e0c3f5d','users/administrator_users/administrator_user'),('content/view/full/43',0,20,1,1,0,'62933a2951ef01f4eafd9bdf4d3cd2f0','media'),('content/view/full/44',0,21,1,1,0,'3ae1aac958e1c82013689d917d34967a','users/anonymous_users'),('content/view/full/45',0,22,1,1,0,'aad93975f09371695ba08292fd9698db','users/anonymous_users/anonymous_user'),('content/view/full/48',0,25,1,1,0,'a0f848942ce863cf53c0fa6cc684007d','setup'),('content/view/full/50',0,27,1,1,0,'c60212835de76414f9bfd21eecb8f221','foo_bar_folder/images/vbanner'),('content/view/full/51',0,28,1,1,0,'38985339d4a5aadfc41ab292b4527046','media/images'),('content/view/full/52',0,29,1,1,0,'ad5a8c6f6aac3b1b9df267fe22e7aef6','media/files'),('content/view/full/53',0,30,1,1,0,'562a0ac498571c6c3529173184a2657c','media/multimedia'),('content/view/full/54',0,31,1,1,0,'e501fe6c81ed14a5af2b322d248102d8','setup/common_ini_settings'),('content/view/full/56',0,32,1,1,0,'2dd3db5dc7122ea5f3ee539bb18fe97d','design/ez_publish'),('content/view/full/58',0,33,1,1,0,'31c13f47ad87dd7baa2d558a91e0fbb9','design');
/*!40000 ALTER TABLE `ezurlalias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml`
--

DROP TABLE IF EXISTS `ezurlalias_ml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml` (
  `action` longtext NOT NULL,
  `action_type` varchar(32) NOT NULL DEFAULT '',
  `alias_redirects` int(11) NOT NULL DEFAULT '1',
  `id` int(11) NOT NULL DEFAULT '0',
  `is_alias` int(11) NOT NULL DEFAULT '0',
  `is_original` int(11) NOT NULL DEFAULT '0',
  `lang_mask` bigint(20) NOT NULL DEFAULT '0',
  `link` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `text` longtext NOT NULL,
  `text_md5` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`parent`,`text_md5`),
  KEY `ezurlalias_ml_act_org` (`action`(32),`is_original`),
  KEY `ezurlalias_ml_actt_org_al` (`action_type`,`is_original`,`is_alias`),
  KEY `ezurlalias_ml_id` (`id`),
  KEY `ezurlalias_ml_par_act_id_lnk` (`action`(32),`id`,`link`,`parent`),
  KEY `ezurlalias_ml_par_lnk_txt` (`parent`,`text`(32),`link`),
  KEY `ezurlalias_ml_text` (`text`(32),`id`,`link`),
  KEY `ezurlalias_ml_text_lang` (`text`(32),`lang_mask`,`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml`
--

LOCK TABLES `ezurlalias_ml` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml` VALUES ('nop:','nop',1,14,0,0,1,14,0,'foo_bar_folder','0288b6883046492fa92e4a84eb67acc9'),('eznode:59','eznode',1,40,0,0,3,38,0,'Home','106a6c241b8797f52e1e77317b96a201'),('eznode:59','eznode',1,38,0,1,3,38,0,'eZ-Publish','10e4c3cb527fb9963258469986c16240'),('eznode:79','eznode',1,74,0,1,8,74,0,'Hello-New-Zealand','1356ba9b7b0eff03b9da23981589ca87'),('eznode:58','eznode',1,25,0,1,3,25,0,'Design','31c13f47ad87dd7baa2d558a91e0fbb9'),('eznode:48','eznode',1,13,0,1,3,13,0,'Setup2','475e97c0146bfb1c490339546d9e72ee'),('nop:','nop',1,17,0,0,1,17,0,'media2','50e2736330de124f6edea9b008556fe6'),('eznode:43','eznode',1,9,0,1,3,9,0,'Media','62933a2951ef01f4eafd9bdf4d3cd2f0'),('nop:','nop',1,21,0,0,1,21,0,'setup3','732cefcf28bf4547540609fb1a786a30'),('eznode:66','eznode',1,47,0,0,2,66,0,'Product-Category-2','75bbc28026a90ab2436f9835b6d425ad'),('eznode:80','eznode',1,77,0,1,4,77,0,'node_80','7f5554c037460d71a6250bd697c6ab74'),('nop:','nop',1,3,0,0,1,3,0,'users2','86425c35a33507d479f71ade53a669aa'),('eznode:5','eznode',1,2,0,1,3,2,0,'Users','9bc65c2abec141778ffaa729489f3e87'),('eznode:72','eznode',1,58,0,0,2,75,0,'About-us','9d7f564aa5e716dbae961dd1f0c23d30'),('eznode:66','eznode',1,50,0,0,2,66,0,'Product-Category-12','b03ffcb78d06483429fe17f1b51e15fe'),('eznode:79','eznode',1,74,0,1,2,74,0,'English','ba0a6ddd94c73698a3658f92ac222f8a'),('eznode:63','eznode',1,44,0,0,4,65,0,'1','c4ca4238a0b923820dcc509a6f75849b'),('eznode:76','eznode',1,64,0,0,2,76,0,'All-products','cfe2d24842ce47408192c3c8643df4bf'),('eznode:2','eznode',1,1,0,1,3,1,0,'','d41d8cd98f00b204e9800998ecf8427e'),('eznode:63','eznode',1,44,0,0,2,65,0,'Product-Category-1','dee18f303f6023f7490721d0b53e564b'),('eznode:133','eznode',0,144,0,1,3,144,2,'Country-editors','0149f839c5b5b7f8c6548e79feb76aa5'),('eznode:139','eznode',1,152,0,1,3,152,2,'Approving-users','6563ec0062d7dbeb01c5eb8f4f4dde36'),('eznode:44','eznode',1,10,0,1,3,10,2,'Anonymous-Users','c2803c3fa1b0b5423237b4e018cae755'),('eznode:13','eznode',1,5,0,1,3,5,2,'Administrator-users','f89fad7f8a3abc8c09e1deb46a420007'),('nop:','nop',1,11,0,0,1,11,3,'anonymous_users2','505e93077a6dde9034ad97a14ab022b1'),('nop:','nop',1,7,0,0,1,7,3,'administrator_users2','a7da338c20bf65f9f789c87296379c2a'),('eznode:13','eznode',1,27,0,0,1,5,3,'administrator_users','aeb8609aa933b0899aa012c71139c58c'),('eznode:44','eznode',1,30,0,0,1,10,3,'anonymous_users','e9e5ad0c05ee1a43715572e5cc545926'),('eznode:15','eznode',1,8,0,1,3,8,5,'Administrator-User','5a9d7b0ec93173ef4fedee023209cb61'),('eznode:141','eznode',1,153,0,1,3,153,5,'Second-Admin','87f6b4be352bc00f9289cb7e7646f258'),('eznode:15','eznode',1,28,0,0,0,8,7,'administrator_user','a3cca2de936df1e2f805710399989971'),('eznode:53','eznode',1,20,0,1,3,20,9,'Multimedia','2e5bc8831f7ae6a29530e7f1bbf2de9c'),('eznode:52','eznode',1,19,0,1,3,19,9,'Files','45b963397aa40d4a0063e0d85e4fe7a1'),('eznode:51','eznode',1,18,0,1,3,18,9,'Images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:45','eznode',1,12,0,1,3,12,10,'Anonymous-User','ccb62ebca03a31272430bc414bd5cd5b'),('eznode:45','eznode',1,31,0,0,1,12,11,'anonymous_user','c593ec85293ecb0e02d50d4c5c6c20eb'),('eznode:54','eznode',1,22,0,1,2,22,13,'Common-INI-settings','4434993ac013ae4d54bb1f51034d6401'),('nop:','nop',1,15,0,0,1,15,14,'images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:50','eznode',1,16,0,1,2,16,15,'vbanner','c54e2d1b93642e280bdc5d99eab2827d'),('eznode:53','eznode',1,34,0,0,1,20,17,'multimedia','2e5bc8831f7ae6a29530e7f1bbf2de9c'),('eznode:52','eznode',1,33,0,0,1,19,17,'files','45b963397aa40d4a0063e0d85e4fe7a1'),('eznode:51','eznode',1,32,0,0,1,18,17,'images','59b514174bffe4ae402b3d63aad79fe0'),('eznode:91','eznode',1,98,0,1,3,98,18,'Product-photos','481bcdccd9d08c5107eef01a7c8a6a2a'),('eznode:71','eznode',1,57,0,0,3,102,18,'Products','86024cad1e83101d97359d7351051156'),('eznode:71','eznode',1,102,0,1,3,102,20,'Products','86024cad1e83101d97359d7351051156'),('eznode:54','eznode',1,35,0,0,1,22,21,'common_ini_settings','e59d6834e86cee752ed841f9cd8d5baf'),('eznode:56','eznode',1,37,0,0,2,24,25,'eZ-publish','10e4c3cb527fb9963258469986c16240'),('eznode:56','eznode',1,24,0,1,2,24,25,'Plain-site','49a39d99a955d95aa5d636275656a07a'),('eznode:63','eznode',1,65,0,1,4,65,64,'1','c4ca4238a0b923820dcc509a6f75849b'),('eznode:77','eznode',1,67,0,1,2,67,65,'Subcategory-1','609bca08f74687210597159dfd6bf107'),('eznode:78','eznode',1,69,0,1,2,69,65,'Subcategory-2','728bf2b56866940b0a0e325d60df97d6'),('eznode:65','eznode',1,46,0,0,2,70,65,'Product-Two','8e56899b11a5fd9a2a9314d52def288f'),('eznode:70','eznode',1,54,0,0,2,71,65,'Product-Hello-123','a5089c40167497d1a319f67a2bf3f9ee'),('eznode:70','eznode',1,55,0,0,2,71,65,'Product-Five','af59c41581e809ecf73b853e5bd9175e'),('eznode:78','eznode',1,73,0,0,2,69,65,'Subcategory-12','b7cbaa725184376641c1a59986247f0d'),('eznode:69','eznode',1,53,0,1,4,53,66,'XX-837-9','31ec1948797aa7e5fca61eb12bc252d4'),('eznode:68','eznode',1,49,0,0,2,68,66,'Product-Four','377328d402931f4fcb2977bffd46c876'),('eznode:67','eznode',1,48,0,1,4,48,66,'CJ-831-3','7438c181e4837e60b34d2c661397101a'),('eznode:68','eznode',1,52,0,0,2,68,66,'Product-Two','8e56899b11a5fd9a2a9314d52def288f'),('eznode:69','eznode',1,92,0,0,2,53,66,'Product-Hello-123','a5089c40167497d1a319f67a2bf3f9ee'),('eznode:69','eznode',1,56,0,0,2,53,66,'Product-Five','af59c41581e809ecf73b853e5bd9175e'),('eznode:67','eznode',1,89,0,0,2,48,66,'Product-Three','b08bdefcb017d3005b81de17127c9319'),('eznode:67','eznode',1,51,0,0,2,48,66,'Product-One','bfe2237875e90bba387e742f895a6dba'),('eznode:67','eznode',1,48,0,1,2,48,66,'CJ-831-Product-Three','cdfc9756ec1367f39ba3b543c1be17e7'),('eznode:69','eznode',1,53,0,1,2,53,66,'XX-837-Product-Hello-9','f3b81aae721113ecc178c3dab567d295'),('eznode:68','eznode',1,87,0,0,2,68,67,'Product-Four','377328d402931f4fcb2977bffd46c876'),('eznode:90','eznode',1,97,0,1,4,97,67,'PR-192-1','42c4bf22047c1ef2f525253f320e1db6'),('eznode:68','eznode',1,68,0,1,2,68,67,'PR-228-Product-Four','55ea2128619d5fc483f1ff60f4ef5261'),('eznode:90','eznode',1,97,0,1,2,97,67,'PR-192-Product-One','c850dee4f06f3df37614e27c930b1350'),('eznode:68','eznode',1,68,0,1,4,68,67,'PR-228-4','e24b3a5146f084bf2c3a800bc0ca35af'),('eznode:70','eznode',1,71,0,1,4,71,69,'XX-837-9','31ec1948797aa7e5fca61eb12bc252d4'),('eznode:65','eznode',1,95,0,0,2,70,69,'Product-Two','8e56899b11a5fd9a2a9314d52def288f'),('eznode:70','eznode',1,91,0,0,2,71,69,'Product-Hello-123','a5089c40167497d1a319f67a2bf3f9ee'),('eznode:65','eznode',1,70,0,1,2,70,69,'XY-827-Product-Two','cc5bf9bb725b7964f9f99e6c9801d35a'),('eznode:70','eznode',1,71,0,1,2,71,69,'XX-837-Product-Hello-9','f3b81aae721113ecc178c3dab567d295'),('eznode:72','eznode',1,75,0,1,2,75,74,'About-us','9d7f564aa5e716dbae961dd1f0c23d30'),('eznode:72','eznode',1,75,0,1,4,75,74,'node_72','a2582210a0eedbfdd3e8e06e25e54655'),('eznode:76','eznode',1,76,0,1,2,76,74,'All-products','cfe2d24842ce47408192c3c8643df4bf'),('eznode:96','eznode',1,107,0,1,2,107,76,'Product-category-3','327252ddf042a192fcadf204a404be99'),('eznode:66','eznode',1,66,0,1,2,66,76,'Product-Category-2','75bbc28026a90ab2436f9835b6d425ad'),('eznode:63','eznode',1,65,0,1,2,65,76,'Product-Category-1','dee18f303f6023f7490721d0b53e564b'),('eznode:81','eznode',1,78,0,1,4,78,77,'node_81','4cd5943f97b06b691dc15be110580887'),('eznode:82','eznode',1,79,0,1,4,79,77,'node_82','680f9d536843107051ff4dbf3a55a4da'),('eznode:81','eznode',1,78,0,1,2,78,77,'About-us','9d7f564aa5e716dbae961dd1f0c23d30'),('eznode:84','eznode',1,82,0,0,4,81,79,'12','c20ad4d76fe97759aa27a0c99bff6710'),('eznode:83','eznode',1,80,0,1,4,80,79,'1','c4ca4238a0b923820dcc509a6f75849b'),('eznode:84','eznode',1,81,0,1,4,81,79,'2','c81e728d9d4c2f636f067f89cc14862c'),('eznode:86','eznode',1,93,0,0,2,84,80,'228821-Product-Hello-123','057bde225ec2f25579e022d561eeb6c0'),('eznode:86','eznode',1,84,0,1,4,84,80,'XX-837-9','31ec1948797aa7e5fca61eb12bc252d4'),('eznode:89','eznode',1,96,0,1,4,96,80,'PR-192-1','42c4bf22047c1ef2f525253f320e1db6'),('eznode:85','eznode',1,83,0,1,4,83,80,'CJ-831-3','7438c181e4837e60b34d2c661397101a'),('eznode:89','eznode',1,96,0,1,2,96,80,'PR-192-Product-One','c850dee4f06f3df37614e27c930b1350'),('eznode:85','eznode',1,83,0,1,2,83,80,'CJ-831-Product-Three','cdfc9756ec1367f39ba3b543c1be17e7'),('eznode:85','eznode',1,90,0,0,2,83,80,'123123-Product-Three','da0a5ad0041affededd706c918c73da0'),('eznode:86','eznode',1,84,0,1,2,84,80,'XX-837-Product-Hello-9','f3b81aae721113ecc178c3dab567d295'),('eznode:87','eznode',1,88,0,0,2,85,81,'2271723-Product-Four','034b40494d9dde76ef852d01d16efcc8'),('eznode:88','eznode',1,86,0,1,4,86,81,'JP-271','453dec7711764b38423dc5a690bd127b'),('eznode:87','eznode',1,85,0,1,2,85,81,'PR-228-Product-Four','55ea2128619d5fc483f1ff60f4ef5261'),('eznode:87','eznode',1,85,0,1,4,85,81,'PR-228-4','e24b3a5146f084bf2c3a800bc0ca35af'),('eznode:94','eznode',1,104,0,0,3,101,98,'Product-Photo-13','31b66b63af734cead87c582c757dd234'),('eznode:94','eznode',1,101,0,1,3,101,98,'Product-Photo-3','56b1f003bfc593afe9d1c953c0527921'),('eznode:92','eznode',1,99,0,1,3,99,98,'Product-Photo-1','57a0a32d53c51d404aeb0b303a7d7133'),('eznode:93','eznode',1,100,0,1,3,100,98,'Product-Photo-2','65139bda1d69de051e418cb4b497ae47'),('eznode:94','eznode',1,105,0,0,3,101,98,'Product-Photo-12','728185d23c2b94efc4033d76e7e76f19'),('eznode:93','eznode',1,100,0,1,5,100,98,'2','c81e728d9d4c2f636f067f89cc14862c'),('eznode:75','eznode',1,63,0,0,3,61,102,'Product-photo-13','31b66b63af734cead87c582c757dd234'),('eznode:75','eznode',1,61,0,1,3,61,102,'Product-photo-3','56b1f003bfc593afe9d1c953c0527921'),('eznode:73','eznode',1,59,0,1,3,59,102,'Product-photo-1','57a0a32d53c51d404aeb0b303a7d7133'),('eznode:74','eznode',1,60,0,1,3,60,102,'Product-photo-2','65139bda1d69de051e418cb4b497ae47'),('eznode:74','eznode',1,62,0,0,3,60,102,'Product-photo-12','728185d23c2b94efc4033d76e7e76f19'),('eznode:139','eznode',1,150,0,0,3,152,144,'Approving-users','6563ec0062d7dbeb01c5eb8f4f4dde36'),('eznode:134','eznode',0,145,0,1,3,145,144,'Japanese','8175469f5b7510febe85d18daa424a3f'),('eznode:136','eznode',0,147,0,1,3,147,144,'English','ba0a6ddd94c73698a3658f92ac222f8a'),('eznode:135','eznode',0,146,0,1,3,146,144,'Chinese','babd2925f9a5fe57586bd10374bdecc6'),('eznode:137','eznode',0,148,0,1,2,148,145,'James-Japanese','ee53e515857b61a8d527df9002210dc8'),('eznode:138','eznode',0,149,0,1,2,149,147,'Eaton-English','41ffd4c1494125ac02bc6ade4a74e609'),('eznode:140','eznode',1,151,0,1,2,151,152,'Eaton-English','41ffd4c1494125ac02bc6ade4a74e609');
/*!40000 ALTER TABLE `ezurlalias_ml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlalias_ml_incr`
--

DROP TABLE IF EXISTS `ezurlalias_ml_incr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlalias_ml_incr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlalias_ml_incr`
--

LOCK TABLES `ezurlalias_ml_incr` WRITE;
/*!40000 ALTER TABLE `ezurlalias_ml_incr` DISABLE KEYS */;
INSERT INTO `ezurlalias_ml_incr` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45),(46),(47),(48),(49),(50),(51),(52),(53),(54),(55),(56),(57),(58),(59),(60),(61),(62),(63),(64),(65),(66),(67),(68),(69),(70),(71),(72),(73),(74),(75),(76),(77),(78),(79),(80),(81),(82),(83),(84),(85),(86),(87),(88),(89),(90),(91),(92),(93),(94),(95),(96),(97),(98),(99),(100),(101),(102),(103),(104),(105),(106),(107),(124),(125),(126),(127),(128),(129),(138),(139),(140),(141),(142),(143),(144),(145),(146),(147),(148),(149),(150),(151),(152),(153);
/*!40000 ALTER TABLE `ezurlalias_ml_incr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezurlwildcard`
--

DROP TABLE IF EXISTS `ezurlwildcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezurlwildcard` (
  `destination_url` longtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_url` longtext NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezurlwildcard`
--

LOCK TABLES `ezurlwildcard` WRITE;
/*!40000 ALTER TABLE `ezurlwildcard` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezurlwildcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser`
--

DROP TABLE IF EXISTS `ezuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser` (
  `contentobject_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(150) NOT NULL DEFAULT '',
  `login` varchar(150) NOT NULL DEFAULT '',
  `password_hash` varchar(50) DEFAULT NULL,
  `password_hash_type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`contentobject_id`),
  KEY `ezuser_login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser`
--

LOCK TABLES `ezuser` WRITE;
/*!40000 ALTER TABLE `ezuser` DISABLE KEYS */;
INSERT INTO `ezuser` VALUES (10,'nospam@ez.no','anonymous','4e6f6184135228ccd45f8233d72a0363',2),(14,'foo@example.com','admin','c78e3b0f3d9244ed8c6d1c29464bdff9',2),(131,'japanese@example.com','japanese','16eb5c16cee61b317aef36ce5ff05481',2),(132,'english@example.com','english','ddcdedd2773895e332c50078b9b3b63d',2),(135,'second@example.com','second','a9527890b54993c3d94be20dbe99b660',2);
/*!40000 ALTER TABLE `ezuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_accountkey`
--

DROP TABLE IF EXISTS `ezuser_accountkey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_accountkey` (
  `hash_key` varchar(32) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hash_key` (`hash_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_accountkey`
--

LOCK TABLES `ezuser_accountkey` WRITE;
/*!40000 ALTER TABLE `ezuser_accountkey` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezuser_accountkey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_discountrule`
--

DROP TABLE IF EXISTS `ezuser_discountrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_discountrule` (
  `contentobject_id` int(11) DEFAULT NULL,
  `discountrule_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_discountrule`
--

LOCK TABLES `ezuser_discountrule` WRITE;
/*!40000 ALTER TABLE `ezuser_discountrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezuser_discountrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_role`
--

DROP TABLE IF EXISTS `ezuser_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_role` (
  `contentobject_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `limit_identifier` varchar(255) DEFAULT '',
  `limit_value` varchar(255) DEFAULT '',
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ezuser_role_contentobject_id` (`contentobject_id`),
  KEY `ezuser_role_role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_role`
--

LOCK TABLES `ezuser_role` WRITE;
/*!40000 ALTER TABLE `ezuser_role` DISABLE KEYS */;
INSERT INTO `ezuser_role` VALUES (12,25,'','',2),(42,31,'','',1),(127,39,'','',1),(127,40,'','',10),(130,41,'Subtree','/1/2/79/',13),(128,42,'Subtree','/1/2/80/',15),(133,43,'Section','1',20);
/*!40000 ALTER TABLE `ezuser_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuser_setting`
--

DROP TABLE IF EXISTS `ezuser_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuser_setting` (
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `max_login` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuser_setting`
--

LOCK TABLES `ezuser_setting` WRITE;
/*!40000 ALTER TABLE `ezuser_setting` DISABLE KEYS */;
INSERT INTO `ezuser_setting` VALUES (1,1000,10),(1,10,14),(1,0,131),(1,0,132),(1,0,135);
/*!40000 ALTER TABLE `ezuser_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezuservisit`
--

DROP TABLE IF EXISTS `ezuservisit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezuservisit` (
  `current_visit_timestamp` int(11) NOT NULL DEFAULT '0',
  `failed_login_attempts` int(11) NOT NULL DEFAULT '0',
  `last_visit_timestamp` int(11) NOT NULL DEFAULT '0',
  `login_count` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `ezuservisit_co_visit_count` (`current_visit_timestamp`,`login_count`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezuservisit`
--

LOCK TABLES `ezuservisit` WRITE;
/*!40000 ALTER TABLE `ezuservisit` DISABLE KEYS */;
INSERT INTO `ezuservisit` VALUES (-1486993941,0,1486360667,7,14),(-1486996107,0,1486996107,1,132),(1486997367,0,1486997367,1,135);
/*!40000 ALTER TABLE `ezuservisit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvatrule`
--

DROP TABLE IF EXISTS `ezvatrule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvatrule` (
  `country_code` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vat_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvatrule`
--

LOCK TABLES `ezvatrule` WRITE;
/*!40000 ALTER TABLE `ezvatrule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezvatrule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvatrule_product_category`
--

DROP TABLE IF EXISTS `ezvatrule_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvatrule_product_category` (
  `product_category_id` int(11) NOT NULL DEFAULT '0',
  `vatrule_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vatrule_id`,`product_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvatrule_product_category`
--

LOCK TABLES `ezvatrule_product_category` WRITE;
/*!40000 ALTER TABLE `ezvatrule_product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezvatrule_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezvattype`
--

DROP TABLE IF EXISTS `ezvattype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezvattype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `percentage` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezvattype`
--

LOCK TABLES `ezvattype` WRITE;
/*!40000 ALTER TABLE `ezvattype` DISABLE KEYS */;
INSERT INTO `ezvattype` VALUES (1,'Std',0);
/*!40000 ALTER TABLE `ezvattype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezview_counter`
--

DROP TABLE IF EXISTS `ezview_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezview_counter` (
  `count` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezview_counter`
--

LOCK TABLES `ezview_counter` WRITE;
/*!40000 ALTER TABLE `ezview_counter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezview_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezwaituntildatevalue`
--

DROP TABLE IF EXISTS `ezwaituntildatevalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezwaituntildatevalue` (
  `contentclass_attribute_id` int(11) NOT NULL DEFAULT '0',
  `contentclass_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_event_id` int(11) NOT NULL DEFAULT '0',
  `workflow_event_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`workflow_event_id`,`workflow_event_version`),
  KEY `ezwaituntildateevalue_wf_ev_id_wf_ver` (`workflow_event_id`,`workflow_event_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezwaituntildatevalue`
--

LOCK TABLES `ezwaituntildatevalue` WRITE;
/*!40000 ALTER TABLE `ezwaituntildatevalue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezwaituntildatevalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezwishlist`
--

DROP TABLE IF EXISTS `ezwishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezwishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productcollection_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezwishlist`
--

LOCK TABLES `ezwishlist` WRITE;
/*!40000 ALTER TABLE `ezwishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezwishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow`
--

DROP TABLE IF EXISTS `ezworkflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_enabled` int(11) NOT NULL DEFAULT '0',
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_type_string` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow`
--

LOCK TABLES `ezworkflow` WRITE;
/*!40000 ALTER TABLE `ezworkflow` DISABLE KEYS */;
INSERT INTO `ezworkflow` VALUES (1486997248,14,1,1,1486997307,14,'Approval',0,'group_ezserial'),(1486997310,14,2,1,1486997326,14,'Before publishing',0,'group_ezserial');
/*!40000 ALTER TABLE `ezworkflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_assign`
--

DROP TABLE IF EXISTS `ezworkflow_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_assign` (
  `access_type` int(11) NOT NULL DEFAULT '0',
  `as_tree` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_assign`
--

LOCK TABLES `ezworkflow_assign` WRITE;
/*!40000 ALTER TABLE `ezworkflow_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_event`
--

DROP TABLE IF EXISTS `ezworkflow_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_event` (
  `data_int1` int(11) DEFAULT NULL,
  `data_int2` int(11) DEFAULT NULL,
  `data_int3` int(11) DEFAULT NULL,
  `data_int4` int(11) DEFAULT NULL,
  `data_text1` varchar(255) DEFAULT NULL,
  `data_text2` varchar(255) DEFAULT NULL,
  `data_text3` varchar(255) DEFAULT NULL,
  `data_text4` varchar(255) DEFAULT NULL,
  `data_text5` longtext,
  `description` varchar(50) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placement` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  `workflow_type_string` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`version`),
  KEY `wid_version_placement` (`workflow_id`,`version`,`placement`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_event`
--

LOCK TABLES `ezworkflow_event` WRITE;
/*!40000 ALTER TABLE `ezworkflow_event` DISABLE KEYS */;
INSERT INTO `ezworkflow_event` VALUES (0,0,0,0,'-1','','14','','','',1,1,0,1,'event_ezapprove'),(1,0,0,0,'-1','','','','-1','',2,1,0,2,'event_ezmultiplexer');
/*!40000 ALTER TABLE `ezworkflow_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_group`
--

DROP TABLE IF EXISTS `ezworkflow_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_group` (
  `created` int(11) NOT NULL DEFAULT '0',
  `creator_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modified` int(11) NOT NULL DEFAULT '0',
  `modifier_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_group`
--

LOCK TABLES `ezworkflow_group` WRITE;
/*!40000 ALTER TABLE `ezworkflow_group` DISABLE KEYS */;
INSERT INTO `ezworkflow_group` VALUES (1024392098,14,1,1024392098,14,'Standard');
/*!40000 ALTER TABLE `ezworkflow_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_group_link`
--

DROP TABLE IF EXISTS `ezworkflow_group_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_group_link` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `group_name` varchar(255) DEFAULT NULL,
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  `workflow_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`workflow_id`,`group_id`,`workflow_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_group_link`
--

LOCK TABLES `ezworkflow_group_link` WRITE;
/*!40000 ALTER TABLE `ezworkflow_group_link` DISABLE KEYS */;
INSERT INTO `ezworkflow_group_link` VALUES (1,'Standard',1,0),(1,'Standard',2,0);
/*!40000 ALTER TABLE `ezworkflow_group_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezworkflow_process`
--

DROP TABLE IF EXISTS `ezworkflow_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezworkflow_process` (
  `activation_date` int(11) DEFAULT NULL,
  `content_id` int(11) NOT NULL DEFAULT '0',
  `content_version` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `event_position` int(11) NOT NULL DEFAULT '0',
  `event_state` int(11) DEFAULT NULL,
  `event_status` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_event_id` int(11) NOT NULL DEFAULT '0',
  `last_event_position` int(11) NOT NULL DEFAULT '0',
  `last_event_status` int(11) NOT NULL DEFAULT '0',
  `memento_key` varchar(32) DEFAULT NULL,
  `modified` int(11) NOT NULL DEFAULT '0',
  `node_id` int(11) NOT NULL DEFAULT '0',
  `parameters` longtext,
  `process_key` varchar(32) NOT NULL DEFAULT '',
  `session_key` varchar(32) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `workflow_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ezworkflow_process_process_key` (`process_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezworkflow_process`
--

LOCK TABLES `ezworkflow_process` WRITE;
/*!40000 ALTER TABLE `ezworkflow_process` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezworkflow_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ezxapprovelocation_items`
--

DROP TABLE IF EXISTS `ezxapprovelocation_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ezxapprovelocation_items` (
  `collaboration_id` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_process_id` int(11) NOT NULL DEFAULT '0',
  `target_node_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ezxapprovelocation_items`
--

LOCK TABLES `ezxapprovelocation_items` WRITE;
/*!40000 ALTER TABLE `ezxapprovelocation_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ezxapprovelocation_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kaliop_migrations`
--

DROP TABLE IF EXISTS `kaliop_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kaliop_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `md5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `execution_date` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `execution_error` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kaliop_migrations`
--

LOCK TABLES `kaliop_migrations` WRITE;
/*!40000 ALTER TABLE `kaliop_migrations` DISABLE KEYS */;
INSERT INTO `kaliop_migrations` VALUES ('20100101000200_MigrateV1ToV2.php','/Users/janit/Sites/yamaha-training/vendor/kaliop/ezmigrationbundle/MigrationVersions/20100101000200_MigrateV1ToV2.php','3d42e27bcb9fea7d6e5243de89f56cc8',1486994578,2,''),('20170213155021_create_users.yml','/Users/janit/Sites/yamaha-training/src/YamahaSiteBundle/MigrationVersions/20170213155021_create_users.yml','359b02e25e0d16a41551d5a7d0201bd3',1486995248,2,'');
/*!40000 ALTER TABLE `kaliop_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-13 18:48:33
